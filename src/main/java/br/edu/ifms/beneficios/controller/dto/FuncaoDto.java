/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Funcao;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class FuncaoDto {
    
    private Long id;
    private String nome;

    public FuncaoDto() {
    }

    public FuncaoDto(Funcao obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
    
    public static List<FuncaoDto> converter(List<Funcao> items) {
        return items.stream().map(FuncaoDto::new).collect(Collectors.toList());
    }
    
    public static Page<FuncaoDto> converter(Page<Funcao> items) {
        return items.map(FuncaoDto::new);
    }
    
}
