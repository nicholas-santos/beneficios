/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Rendimento;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class RendimentoDto {

    private Long sequencia;
    private CondicaoTrabalhoDto condicaoTrabalhoDto;
    private BigDecimal valor;
    private LocalDate admissao;
    private LocalDate demissao;
    private Boolean deleted;

    public RendimentoDto() {
        this.deleted = Boolean.FALSE;
    }

    public RendimentoDto(Rendimento obj) {
        this();
        this.sequencia = obj.getId().getSequencia();
        this.condicaoTrabalhoDto = new CondicaoTrabalhoDto(obj.getCondicaoTrabalho());
        this.valor = obj.getValor();
        this.admissao = obj.getAdmissao();
        this.demissao = obj.getDemissao();
    }

    public Long getSequencia() {
        return sequencia;
    }

    public CondicaoTrabalhoDto getCondicaoTrabalhoDto() {
        return condicaoTrabalhoDto;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public LocalDate getAdmissao() {
        return admissao;
    }

    public LocalDate getDemissao() {
        return demissao;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    
    public static List<RendimentoDto> converter(List<Rendimento> items) {
        return items.stream().map(RendimentoDto::new).collect(Collectors.toList());
    }
    
}
