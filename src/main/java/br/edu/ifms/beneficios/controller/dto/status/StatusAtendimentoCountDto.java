/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.controller.dto.status;

import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimento;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class StatusAtendimentoCountDto implements IStatusAtendimentoCount {
    
    private StatusAtendimento statusAtendimento;
    private Long totalStatus;
    private Double percentualStatus;

    public StatusAtendimentoCountDto() {
    }

    public StatusAtendimentoCountDto(IStatusAtendimentoCount obj) {
        this.statusAtendimento = obj.getStatusAtendimento();
        this.totalStatus = obj.getTotalStatus();
        this.percentualStatus = obj.getPercentualStatus();
    }

    @Override
    public StatusAtendimento getStatusAtendimento() {
        return statusAtendimento;
    }

    @Override
    public Long getTotalStatus() {
        return this.totalStatus;
    }

    @Override
    public Double getPercentualStatus() {
        return this.percentualStatus;
    }

    public static List<StatusAtendimentoCountDto> converter(List<IStatusAtendimentoCount> items) {
        return items.stream().map(StatusAtendimentoCountDto::new).collect(Collectors.toList());
    }
    
}
