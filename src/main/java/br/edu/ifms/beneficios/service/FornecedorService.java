/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.DocumentoPessoaDto;
import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.form.FornecedorForm;
import br.edu.ifms.beneficios.modelo.Fornecedor;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.CepRepository;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.ContatoPessoaRepository;
import br.edu.ifms.beneficios.repository.DocumentoPessoaRepository;
import br.edu.ifms.beneficios.repository.DocumentoRepository;
import br.edu.ifms.beneficios.repository.EnderecoRepository;
import br.edu.ifms.beneficios.repository.FornecedorRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.OrgaoExpedidorRepository;
import br.edu.ifms.beneficios.controller.dto.fornecedor.FornecedorDetalheDto;
import br.edu.ifms.beneficios.modelo.DocumentoPessoa;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import br.edu.ifms.beneficios.repository.ContatoRepository;

/**
 *
 * @author santos
 */
@Service
public class FornecedorService {

    @Autowired
    private FornecedorRepository repository;

    @Autowired
    private ContatoPessoaRepository contatoPessoaRepository;

    @Autowired
    private DocumentoPessoaRepository documentoPessoaRepository;

    @Autowired
    private ContatoRepository tipoContatoRepository;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private CepRepository cepRepository;

    @Autowired
    private CidadeRepository cidadeRepository;

    @Autowired
    private BairroRepository bairroRepository;

    @Autowired
    private LogradouroRepository logradouroRepository;

    @Autowired
    private DocumentoRepository documentoRepository;

    @Autowired
    private OrgaoExpedidorRepository orgaoExpedidorRepository;

    public Page<Fornecedor> listar(String nome, Pageable paginacao) {
        if (nome != null) {
            return repository.findByNomeContainingIgnoreCase(nome, paginacao);
        }
        return repository.findAll(paginacao);
    }

    public List<Fornecedor> listar() {
        return repository.findAll();
    }

    public Optional<Fornecedor> buscarPor(Long id) {
        return repository.findById(id);
    }

    public FornecedorDetalheDto criar(FornecedorForm form) {
        Fornecedor fornecedor = form.converter(repository,
                tipoContatoRepository, enderecoRepository, cepRepository,
                cidadeRepository, bairroRepository, logradouroRepository,
                documentoRepository, orgaoExpedidorRepository);
        
        fornecedor = repository.save(fornecedor);
        
        return new FornecedorDetalheDto(fornecedor);
    }

    public FornecedorDetalheDto atualizar(Long id, FornecedorForm form) {
        Optional<Fornecedor> optional = buscarPor(id);
        if (optional.isPresent()) {
            Fornecedor fornecedor = form.atualizar(id, repository,
                    tipoContatoRepository, enderecoRepository, cepRepository,
                    cidadeRepository, bairroRepository, logradouroRepository,
                    documentoRepository, orgaoExpedidorRepository);
            
            return new FornecedorDetalheDto(fornecedor);
        }

        return null;
    }

    public Optional<Fornecedor> remover(Long id) {
        Optional<Fornecedor> optional = buscarPor(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        }

        return optional;
    }

    public MessageDto isValido(FornecedorForm form) {
        String message = "";

        // verifica os documentos do fornecedor
        for(DocumentoPessoaDto dto : form.getDocumentos()) {
            List<DocumentoPessoa> list = documentoPessoaRepository
                    .findByIdNumeroAndIdDocumentoIdAndOrgaoExpedidorId(
                            dto.getNumero(), dto.getDocumentoDto().getId(), 
                            dto.getOrgaoExpedidorDto() != null ? dto.getOrgaoExpedidorDto().getId() : null);
            if (!list.isEmpty()) {
                DocumentoPessoa obj = list.get(0);
                message = "O documento %s, número %s já foi atribuído a outro fornecedor.";
                return new MessageDto(HttpStatus.BAD_REQUEST.value(), 
                    String.format(message, obj.getId().getDocumento().getDescricao(), obj.getId().getNumero()));
            }
        }
        return null;
    }

}
