/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Logradouro;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface LogradouroRepository extends JpaRepository<Logradouro, Long> {
    
    Optional<Logradouro> findByNomeIgnoreCase(String nome);
    
    @Query("SELECT coalesce(max(l.id), 0) FROM Logradouro l")
    Long getMaxId();
    
    Page<Logradouro> findByNomeContainingIgnoreCase(String nome, Pageable paginacao);
    
}
