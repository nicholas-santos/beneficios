/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.Autorizacao;
import br.edu.ifms.beneficios.modelo.tipos.AutorizacaoConverter;
import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Type;

/**
 *
 * @author santos
 */
@Entity
public class Analise implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    private Funcionario tecnico;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime emissao;

    @Lob
    @Type(type = "text")
    private String parecer;
    
    @Convert(converter = AutorizacaoConverter.class)
    private Autorizacao autorizacao;
    
    @OneToOne(mappedBy = "analise")
    private Atendimento atendimento;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "analise_arquivo",
            joinColumns = {
                @JoinColumn(name = "analise_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "arquivo_id", referencedColumnName = "id")}
    )
    private Arquivo arquivo;
    private Boolean encaminhamento;

    @OneToMany(mappedBy = "id.analise", fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
    private List<ItemAnalise> itens = new ArrayList();

    public Analise() {
        this.emissao = LocalDateTime.now();
    }

    public Analise(Long id, Funcionario tecnico, LocalDateTime emissao, String parecer, Autorizacao autorizacao, Arquivo arquivo) {
        this.id = id;
        this.tecnico = tecnico;
        this.emissao = emissao;
        this.parecer = parecer;
        this.autorizacao = autorizacao;
        this.arquivo = arquivo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Funcionario getTecnico() {
        return tecnico;
    }

    public void setTecnico(Funcionario tecnico) {
        this.tecnico = tecnico;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDateTime emissao) {
        this.emissao = emissao;
    }

    public String getParecer() {
        return parecer;
    }

    public void setParecer(String parecer) {
        this.parecer = parecer;
    }

    public Autorizacao getAutorizacao() {
        return autorizacao;
    }

    public void setAutorizacao(Autorizacao autorizacao) {
        this.autorizacao = autorizacao;
    }
    
    public Boolean isAutorizado() {
        return Autorizacao.AUTORIZADO.equals(this.autorizacao);
    }

    public Atendimento getAtendimento() {
        return atendimento;
    }

    public void setAtendimento(Atendimento atendimento) {
        this.atendimento = atendimento;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    public Boolean getEncaminhamento() {
        return encaminhamento;
    }

    public void setEncaminhamento(Boolean encaminhamento) {
        this.encaminhamento = encaminhamento;
    }

    public List<ItemAnalise> getItens() {
        return itens;
    }

    public void setItens(List<ItemAnalise> itens) {
        this.itens = itens;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.tecnico);
        hash = 37 * hash + Objects.hashCode(this.emissao);
        hash = 37 * hash + Objects.hashCode(this.parecer);
        hash = 37 * hash + Objects.hashCode(this.autorizacao);
        hash = 37 * hash + Objects.hashCode(this.arquivo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Analise other = (Analise) obj;
        if (!Objects.equals(this.parecer, other.getParecer())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.tecnico, other.getTecnico())) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        if (this.autorizacao != other.getAutorizacao()) {
            return false;
        }
        if (!Objects.equals(this.arquivo, other.getArquivo())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Analise{" + "id=" + id + ", emissao=" +
                emissao + ", parecer=" + parecer + ", autorizacao=" + 
                autorizacao + ", atendimento=" + atendimento.toString() + 
                ", encaminhamento=" + encaminhamento + '}';
    }
    
}
