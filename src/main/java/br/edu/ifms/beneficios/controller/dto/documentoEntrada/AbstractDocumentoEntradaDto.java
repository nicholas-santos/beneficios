/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoEntrada;

import br.edu.ifms.beneficios.controller.dto.fornecedor.FornecedorDto;
import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.time.LocalDateTime;

/**
 *
 * @author santos
 */
public class AbstractDocumentoEntradaDto {
    private Long id;
    private FuncionarioDto funcionario;
    private UnidadeAtendimentoDto unidadeAtendimento;
    private LocalDateTime emissao;
    private Status status;
    private String observacao;
    private Boolean doacao;
    
    private FornecedorDto fornecedor;
    private String processo;
    private String ata;
    private String pregao;
    private String empenhoContabil;
    private String contrato;
    private String numeroNotaFiscal;

    public AbstractDocumentoEntradaDto() {
    }

    public AbstractDocumentoEntradaDto(DocumentoEntrada obj) {
        this.id = obj.getId();
        this.funcionario = new FuncionarioDto(obj.getFuncionario());
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getUnidadeAtendimento());
        this.emissao = obj.getEmissao();
        this.status = obj.getStatus();
        this.observacao = obj.getObservacao();
        this.doacao = obj.getDoacao();
        this.fornecedor = new FornecedorDto(obj.getFornecedor());
        this.processo = obj.getProcesso();
        this.ata = obj.getAta();
        this.pregao = obj.getPregao();
        this.empenhoContabil = obj.getEmpenhoContabil();
        this.contrato = obj.getContrato();
        this.numeroNotaFiscal = obj.getNumeroNotaFiscal();
    }

    public Long getId() {
        return id;
    }

    public FuncionarioDto getFuncionario() {
        return funcionario;
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public Status getStatus() {
        return status;
    }

    public String getObservacao() {
        return observacao;
    }

    public Boolean getDoacao() {
        return doacao;
    }

    public FornecedorDto getFornecedor() {
        return fornecedor;
    }

    public String getProcesso() {
        return processo;
    }

    public String getAta() {
        return ata;
    }

    public String getPregao() {
        return pregao;
    }

    public String getEmpenhoContabil() {
        return empenhoContabil;
    }

    public String getContrato() {
        return contrato;
    }

    public String getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }
    
}
