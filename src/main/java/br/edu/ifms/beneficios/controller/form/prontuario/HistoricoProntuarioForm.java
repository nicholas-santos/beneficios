/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form.prontuario;

/**
 *
 * @author santos
 */
public class HistoricoProntuarioForm {

    private Long prontuarioId;
    private Long unidadeAtendimentoId;
    private String observacao;

    public HistoricoProntuarioForm() {
    }

    public HistoricoProntuarioForm(Long prontuarioId, Long unidadeAtendimentoId, String observacao) {
        this.prontuarioId = prontuarioId;
        this.unidadeAtendimentoId = unidadeAtendimentoId;
        this.observacao = observacao;
    }

    public Long getProntuarioId() {
        return prontuarioId;
    }

    public void setProntuarioId(Long id) {
        this.prontuarioId = id;
    }

    public Long getUnidadeAtendimentoId() {
        return unidadeAtendimentoId;
    }

    public void setUnidadeAtendimentoId(Long uiId) {
        this.unidadeAtendimentoId = uiId;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
}
