/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.controller.dto.usuario.UsuarioDto;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author santos
 */
public class JwtResponse {

    private final String token;
    private final UsuarioDto user;
    private final Long expirationDate;

    public JwtResponse(String token, Usuario user, Long expirationDate) {
        this.token = token;
        this.user = new UsuarioDto(user);
        this.expirationDate = expirationDate;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public String getToken() {
        return this.token;
    }

    public UsuarioDto getUser() {
        return user;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
