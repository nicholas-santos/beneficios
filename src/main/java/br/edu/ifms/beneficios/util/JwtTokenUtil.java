/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.util;

import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.property.BeneficiosProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 *
 * @author santos
 */
public class JwtTokenUtil {

    public static BeneficiosProperties beneficioProperties;

    private static final String AUDIENCE = "beneficios";
    private String token = null;

    public JwtTokenUtil(String jwtToken) {
        this.token = jwtToken;
    }

    public String getToken() {
        return token;
    }

    public String getUsernameFromToken() {
        return getClaimFromToken(Claims::getSubject);
    }

    public Date getExpirationDateFromToken() {
        return getClaimFromToken(Claims::getExpiration);
    }

    public <T> T getClaimFromToken(Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken();
        return claimsResolver.apply(claims);
    }

    public String getClaimFromTokenByName(String name) {
        final Claims claims = getAllClaimsFromToken();
        return (String) claims.get(name);
    }

    private Claims getAllClaimsFromToken() {
        String secret = beneficioProperties.getJwt().getSecret();
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired() {
        final Date expiration = getExpirationDateFromToken();
        return expiration.before(new Date());
    }

    public static JwtTokenUtil generateToken(Usuario user) {
        Map<String, Object> claims = addClaims(user);
        return doGenerateToken(claims, user.getUsername());
    }

    private static Map<String, Object> addClaims(Usuario user) {
        Map<String, Object> claims = new HashMap<String, Object>();

        claims.put("id", user.getId());
        claims.put("authorities", user.getAuthorityNames());

        return claims;
    }

    private static JwtTokenUtil doGenerateToken(Map<String, Object> claims, String subject) {
        long expiration = beneficioProperties.getJwt().getExpiration().toMillis();
        String secret = beneficioProperties.getJwt().getSecret();
        String token = Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setAudience(AUDIENCE)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .signWith(SignatureAlgorithm.HS512, secret).compact();

        JwtTokenUtil jwtUtil = new JwtTokenUtil(token);
        return jwtUtil;
    }

    public Boolean validateToken(Usuario user) {
        final String email = getUsernameFromToken();
        return (user != null && email.equals(user.getEmail()) && !isTokenExpired());
    }

    public Boolean validateToken() {
        return (!isTokenExpired());
    }
}
