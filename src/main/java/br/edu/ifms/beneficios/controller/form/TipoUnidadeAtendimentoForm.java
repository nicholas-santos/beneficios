/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import br.edu.ifms.beneficios.repository.TipoUnidadeAtendimentoRepository;

/**
 *
 * @author tanabe
 */
public class TipoUnidadeAtendimentoForm {

    @NotNull
    @NotEmpty
    @Length(min = 3)
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setDescricao(String nome) {
        this.nome = nome;
    }

    public TipoUnidadeAtendimento converter(TipoUnidadeAtendimentoRepository repository) {
        Long maxId = repository.getMaxId() + 1;
        return new TipoUnidadeAtendimento(maxId, this.nome);
    }

    public TipoUnidadeAtendimento atualizar(Long id, TipoUnidadeAtendimentoRepository repository) {
    	TipoUnidadeAtendimento tipoUnidadeAtenimento = repository.getOne(id);
    	tipoUnidadeAtenimento.setNome(this.nome);

        return tipoUnidadeAtenimento;
    }
}
