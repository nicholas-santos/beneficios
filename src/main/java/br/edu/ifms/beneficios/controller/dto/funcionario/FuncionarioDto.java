/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.funcionario;

import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.modelo.Funcionario;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class FuncionarioDto extends AbstractFuncionarioDto {
    
    private UnidadeAtendimentoDto unidadeAtendimento;

    public FuncionarioDto() {
    }

    public FuncionarioDto(Funcionario obj) {
        super(obj);
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getUnidadeAtendimento());
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }
    
    public static List<FuncionarioDto> converter(List<Funcionario> items) {
        return items.stream().map(FuncionarioDto::new).collect(Collectors.toList());
    }
    
}
