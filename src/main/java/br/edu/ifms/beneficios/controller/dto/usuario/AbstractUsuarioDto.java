/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.usuario;

import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDetalheDto;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;

/**
 *
 * @author santos
 */
public abstract class AbstractUsuarioDto {

    private Long id;
    private String nome;
    private String email;
    private Status status;
    private Boolean enabled;
    private FuncionarioDetalheDto funcionario;

    public AbstractUsuarioDto() {
    }

    public AbstractUsuarioDto(Usuario obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.email = obj.getEmail();
        this.status = obj.getStatus();
        this.enabled = obj.getEnabled();
        this.funcionario = obj.getFuncionario() != null ? new FuncionarioDetalheDto(obj.getFuncionario()) : null;
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public Status getStatus() {
        return status;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public FuncionarioDetalheDto getFuncionario() {
        return funcionario;
    }
    

}
