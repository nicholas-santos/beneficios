/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.DocumentoDto;
import br.edu.ifms.beneficios.controller.form.DocumentoForm;
import br.edu.ifms.beneficios.modelo.Documento;
import br.edu.ifms.beneficios.repository.DocumentoRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/documentos")
public class DocumentoController {

    @Autowired
    private DocumentoRepository repository;

    @GetMapping
    @Cacheable(value = "pageDocumento")
    public Page<DocumentoDto> listar(
            @PageableDefault(sort = "descricao", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

            Page<Documento> documentos = repository.findAll(paginacao);
            return DocumentoDto.converter(documentos);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaDocumento")
    public List<DocumentoDto> listar() {
        List<Documento> documento = repository.findAll();
        return DocumentoDto.converter(documento);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DocumentoDto> visualizar(@PathVariable Long id) {
        Optional<Documento> documento = repository.findById(id);
        if (documento.isPresent()) {
            return ResponseEntity.ok(new DocumentoDto(documento.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageDocumento", "listaDocumento"}, allEntries = true)
    public ResponseEntity<DocumentoDto> criar(@RequestBody @Valid DocumentoForm form, UriComponentsBuilder uriBuilder) {
    	Documento documento = form.converter();
        repository.save(documento);

        URI uri = uriBuilder.path("/documentos/{id}").buildAndExpand(documento.getId()).toUri();
        return ResponseEntity.created(uri).body(new DocumentoDto(documento));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageDocumento", "listaDocumento"}, allEntries = true)
    public ResponseEntity<DocumentoDto> atualizar(@PathVariable Long id, @RequestBody @Valid DocumentoForm form) {
        Optional<Documento> optional = repository.findById(id);
        if (optional.isPresent()) {
        	Documento documento = form.atualizar(id, repository);
            return ResponseEntity.ok(new DocumentoDto(documento));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageDocumento", "listaDocumento"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Documento> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
