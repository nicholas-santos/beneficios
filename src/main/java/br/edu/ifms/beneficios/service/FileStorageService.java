/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.exception.FileStorageException;
import br.edu.ifms.beneficios.exception.MyFileNotFoundException;
import br.edu.ifms.beneficios.modelo.Arquivo;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.repository.ArquivoRepository;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author santos
 */
@Service
public class FileStorageService {

    private static final String[] IMAGES_TYPES = {
        "text/jpg", "text/jpeg", "text/png", "text/gif"
    };
    private Path fileStorageLocation;

    @Autowired
    private ArquivoRepository repository;

    @Autowired
    private MultipartProperties fileStorageProperties;

    public FileStorageService(MultipartProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getLocation())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.\n"+
                    this.fileStorageLocation.toAbsolutePath().toString(), ex);
        }
    }

    public void setFileStorageLocation(String sPath) {
        this.fileStorageLocation = Paths
                .get(fileStorageProperties.getLocation() + "/" + sPath)
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    private String generateRandonFilename() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString;
    }

    public byte[] resizeImageToByte(MultipartFile multipartFile, int targetWidth) {
        try {
            /**
             * Converte o arquivo para BufferedImage para realizar o
             * redimensionamento da imagem
             */
            BufferedImage bi = ImageIO.read(multipartFile.getInputStream());
            BufferedImage resizeImage = Scalr.resize(bi, targetWidth);
            /**
             * Converte a imagem redimensionada para byte
             */
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            String type = multipartFile.getContentType().split("/")[1];
            ImageIO.write(resizeImage, type, baos);
            return baos.toByteArray();
        } catch (Exception ex) {
            Logger.getLogger(FileStorageService.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return null;
    }

    private void saveFile(MultipartFile file, String fileName,
            Usuario usuario, String docType) {
        Optional<Arquivo> optional = repository.findByUsuarioAndDocumentType(usuario, docType);
        Arquivo doc = null;
        if (optional.isPresent()) {
            doc = optional.get();
            doc.setDocumentFormat(file.getContentType());
            doc.setFileName(fileName);
            repository.save(doc);
        } else {
            doc = new Arquivo();
            doc.setUsuario(usuario);
            doc.setDocumentFormat(file.getContentType());
            doc.setFileName(fileName);
            doc.setDocumentType(docType);
        }
        repository.save(doc);
    }

    public String storeFile(MultipartFile file, Arquivo arquivo) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(arquivo.getFileName());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

//            setFileStorageLocation(arquivo.getUploadDir());
            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public String storeFile(MultipartFile file, String fileName) {
        // Normalize file name
        fileName = StringUtils.cleanPath(fileName);

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public String storeFile(MultipartFile file, Usuario usuario, String docType) {
        String fileName = this.storeFile(file, String.format("%s-%s-",
                usuario.getId(), docType));
        // Grava os dados do arquivo no banco de dados
        saveFile(file, fileName, usuario, docType);
        return fileName;
    }

    public ByteArrayResource loadFile(String fileName) {
        try {
            fileName = this.fileStorageProperties.getLocation() + fileName;
            Path filePath = Paths.get(fileName).normalize();

//            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(filePath));
            return resource;
        } catch (IOException ex) {
            throw new MyFileNotFoundException("FileStorageService: File not found " + fileName, ex);
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            fileName = this.fileStorageProperties.getLocation() + fileName;
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    /**
     * Recupera o nome do arquivo do usuario
     *
     * @param usuario
     * @param docType
     * @return
     */
    public String getDocumentName(Usuario usuario, String docType) {
        Optional<Arquivo> optional = repository.findByUsuarioAndDocumentType(usuario, docType);
        if (optional.isPresent()) {
            return optional.get().getFileName();
        }
        return null;
    }
}
