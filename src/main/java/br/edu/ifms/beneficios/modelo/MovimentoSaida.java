/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.util.Objects;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
@DiscriminatorValue(value = "S")
public class MovimentoSaida extends Movimento {
    
    @ManyToOne
    private ItemSaida itemSaida;

    public MovimentoSaida() {
        super();
    }

    public ItemSaida getItemSaida() {
        return itemSaida;
    }

    public void setItemSaida(ItemSaida itemSaida) {
        this.itemSaida = itemSaida;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.itemSaida);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final MovimentoSaida other = (MovimentoSaida) obj;
        if (!Objects.equals(this.itemSaida, other.getItemSaida())) {
            return false;
        }
        return true;
    }
    
}
