/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Logradouro;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class CepId implements Serializable {
    
    @ManyToOne
    private Cidade cidade;
    
    @ManyToOne
    private Bairro bairro;
    
    @ManyToOne
    private Logradouro logradouro;

    public CepId() {
    }

    public CepId(Cidade cidade, Bairro bairro, Logradouro logradouro) {
        this.cidade = cidade;
        this.bairro = bairro;
        this.logradouro = logradouro;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Bairro getBairro() {
        return bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }

    public Logradouro getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(Logradouro logradouro) {
        this.logradouro = logradouro;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.cidade);
        hash = 83 * hash + Objects.hashCode(this.bairro);
        hash = 83 * hash + Objects.hashCode(this.logradouro);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final CepId other = (CepId) obj;
        if (!Objects.equals(this.cidade, other.getCidade())) {
            return false;
        }
        if (!Objects.equals(this.bairro, other.getBairro())) {
            return false;
        }
        if (!Objects.equals(this.logradouro, other.getLogradouro())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CepId{" + "cidade=" + cidade + ", bairro=" + bairro + ", logradouro=" + logradouro + '}';
    }
    
}
