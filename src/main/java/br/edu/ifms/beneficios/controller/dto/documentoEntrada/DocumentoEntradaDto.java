/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoEntrada;

import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class DocumentoEntradaDto extends AbstractDocumentoEntradaDto {

    public DocumentoEntradaDto() {
    }

    public DocumentoEntradaDto(DocumentoEntrada obj) {
        super(obj);
    }
    
    public static Page<DocumentoEntradaDto> converter(Page<DocumentoEntrada> items) {
        return items.map(DocumentoEntradaDto::new);
    }
    
    public static List<DocumentoEntradaDto> converter(List<DocumentoEntrada> items) {
        return items.stream().map(DocumentoEntradaDto::new).collect(Collectors.toList());
    }
}
