/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoSaida;

import br.edu.ifms.beneficios.controller.dto.fornecedor.FornecedorDto;
import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.modelo.DocumentoSaida;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.time.LocalDateTime;

/**
 *
 * @author santos
 */
public class AbstractDocumentoSaidaDto {
    private Long id;
    private FuncionarioDto funcionario;
    private UnidadeAtendimentoDto unidadeAtendimento;
    private LocalDateTime emissao;
    private Status status;
    private String observacao;

    public AbstractDocumentoSaidaDto() {
    }

    public AbstractDocumentoSaidaDto(DocumentoSaida obj) {
        this.id = obj.getId();
        this.funcionario = new FuncionarioDto(obj.getFuncionario());
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getUnidadeAtendimento());
        this.emissao = obj.getEmissao();
        this.status = obj.getStatus();
        this.observacao = obj.getObservacao();
    }

    public Long getId() {
        return id;
    }

    public FuncionarioDto getFuncionario() {
        return funcionario;
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public Status getStatus() {
        return status;
    }

    public String getObservacao() {
        return observacao;
    }
    
}
