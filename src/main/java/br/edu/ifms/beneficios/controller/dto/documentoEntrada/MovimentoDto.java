/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoEntrada;

import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDto;
import br.edu.ifms.beneficios.modelo.Movimento;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author santos
 */
public class MovimentoDto {

    private Long numero;
    private UnidadeAtendimentoDto unidadeAtendimento;
    private FuncionarioDto funcionario;
    private BigDecimal quantidade;
    private LocalDateTime emissao;

    public MovimentoDto() {
    }

    public MovimentoDto(Movimento obj) {
        this.numero = obj.getNumero();
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getUnidadeAtendimento());
        this.quantidade = obj.getQuantidade();
        this.emissao = obj.getEmissao();
        this.funcionario = new FuncionarioDto(obj.getFuncionario());
    }

    public Long getNumero() {
        return numero;
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public FuncionarioDto getFuncionario() {
        return funcionario;
    }
    
}
