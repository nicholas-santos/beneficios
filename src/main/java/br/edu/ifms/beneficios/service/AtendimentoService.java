/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.atendimento.AtendimentoDto;
import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.form.AtendimentoForm;
import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.modelo.Atendimento;
import br.edu.ifms.beneficios.modelo.HistoricoAtendimento;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.Prontuario;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimento;
import br.edu.ifms.beneficios.repository.AtendimentoRepository;
import br.edu.ifms.beneficios.repository.HistoricoAtendimentoRepository;
import br.edu.ifms.beneficios.repository.PessoaRepository;
import br.edu.ifms.beneficios.repository.ProntuarioRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import br.edu.ifms.beneficios.controller.dto.status.IStatusAtendimentoCount;
import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import javax.persistence.EntityNotFoundException;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author santos
 */
@Service
public class AtendimentoService {

    @Autowired
    private AtendimentoRepository repository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private ProntuarioRepository prontuarioRepository;

    @Autowired
    private HistoricoAtendimentoRepository historicoAtendimentoRepository;

    public Page<Atendimento> listar(
            Long pessoaId, StatusAtendimento status, 
            Long unidadeAtendimentoId,
            Pageable paginacao) {
        Boolean hasPessoa = pessoaId != null,
                hasStatus = status != null,
                hasUaid = unidadeAtendimentoId != null && unidadeAtendimentoId > 0;
        
        if (hasPessoa) {
            Optional<Pessoa> op = pessoaRepository.findById(pessoaId);
            Pessoa p = op.get();
            
            if (hasUaid && !hasStatus) {
                return repository.findByPessoaAndAtendenteUnidadeAtendimentoId(p, unidadeAtendimentoId, paginacao);
            }
            if (!hasUaid && hasStatus) {
                return repository.findByPessoaAndStatus(p, status, paginacao);
            }
            if (hasUaid && hasStatus) {
                return repository.findByPessoaAndStatusAndAtendenteUnidadeAtendimentoId(p, status, unidadeAtendimentoId, paginacao);
            }
            return repository.findByPessoa(p, paginacao);
        }
        
        if (hasUaid && !hasStatus) {
            return repository.findByAtendenteUnidadeAtendimentoId(unidadeAtendimentoId, paginacao);
        }
        if (!hasUaid && hasStatus) {
            return repository.findByStatus(status, paginacao);
        }
        if (hasUaid && hasStatus) { 
           return repository.findByStatusAndAtendenteUnidadeAtendimentoId(status, unidadeAtendimentoId, paginacao);
        }
        return repository.findAll(paginacao);
    }

    public Page<Atendimento> listar(
            Long prontuarioId, Long unidadeAtendimentoId, 
            LocalDateTime emissao,
            Pageable paginacao) {
        return repository.findByProntuarioIdAndProntuarioUnidadeAtendimentoIdAndEmissaoLessThan(
                prontuarioId, unidadeAtendimentoId, emissao, paginacao);
    }

    public List<Atendimento> listar() {
        return repository.findAll();
    }
    
    public List<IStatusAtendimentoCount> listarContagem(Long unidadeAtendimentoId) {
        if (unidadeAtendimentoId != null && unidadeAtendimentoId > 0) {
            return repository.countTotalAtendimentoByStatusAtendimentoAndUnidadeAtendimentoId(unidadeAtendimentoId);
        }
        return repository.countTotalAtendimentoByStatusAtendimento();
    }

    public Atendimento buscarPor(Long id) {
        Optional<Atendimento> optional = repository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Não existe benefício eventual com o código " + id);
    }

    public Atendimento buscarPorAnaliseId(Long id) {
        Optional<Atendimento> optional = repository.findByAnaliseId(id);
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Não existe Analise com o código " + id);
    }

    public AtendimentoDto criar(AtendimentoForm form) {
        Atendimento atendimento = form.converter(pessoaRepository);

        Usuario usuario = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        usuario = usuarioRepository.getById(usuario.getId());
        atendimento.setAtendente(usuario.getFuncionario());
        atendimento.setProntuario(getProntuarioAtivo(atendimento));
        atendimento = repository.save(atendimento);

        return new AtendimentoDto(atendimento);
    }

    public AtendimentoDto atualizar(Long id, AtendimentoForm form) {
        try {
            Atendimento atendimento = form.atualizar(id, repository,
                    pessoaRepository);

            return new AtendimentoDto(atendimento);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Não existe atendimento com o código " + id, ex);
        }
    }

    public void remover(Long id) {
        Optional<Atendimento> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Não existe atendimento com o código " + id);
        }
    }

    public MessageDto podeImportarProntuario(Atendimento atendimento) {
        Prontuario prontuario = getProntuarioAtivo(atendimento);
        if (prontuario == null) {
            return new MessageDto(
                    HttpStatus.BAD_REQUEST.value(),
                    "Não existe prontuário ativo para importar!");
        }
        return null;
    }

    public AtendimentoDto importarProntuario(Atendimento atendimento) {
        Prontuario prontuario = getProntuarioAtivo(atendimento);
        prontuario = prontuarioRepository.getOne(prontuario.getId());
        atendimento.setProntuario(prontuario);

        return new AtendimentoDto(repository.save(atendimento));
    }

    public AtendimentoDto ativar(Long id, StatusAtendimento status) {
        Optional<Atendimento> optional = repository.findById(id);

        if (optional.isPresent()) {
            Atendimento atendimento = optional.get();
            atendimento.setStatus(status);

            criarHistoricoAtendimento(atendimento);

            return new AtendimentoDto(atendimento);
        }

        return null;
    }

    public void criarHistoricoAtendimento(Atendimento atendimento) {
        Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);

        HistoricoAtendimento ha = new HistoricoAtendimento();
        ha.setAtendimento(atendimento);
        ha.setStatus(atendimento.getStatus());
        ha.setFuncionario(usuario.getFuncionario());

        historicoAtendimentoRepository.save(ha);
    }

    public Prontuario getProntuarioAtivo(Atendimento atendimento) {
        Pessoa pessoa = atendimento.getPessoa();
        /**
         * Verifica se a pessoa é titular de um prontuario
         */
        List<Prontuario> lista = prontuarioRepository
                .findByTitularAndStatus(pessoa, Status.ATIVO);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }

        /**
         * Verifica se a pessoa é dependente de algum prontuário
         */
        lista = prontuarioRepository
                .findByStatusAndDependentesIdPessoa(Status.ATIVO, pessoa);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        return null;
    }
}
