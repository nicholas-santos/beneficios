/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.controller.dto.status;

import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class StatusCountDto implements IStatusCount {
    
    private Status status;
    private Long totalStatus;
    private Double percentualStatus;

    public StatusCountDto() {
    }

    public StatusCountDto(IStatusCount obj) {
        this.status = obj.getStatus();
        this.totalStatus = obj.getTotalStatus();
        this.percentualStatus = obj.getPercentualStatus();
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public Long getTotalStatus() {
        return this.totalStatus;
    }

    @Override
    public Double getPercentualStatus() {
        return this.percentualStatus;
    }

    public static List<StatusCountDto> converter(List<IStatusCount> items) {
        return items.stream().map(StatusCountDto::new).collect(Collectors.toList());
    }
    
}
