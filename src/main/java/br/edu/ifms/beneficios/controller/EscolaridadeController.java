/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.EscolaridadeDto;
import br.edu.ifms.beneficios.controller.form.EscolaridadeForm;
import br.edu.ifms.beneficios.modelo.Escolaridade;
import br.edu.ifms.beneficios.repository.EscolaridadeRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/escolaridades")
public class EscolaridadeController {

    @Autowired
    private EscolaridadeRepository repository;

    @GetMapping
    @Cacheable(value = "pageDeEscolaridade")
    public Page<EscolaridadeDto> listar(
            @PageableDefault(sort = "descricao", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

            Page<Escolaridade> escolaridades = repository.findAll(paginacao);
            return EscolaridadeDto.converter(escolaridades);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaDeEscolaridade")
    public List<EscolaridadeDto> listar() {
        List<Escolaridade> escolaridade = repository.findAll();
        return EscolaridadeDto.converter(escolaridade);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EscolaridadeDto> visualizar(@PathVariable Long id) {
        Optional<Escolaridade> escolaridade = repository.findById(id);
        if (escolaridade.isPresent()) {
            return ResponseEntity.ok(new EscolaridadeDto(escolaridade.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageDeEscolaridade", "listaDeEscolaridade"}, allEntries = true)
    public ResponseEntity<EscolaridadeDto> criar(@RequestBody @Valid EscolaridadeForm form, UriComponentsBuilder uriBuilder) {
    	Escolaridade escolaridade = form.converter();
        repository.save(escolaridade);

        URI uri = uriBuilder.path("/escolaridades/{id}").buildAndExpand(escolaridade.getId()).toUri();
        return ResponseEntity.created(uri).body(new EscolaridadeDto(escolaridade));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageDeEscolaridade", "listaDeEscolaridade"}, allEntries = true)
    public ResponseEntity<EscolaridadeDto> atualizar(@PathVariable Long id, @RequestBody @Valid EscolaridadeForm form) {
        Optional<Escolaridade> optional = repository.findById(id);
        if (optional.isPresent()) {
            Escolaridade escolaridade = form.atualizar(id, repository);
            return ResponseEntity.ok(new EscolaridadeDto(escolaridade));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageDeEscolaridade", "listaDeEscolaridade"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Escolaridade> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
