/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.modelo.Menu;
import br.edu.ifms.beneficios.modelo.tipos.MenuTipo;
import br.edu.ifms.beneficios.repository.MenuRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author santos
 */
public class MenuForm {
    @NotNull @NotEmpty @Length(min = 3)
    private String nome;
    private String remotePath;
    private Boolean disponivel;
    private MenuTipo menuTipo;
    
    public MenuForm() {
        menuTipo = MenuTipo.CADASTRO;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setRemotePath(String endereco) {
        this.remotePath = endereco;
    }

    public Boolean getDisponivel() {
        return disponivel;
    }

    public void setDisponivel(Boolean disponivel) {
        this.disponivel = disponivel;
    }

    public MenuTipo getMenuTipo() {
        return menuTipo;
    }

    public void setMenuTipo(MenuTipo menuTipo) {
        this.menuTipo = menuTipo;
    }
    
    public Menu converter() {
        Menu obj = new Menu(this.nome, this.remotePath, this.disponivel);
        obj.setTipo(menuTipo);
        return obj;
    }
    
    public Menu atualizar(Long id, MenuRepository repository) {
        Menu menu = repository.getOne(id);
        menu.setNome(this.nome);
        menu.setRemotePath(this.remotePath);
        menu.setDisponivel(this.disponivel);
        menu.setTipo(menuTipo);
        
        return menu;
    }
}
