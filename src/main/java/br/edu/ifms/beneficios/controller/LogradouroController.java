/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.LogradouroDto;
import br.edu.ifms.beneficios.controller.form.LogradouroForm;
import br.edu.ifms.beneficios.modelo.Logradouro;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.TipoLogradouroRepository;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/logradouros")
@Profile(value = {"prod", "test"})
public class LogradouroController {

    @Autowired
    private LogradouroRepository repository;

    @Autowired
    private TipoLogradouroRepository tipoRepository;

    @GetMapping
    @Cacheable(value = "pageLogradouros")
    public Page<LogradouroDto> listar(
            @RequestParam(required = false) String nome,
            @PageableDefault(sort = "nome", direction = Sort.Direction.ASC,
                    page = 0, size = 10) Pageable paginacao) {
        Page<Logradouro> items;
        if (nome != null) {
            items = repository.findByNomeContainingIgnoreCase(nome, paginacao);
        } else {
            items = repository.findAll(paginacao);
        }
        return LogradouroDto.converter(items);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaDeLogradouros")
    public List<LogradouroDto> listar() {
        List<Logradouro> items = repository.findAll();

        return LogradouroDto.converter(items);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LogradouroDto> visualizar(@PathVariable Long id) {

        Optional<Logradouro> logradouro = repository.findById(id);
        if (logradouro.isPresent()) {
            return ResponseEntity.ok(new LogradouroDto(logradouro.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageLogradouros", "listaDeLogradouros"}, allEntries = true)
    public ResponseEntity<LogradouroDto> cadastrar(@RequestBody @Valid LogradouroForm form,
            UriComponentsBuilder uriBuilder) {
        Logradouro logradouro = form.converter(tipoRepository);
        repository.save(logradouro);

        URI uri = uriBuilder.path("/logradouros/{id}")
                .buildAndExpand(logradouro.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new LogradouroDto(logradouro));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageLogradouros", "listaDeLogradouros"}, allEntries = true)
    public ResponseEntity<LogradouroDto> atualizar(
            @PathVariable Long id, @RequestBody @Valid LogradouroForm form) {
        Optional<Logradouro> optional = repository.findById(id);
        if (optional.isPresent()) {
            Logradouro logradouro = form.atualizar(id, repository, tipoRepository);
            return ResponseEntity.ok(new LogradouroDto(logradouro));
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageLogradouros", "listaDeLogradouros"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Logradouro> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}
