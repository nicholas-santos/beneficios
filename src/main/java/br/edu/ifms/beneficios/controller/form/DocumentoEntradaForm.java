/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.documentoEntrada.ItemEntradaDto;
import br.edu.ifms.beneficios.controller.dto.fornecedor.FornecedorDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.helper.DateHelper;
import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import br.edu.ifms.beneficios.modelo.Fornecedor;
import br.edu.ifms.beneficios.modelo.ItemEntrada;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.id.ItemEntradaId;
import br.edu.ifms.beneficios.repository.BeneficioEventualRepository;
import br.edu.ifms.beneficios.repository.DocumentoEntradaRepository;
import br.edu.ifms.beneficios.repository.FornecedorRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author santos
 */
public class DocumentoEntradaForm {

    @NotNull
    private UnidadeAtendimentoDto unidadeAtendimento;

    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date emissao;
    private String observacao;

    @NotNull
    private FornecedorDto fornecedor;
    private String processo;
    private String ata;
    private String pregao;
    private String empenhoContabil;
    private String contrato;
    private String numeroNotaFiscal;    
    private Boolean doacao;

    private List<ItemEntradaDto> itens = new ArrayList();

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimentoDto unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public Date getEmissao() {
        return emissao;
    }

    public void setEmissao(Date emissao) {
        this.emissao = emissao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public FornecedorDto getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(FornecedorDto fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public String getAta() {
        return ata;
    }

    public void setAta(String ata) {
        this.ata = ata;
    }

    public String getPregao() {
        return pregao;
    }

    public void setPregao(String pregao) {
        this.pregao = pregao;
    }

    public String getEmpenhoContabil() {
        return empenhoContabil;
    }

    public void setEmpenhoContabil(String empenhoContabil) {
        this.empenhoContabil = empenhoContabil;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(String numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    public Boolean getDoacao() {
        return doacao;
    }

    public void setDoacao(Boolean doacao) {
        this.doacao = doacao;
    }

    public List<ItemEntradaDto> getItens() {
        return itens;
    }

    public void setItens(List<ItemEntradaDto> itens) {
        this.itens = itens;
    }

    public String isValido() {
        if (!doacao) {
            if (processo == null || processo.isBlank() || processo.isEmpty()) {
                return "Você deve informar um valor para o PROCESSO";
            }
            
            if (ata == null || ata.isBlank() || ata.isEmpty()) {
                return "Você deve informar um valor para a ATA";
            }
            
            if (pregao == null || pregao.isBlank() || pregao.isEmpty()) {
                return "Você deve informar um valor para o PREGÃO";
            }
            
            if (empenhoContabil == null || empenhoContabil.isBlank() || empenhoContabil.isEmpty()) {
                return "Você deve informar um valor para o EMPENHO CONTÁBIL";
            }
            
            if (contrato == null || contrato.isBlank() || contrato.isEmpty()) {
                return "Você deve informar um valor para o CONTRATO";
            }
            
            if (numeroNotaFiscal == null || numeroNotaFiscal.isBlank() || numeroNotaFiscal.isEmpty()) {
                return "Você deve informar um valor para o NÚMERO DA NOTA FISCAL";
            }
        }
        
        if (itens.isEmpty()) {
            return "Você deve lançar ao menos UM item no Documento de Entrada.";
        }

        int countItens = this.itens.stream().mapToInt(dto
                -> !dto.hasPositiveValue() ? 1 : 0).sum();
        if (countItens > 1) {
            return "Existem itens com quantidades incorretas!";
        }
        
        countItens = this.itens.stream().mapToInt(dto
                -> !dto.hasPositiveValue() ? 1 : 0).sum();
        if (countItens > 1) {
            return "Existem itens com quantidades incorretas!";
        }

        return "";
    }

    private void updateItensEntrada(DocumentoEntrada obj,
            BeneficioEventualRepository beneficioEventualRepository) {

        itens.stream().map(dto -> {
            obj.getItens().removeIf(item
                    -> dto.isDeleted()
                    && item.getId().getNumero().equals(dto.getNumero())
                    && item.getBeneficioEventual().getId().equals(dto.getBeneficioEventual().getId()));

            if (!dto.isDeleted() && !dto.isSetNumero()) {
                BeneficioEventual beneficio = beneficioEventualRepository
                        .getOne(dto.getBeneficioEventual().getId());

                ItemEntradaId id = new ItemEntradaId(dto.getNumero(), obj);
                ItemEntrada itemEntrada = new ItemEntrada(id, dto.getQuantidade(), beneficio);
                itemEntrada.setDeleted(dto.isDeleted());
                obj.add(itemEntrada);
            }
            return dto;
        }).collect(Collectors.toList());
    }

    private DocumentoEntrada toDocumentoEntrada(DocumentoEntrada obj,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            FornecedorRepository fornecedorRepository,
            BeneficioEventualRepository beneficioEventualRepository) {
        UnidadeAtendimento ua = unidadeAtendimentoRepository.getOne(unidadeAtendimento.getId());
        obj.setUnidadeAtendimento(ua);

        Fornecedor forn = fornecedorRepository.getOne(this.fornecedor.getId());
        obj.setFornecedor(forn);

        obj.setEmissao(DateHelper.toLocalDateTime(emissao));
        obj.setObservacao(observacao);
        obj.setProcesso(processo);
        obj.setAta(ata);
        obj.setPregao(pregao);
        obj.setEmpenhoContabiL(empenhoContabil);
        obj.setContrato(contrato);
        obj.setNumeroNotaFiscal(numeroNotaFiscal);
        obj.setDoacao(doacao);

        updateItensEntrada(obj, beneficioEventualRepository);

        return obj;
    }

    public DocumentoEntrada converter(DocumentoEntradaRepository repository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            FornecedorRepository fornecedorRepository,
            BeneficioEventualRepository beneficioEventualRepository) {
        Long maxId = repository.getMaxId() + 1;
        return toDocumentoEntrada(new DocumentoEntrada(maxId),
                unidadeAtendimentoRepository,
                fornecedorRepository,
                beneficioEventualRepository);
    }

    public DocumentoEntrada atualizar(Long id,
            DocumentoEntradaRepository repository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            FornecedorRepository fornecedorRepository,
            BeneficioEventualRepository beneficioEventualRepository) {

        return toDocumentoEntrada(repository.getOne(id),
                unidadeAtendimentoRepository,
                fornecedorRepository,
                beneficioEventualRepository);
    }
}
