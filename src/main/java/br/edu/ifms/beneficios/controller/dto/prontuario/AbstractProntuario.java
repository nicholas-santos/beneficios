/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.prontuario;

import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.modelo.Prontuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.time.LocalDateTime;

/**
 *
 * @author santos
 */
public abstract class AbstractProntuario {
    private Long id;
    private UnidadeAtendimentoDto unidadeAtendimento;
    private LocalDateTime emissao;
    private Boolean acompanhamento;
    private Status status;

    public AbstractProntuario() {
    }

    public AbstractProntuario(Prontuario obj) {
        this.id = obj.getId();
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getUnidadeAtendimento());
        this.emissao = obj.getEmissao();
        this.acompanhamento = obj.getAcompanhamento();
        this.status = obj.getStatus();
    }

    public Long getId() {
        return id;
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public Boolean getAcompanhamento() {
        return acompanhamento;
    }

    public Status getStatus() {
        return status;
    }
}
