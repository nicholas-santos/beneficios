/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.beneficioEventual;

import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class BeneficioEventualDto extends AbstractBeneficioEventualDto {
    
    public BeneficioEventualDto() {
    }

    public BeneficioEventualDto(BeneficioEventual obj) {
        super(obj);
    }
    
    public static Page<BeneficioEventualDto> converter(Page<BeneficioEventual> items) {
        return items.map(BeneficioEventualDto::new);
    }
    
    public static List<BeneficioEventualDto> converter(List<BeneficioEventual> items) {
        return items.stream().map(BeneficioEventualDto::new).collect(Collectors.toList());
    }
    
}
