/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.fornecedor;

import br.edu.ifms.beneficios.controller.dto.ContatoPessoaDto;
import br.edu.ifms.beneficios.controller.dto.DocumentoPessoaDto;
import br.edu.ifms.beneficios.modelo.Fornecedor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class FornecedorDetalheDto extends AbstractFornecedorDto {

    private List<ContatoPessoaDto> contatos = new ArrayList();
    private List<DocumentoPessoaDto> documentos = new ArrayList();

    public FornecedorDetalheDto() {
    }

    public FornecedorDetalheDto(Fornecedor obj) {
        super(obj);
        this.contatos = ContatoPessoaDto.converter(obj.getContatos());
        this.documentos = DocumentoPessoaDto.converter(obj.getDocumentos());

    }

    public List<ContatoPessoaDto> getContatos() {
        return contatos;
    }

    public List<DocumentoPessoaDto> getDocumentos() {
        return documentos;
    }

    public static Page<FornecedorDetalheDto> converter(Page<Fornecedor> items) {
        return items.map(FornecedorDetalheDto::new);
    }

    public static List<FornecedorDetalheDto> converter(List<Fornecedor> items) {
        return items.stream().map(FornecedorDetalheDto::new).collect(Collectors.toList());
    }

}
