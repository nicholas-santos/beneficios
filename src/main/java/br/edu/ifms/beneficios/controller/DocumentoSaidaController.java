/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.documentoSaida.DocumentoSaidaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.documentoSaida.DocumentoSaidaDto;
import br.edu.ifms.beneficios.controller.dto.documentoSaida.ItemSaidaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.documentoSaida.MovimentoSaidaDto;
import br.edu.ifms.beneficios.controller.dto.status.StatusCountDto;
import br.edu.ifms.beneficios.controller.form.DocumentoSaidaForm;
import br.edu.ifms.beneficios.controller.form.MovimentoSaidaForm;
import br.edu.ifms.beneficios.modelo.DocumentoSaida;
import br.edu.ifms.beneficios.modelo.ItemSaida;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.service.DocumentoSaidaService;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/documento-saida")
@Profile(value = {"prod", "test"})
public class DocumentoSaidaController {

    @Autowired
    private DocumentoSaidaService service;

    @GetMapping
    public Page<DocumentoSaidaDetalheDto> listar(
            @RequestParam(required = false) Status status,
            @PageableDefault(sort = "emissao", direction = Sort.Direction.DESC,
                    page = 0, size = 10) Pageable paginacao) {
        return DocumentoSaidaDetalheDto.converter(
                service.listar(status, paginacao));
    }

    @GetMapping("/itens")
    public Page<ItemSaidaDetalheDto> listarPorStatus(
            @RequestParam(required = false) Status status,
            @PageableDefault(sort = "id.documentoSaida.emissao", direction = Sort.Direction.DESC,
                    page = 0, size = 10) Pageable paginacao) {
        return ItemSaidaDetalheDto.converter(
                service.listarPor(status, paginacao));
    }

    @GetMapping("/list")
    public List<DocumentoSaidaDto> listar() {
        return DocumentoSaidaDto.converter(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DocumentoSaidaDetalheDto> visualizar(@PathVariable Long id) {
        Optional<DocumentoSaida> optional = service.buscarPor(id);
        if (optional.isPresent()) {
            DocumentoSaida documentoSaida = optional.get();
            return ResponseEntity.ok(new DocumentoSaidaDetalheDto(documentoSaida));
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/item/{documentoSaidaId}/{numero}")
    public ResponseEntity<ItemSaidaDetalheDto> visualizar(
            @PathVariable Long documentoSaidaId,
            @PathVariable Long numero
    ) {
        Optional<ItemSaida> optional = service.buscarPor(documentoSaidaId, numero);
        if (optional.isPresent()) {
            ItemSaida itemSaida = optional.get();
            return ResponseEntity.ok(new ItemSaidaDetalheDto(itemSaida));
        }

        return ResponseEntity.notFound().build();
    }
    
    @GetMapping("/movimentos-de-saida/{numero}/{documentoSaidaId}")
    public List<MovimentoSaidaDto> listar(
            @PathVariable Long numero,
            @PathVariable Long documentoSaidaId
    ) {
        return MovimentoSaidaDto.converter(service.listar(numero, documentoSaidaId));
    }

    @GetMapping("/contagem")
    public List<StatusCountDto> listarContagem() {
        return StatusCountDto.converter(service.listarContagem());
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> criar(@RequestBody @Valid DocumentoSaidaForm form, UriComponentsBuilder uriBuilder) {
        MessageDto msgDto = service.isValido(form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            DocumentoSaidaDetalheDto dto = service.criar(form);

            URI uri = uriBuilder.path("/documento-saida/{id}").buildAndExpand(dto.getId()).toUri();
            return ResponseEntity.created(uri).body(dto);
        }
    }

    @PostMapping("/movimento")
    @Transactional
    public ResponseEntity<?> criar(@RequestBody @Valid MovimentoSaidaForm form, UriComponentsBuilder uriBuilder) {
        MovimentoSaidaDto dto = service.criar(form);

        URI uri = uriBuilder.path("/documento-saida-conferencia/{documentoSaidaId/{numero}").buildAndExpand(
                dto.getItemEntrada().getDocumentoSaidaId(),
                dto.getItemEntrada().getNumero()).toUri();
        return ResponseEntity.created(uri).body(dto);

    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<?> atualizar(@PathVariable Long id, @RequestBody @Valid DocumentoSaidaForm form) {
        MessageDto msgDto = service.isValido(form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            DocumentoSaidaDetalheDto dto = service.atualizar(id, form);
            if (dto != null) {
                return ResponseEntity.ok(dto);
            }
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<DocumentoSaida> optional = service.remover(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}
