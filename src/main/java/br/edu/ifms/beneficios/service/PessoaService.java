/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.config.validacao.ErroDeFormularioDto;
import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDetalheDto;
import br.edu.ifms.beneficios.controller.form.PessoaForm;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.repository.ArquivoRepository;
import br.edu.ifms.beneficios.repository.AuxilioRepository;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.CepRepository;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.CondicaoMoradiaRepository;
import br.edu.ifms.beneficios.repository.CondicaoTrabalhoRepository;
import br.edu.ifms.beneficios.repository.ContatoPessoaRepository;
import br.edu.ifms.beneficios.repository.DocumentoPessoaRepository;
import br.edu.ifms.beneficios.repository.DocumentoRepository;
import br.edu.ifms.beneficios.repository.EnderecoRepository;
import br.edu.ifms.beneficios.repository.EscolaridadeRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.MoradiaRepository;
import br.edu.ifms.beneficios.repository.OrgaoExpedidorRepository;
import br.edu.ifms.beneficios.repository.PessoaRepository;
import br.edu.ifms.beneficios.repository.ProgramaGovernoRepository;
import br.edu.ifms.beneficios.repository.TipoMoradiaRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import br.edu.ifms.beneficios.repository.RendimentoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import br.edu.ifms.beneficios.repository.ContatoRepository;

/**
 *
 * @author santos
 */
@Service
public class PessoaService {

    @Autowired
    private PessoaRepository repository;

    @Autowired
    private RendimentoRepository rendimentoRepository;

    @Autowired
    private CondicaoTrabalhoRepository condicaoTrabalhoRepository;

    @Autowired
    private EscolaridadeRepository escolaridadeRepository;

    @Autowired
    private ContatoPessoaRepository contatoPessoaRepository;

    @Autowired
    private DocumentoPessoaRepository documentoPessoaRepository;

    @Autowired
    private ContatoRepository tipoContatoRepository;

    @Autowired
    private MoradiaRepository moradiaRepository;

    @Autowired
    private TipoMoradiaRepository tipoMoradiaRepository;

    @Autowired
    private CondicaoMoradiaRepository condicaoMoradiaRepository;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private CepRepository cepRepository;

    @Autowired
    private CidadeRepository cidadeRepository;

    @Autowired
    private BairroRepository bairroRepository;

    @Autowired
    private LogradouroRepository logradouroRepository;

    @Autowired
    private DocumentoRepository documentoRepository;

    @Autowired
    private OrgaoExpedidorRepository orgaoExpedidorRepository;

    @Autowired
    private ProgramaGovernoRepository programaGovernoRepository;

    @Autowired
    private AuxilioRepository auxilioRepository;

    @Autowired
    private ArquivoRepository arquivoRepository;

    @Autowired
    private FileStorageService fileStorageService;

    public Page<Pessoa> listar(String nome, String documento, Pageable paginacao) {
        if (nome != null) {
            return repository.findByNomeContainingIgnoreCase(nome, paginacao);
        }
        if (documento != null) {
            return repository.findDistinctByDocumentos_IdNumeroContaining(documento, paginacao);
        }
        return repository.findAll(paginacao);
    }

    public List<Pessoa> listar() {
        return repository.findAll();
    }

    public Optional<Pessoa> buscarPor(Long id) {
        return repository.findById(id);
    }

    public PessoaDetalheDto criar(MultipartFile file, PessoaForm form) {
        Pessoa pessoa = form.converter(file, repository,
                condicaoTrabalhoRepository, escolaridadeRepository,
                tipoContatoRepository, tipoMoradiaRepository,
                condicaoMoradiaRepository, enderecoRepository, cepRepository,
                cidadeRepository, bairroRepository, logradouroRepository,
                documentoRepository, orgaoExpedidorRepository,
                programaGovernoRepository, fileStorageService);

        pessoa = repository.save(pessoa);

        return new PessoaDetalheDto(pessoa);
    }

    public PessoaDetalheDto atualizar(Long id, MultipartFile file, PessoaForm form) {
        Optional<Pessoa> optional = buscarPor(id);
        if (optional.isPresent()) {
            Pessoa pessoa = form.atualizar(id, file, repository,
                    condicaoTrabalhoRepository, escolaridadeRepository,
                    tipoContatoRepository, tipoMoradiaRepository,
                    condicaoMoradiaRepository, enderecoRepository, cepRepository,
                    cidadeRepository, bairroRepository, logradouroRepository,
                    documentoRepository, orgaoExpedidorRepository,
                    programaGovernoRepository, fileStorageService);

            return new PessoaDetalheDto(pessoa);
        }

        return null;
    }

    public Optional<Pessoa> remover(Long id) {
        Optional<Pessoa> optional = buscarPor(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        }

        return optional;
    }

    public MessageDto isValido(MultipartFile file, PessoaForm form) {
        return isValido(file, null, form);
    }

    public MessageDto isValido(MultipartFile file, Long id, PessoaForm form) {
        ErroDeFormularioDto message = form.isValido(id, 
                documentoPessoaRepository, 
                moradiaRepository, 
                enderecoRepository, 
                cepRepository, 
                cidadeRepository, 
                bairroRepository, 
                logradouroRepository);
        if (message.hasErro()) {
            return new MessageDto(HttpStatus.BAD_REQUEST.value(), message.getErro());
        }

        // verifica se já existe alguém morando no mesmo endereço
        return null;
    }

}
