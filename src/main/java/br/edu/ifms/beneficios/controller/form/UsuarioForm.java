/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.ListaPerfilDto;
import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDto;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.FuncionarioRepository;
import br.edu.ifms.beneficios.repository.PerfilRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class UsuarioForm {

    @NotNull
    @NotEmpty
    @Length(min = 3)
    private String nome;
    @NotNull
    @NotEmpty
    @Email
    private String email;
    private Status status;
    private boolean enabled;
    
    @NotNull
    private FuncionarioDto funcionario;
    
    private List<ListaPerfilDto> perfis = new ArrayList();

    public UsuarioForm(){
        this.status = Status.ATIVO;
        this.enabled = true;
    }
    
    public UsuarioForm(Usuario obj) {
        this.nome = obj.getNome();
        this.email = obj.getEmail();
        this.status = obj.getStatus();
        this.enabled = obj.isEnabled();
        this.funcionario = new FuncionarioDto(obj.getFuncionario());
        this.perfis = ListaPerfilDto.converter(obj.getPerfis());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public FuncionarioDto getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(FuncionarioDto funcionario) {
        this.funcionario = funcionario;
    }

    public List<ListaPerfilDto> getPerfis() {
        return perfis;
    }

    public void setPerfis(List<ListaPerfilDto> perfis) {
        this.perfis = perfis;
    }
    
    private Usuario toUsuario(Usuario usuario, PerfilRepository perfilRepository,
            FuncionarioRepository funcionarioRepository) {
         usuario.setNome(this.nome);
        usuario.setEmail(email);
    	usuario.setStatus(status);
        usuario.setEnabled(enabled);
        usuario.setPerfis(perfis.stream()
                .map(p -> perfilRepository.getOne(p.getId()))
                .collect(Collectors.toList()));
        usuario.setFuncionario(funcionarioRepository.getOne(funcionario.getId()));

        return usuario;
    }
    
    public Usuario converter(PerfilRepository perfilRepository,
            FuncionarioRepository funcionarioRepository) {
        return toUsuario(new Usuario(), perfilRepository, funcionarioRepository);
    }
    
    public Usuario atualizar(Long id, UsuarioRepository repository, PerfilRepository perfilRepository,
            FuncionarioRepository funcionarioRepository) {
        return toUsuario(repository.getOne(id), perfilRepository, funcionarioRepository);
    }
}
