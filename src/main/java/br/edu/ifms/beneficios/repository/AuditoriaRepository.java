package br.edu.ifms.beneficios.repository;


import br.edu.ifms.beneficios.modelo.Auditoria;
import org.springframework.data.jpa.repository.JpaRepository;



public interface AuditoriaRepository  extends JpaRepository<Auditoria, Long> {
    
}
