/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.Prontuario;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class DependenteId implements Serializable {
    
    @ManyToOne
    private Pessoa pessoa;
    
    @ManyToOne
    private Prontuario prontuario;

    public DependenteId(Pessoa pessoa, Prontuario prontuario) {
        this.pessoa = pessoa;
        this.prontuario = prontuario;
    }

    public DependenteId() {
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Prontuario getProntuario() {
        return prontuario;
    }

    public void setProntuario(Prontuario prontuario) {
        this.prontuario = prontuario;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.pessoa);
        hash = 97 * hash + Objects.hashCode(this.prontuario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final DependenteId other = (DependenteId) obj;
        if (!Objects.equals(this.pessoa, other.getPessoa())) {
            return false;
        }
        if (!Objects.equals(this.prontuario, other.getProntuario())) {
            return false;
        }
        return true;
    }
    
}
