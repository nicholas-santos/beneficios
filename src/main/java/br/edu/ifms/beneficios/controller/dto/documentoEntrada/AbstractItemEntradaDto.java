/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoEntrada;

import br.edu.ifms.beneficios.controller.dto.beneficioEventual.BeneficioEventualDetalheDto;
import br.edu.ifms.beneficios.modelo.ItemEntrada;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.math.BigDecimal;

/**
 *
 * @author santos
 */
public class AbstractItemEntradaDto {
    private Long numero;
    private Long documentoEntradaId;
    private BigDecimal quantidade;
    private BeneficioEventualDetalheDto beneficioEventual;
    private Status status;
    private Boolean deleted;
    private DocumentoEntradaDto documentoEntrada;

    public AbstractItemEntradaDto() {
        this.deleted = Boolean.FALSE;
        this.quantidade = BigDecimal.ZERO;
    }

    public AbstractItemEntradaDto(ItemEntrada obj) {
        this();
        this.documentoEntradaId = obj.getId().getDocumentoEntrada().getId();
        this.numero = obj.getId().getNumero();
        this.quantidade = obj.getQuantidade();
        this.beneficioEventual = new BeneficioEventualDetalheDto(obj.getBeneficioEventual());
        this.status = obj.getStatus();
        this.deleted = obj.isDeleted();
        this.documentoEntrada = new DocumentoEntradaDto(obj.getId().getDocumentoEntrada());
    }

    public Long getNumero() {
        return numero;
    }

    public Long getDocumentoEntradaId() {
        return documentoEntradaId;
    }

    public DocumentoEntradaDto getDocumentoEntrada() {
        return documentoEntrada;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public BeneficioEventualDetalheDto getBeneficioEventual() {
        return beneficioEventual;
    }

    public Status getStatus() {
        return status;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    
    public Boolean hasPositiveValue() {
        return this.quantidade.compareTo(BigDecimal.ZERO) > 0;
    }
    
    public Boolean isSetNumero() {
        return this.numero != null;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    
}
