/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.BairroDto;
import br.edu.ifms.beneficios.controller.form.BairroForm;
import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.repository.BairroRepository;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/bairros")
@Profile(value = {"prod", "test"})
public class BairroController {

    @Autowired
    private BairroRepository repository;

    @GetMapping
    @Cacheable(value = "pageBairros")
    public Page<BairroDto> listar(
            @RequestParam(required = false) String nome,
            @PageableDefault(sort = "nome", direction = Sort.Direction.ASC,
                    page = 0, size = 10) Pageable paginacao) {

        Page<Bairro> items;
        if (nome != null && !nome.isBlank() && !nome.isEmpty()) {
            items = repository.findByNomeContainingIgnoreCase(nome, paginacao);
        } else {
            items = repository.findAll(paginacao);
        }
        return BairroDto.converter(items);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaDeBairros")
    public List<BairroDto> listar() {

        List<Bairro> items = repository.findAll();
        return BairroDto.converter(items);
    }

    /**
     * Visualizar cidade.Este método é responsável por retornar um objeto da
     * classe <code>Bairro</code>
     *
     *
     * @param id Número de identificação da cidade.
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<BairroDto> visualizar(@PathVariable Long id) {

        Optional<Bairro> bairro = repository.findById(id);
        if (bairro.isPresent()) {
            return ResponseEntity.ok(new BairroDto(bairro.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageBairros", "listaDeBairros"}, allEntries = true)
    public ResponseEntity<BairroDto> cadastrar(@RequestBody @Valid BairroForm form,
            UriComponentsBuilder uriBuilder) {
        Bairro bairro = form.converter(repository);
        repository.save(bairro);

        URI uri = uriBuilder.path("/bairros/{id}")
                .buildAndExpand(bairro.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new BairroDto(bairro));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageBairros", "listaDeBairros"}, allEntries = true)
    public ResponseEntity<BairroDto> atualizar(
            @PathVariable Long id, @RequestBody @Valid BairroForm form) {
        Optional<Bairro> optional = repository.findById(id);
        if (optional.isPresent()) {
            Bairro bairro = form.atualizar(id, repository);
            return ResponseEntity.ok(new BairroDto(bairro));
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageBairros", "listaDeBairros"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Bairro> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}
