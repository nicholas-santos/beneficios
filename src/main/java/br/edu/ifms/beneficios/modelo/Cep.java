/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.CepId;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 *
 * @author santos
 */
@Entity
public class Cep implements Serializable {
    
    @EmbeddedId
    private CepId id;

    public Cep() {
    }

    public Cep(CepId id) {
        this.id = id;
    }

    public CepId getId() {
        return id;
    }

    public void setId(CepId id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Cep other = (Cep) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cep{" + "id=" + id + '}';
    }
    
}
