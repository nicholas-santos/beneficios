/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDetalheDto;
import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDto;
import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.form.FuncionarioForm;
import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.repository.FuncaoRepository;
import br.edu.ifms.beneficios.repository.FuncionarioRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/funcionarios")
public class FuncionarioController {

    @Autowired
    private FuncionarioRepository repository;

    @Autowired
    private FuncaoRepository funcaoRepository;

    @Autowired
    private UnidadeAtendimentoRepository unidadeAtendimentoRepository;

    @GetMapping
    @Cacheable(value = "pageFuncionario")
    public Page<FuncionarioDetalheDto> listar(
            @RequestParam(required = false) String nome,
            @RequestParam(required = false) Long unidadeAtendimentoId,
            @PageableDefault(sort = "nome", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {
        Page<Funcionario> funcionarios;
        if (nome != null && !nome.isBlank() && !nome.isEmpty()
                && unidadeAtendimentoId != null) {
            funcionarios = repository.findByNomeContainingIgnoreCaseAndUnidadeAtendimento_Id(nome, unidadeAtendimentoId, paginacao);
        } else if (nome != null && !nome.isBlank() && !nome.isEmpty()) {
            funcionarios = repository.findByNomeContainingIgnoreCase(nome, paginacao);
        } else if (unidadeAtendimentoId != null) {
            funcionarios = repository.findByUnidadeAtendimentoId(unidadeAtendimentoId, paginacao);
        } else {
            funcionarios = repository.findAll(paginacao);
        }
        return FuncionarioDetalheDto.converter(funcionarios);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaFuncionario")
    public List<FuncionarioDto> listar() {

        List<Funcionario> items;
        items = repository.findAll();

        return FuncionarioDto.converter(items);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FuncionarioDetalheDto> visualizar(@PathVariable Long id) {
        Optional<Funcionario> funcionario = repository.findById(id);
        if (funcionario.isPresent()) {
            return ResponseEntity.ok(new FuncionarioDetalheDto(funcionario.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageFuncionario", "listaFuncionario"}, allEntries = true)
    public ResponseEntity<FuncionarioDetalheDto> criar(@RequestBody @Valid FuncionarioForm form, UriComponentsBuilder uriBuilder) {
        Funcionario funcionario = form.converter(repository, funcaoRepository,
                unidadeAtendimentoRepository);
        repository.save(funcionario);

        URI uri = uriBuilder.path("/funcionarios/{id}").buildAndExpand(funcionario.getId()).toUri();
        return ResponseEntity.created(uri).body(new FuncionarioDetalheDto(funcionario));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageFuncionario", "listaFuncionario"}, allEntries = true)
    public ResponseEntity<FuncionarioDetalheDto> atualizar(@PathVariable Long id, @RequestBody @Valid FuncionarioForm form) {
        Optional<Funcionario> optional = repository.findById(id);
        if (optional.isPresent()) {
            Funcionario funcionario = form.atualizar(id, repository, funcaoRepository,
                    unidadeAtendimentoRepository);
            return ResponseEntity.ok(new FuncionarioDetalheDto(funcionario));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageFuncionario", "listaFuncionario"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Funcionario> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
