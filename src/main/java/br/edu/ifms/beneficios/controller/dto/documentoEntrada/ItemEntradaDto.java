/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoEntrada;

import br.edu.ifms.beneficios.modelo.ItemEntrada;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class ItemEntradaDto extends AbstractItemEntradaDto {

    public ItemEntradaDto() {
    }

    public ItemEntradaDto(ItemEntrada obj) {
        super(obj);
    }

    public static Page<ItemEntradaDto> converter(Page<ItemEntrada> items) {
        return items.map(ItemEntradaDto::new);
    }

    public static List<ItemEntradaDto> converter(List<ItemEntrada> items) {
        return items.stream().map(ItemEntradaDto::new).collect(Collectors.toList());
    }
    
}
