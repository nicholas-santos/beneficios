/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.DocumentoPessoaId;
import br.edu.ifms.beneficios.modelo.tipos.LocalDateAttributeConverter;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author santos
 */
@Entity
public class DocumentoPessoa implements Serializable {
    
    @EmbeddedId
    private DocumentoPessoaId id;
    
    @ManyToOne
    private OrgaoExpedidor orgaoExpedidor;
    
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate emissao;
    
    @Transient
    private Boolean deleted;

    public DocumentoPessoa() {
        deleted = Boolean.FALSE;
    }

    public DocumentoPessoa(DocumentoPessoaId id, OrgaoExpedidor orgaoExpedidor, LocalDate emissao) {
        this();
        this.id = id;
        this.orgaoExpedidor = orgaoExpedidor;
        this.emissao = emissao;
    }

    public DocumentoPessoaId getId() {
        return id;
    }

    public void setId(DocumentoPessoaId id) {
        this.id = id;
    }

    public OrgaoExpedidor getOrgaoExpedidor() {
        return orgaoExpedidor;
    }

    public void setOrgaoExpedidor(OrgaoExpedidor orgaoExpedidor) {
        this.orgaoExpedidor = orgaoExpedidor;
    }

    public LocalDate getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDate emissao) {
        this.emissao = emissao;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        hash = 23 * hash + Objects.hashCode(this.orgaoExpedidor);
        hash = 23 * hash + Objects.hashCode(this.emissao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final DocumentoPessoa other = (DocumentoPessoa) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.orgaoExpedidor, other.getOrgaoExpedidor())) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        return true;
    }
    
}
