/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.Menu;
import br.edu.ifms.beneficios.modelo.Perfil;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class PerfilMenuId implements Serializable {
    
    @ManyToOne
    private Perfil perfil;
    
    @ManyToOne
    private Menu menu;

    public PerfilMenuId() {
    }

    public PerfilMenuId(Perfil perfil, Menu menu) {
        this.perfil = perfil;
        this.menu = menu;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.perfil);
        hash = 59 * hash + Objects.hashCode(this.menu);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final PerfilMenuId other = (PerfilMenuId) obj;
        if (!Objects.equals(this.perfil, other.getPerfil())) {
            return false;
        }
        if (!Objects.equals(this.menu, other.getMenu())) {
            return false;
        }
        return true;
    }
    
    
}
