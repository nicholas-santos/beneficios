/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.Escolaridade;
import br.edu.ifms.beneficios.repository.EscolaridadeRepository;

/**
 *
 * @author tanabe
 */
public class EscolaridadeForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Escolaridade converter() {
        return new Escolaridade(this.descricao);
    }

    public Escolaridade atualizar(Long id, EscolaridadeRepository repository) {
        Escolaridade escolaridade = repository.getOne(id);
        escolaridade.setDescricao(this.descricao);

        return escolaridade;
    }
}
