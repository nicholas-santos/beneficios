/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import br.edu.ifms.beneficios.repository.BeneficioEventualRepository;

/**
 *
 * @author santos
 */
public class BeneficioEventualForm {
    
    @NotNull @NotEmpty @Length(min = 5)
    private String descricao;
    private Boolean outraConcessao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getOutraConcessao() {
        return outraConcessao;
    }

    public void setOutraConcessao(Boolean outraConcessao) {
        this.outraConcessao = outraConcessao;
    }
    
    public BeneficioEventual converter(BeneficioEventualRepository repository) {
        Long maxId = repository.getMaxId() + 1;
        return new BeneficioEventual(maxId, this.descricao, this.outraConcessao);
    }
    
    public BeneficioEventual atualizar(Long id, BeneficioEventualRepository repository) {
        BeneficioEventual beneficio = repository.getById(id);
        beneficio.setDescricao(this.descricao);
        beneficio.setOutraConcessao(this.outraConcessao);
        
        return beneficio;
    }
}
