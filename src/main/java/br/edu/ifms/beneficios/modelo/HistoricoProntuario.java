/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
public class HistoricoProntuario extends AbstractHistorico {

    @ManyToOne
    private Prontuario prontuario;
    private String observacao;

    public HistoricoProntuario() {
        super();
    }
    
    public HistoricoProntuario(Long id, Prontuario prontuario, Funcionario funcionario) {
        super.setId(id);
        super.setStatus(prontuario.getStatus());
        super.setFuncionario(funcionario);
        
        this.prontuario = prontuario;
        this.observacao = prontuario.getObservacao();
    }

    public Prontuario getProntuario() {
        return prontuario;
    }

    public void setProntuario(Prontuario prontuario) {
        this.prontuario = prontuario;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.prontuario);
        hash = 59 * hash + Objects.hashCode(this.observacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final HistoricoProntuario other = (HistoricoProntuario) obj;
        if (!Objects.equals(this.observacao, other.getObservacao())) {
            return false;
        }
        if (!Objects.equals(this.prontuario.getId(), other.getProntuario().getId())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HistoricoProntuario{" + super.toString() + ", prontuario=" + prontuario + ", observacao=" + observacao + '}';
    }
}
