/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.documentoSaida.ItemSaidaDetalheDto;
import br.edu.ifms.beneficios.modelo.ItemSaida;
import br.edu.ifms.beneficios.modelo.MovimentoSaida;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.ItemSaidaRepository;
import java.math.BigDecimal;
import java.util.Optional;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author santos
 */
public class MovimentoSaidaForm {

    @NotNull
    private ItemSaidaDetalheDto itemSaida;

    @NotNull
    @Min(1)
    private BigDecimal quantidade;

    public ItemSaidaDetalheDto getItemSaida() {
        return itemSaida;
    }

    public void setItemSaida(ItemSaidaDetalheDto itemSaida) {
        this.itemSaida = itemSaida;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }
    
    public MovimentoSaida converter(ItemSaidaRepository itemSaidaRepository) {
        Optional<ItemSaida> opItem = itemSaidaRepository.findByIdDocumentoSaidaIdAndIdNumero(
                this.itemSaida.getDocumentoSaidaId(),
                this.itemSaida.getNumero()
        );
        ItemSaida item = opItem.get();
        if (item.getSaldo().compareTo(quantidade) < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                "Quantidade solicitada é maior do que o saldo disponível");
        }
        if (item.getSaldo().subtract(quantidade).compareTo(BigDecimal.ZERO) <= 0) {
            item.setStatus(Status.ENVIADO);
        } else {
            item.setStatus(Status.PARCIAL);
        }
        
        MovimentoSaida movimentoSaida = new MovimentoSaida();
        movimentoSaida.setUnidadeAtendimento(item.getId().getDocumentoSaida().getUnidadeAtendimento());
        movimentoSaida.setItemSaida(item);
        movimentoSaida.setQuantidade(quantidade);
        return movimentoSaida;
    }
    
}
