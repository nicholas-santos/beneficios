/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Contato;
import br.edu.ifms.beneficios.modelo.tipos.TipoContato;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class ContatoDto {
    
    private Long id;
    private String descricao;
    private TipoContato tipoContato;

    public ContatoDto() {
    }

    public ContatoDto(Contato obj) {
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
        this.tipoContato = obj.getTipoContato();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }
    
    public static Page<ContatoDto> converter(Page<Contato> items) {
        return items.map(ContatoDto::new);
    }
    
    public static List<ContatoDto> converter(List<Contato> items) {
        return items.stream().map(ContatoDto::new).collect(Collectors.toList());
    }
    
}
