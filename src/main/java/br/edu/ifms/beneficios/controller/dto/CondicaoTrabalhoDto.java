/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.CondicaoTrabalho;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class CondicaoTrabalhoDto {
    
    private Long id;
    private String descricao;

    public CondicaoTrabalhoDto() {
    }

    public CondicaoTrabalhoDto(CondicaoTrabalho obj) {
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public static Page<CondicaoTrabalhoDto> converter(Page<CondicaoTrabalho> items) {
        return items.map(CondicaoTrabalhoDto::new);
    }
    
    public static List<CondicaoTrabalhoDto> converter(List<CondicaoTrabalho> items) {
        return items.stream().map(CondicaoTrabalhoDto::new).collect(Collectors.toList());
    }
    
}
