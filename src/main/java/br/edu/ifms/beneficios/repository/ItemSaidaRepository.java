/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.ItemSaida;
import br.edu.ifms.beneficios.modelo.id.ItemSaidaId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface ItemSaidaRepository extends JpaRepository<ItemSaida, ItemSaidaId> {

    Page<ItemSaida> findByStatus(Status status, Pageable paginacao);
    Page<ItemSaida> findByIdDocumentoSaidaUnidadeAtendimentoId(Long unidadeAtendimentoId, Pageable paginacao);
    Page<ItemSaida> findByStatusAndIdDocumentoSaidaUnidadeAtendimentoId(Status status, Long unidadeAtendimentoId, Pageable paginacao);
    
    Optional<ItemSaida> findByIdDocumentoSaidaIdAndIdNumero(Long documentoSaidaId, Long numero);
}
