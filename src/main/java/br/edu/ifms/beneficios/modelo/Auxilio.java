/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.AuxilioId;
import br.edu.ifms.beneficios.modelo.tipos.LocalDateAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author santos
 */
//@EntityListeners(AuditoriaListener.class)
@Entity
public class Auxilio implements Serializable {

    @EmbeddedId
    private AuxilioId id;

    @ManyToOne
    private ProgramaGoverno programaGoverno;

    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate dataRegistro;

    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate dataFim;

    private BigDecimal valor;
    /**
     * Atributo utilizado para registrar status
     *
     * A situação pode ser:<br/>
     * <ul>
     * <li>ATIVO</li>
     * <li>INATIVO</li>
     * </ul>
     */
    @Column(name = "status", nullable = true)
    @Convert(converter = StatusConverter.class)
    private Status status;
    
    @Transient
    private Boolean deleted;

    public Auxilio() {
        this.status = Status.ATIVO;
        deleted = Boolean.FALSE;
    }

    public Auxilio(AuxilioId id, ProgramaGoverno programaGoverno, 
            LocalDate dataRegistro, LocalDate dataFim, BigDecimal valor, 
            Status status) {
        this();
        this.id = id;
        this.programaGoverno = programaGoverno;
        this.dataRegistro = dataRegistro;
        this.dataFim = dataFim;
        this.valor = valor;
        this.status = status;
    }
    
    public void update(Auxilio obj) {
        this.programaGoverno = obj.getProgramaGoverno();
        this.dataRegistro = obj.getDataRegistro();
        this.dataFim = obj.getDataFim();
        this.valor = obj.getValor();
        this.status = obj.getStatus();
    }

    public AuxilioId getId() {
        return id;
    }

    public void setId(AuxilioId id) {
        this.id = id;
    }

    public ProgramaGoverno getProgramaGoverno() {
        return programaGoverno;
    }

    public void setProgramaGoverno(ProgramaGoverno programaGoverno) {
        this.programaGoverno = programaGoverno;
    }

    public LocalDate getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(LocalDate dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public void setDataFim(LocalDate dataFim) {
        this.dataFim = dataFim;
    }

    public BigDecimal getValor() {
        if (Status.ATIVO.equals(this.status)) {
            return valor;
        }
        return BigDecimal.ZERO;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.id);
        hash = 73 * hash + Objects.hashCode(this.programaGoverno);
        hash = 73 * hash + Objects.hashCode(this.dataRegistro);
        hash = 73 * hash + Objects.hashCode(this.dataFim);
        hash = 73 * hash + Objects.hashCode(this.valor);
        hash = 73 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Auxilio other = (Auxilio) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

}
