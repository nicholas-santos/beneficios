/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


import br.edu.ifms.beneficios.modelo.Documento;
import br.edu.ifms.beneficios.repository.DocumentoRepository;

/**
 *
 * @author tanabe
 */
public class DocumentoForm {

    @NotNull
    @NotEmpty
    private String descricao;
    private Boolean exigeOrgaoExpedidor;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getExigeOrgaoExpedidor() {
        return exigeOrgaoExpedidor;
    }

    public void setExigeOrgaoExpedidor(Boolean exigeOrgaoExpedidor) {
        this.exigeOrgaoExpedidor = exigeOrgaoExpedidor;
    }

    public Documento converter() {
        return new Documento(null, this.descricao, this.exigeOrgaoExpedidor);
    }

    public Documento atualizar(Long id, DocumentoRepository repository) {
    	Documento documento = repository.getOne(id);
    	documento.setDescricao(this.descricao);
        documento.setExigeOrgaoExpeditor(this.exigeOrgaoExpedidor);

        return documento;
    }
}
