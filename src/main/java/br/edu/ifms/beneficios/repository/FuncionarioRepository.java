/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.Pessoa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 *
 * @author tanabe
 */
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {
    
    @Query("SELECT coalesce(max(f.id), 0) FROM Funcionario f")
    Long getMaxId();
    
    Page<Funcionario> findByNomeContainingIgnoreCase(String nome, Pageable paginacao);
    Page<Funcionario> findByNomeContainingIgnoreCaseAndUnidadeAtendimento_Id(String nome, Long unidadeAtendimentoId, Pageable paginacao);
    Page<Funcionario> findByUnidadeAtendimentoId(Long unidadeAtendimentoId, Pageable paginacao);
}
