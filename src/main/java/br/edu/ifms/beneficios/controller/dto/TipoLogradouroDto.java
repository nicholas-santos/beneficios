/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.TipoLogradouro;
import br.edu.ifms.beneficios.modelo.Uf;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class TipoLogradouroDto {
    private Long id;
    private String nome;

    public TipoLogradouroDto(TipoLogradouro obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
    
    public static Page<TipoLogradouroDto> converter(Page<TipoLogradouro> items) {
        return items.map(TipoLogradouroDto::new);
    }
    
    public static List<TipoLogradouroDto> converter(List<TipoLogradouro> items) {
        return items.stream().map(TipoLogradouroDto::new).collect(Collectors.toList());
    }
}
