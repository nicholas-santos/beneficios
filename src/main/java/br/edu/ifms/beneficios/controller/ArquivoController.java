/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.UploadFileResponse;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import br.edu.ifms.beneficios.service.FileStorageService;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/arquivos")
@Profile(value = {"prod", "test"})
public class ArquivoController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping(value = "/upload-file")
    public UploadFileResponse uploadFile(
            @RequestParam("file") MultipartFile file,
            @RequestParam("userId") Long userId,
            @RequestParam("docType") String docType
    ) {
        try {
            Optional<Usuario> optional = usuarioRepository.findById(userId);
            String fileName = fileStorageService
                    .storeFile(file, optional.get(), docType);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/downloadFile/")
                    .path(fileName)
                    .toUriString();
            return new UploadFileResponse(fileName, fileDownloadUri,
                    file.getContentType(), file.getSize(), file.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(ArquivoController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return null;
    }

//    @PostMapping("/uploadMultipleFiles")
//    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//        return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
//    }
    @GetMapping("/download-file")
    public ResponseEntity<Resource> downloadFile(
            @RequestParam("userId") Long userId,
            @RequestParam("docType") String docType,
            HttpServletRequest request) {
        Optional<Usuario> optional = usuarioRepository.findById(userId);
        String fileName = fileStorageService.getDocumentName(optional.get(), docType);

        Resource resource = null;
        if (fileName != null && !fileName.isEmpty()) {
            try {
                resource = fileStorageService.loadFileAsResource(fileName);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Try to determine file's content type
            String contentType = null;
            try {
                contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            } catch (IOException ex) {
                //logger.info("Could not determine file type.");
            }

            // Fallback to the default content type if type could not be determined
            if (contentType == null) {
                contentType = "application/octet-stream";
            }
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
