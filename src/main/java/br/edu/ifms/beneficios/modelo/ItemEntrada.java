/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.ItemEntradaId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author santos
 */
//@EntityListeners(AuditoriaListener.class)
@Entity
public class ItemEntrada implements Serializable {

    @EmbeddedId
    private ItemEntradaId id;
    private BigDecimal quantidade;

    @ManyToOne
    private BeneficioEventual beneficioEventual;

    @Column(name = "status", nullable = false)
    @Convert(converter = StatusConverter.class)
    private Status status;

    @Transient
    private Boolean deleted;

    @OneToMany(mappedBy = "itemEntrada")
    private List<MovimentoEntrada> movimentos = new ArrayList();

    public ItemEntrada() {
        this.status = Status.PENDENTE;
        this.deleted = Boolean.FALSE;
    }

    public ItemEntrada(ItemEntradaId id, BigDecimal quantidade, BeneficioEventual beneficioEventual) {
        this();
        this.id = id;
        this.quantidade = quantidade;
        this.beneficioEventual = beneficioEventual;
    }

    public ItemEntradaId getId() {
        return id;
    }

    public void setId(ItemEntradaId id) {
        this.id = id;
    }

    public Boolean isSetNumero() {
        return id.getNumero() != null;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BeneficioEventual getBeneficioEventual() {
        return beneficioEventual;
    }

    public void setBeneficioEventual(BeneficioEventual beneficioEventual) {
        this.beneficioEventual = beneficioEventual;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
        this.movimentos.clear();
    }

    public List<MovimentoEntrada> getMovimentos() {
        return movimentos;
    }

    public void setMovimentos(List<MovimentoEntrada> movimentos) {
        this.movimentos = movimentos;
    }

    public void add(MovimentoEntrada movimento) {
        this.movimentos.add(movimento);
    }

    public void remove(MovimentoEntrada movimento) {
        this.movimentos.remove(movimento);
        movimento.setItemEntrada(null);
    }
    
    public BigDecimal getQuantidadeMovimentada() {
        if (this.getMovimentos().isEmpty()) {
            return BigDecimal.ZERO;
        }
        return this.getMovimentos().stream()
                .map(m -> m.getQuantidade())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    
    public BigDecimal getSaldo() {
        return this.quantidade.subtract(getQuantidadeMovimentada());
    }
    
    public Boolean hasSaldo() {
        return this.getSaldo().compareTo(BigDecimal.ZERO) > 0;
    }
    
    public Boolean isRecebido() {
        return Status.RECEBIDO.equals(this.status);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.quantidade);
        hash = 59 * hash + Objects.hashCode(this.beneficioEventual);
        hash = 59 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ItemEntrada other = (ItemEntrada) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.quantidade, other.getQuantidade())) {
            return false;
        }
        if (!Objects.equals(this.beneficioEventual, other.getBeneficioEventual())) {
            return false;
        }
        if (!Objects.equals(this.status, other.getStatus())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItemEntrada{" + "id=" + id + ", quantidade=" + quantidade + ", beneficioEventual=" + beneficioEventual + ", status=" + status + ", deleted=" + deleted + '}';
    }

}
