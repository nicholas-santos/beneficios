/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.PerfilMenuId;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 *
 * @author tanabe
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class PerfilMenu implements Serializable {
    @EmbeddedId
    private PerfilMenuId id;
    
    @Column(columnDefinition = "boolean default true")
    private Boolean ler;
    
    @Column(columnDefinition = "boolean default true")
    private Boolean escrever;
    
    @Column(columnDefinition = "boolean default true")
    private Boolean remover;
    
    @Transient
    private Boolean deleted;

    public PerfilMenu() {
        deleted = Boolean.FALSE;
    }

    public PerfilMenu(PerfilMenuId id) {
        this();
        this.id = id;
    }

    public PerfilMenu(PerfilMenuId id, Boolean ler, Boolean escrever, Boolean remover) {
        this();
        this.id = id;
        this.ler = ler;
        this.escrever = escrever;
        this.remover = remover;
    }
    
    public void update(PerfilMenu obj) {
        this.escrever = obj.getEscrever();
        this.ler = obj.getLer();
        this.remover = obj.getRemover();
        this.deleted = obj.isDeleted();
    }

    public PerfilMenuId getId() {
        return id;
    }

    public void setId(PerfilMenuId id) {
        this.id = id;
    }

    public Boolean getLer() {
        return ler;
    }

    public void setLer(Boolean ler) {
        this.ler = ler;
    }

    public Boolean getEscrever() {
        return escrever;
    }

    public void setEscrever(Boolean escrever) {
        this.escrever = escrever;
    }

    public Boolean getRemover() {
        return remover;
    }

    public void setRemover(Boolean remover) {
        this.remover = remover;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.ler);
        hash = 59 * hash + Objects.hashCode(this.escrever);
        hash = 59 * hash + Objects.hashCode(this.remover);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final PerfilMenu other = (PerfilMenu) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        
        return true;
    }
    
}
