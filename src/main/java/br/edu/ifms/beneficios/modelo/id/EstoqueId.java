/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class EstoqueId implements Serializable {

    @ManyToOne
    private UnidadeAtendimento unidadeAtendimento;

    @ManyToOne
    private BeneficioEventual beneficioEventual;

    public EstoqueId() {
    }

    public EstoqueId(UnidadeAtendimento unidadeAtendimento, BeneficioEventual beneficioEventual) {
        this.unidadeAtendimento = unidadeAtendimento;
        this.beneficioEventual = beneficioEventual;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public BeneficioEventual getBeneficioEventual() {
        return beneficioEventual;
    }

    public void setBeneficioEventual(BeneficioEventual beneficioEventual) {
        this.beneficioEventual = beneficioEventual;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.unidadeAtendimento);
        hash = 71 * hash + Objects.hashCode(this.beneficioEventual);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final EstoqueId other = (EstoqueId) obj;
        if (!Objects.equals(this.unidadeAtendimento, other.getUnidadeAtendimento())) {
            return false;
        }
        if (!Objects.equals(this.beneficioEventual, other.getBeneficioEventual())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EstoqueId{" + "unidadeAtendimento=" + unidadeAtendimento + ", beneficioEventual=" + beneficioEventual + '}';
    }

}
