/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.Documento;
import br.edu.ifms.beneficios.modelo.AbstractPessoa;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class DocumentoPessoaId implements Serializable {

    @ManyToOne
    private Documento documento;

    @ManyToOne
    @JoinColumn(name = "pessoa_id", referencedColumnName = "id")
    private AbstractPessoa pessoa;
    
    private String numero;

    public DocumentoPessoaId() {
    }

    public DocumentoPessoaId(Documento documento, AbstractPessoa pessoa, String numero) {
        this.documento = documento;
        this.pessoa = pessoa;
        this.numero = numero;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public AbstractPessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(AbstractPessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.documento);
        hash = 41 * hash + Objects.hashCode(this.pessoa);
        hash = 41 * hash + Objects.hashCode(this.numero);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final DocumentoPessoaId other = (DocumentoPessoaId) obj;
        if (!Objects.equals(this.numero, other.getNumero())) {
            return false;
        }
        if (!Objects.equals(this.documento, other.getDocumento())) {
            return false;
        }
        if (!Objects.equals(this.pessoa, other.getPessoa())) {
            return false;
        }
        return true;
    }
    

}
