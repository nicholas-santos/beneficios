package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.springframework.security.core.GrantedAuthority;

//@EntityListeners(AuditoriaListener.class)
@Entity
public class Perfil implements GrantedAuthority {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;
    /**
     * Atributo utilizado para registrar statuso do usuário.
     *
     * A situação do estudante pode ser:<br/>
     * <ul>
     * <li>ATIVO</li>
     * <li>INATIVO</li>
     * <li>BLOQUEADO</li>
     * </ul>
     */
    @Column(name = "status", nullable = true)
    @Convert(converter = StatusConverter.class)
    private Status status;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime created;
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime updated;

    @OneToMany(mappedBy = "id.perfil", fetch = FetchType.LAZY)
    private List<PerfilMenu> menus = new ArrayList();

    @ManyToMany(mappedBy = "perfis", targetEntity = Usuario.class)
    private List<Usuario> usuarios = new ArrayList();

    public Perfil() {
        this(null);
    }

    public Perfil(String nome) {
        this(nome, Status.ATIVO);
    }

    public Perfil(String nome, Status status) {
        this(null, nome, status);
    }

    public Perfil(Long id, String nome, Status status) {
        this.created = LocalDateTime.now();
        this.id = id;
        this.nome = nome;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public List<PerfilMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<PerfilMenu> menus) {
        this.menus = menus;
    }

    public void add(PerfilMenu perfilMenu) {
        if (!this.menus.contains(perfilMenu)) {
            this.menus.add(perfilMenu);
        } else {
            this.menus.stream()
                    .filter(obj -> obj.getId().equals(perfilMenu.getId()))
                    .map((obj) -> {
                        obj.update(perfilMenu);
                        return obj;
                    })
                    .collect(Collectors.toList());
        }
    }
    
    public void clearPerfilMenu() {
        this.menus.clear();
    }

    public void remove(PerfilMenu perfilMenu) {
        this.menus.remove(perfilMenu);
        perfilMenu.getId().setMenu(null);
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.nome);
        hash = 83 * hash + Objects.hashCode(this.status);
        hash = 83 * hash + Objects.hashCode(this.created);
        hash = 83 * hash + Objects.hashCode(this.updated);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Perfil other = (Perfil) obj;
        if (!Objects.equals(this.nome, other.getNome())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        if (!Objects.equals(this.created, other.getCreated())) {
            return false;
        }
        if (!Objects.equals(this.updated, other.getUpdated())) {
            return false;
        }
        return true;
    }

    @Override
    public String getAuthority() {
        return nome;
    }

}
