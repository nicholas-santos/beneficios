/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Type;

/**
 *
 * @author santos
 */
//@EntityListeners(AuditoriaListener.class)
@Entity
public class Prontuario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private UnidadeAtendimento unidadeAtendimento;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime emissao;

    @Lob
    @Type(type = "text")
    private String descricaoSaude;
    private String observacao;

    @Column(name = "status", nullable = true)
    @Convert(converter = StatusConverter.class)
    private Status status;

    @Column(columnDefinition = "boolean default false")
    private Boolean acompanhamento;

    @ManyToOne
    private GrupoSocioeducativo grupoSocioeducativo;

    @ManyToOne(targetEntity = Pessoa.class)
    private Pessoa titular;

    @OneToMany(mappedBy = "id.prontuario",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Dependente> dependentes = new ArrayList();

    @OneToMany(mappedBy = "prontuario",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<HistoricoProntuario> historicos = new ArrayList();

    public Prontuario() {
        this.emissao = LocalDateTime.now();
        this.status = Status.PENDENTE;
    }

    public Prontuario(Long id, UnidadeAtendimento ua) {
        this.id = id;
        this.unidadeAtendimento = ua;
    }

    public Prontuario(Long id, UnidadeAtendimento ua,
            LocalDateTime emissao,
            String descricaoSaude, Status status,
            GrupoSocioeducativo grupoSocioeducativo, Pessoa titular) {
        this.id = id;
        this.unidadeAtendimento = ua;
        this.emissao = emissao;
        this.descricaoSaude = descricaoSaude;
        this.status = status;
        this.grupoSocioeducativo = grupoSocioeducativo;
        this.titular = titular;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDateTime emissao) {
        this.emissao = emissao;
    }

    public String getDescricaoSaude() {
        return descricaoSaude;
    }

    public void setDescricaoSaude(String descricaoSaude) {
        this.descricaoSaude = descricaoSaude;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean isPendente() {
        return Status.PENDENTE.equals(this.status);
    }

    public Boolean isAtivo() {
        return Status.ATIVO.equals(this.status);
    }

    public GrupoSocioeducativo getGrupoSocioeducativo() {
        return grupoSocioeducativo;
    }

    public Boolean getAcompanhamento() {
        return acompanhamento;
    }

    public void setAcompanhamento(Boolean acompanhamento) {
        this.acompanhamento = acompanhamento;
    }

    public void setGrupoSocioeducativo(GrupoSocioeducativo grupoSocioeducativo) {
        this.grupoSocioeducativo = grupoSocioeducativo;
    }

    public Pessoa getTitular() {
        return titular;
    }

    public void setTitular(Pessoa titular) {
        this.titular = titular;
    }

    public List<Dependente> getDependentes() {
        return dependentes;
    }

    public void setDependentes(List<Dependente> dependentes) {
        this.dependentes.clear();
        if (!dependentes.isEmpty()) {
            this.dependentes.addAll(dependentes);
        }
    }

    public void add(Dependente dependente) {
        if (!this.dependentes.contains(dependente)) {
            this.dependentes.add(dependente);
        } else {
            this.dependentes.stream().map((obj) -> {
                obj.setParentesco(dependente.getParentesco());
                return obj;
            });
        }
    }

    public void remove(Dependente dependente) {
        this.dependentes.remove(dependente);
        dependente.getId().setProntuario(null);
    }

    public BigDecimal getValorRendimentos() {
        BigDecimal total = titular.getValorRendimentos();

        total = total.add(dependentes.stream()
                .map(r -> r.getId().getPessoa().getValorRendimentos())
                .reduce(BigDecimal.ZERO, BigDecimal::add));

        return total;
    }

    public BigDecimal getValorAuxilios() {
        BigDecimal total = titular.getValorAuxilios();

        total = total.add(dependentes.stream()
                .map(r -> r.getId().getPessoa().getValorAuxilios())
                .reduce(BigDecimal.ZERO, BigDecimal::add));

        return total;
    }
    
    public void clearDependentes() {
        this.dependentes.clear();
    }

    public void clearHistorico() {
        this.historicos.clear();
    }

    public List<HistoricoProntuario> getHistoricos() {
        return historicos;
    }

    public void setHistoricos(List<HistoricoProntuario> historicos) {
        clearHistorico();
        if (!historicos.isEmpty()) {
            this.historicos.addAll(historicos);
        }
    }

    public void add(HistoricoProntuario historicoProntuario) {
        this.historicos.add(historicoProntuario);
    }

    public void remove(HistoricoProntuario historicoProntuario) {
        this.historicos.remove(historicoProntuario);
        historicoProntuario.setProntuario(null);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        hash = 29 * hash + Objects.hashCode(this.emissao);
        hash = 29 * hash + Objects.hashCode(this.descricaoSaude);
        hash = 29 * hash + Objects.hashCode(this.status);
        hash = 29 * hash + Objects.hashCode(this.grupoSocioeducativo);
        hash = 29 * hash + Objects.hashCode(this.titular);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Prontuario other = (Prontuario) obj;
        if (!Objects.equals(this.descricaoSaude, other.getDescricaoSaude())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        if (!Objects.equals(this.grupoSocioeducativo, other.getGrupoSocioeducativo())) {
            return false;
        }
        if (!Objects.equals(this.titular, other.getTitular())) {
            return false;
        }
        return true;
    }

}
