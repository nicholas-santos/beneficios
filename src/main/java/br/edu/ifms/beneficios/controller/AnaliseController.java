/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.AnaliseDto;
import br.edu.ifms.beneficios.controller.form.AnaliseForm;
import br.edu.ifms.beneficios.modelo.Analise;
import br.edu.ifms.beneficios.service.AnaliseService;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/analises")
@Profile(value = {"prod", "test"})
public class AnaliseController {

    @Autowired
    private AnaliseService service;

    @GetMapping
    public Page<AnaliseDto> listar(
            @PageableDefault(sort = "emissao", direction = Sort.Direction.DESC,
                    page = 0, size = 10) Pageable paginacao) {
        return AnaliseDto.converter(service.listar(paginacao));
    }

    @GetMapping("/list")
    public List<AnaliseDto> listar() {
        return AnaliseDto.converter(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<AnaliseDto> visualizar(@PathVariable Long id) {
        Optional<Analise> analise = service.buscarPor(id);
        if (analise.isPresent()) {
            return ResponseEntity.ok(new AnaliseDto(analise.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> cadastrar(
            @RequestParam(name = "file", required = false) MultipartFile file,
            @RequestPart("form") @Valid AnaliseForm form,
            UriComponentsBuilder uriBuilder) throws IOException {
            AnaliseDto dto = service.criar(file, form);

            URI uri = uriBuilder.path("/analises/{id}")
                    .buildAndExpand(dto.getId())
                    .toUri();
            return ResponseEntity.created(uri).body(dto);
    }
}
