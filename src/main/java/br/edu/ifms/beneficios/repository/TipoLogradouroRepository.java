/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.TipoLogradouro;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface TipoLogradouroRepository extends JpaRepository<TipoLogradouro, Long> {
    
    Optional<TipoLogradouro> findByNomeIgnoreCase(String nome);
    
    @Query("SELECT coalesce(max(tl.id), 0) FROM TipoLogradouro tl")
    Long getMaxId();
    
}
