/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.ParentescoForm;
import br.edu.ifms.beneficios.modelo.Parentesco;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ParentescoControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/parentescos";
    private final String descricao = "Pai";
    private final String outraDescricao = "Mae";
    private Gson gson = new Gson();

    @BeforeEach
    public void setUp() {
        BeneficiosFactory.MVC = mvc;
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosParentescos() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1Parentesco() throws Exception {
        Parentesco obj = BeneficiosFactory.createParentesco(descricao);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarParentescoComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmParentesco() throws Exception {
        ParentescoForm obj = new ParentescoForm();
        obj.setDescricao(descricao);
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(obj));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(3)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmParentesco() throws Exception {
        Parentesco obj = BeneficiosFactory.createParentesco(descricao);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        obj.setDescricao(outraDescricao);
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(obj));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString(outraDescricao)));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmParentesco() throws Exception {
        Parentesco obj = BeneficiosFactory.createParentesco(descricao);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmParentescoComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

}
