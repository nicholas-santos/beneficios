/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.property;

import org.springframework.mail.SimpleMailMessage;

/**
 *
 * @author santos
 */
public interface MailService {
    void send(SimpleMailMessage msg);
}
