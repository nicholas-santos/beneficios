/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.beneficioEventual;

import br.edu.ifms.beneficios.controller.dto.EstoqueDto;
import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class BeneficioEventualDetalheDto extends AbstractBeneficioEventualDto {

    private List<EstoqueDto> estoque = new ArrayList();

    public BeneficioEventualDetalheDto() {
    }

    public BeneficioEventualDetalheDto(BeneficioEventual obj) {
        super(obj);
        this.estoque = EstoqueDto.converter(obj.getEstoque());
    }

    public BeneficioEventualDetalheDto(
            BeneficioEventual obj,
            UnidadeAtendimento unidadeAtendimento
    ) {
        super(obj);
        this.estoque = EstoqueDto.converter(obj.getEstoque(),
                unidadeAtendimento);
    }

    public List<EstoqueDto> getEstoque() {
        return estoque;
    }

    public static Page<BeneficioEventualDetalheDto> converter(Page<BeneficioEventual> items) {
        return items.map(BeneficioEventualDetalheDto::new);
    }

    public static List<BeneficioEventualDetalheDto> converter(List<BeneficioEventual> items) {
        return items.stream().map(BeneficioEventualDetalheDto::new).collect(Collectors.toList());
    }

}
