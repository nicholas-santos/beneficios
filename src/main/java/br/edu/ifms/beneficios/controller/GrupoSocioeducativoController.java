/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.GrupoSocioeducativoDto;
import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.form.GrupoSocioeducativoForm;
import br.edu.ifms.beneficios.modelo.GrupoSocioeducativo;
import br.edu.ifms.beneficios.repository.GrupoSocioeducativoRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/grupo-socioeducativo")
public class GrupoSocioeducativoController {

    @Autowired
    private GrupoSocioeducativoRepository repository;

    @GetMapping
    @Cacheable(value = "pageGrupoSocioeducativo")
    public Page<GrupoSocioeducativoDto> listar(
            @PageableDefault(sort = "nome", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

            Page<GrupoSocioeducativo> grupoSocioeducativo = repository.findAll(paginacao);
            return GrupoSocioeducativoDto.converter(grupoSocioeducativo);
    }
    
    @GetMapping("/list")
    @Cacheable(value = "listaGrupoSocioeducativo")
    public List<GrupoSocioeducativoDto> listar() {

        List<GrupoSocioeducativo> items;
        items = repository.findAll();
        
        return GrupoSocioeducativoDto.converter(items);
    }
    
    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageGrupoSocioeducativo", "listaGrupoSocioeducativo"}, allEntries = true)
    public ResponseEntity<GrupoSocioeducativoDto> criar(@RequestBody @Valid GrupoSocioeducativoForm form, UriComponentsBuilder uriBuilder) {
    	GrupoSocioeducativo grupoSocioeducativo = form.converter();
        repository.save(grupoSocioeducativo);

        URI uri = uriBuilder.path("/grupo-socioeducativo/{id}").buildAndExpand(grupoSocioeducativo.getId()).toUri();
        return ResponseEntity.created(uri).body(new GrupoSocioeducativoDto(grupoSocioeducativo));
    }

    @GetMapping("/{id}")
    public ResponseEntity<GrupoSocioeducativoDto> visualizar(@PathVariable Long id) {
        Optional<GrupoSocioeducativo> tipoUnidade = repository.findById(id);
        if (tipoUnidade.isPresent()) {
            return ResponseEntity.ok(new GrupoSocioeducativoDto(tipoUnidade.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageGrupoSocioeducativo", "listaGrupoSocioeducativo"}, allEntries = true)
    public ResponseEntity<GrupoSocioeducativoDto> atualizar(@PathVariable Long id, @RequestBody @Valid GrupoSocioeducativoForm form) {
        Optional<GrupoSocioeducativo> optional = repository.findById(id);
        if (optional.isPresent()) {
        	GrupoSocioeducativo tipoUnidade = form.atualizar(id, repository);
            return ResponseEntity.ok(new GrupoSocioeducativoDto(tipoUnidade));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageGrupoSocioeducativo", "listaGrupoSocioeducativo"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<GrupoSocioeducativo> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
