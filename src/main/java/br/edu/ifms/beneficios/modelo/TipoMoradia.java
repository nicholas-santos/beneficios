/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author santos
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class TipoMoradia implements Serializable {

    @Id
    private Long id;

    private String descricao;
    
    @Column(columnDefinition = "boolean default false")
    private Boolean complementar;

    public TipoMoradia() {
        this.complementar = Boolean.FALSE;
    }

    public TipoMoradia(Long id, String descricao, Boolean complementar) {
        this.id = id;
        this.descricao = descricao;
        this.complementar = complementar;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean isComplementar() {
        return complementar;
    }

    public void setComplementar(Boolean complementar) {
        this.complementar = complementar;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.descricao);
        hash = 59 * hash + Objects.hashCode(this.complementar);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final TipoMoradia other = (TipoMoradia) obj;
        if (!Objects.equals(this.descricao, other.getDescricao())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return Objects.equals(this.complementar, other.isComplementar());
    }

}
