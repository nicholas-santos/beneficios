/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.UnidadeFuncionarioId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 *
 * @author santos
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class UnidadeFuncionario implements Serializable {
    @EmbeddedId
    private UnidadeFuncionarioId id;
    
    @Column(name = "status", nullable = true)
    @Convert(converter = StatusConverter.class)
    private Status status;

    public UnidadeFuncionario() {
    }

    public UnidadeFuncionario(UnidadeFuncionarioId id, Status status) {
        this.id = id;
        this.status = status;
    }

    public UnidadeFuncionarioId getId() {
        return id;
    }

    public void setId(UnidadeFuncionarioId id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeFuncionario other = (UnidadeFuncionario) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        return true;
    }
    
}
