/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 *
 * @author santos
 */
@Entity
@PrimaryKeyJoinColumn(name="id")
public class Fornecedor extends AbstractPessoa {
    
    private String codigo;
    
    @OneToOne
    private Endereco endereco;

    public Fornecedor() {
    }

    public Fornecedor(Long id) {
        super(id);
    }

    public Fornecedor(Long id, String nome) {
        super(id, nome);
    }

    public Fornecedor(String codigo, Endereco endereco, Long id, String nome) {
        super(id, nome);
        this.codigo = codigo;
        this.endereco = endereco;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.codigo);
        hash = 73 * hash + Objects.hashCode(this.endereco);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Fornecedor other = (Fornecedor) obj;
        if (!Objects.equals(this.codigo, other.getCodigo())) {
            return false;
        }
        if (!Objects.equals(this.endereco, other.getEndereco())) {
            return false;
        }
        return true;
    }
    
}
