/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.atendimento.AtendimentoDto;
import br.edu.ifms.beneficios.controller.dto.ItemAnaliseDto;
import br.edu.ifms.beneficios.modelo.Analise;
import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import br.edu.ifms.beneficios.modelo.ItemAnalise;
import br.edu.ifms.beneficios.modelo.id.ItemAnaliseId;
import br.edu.ifms.beneficios.modelo.tipos.Autorizacao;
import br.edu.ifms.beneficios.repository.BeneficioEventualRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author santos
 */
public class AnaliseForm {

    private LocalDateTime emissao;

    @NotNull
    @NotEmpty
    private String parecer;

    @NotNull
    private Autorizacao autorizacao;

    private AtendimentoDto atendimento;
    private Boolean encaminhamento;

    private List<ItemAnaliseDto> itens = new ArrayList();

    public AnaliseForm() {
        this.autorizacao = Autorizacao.AUTORIZADO;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDateTime emissao) {
        this.emissao = emissao;
    }

    public String getParecer() {
        return parecer;
    }

    public void setParecer(String parecer) {
        this.parecer = parecer;
    }

    public Autorizacao getAutorizacao() {
        return autorizacao;
    }

    public void setAutorizacao(Autorizacao autorizacao) {
        this.autorizacao = autorizacao;
    }

    public AtendimentoDto getAtendimento() {
        return atendimento;
    }

    public void setAtendimento(AtendimentoDto atendimento) {
        this.atendimento = atendimento;
    }

    public Boolean getEncaminhamento() {
        return encaminhamento;
    }

    public void setEncaminhamento(Boolean encaminhamento) {
        this.encaminhamento = encaminhamento;
    }

    public List<ItemAnaliseDto> getItens() {
        return itens;
    }

    public void setItens(List<ItemAnaliseDto> itens) {
        this.itens = itens;
    }

    private List<ItemAnalise> toItemAnaliseList(Analise analise,
            BeneficioEventualRepository beneficioEventualRepository) {
        return IntStream.range(0, itens.size()).mapToObj(index -> {
            ItemAnaliseDto dto = itens.get(index);
            
            ItemAnaliseId itemId = new ItemAnaliseId(Long.valueOf(index+1), analise);
            BeneficioEventual beneficioEventual = beneficioEventualRepository
                    .getById(dto.getBeneficioEventual().getId());

            return new ItemAnalise(itemId, beneficioEventual,
                    dto.getQuantidade(), dto.getStatus());
        }).collect(Collectors.toList());
    }

    private Analise toAnalise(Analise analise,
            BeneficioEventualRepository beneficioEventualRepository) {
        analise.setParecer(this.parecer);
        analise.setAutorizacao(this.autorizacao);
        analise.setEncaminhamento(encaminhamento);
        analise.setItens(toItemAnaliseList(analise, beneficioEventualRepository));

        return analise;
    }

    public Analise converter(BeneficioEventualRepository beneficioEventualRepository) {
        return toAnalise(new Analise(), beneficioEventualRepository);
    }

    public String isValido() {
        if (this.atendimento == null) {
            return "Nenhum atendimento foi vinculado à análise.";
        }

        if (!this.atendimento.hasProntuario()) {
            return "Para registrar a análise do atendimento a PESSOA deve estar "
                    + "vinculada a um PRONTUÁRIO";
        }

        if (Autorizacao.AUTORIZADO.equals(this.autorizacao)
                && (this.encaminhamento == null || !this.encaminhamento)
                && this.itens.isEmpty()) {
            return "Você precisa informar quais Benefícios Eventuais foram "
                    + "autorizados.";
        }
        if (!this.itens.isEmpty()) {
            if (Autorizacao.NEGADO.equals(this.autorizacao)) {
                return "Não é possível disponibilizar Benefícios Eventuais quando "
                        + "a o Registro da Análise está NEGADO."
                        + "Remova os benefícios da lista.";
            }
            int size = this.itens.size();
            int contBeneficios = this.itens.stream().mapToInt(dto
                    -> dto.hasBeneficioEventual() ? 1 : 0).sum();
            if (contBeneficios != size) {
                return "Existem existem itens lançados sem a identificação "
                        + "de Benefício Eventual!";
            }

            int countQuantidade = this.itens.stream().mapToInt(dto
                    -> dto.hasQuantidade() ? 1 : 0).sum();
            if (countQuantidade != size) {
                return "Existem existem itens lançados sem quantidade "
                        + "informada para o Benefício Eventual!";
            }
        }
        return "";
    }
}
