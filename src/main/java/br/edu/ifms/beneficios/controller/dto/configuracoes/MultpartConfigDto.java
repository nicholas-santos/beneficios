/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.configuracoes;

import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;

/**
 *
 * @author santos
 */
public class MultpartConfigDto {
    private String location;
    private Long maxFileSize;
    private Long maxRequestSize;
    private Long fileSizeThreshold;

    public MultpartConfigDto() {
    }

    public MultpartConfigDto(MultipartProperties obj) {
        this.location = obj.getLocation();
        this.maxFileSize = obj.getMaxFileSize().toMegabytes();
        this.maxRequestSize = obj.getMaxRequestSize().toMegabytes();
        this.fileSizeThreshold = obj.getFileSizeThreshold().toKilobytes();
    }

    public String getLocation() {
        return location;
    }

    public Long getMaxFileSize() {
        return maxFileSize;
    }

    public Long getMaxRequestSize() {
        return maxRequestSize;
    }

    public Long getFileSizeThreshold() {
        return fileSizeThreshold;
    }
    
}
