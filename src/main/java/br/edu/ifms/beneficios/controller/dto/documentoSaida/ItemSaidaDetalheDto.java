/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoSaida;

import br.edu.ifms.beneficios.modelo.ItemEntrada;
import br.edu.ifms.beneficios.modelo.ItemSaida;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class ItemSaidaDetalheDto extends AbstractItemSaidaDto {

    private BigDecimal quantidadeConferida;
    private BigDecimal quantidadePendente;

    public ItemSaidaDetalheDto() {
        super();
    }

    public ItemSaidaDetalheDto(ItemSaida obj) {
        super(obj);
        
        this.quantidadeConferida = obj.getQuantidadeMovimentada();
        this.quantidadePendente = super.getQuantidade().subtract(this.quantidadeConferida);
    }

    public BigDecimal getQuantidadePendente() {
        return quantidadePendente;
    }

    public BigDecimal getQuantidadeConferida() {
        return quantidadeConferida;
    }

    public static Page<ItemSaidaDetalheDto> converter(Page<ItemSaida> items) {
        return items.map(ItemSaidaDetalheDto::new);
    }

    public static List<ItemSaidaDetalheDto> converter(List<ItemSaida> items) {
        return items.stream().map(ItemSaidaDetalheDto::new).collect(Collectors.toList());
    }

}
