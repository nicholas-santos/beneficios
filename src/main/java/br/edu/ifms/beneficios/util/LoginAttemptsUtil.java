/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.util;

import br.edu.ifms.beneficios.exception.TooManyFailedIpLoginsAuthenticationException;
import br.edu.ifms.beneficios.exception.TooManyFailedLoginsAuthenticationException;
import br.edu.ifms.beneficios.exception.UserServiceAuthenticationException;
import br.edu.ifms.beneficios.modelo.IpLog;
import br.edu.ifms.beneficios.modelo.IpTracker;
import br.edu.ifms.beneficios.modelo.JwtRequest;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.service.UsuarioService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author santos
 */
public class LoginAttemptsUtil {

    private static final Logger LOG = LoggerFactory.getLogger(LoginAttemptsUtil.class);

    private static final int MAX_FAILED_LOGINS = 4;
    private static final long FAILED_LOGIN_TIMEOUT_PERIOD = 24 * 3600 * 1000;

    private UsuarioService usuarioService;
    private IpTracker ipTracker;

    public LoginAttemptsUtil(UsuarioService usuarioService, IpTracker ipTracker) {
        this.usuarioService = usuarioService;
        this.ipTracker = ipTracker;
    }

    public void checkMaxLoginAttempts(JwtRequest jwtRequest) {
        LOG.debug("Checking for too many failed logins");

        if (jwtRequest != null && jwtRequest.getEmail() != null) {
            Usuario user = usuarioService.loadUsuarioByEmail(jwtRequest.getEmail());
            checkForFailedLogins(user);
        } else {
            throw new UserServiceAuthenticationException("Can't parse login request!");
        }
    }

    private void checkForFailedLogins(Usuario user) {
        if (user.getFailedAttempt()!= null) {
            if (user.getFailedAttempt() >= MAX_FAILED_LOGINS) {
                checkDateThreshold(user);
            }
        }
    }

    private void checkDateThreshold(Usuario user) {
        if (user.getLastFailedLoginTime() != null) {
            Long now = System.currentTimeMillis();
            Long difference = now - user.getLastFailedLoginTime();

            if (difference < FAILED_LOGIN_TIMEOUT_PERIOD) {
                throw new TooManyFailedLoginsAuthenticationException("Too many failed logins!");
            }
        }
    }

    /**
     * Check to make sure this user hasn't failed authentication too many times
     * from the same IP address.
     */
    public void checkIpValidity(HttpServletRequest request) {
        String ipAddress = request.getRemoteAddr();

        //timeframe in the past 24 hours
        Long timeframe = System.currentTimeMillis() - FAILED_LOGIN_TIMEOUT_PERIOD;

        List<IpLog> list = ipTracker.fetchIpFailureRecord(ipAddress, timeframe);
        if (list != null && list.size() >= MAX_FAILED_LOGINS) {
            throw new TooManyFailedIpLoginsAuthenticationException("Too many failed logins from this IP address!");
        }
    }
}
