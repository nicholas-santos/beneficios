/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "origem", length = 1, discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("M")
public class Movimento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long numero;
    
    @ManyToOne
    private UnidadeAtendimento unidadeAtendimento;
    
    @ManyToOne
    private Funcionario funcionario;
    
    private BigDecimal quantidade;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime emissao;

    public Movimento() {
        this.emissao = LocalDateTime.now();
        this.quantidade = BigDecimal.ZERO;
    }

    public Movimento(Long numero, UnidadeAtendimento unidadeAtendimento, BigDecimal quantidade, LocalDateTime emissao) {
        this();
        this.numero = numero;
        this.unidadeAtendimento = unidadeAtendimento;
        this.quantidade = quantidade;
        this.emissao = emissao;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }
    
    public Boolean isSetNumero() {
        return this.numero != null;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDateTime emissao) {
        this.emissao = emissao;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.numero);
        hash = 23 * hash + Objects.hashCode(this.unidadeAtendimento);
        hash = 23 * hash + Objects.hashCode(this.quantidade);
        hash = 23 * hash + Objects.hashCode(this.emissao);
        hash = 23 * hash + Objects.hashCode(this.funcionario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Movimento other = (Movimento) obj;
        if (!Objects.equals(this.numero, other.getNumero())) {
            return false;
        }
        if (!Objects.equals(this.unidadeAtendimento, other.getUnidadeAtendimento())) {
            return false;
        }
        if (!Objects.equals(this.quantidade, other.getQuantidade())) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        if (!Objects.equals(this.funcionario, other.getFuncionario())) {
            return false;
        }
        
        return true;
    }
    
}
