/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.Analise;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class ItemAnaliseId implements Serializable {
    
    private Long id;
    
    @ManyToOne
    private Analise analise;

    public ItemAnaliseId() {
    }

    public ItemAnaliseId(Long id, Analise analise) {
        this.id = id;
        this.analise = analise;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Analise getAnalise() {
        return analise;
    }

    public void setAnalise(Analise analise) {
        this.analise = analise;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.analise);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ItemAnaliseId other = (ItemAnaliseId) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.analise, other.getAnalise())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItemAnaliseId{" + "id=" + id + ", analise=" + analise.toString() + '}';
    }
    
}
