/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.dto.UfDto;
import br.edu.ifms.beneficios.controller.form.CidadeForm;
import br.edu.ifms.beneficios.modelo.Pais;
import br.edu.ifms.beneficios.modelo.Uf;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author santos
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CidadeControllerTest {

    @Autowired
    private MockMvc mvc;

    private final String sURI = "/api/cidades";
    private Gson gson = new Gson();
    private CidadeForm cidade1;
    private CidadeForm cidade2;

    private Uf loadPreData() {
        BeneficiosFactory.MVC = mvc;
        Pais pais = BeneficiosFactory.createPais("Brasil");
        Uf uf = BeneficiosFactory.createUf("Mato Grosso do Sul", "MS", pais);

        return uf;
    }

    @BeforeEach
    public void setUp() {
        UfDto ufdto = new UfDto(loadPreData());

        cidade1 = new CidadeForm();
        cidade1.setNome("Naviraí");
        cidade1.setUf(ufdto);

        cidade2 = new CidadeForm();
        cidade2.setNome("Itaquiraí");
        cidade2.setUf(ufdto);
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosCidades() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1Cidade() throws Exception {
        URI uri = new URI(sURI + "/2");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarCidadeComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmCidade() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(cidade1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmCidade() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(cidade1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));

        uri = new URI(sURI);
        request = MockRequestBuilderHelper.post(uri, gson.toJson(cidade2));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(2)));

        uri = new URI(sURI + "/2");
        cidade2.setNome("Mundo Novo");
        // dados a serem alterados
        request = MockRequestBuilderHelper.put(uri, gson.toJson(cidade2));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("Mundo Novo")));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmCidade() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(cidade1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(4)));
        request = MockRequestBuilderHelper.post(uri, gson.toJson(cidade2));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(5)));

        uri = new URI(sURI + "/2");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmCidadeComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

}
