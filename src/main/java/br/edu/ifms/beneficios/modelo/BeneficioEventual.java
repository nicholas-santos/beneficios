/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author santos
 */
//@EntityListeners(AuditoriaListener.class)
@Entity
public class BeneficioEventual implements Serializable {

    @Id
    private Long id;
    private String descricao;
    private Boolean outraConcessao;

    @OneToMany(mappedBy = "id.beneficioEventual", fetch = FetchType.LAZY)
    private List<Estoque> estoque = new ArrayList();

    public BeneficioEventual() {
        outraConcessao = Boolean.FALSE;
    }

    public BeneficioEventual(Long id, String descricao) {
        this(id, descricao, Boolean.FALSE);
    }

    public BeneficioEventual(String descricao) {
        this(null, descricao, Boolean.FALSE);
    }

    public BeneficioEventual(String descricao, Boolean outraConcessao) {
        this(null, descricao, outraConcessao);
    }

    public BeneficioEventual(Long id, String descricao, Boolean outraConcessao) {
        this.id = id;
        this.descricao = descricao;
        this.outraConcessao = outraConcessao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getOutraConcessao() {
        return outraConcessao;
    }

    public void setOutraConcessao(Boolean outraConcessao) {
        this.outraConcessao = outraConcessao;
    }

    public List<Estoque> getEstoque() {
        return estoque;
    }

    public void setEstoque(List<Estoque> estoque) {
        this.estoque = estoque;
    }

    public BigDecimal getSaldo() {
        BigDecimal total = this.estoque.stream()
                .map(obj -> obj.getQuantidade())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        
        return total;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        hash = 67 * hash + Objects.hashCode(this.descricao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final BeneficioEventual other = (BeneficioEventual) obj;
        if (!Objects.equals(this.descricao, other.getDescricao())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }
}
