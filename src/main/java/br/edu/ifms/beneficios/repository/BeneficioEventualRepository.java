/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface BeneficioEventualRepository extends JpaRepository<BeneficioEventual, Long> {
    
    @Query("SELECT coalesce(max(b.id), 0) FROM BeneficioEventual b")
    Long getMaxId();
    Page<BeneficioEventual> findByDescricaoContainingIgnoreCase(String nome, Pageable paginacao);
    
}
