/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.config.validacao.ErroDeFormularioDto;
import br.edu.ifms.beneficios.controller.dto.AuxilioDto;
import br.edu.ifms.beneficios.controller.dto.CondicaoMoradiaDto;
import br.edu.ifms.beneficios.controller.dto.EnderecoDto;
import br.edu.ifms.beneficios.controller.dto.EscolaridadeDto;
import br.edu.ifms.beneficios.controller.dto.MoradiaDto;
import br.edu.ifms.beneficios.controller.dto.RendimentoDto;
import br.edu.ifms.beneficios.controller.dto.TipoMoradiaDto;
import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.helper.DateHelper;
import br.edu.ifms.beneficios.modelo.Auxilio;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.modelo.CondicaoMoradia;
import br.edu.ifms.beneficios.modelo.CondicaoTrabalho;
import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.Moradia;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.ProgramaGoverno;
import br.edu.ifms.beneficios.modelo.Rendimento;
import br.edu.ifms.beneficios.modelo.TipoMoradia;
import br.edu.ifms.beneficios.modelo.id.AuxilioId;
import br.edu.ifms.beneficios.modelo.id.EnderecoId;
import br.edu.ifms.beneficios.modelo.id.MoradiaId;
import br.edu.ifms.beneficios.modelo.id.RendimentoId;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.CepRepository;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.CondicaoMoradiaRepository;
import br.edu.ifms.beneficios.repository.CondicaoTrabalhoRepository;
import br.edu.ifms.beneficios.repository.DocumentoRepository;
import br.edu.ifms.beneficios.repository.EnderecoRepository;
import br.edu.ifms.beneficios.repository.EscolaridadeRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.OrgaoExpedidorRepository;
import br.edu.ifms.beneficios.repository.PessoaRepository;
import br.edu.ifms.beneficios.repository.ProgramaGovernoRepository;
import br.edu.ifms.beneficios.repository.TipoMoradiaRepository;
import br.edu.ifms.beneficios.service.FileStorageService;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;
import br.edu.ifms.beneficios.repository.ContatoRepository;
import br.edu.ifms.beneficios.repository.DocumentoPessoaRepository;
import br.edu.ifms.beneficios.repository.MoradiaRepository;
import java.util.Optional;

/**
 *
 * @author santos
 */
public class PessoaForm extends AbstractPessoaForm {

    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date nascimento;

    @NotNull
    private EscolaridadeDto escolaridadeDto;

    @NotNull
    private Sexo sexo;

    private List<MoradiaDto> moradias = new ArrayList();
    private List<RendimentoDto> rendimentos = new ArrayList();
    private List<AuxilioDto> auxilios = new ArrayList();

    public PessoaForm() {
    }

    public PessoaForm(Pessoa obj) {
        super.setNome(obj.getNome());
        this.sexo = obj.getSexo();
        this.nascimento = DateHelper.toDate(obj.getNascimento());
        this.escolaridadeDto = new EscolaridadeDto(obj.getEscolaridade());
    }

    public PessoaForm(String nome, Date nascimento) {
        super.setNome(nome);
        this.nascimento = nascimento;
    }

    public PessoaForm(String nome, Date nascimento, EscolaridadeDto escolaridadeDto, Sexo sexo) {
        super.setNome(nome);
        this.nascimento = nascimento;
        this.escolaridadeDto = escolaridadeDto;
        this.sexo = sexo;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public EscolaridadeDto getEscolaridadeDto() {
        return escolaridadeDto;
    }

    public void setEscolaridadeDto(EscolaridadeDto escolaridadeDto) {
        this.escolaridadeDto = escolaridadeDto;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public List<MoradiaDto> getMoradias() {
        return moradias;
    }

    public void setMoradias(List<MoradiaDto> moradias) {
        this.moradias = moradias;
    }

    public List<RendimentoDto> getRendimentos() {
        return rendimentos;
    }

    public void setRendimento(List<RendimentoDto> rendimentos) {
        this.rendimentos = rendimentos;
    }

    public List<AuxilioDto> getAuxilios() {
        return auxilios;
    }

    public void setAuxilios(List<AuxilioDto> auxilios) {
        this.auxilios = auxilios;
    }

    private Optional<Endereco> buscarEndereco(
            EnderecoDto enderecoDto,
            EnderecoRepository enderecoRepository,
            CepRepository cepRepository,
            CidadeRepository cidadeRepository,
            BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository) {
        Cep cep = ControllerHelper.getCep(
                enderecoDto,
                cepRepository, cidadeRepository, bairroRepository,
                logradouroRepository);

        EnderecoId enderecoId = new EnderecoId(cep, enderecoDto.getNumero());
        return enderecoRepository.findById(enderecoId);
    }

    public ErroDeFormularioDto isValido(
            Long pessoaId,
            DocumentoPessoaRepository documentoPessoaRepository,
            MoradiaRepository moradiaRepository,
            EnderecoRepository enderecoRepository,
            CepRepository cepRepository,
            CidadeRepository cidadeRepository,
            BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository) {
        ErroDeFormularioDto message;
        if (!super.hasDocumentos()) {
            message = new ErroDeFormularioDto.Builder()
                    .campo("Documento")
                    .erro("Você deve lançar ao menos UM documento para identificar a pessoa.")
                    .build();
        }

        message = super.existeDocumentosDuplicados(pessoaId, documentoPessoaRepository);
        if (message.hasErro()) {
            return message;
        }

        message = validaDuplicacaoDeMoradiasAtivas();
        if (message.hasErro()) {
            return message;
        }

        message = validaMoradiaDeOutraPessoa(pessoaId,
                moradiaRepository,
                enderecoRepository,
                cepRepository,
                cidadeRepository,
                bairroRepository,
                logradouroRepository);
        if (message.hasErro()) {
            return message;
        }

        return message;
    }

    private ErroDeFormularioDto validaDuplicacaoDeMoradiasAtivas() {
        int countMoradias = this.moradias.stream().mapToInt(dto
                -> dto.isOcupada() ? 1 : 0).sum();
        if (!this.moradias.isEmpty() && countMoradias > 1) {
            new ErroDeFormularioDto.Builder()
                    .campo("Moradia")
                    .erro("Existem duas ou mais moradias ativas. Registre a desocupação "
                            + "delas e mantenha apenas UMA ativa!")
                    .build();
        }
        return new ErroDeFormularioDto();
    }

    private ErroDeFormularioDto validaMoradiaDeOutraPessoa(
            Long pessoaId,
            MoradiaRepository repository,
            EnderecoRepository enderecoRepository,
            CepRepository cepRepository,
            CidadeRepository cidadeRepository,
            BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository) {
        // Validação sobre as moradias
        ErroDeFormularioDto message = new ErroDeFormularioDto();
        for (MoradiaDto dto : moradias) {
            if (dto.isOcupada()) {
                /**
                 * verifica se a moradia exige complemento e se ele está
                 * preenchido, do contrário, apresenta mensagem de aviso
                 * proibindo o cadastro
                 */
                if (dto.exigeComplemento() && !dto.hasComplemento()) {
                    String tipo = dto.getTipoMoradiaDto().getDescricao();
                    return new ErroDeFormularioDto.Builder()
                            .campo("Moradia")
                            .erro(String.format("O TIPO DE MORADIA %s informado "
                                    + "no cadastro de Moradias "
                                    + "EXIGE o preenchimento de COMPLEMENTO. "
                                    + "Favor, informe uma identificação para "
                                    + "o(a) %s.", tipo, tipo))
                            .build();
                }

                Optional<Endereco> optionaEndereco = buscarEndereco(dto.getEnderecoDto(),
                        enderecoRepository,
                        cepRepository,
                        cidadeRepository,
                        bairroRepository,
                        logradouroRepository);
                // Recupera o endereço para verificar se existem outra pessoas com o mesmo endereço.
                if (optionaEndereco.isPresent()) {
                    // buscar por moradias com o mesmo endereço
                    List<Moradia> listaMoradias = repository
                            .findByDataSaidaIsNullAndEndereco(optionaEndereco.get());
                    /**
                     * Se a moradia exige complemento, verifica na lista a
                     * existência de moradia para pessoa diferente e complemento
                     * diferente
                     */
                    if (dto.exigeComplemento()) {
                        Optional<Moradia> op = listaMoradias.stream()
                                .filter(obj -> !obj.getId().getPessoa().getId().equals(pessoaId)
                                && dto.getEnderecoDto().getComplemento().equalsIgnoreCase(obj.getEndereco().getComplemento()))
                                .findFirst();
                        if (op.isPresent()) {
                            Moradia m = op.get();
                            Pessoa p = m.getId().getPessoa();
                            String artigo = p.isFeminino() ? "à" : "ao";
                            return new ErroDeFormularioDto.Builder()
                                    .campo("Moradia")
                                    .erro(String.format("O endereço informado na "
                                            + "MORADIA ATIVA já foi atribuído "
                                            + "%s %s [%d] com o mesmo COMPLEMENTO.",
                                            artigo,
                                            p.getNome(),
                                            p.getId()))
                                    .build();
                        }
                    } else {
                        Optional<Moradia> op = listaMoradias.stream()
                                .filter(obj -> !obj.getId().getPessoa().getId().equals(pessoaId))
                                .findFirst();
                        if (op.isPresent()) {
                            Moradia m = op.get();
                            Pessoa p = m.getId().getPessoa();
                            String artigo = p.isFeminino() ? "à" : "ao";
                            return new ErroDeFormularioDto.Builder()
                                    .campo("Moradia")
                                    .erro(String.format("O endereço informado na "
                                            + "MORADIA ATIVA já foi atribuído "
                                            + "%s %s [%d].",
                                            artigo,
                                            p.getNome(),
                                            p.getId()))
                                    .build();
                        }
                    }
                }
            }
        }
        return message;
    }

    private void updateMoradiaList(Pessoa pessoa,
            TipoMoradiaRepository tipoMoradiaRepository,
            CondicaoMoradiaRepository condicaoMoradiaRepository,
            EnderecoRepository enderecoRepository, CepRepository cepRepository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository) {
        moradias.stream().map(dto -> {
            TipoMoradiaDto tipoMoradiaDto = dto.getTipoMoradiaDto();
            TipoMoradia tipoMoradia = tipoMoradiaRepository.getById(tipoMoradiaDto.getId());

            CondicaoMoradiaDto condicaoMoradiaDto = dto.getCondicaoMoradiaDto();
            CondicaoMoradia condicaoMoradia = condicaoMoradiaRepository
                    .getById(condicaoMoradiaDto.getId());

            EnderecoDto enderecoDto = dto.getEnderecoDto();
            Cep cep = ControllerHelper.getCep(
                    enderecoDto,
                    cepRepository, cidadeRepository, bairroRepository,
                    logradouroRepository);
            EnderecoId enderecoId = new EnderecoId(cep, enderecoDto.getNumero());
            Optional<Endereco> optional = enderecoRepository.findById(enderecoId);

            Endereco endereco = null;
            if (!optional.isEmpty()) {
                endereco = optional.get();
            } else {
                endereco = new Endereco(enderecoId);
            }
            endereco.setComplemento(enderecoDto.getComplemento());
            endereco.setReferencia(enderecoDto.getReferencia());

            MoradiaId moradiaId = new MoradiaId(dto.getId(), pessoa);
            Moradia moradia = new Moradia(moradiaId,
                    dto.getDataOcupacao(), dto.getDataSaida(),
                    dto.getValor(), condicaoMoradia, tipoMoradia, endereco);
            moradia.setDeleted(dto.isDeleted());

            if (moradia.isDeleted()) {
                pessoa.remove(moradia);
            } else {
                pessoa.add(moradia);
            }
            return dto;
        }).collect(Collectors.toList());
    }

    private void updateRendimentosList(Pessoa pessoa,
            CondicaoTrabalhoRepository condicaoTrabalhoRepository) {
        rendimentos.stream().map(dto -> {
            CondicaoTrabalho condicaoTrabalho = condicaoTrabalhoRepository
                    .getById(dto.getCondicaoTrabalhoDto().getId());
            RendimentoId rendimentoId = new RendimentoId(dto.getSequencia(), pessoa);
            Rendimento rendimento = new Rendimento(rendimentoId, condicaoTrabalho,
                    dto.getValor(), dto.getAdmissao(),
                    dto.getDemissao());
            rendimento.setDeleted(dto.isDeleted());

            if (rendimento.isDeleted()) {
                pessoa.remove(rendimento);
            } else {
                pessoa.add(rendimento);
            }

            return dto;
        }).collect(Collectors.toList());
    }

    private void updateAuxiliosList(Pessoa pessoa,
            ProgramaGovernoRepository programaGovernoRepository) {
        auxilios.stream().map(dto -> {
            ProgramaGoverno programaGoverno = programaGovernoRepository
                    .getById(dto.getProgramaGoverno().getId());

            AuxilioId id = new AuxilioId(dto.getId(), pessoa);
            Auxilio auxilio = new Auxilio(id, programaGoverno,
                    dto.getDataRegistro(), dto.getDataFim(),
                    dto.getValor(), dto.getStatus());
            auxilio.setDeleted(dto.isDeleted());

            if (auxilio.isDeleted()) {
                pessoa.remove(auxilio);
            } else {
                pessoa.add(auxilio);
            }

            return dto;
        }).collect(Collectors.toList());
    }

    private Pessoa toPessoa(Pessoa pessoa, MultipartFile file,
            CondicaoTrabalhoRepository condicaoTrabalhoRepository,
            EscolaridadeRepository escolaridadeRepository,
            ContatoRepository tipoContatoRepository,
            TipoMoradiaRepository tipoMoradiaRepository,
            CondicaoMoradiaRepository condicaoMoradiaRepository,
            EnderecoRepository enderecoRepository, CepRepository cepRepository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository,
            DocumentoRepository documentoRepository,
            OrgaoExpedidorRepository orgaoExpedidorRepository,
            ProgramaGovernoRepository programaGovernoRepository,
            FileStorageService fileStorageService) {
        pessoa.setNome(this.getNome());
        pessoa.setNascimento(DateHelper.toLocalDate(nascimento));
        pessoa.setSexo(this.sexo);
        pessoa.setEscolaridade(escolaridadeRepository.getById(this.escolaridadeDto.getId()));

        updateContatoPessoaList(pessoa, tipoContatoRepository);
        updateDocumentoPessoaList(pessoa, documentoRepository, orgaoExpedidorRepository);

        updateMoradiaList(pessoa,
                tipoMoradiaRepository, condicaoMoradiaRepository,
                enderecoRepository, cepRepository, cidadeRepository,
                bairroRepository, logradouroRepository);

        updateRendimentosList(pessoa, condicaoTrabalhoRepository);
        updateAuxiliosList(pessoa, programaGovernoRepository);
        if (file != null) {
            pessoa.setFoto(fileStorageService.resizeImageToByte(file, 200));
        }

        return pessoa;
    }

    public Pessoa converter(MultipartFile file,
            PessoaRepository repository,
            CondicaoTrabalhoRepository condicaoTrabalhoRepository,
            EscolaridadeRepository escolaridadeRepository,
            ContatoRepository tipoContatoRepository,
            TipoMoradiaRepository tipoMoradiaRepository,
            CondicaoMoradiaRepository condicaoMoradiaRepository,
            EnderecoRepository enderecoRepository, CepRepository cepRepository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository,
            DocumentoRepository documentoRepository,
            OrgaoExpedidorRepository orgaoExpedidorRepository,
            ProgramaGovernoRepository programaGovernoRepository,
            FileStorageService fileServiceStorage) {

        return toPessoa(new Pessoa(), file, condicaoTrabalhoRepository,
                escolaridadeRepository, tipoContatoRepository,
                tipoMoradiaRepository, condicaoMoradiaRepository,
                enderecoRepository, cepRepository, cidadeRepository,
                bairroRepository, logradouroRepository,
                documentoRepository, orgaoExpedidorRepository,
                programaGovernoRepository, fileServiceStorage);
    }

    public Pessoa atualizar(Long id, MultipartFile file, PessoaRepository pessoaRepository,
            CondicaoTrabalhoRepository condicaoTrabalhoRepository,
            EscolaridadeRepository escolaridadeRepository,
            ContatoRepository tipoContatoRepository,
            TipoMoradiaRepository tipoMoradiaRepository,
            CondicaoMoradiaRepository condicaoMoradiaRepository,
            EnderecoRepository enderecoRepository, CepRepository cepRepository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository,
            DocumentoRepository documentoRepository,
            OrgaoExpedidorRepository orgaoExpedidorRepository,
            ProgramaGovernoRepository programaGovernoRepository,
            FileStorageService fileServiceStorage) {
        return toPessoa(pessoaRepository.getById(id), file,
                condicaoTrabalhoRepository, escolaridadeRepository,
                tipoContatoRepository, tipoMoradiaRepository,
                condicaoMoradiaRepository, enderecoRepository, cepRepository,
                cidadeRepository, bairroRepository, logradouroRepository,
                documentoRepository, orgaoExpedidorRepository,
                programaGovernoRepository, fileServiceStorage);
    }
}
