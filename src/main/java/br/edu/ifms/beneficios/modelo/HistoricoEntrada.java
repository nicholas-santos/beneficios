/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
public class HistoricoEntrada extends AbstractHistorico {

    @ManyToOne
    private DocumentoEntrada documentoEntrada;

    public HistoricoEntrada() {
        super();
    }

    public DocumentoEntrada getDocumentoEntrada() {
        return documentoEntrada;
    }

    public void setDocumentoEntrada(DocumentoEntrada documentoEntrada) {
        this.documentoEntrada = documentoEntrada;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.documentoEntrada);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final HistoricoEntrada other = (HistoricoEntrada) obj;
        if (!Objects.equals(this.documentoEntrada, other.getDocumentoEntrada())) {
            return false;
        }
        return true;
    }
    
}
