/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.LogradouroView;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class LogradouroViewDto {
    
    private Long id;
    private String logradouro;
    private String nome;
    private Long tipoLogradouroId;
    private Long bairroId;
    private String bairroNome;
    private Long cidadeId;
    private String cidadeNome;
    private Long ufId;
    private String sigla;

    public LogradouroViewDto(LogradouroView obj) {
        this.id = obj.getId();
        this.logradouro = obj.getLogradouro();
        this.nome = obj.getNome();
        this.tipoLogradouroId = obj.getTipoLogradouroId();
        this.bairroId = obj.getBairroId();
        this.bairroNome = obj.getBairroNome();
        this.cidadeId = obj.getCidadeId();
        this.cidadeNome = obj.getCidadeNome();
        this.ufId = obj.getUfId();
        this.sigla = obj.getSigla();
    }

    public Long getId() {
        return id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getNome() {
        return nome;
    }

    public Long getTipoLogradouroId() {
        return tipoLogradouroId;
    }

    public Long getBairroId() {
        return bairroId;
    }

    public Long getCidadeId() {
        return cidadeId;
    }

    public Long getUfId() {
        return ufId;
    }

    public String getBairroNome() {
        return bairroNome;
    }

    public String getCidadeNome() {
        return cidadeNome;
    }

    public String getSigla() {
        return sigla;
    }
    
    public static List<LogradouroViewDto> converter(List<LogradouroView> items) {
        return items.stream().map(LogradouroViewDto::new).collect(Collectors.toList());
    }
}
