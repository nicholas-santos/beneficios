/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.usuario;

import br.edu.ifms.beneficios.controller.dto.ArquivoDto;
import br.edu.ifms.beneficios.controller.dto.PerfilDto;
import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Usuario;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class UsuarioDetalheDto extends AbstractUsuarioDto {

    private byte[] foto;
    private ArquivoDto arquivo;
    private List<PerfilDto> perfis;

    public UsuarioDetalheDto() {
    }

    public UsuarioDetalheDto(Usuario obj) {
        super(obj);
        if (obj.getFoto() != null) {            
            this.arquivo = new ArquivoDto(obj.getFoto());
        }
        this.perfis = PerfilDto.converter(obj.getPerfis());
    }

    public byte[] getFoto() {
        return foto;
    }
    
    public ArquivoDto getArquivo() {
        return arquivo;
    }

    public List<PerfilDto> getPerfis() {
        return perfis;
    }

    public static List<UsuarioDetalheDto> converter(List<Usuario> items) {
        return items.stream().map(UsuarioDetalheDto::new).collect(Collectors.toList());
    }

    public static Page<UsuarioDetalheDto> converter(Page<Usuario> items) {
        return items.map(UsuarioDetalheDto::new);
    }
}
