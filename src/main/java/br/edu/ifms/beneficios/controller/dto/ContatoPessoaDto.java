/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.helper.DateHelper;
import br.edu.ifms.beneficios.modelo.ContatoPessoa;
import br.edu.ifms.beneficios.modelo.id.ContatoPessoaId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class ContatoPessoaDto {
    private Long id;
    private String descricao;
    private Status status;
    private ContatoDto tipoContatoDto;
    private Boolean deleted;

    public ContatoPessoaDto() {
        this.deleted = false;
    }

    public ContatoPessoaDto(ContatoPessoa obj) {
        ContatoPessoaId contatoPessoaId = obj.getId();
        this.id = contatoPessoaId.getId();
        this.descricao = obj.getDescricao();
        this.status = obj.getStatus();
        this.tipoContatoDto = new ContatoDto(obj.getTipoContato());
        this.deleted = obj.isDeleted();
    }

    public Long getId() {
        return id;
    }
    
    public Boolean hasId() {
        return this.id != null && this.id > 0;
    }

    public String getDescricao() {
        return descricao;
    }

    public Status getStatus() {
        return status;
    }

    public ContatoDto getTipoContatoDto() {
        return tipoContatoDto;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    
    public static List<ContatoPessoaDto> converter(List<ContatoPessoa> items) {
        return items.stream().map(ContatoPessoaDto::new).collect(Collectors.toList());
    }
    
}
