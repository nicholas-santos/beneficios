/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.OptionalLong;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author santos
 */
@Entity
public class DocumentoEntrada extends AbstractDocumento {

    @ManyToOne
    private Fornecedor fornecedor;

    private String processo;
    private String ata;
    private String pregao;
    private String empenhoContabil;
    private String contrato;
    private String numeroNotaFiscal;
    private Boolean doacao;

    @OneToMany(mappedBy = "id.documentoEntrada",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<ItemEntrada> itens = new ArrayList();
    
    @OneToMany(mappedBy = "documentoEntrada",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<HistoricoEntrada> historicos = new ArrayList();

    public DocumentoEntrada() {
    }

    public DocumentoEntrada(Long id) {
        super(id);
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public String getAta() {
        return ata;
    }

    public void setAta(String ata) {
        this.ata = ata;
    }

    public String getPregao() {
        return pregao;
    }

    public void setPregao(String pregao) {
        this.pregao = pregao;
    }

    public String getEmpenhoContabil() {
        return empenhoContabil;
    }

    public void setEmpenhoContabiL(String empenhoContabil) {
        this.empenhoContabil = empenhoContabil;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(String numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    public Boolean getDoacao() {
        return doacao;
    }

    public void setDoacao(Boolean doacao) {
        this.doacao = doacao;
    }

    public List<ItemEntrada> getItens() {
        return itens;
    }

    public void setItens(List<ItemEntrada> itens) {
        this.itens.clear();
        if (!itens.isEmpty()) {
            this.itens.addAll(itens);
        }
    }

    public void add(ItemEntrada itemEntrada) {
        if (!itemEntrada.isSetNumero()) {
            OptionalLong maxId = itens.stream()
                    .mapToLong(obj -> obj.getId().getNumero())
                    .max();
            itemEntrada.getId().setNumero((maxId.isPresent()
                    ? maxId.getAsLong() : 0) + 1L);
            this.itens.add(itemEntrada);
        } else {
            this.itens.stream().map((obj) -> {
                if (itemEntrada.getId().getNumero().equals(obj.getId().getNumero())) {
                    obj.setQuantidade(itemEntrada.getQuantidade());
                    obj.setBeneficioEventual(itemEntrada.getBeneficioEventual());
                    obj.setStatus(itemEntrada.getStatus());
                }
                return obj;
            });
        }
    }

    public void remove(ItemEntrada item) {
        this.itens.remove(item);
        item.getId().setDocumentoEntrada(null);
    }
    
    public void updateStatus() {
        Integer recebidos = this.itens.stream()
                .mapToInt(i -> i.isRecebido() ? 1 : 0)
                .sum();
        if (recebidos < this.itens.size()) {
            setStatus(Status.PARCIAL);
        } else {
            setStatus(Status.FINALIZADO);
        }
    }

    public List<HistoricoEntrada> getHistoricos() {
        return historicos;
    }

    public void setHistoricos(List<HistoricoEntrada> historicos) {
        this.historicos = historicos;
    }

    public void add(HistoricoEntrada historico) {
        this.historicos.add(historico);
    }

    public void remove(HistoricoEntrada historico) {
        this.historicos.remove(historico);
        historico.setDocumentoEntrada(null);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.fornecedor);
        hash = 89 * hash + Objects.hashCode(this.processo);
        hash = 89 * hash + Objects.hashCode(this.ata);
        hash = 89 * hash + Objects.hashCode(this.pregao);
        hash = 89 * hash + Objects.hashCode(this.empenhoContabil);
        hash = 89 * hash + Objects.hashCode(this.contrato);
        hash = 89 * hash + Objects.hashCode(this.numeroNotaFiscal);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final DocumentoEntrada other = (DocumentoEntrada) obj;
        if (!Objects.equals(this.processo, other.getProcesso())) {
            return false;
        }
        if (!Objects.equals(this.fornecedor, other.getFornecedor())) {
            return false;
        }
        if (!Objects.equals(this.ata, other.getAta())) {
            return false;
        }
        if (!Objects.equals(this.pregao, other.getPregao())) {
            return false;
        }
        if (!Objects.equals(this.empenhoContabil, other.getEmpenhoContabil())) {
            return false;
        }
        if (!Objects.equals(this.contrato, other.getContrato())) {
            return false;
        }
        if (!Objects.equals(this.numeroNotaFiscal, other.getNumeroNotaFiscal())) {
            return false;
        }
        return true;
    }

}
