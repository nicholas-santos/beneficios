/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.atendimento.AtendimentoDto;
import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.status.StatusAtendimentoCountDto;
import br.edu.ifms.beneficios.controller.form.AtendimentoForm;
import br.edu.ifms.beneficios.modelo.Atendimento;
import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimento;
import br.edu.ifms.beneficios.service.AtendimentoService;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/atendimentos")
@Profile(value = {"prod", "test"})
public class AtendimentoController {

    @Autowired
    private AtendimentoService service;

    @GetMapping
    public Page<AtendimentoDto> listar(
            @RequestParam(required = false) Long pessoaId,
            @RequestParam(required = false) StatusAtendimento status,
            @RequestParam(required = false) Long unidadeAtendimentoId,
            @PageableDefault(sort = "emissao", direction = Sort.Direction.DESC,
                    page = 0, size = 10) Pageable paginacao) {
        return AtendimentoDto.converter(service.listar(pessoaId, status,
                unidadeAtendimentoId, paginacao));
    }

    @GetMapping("/historico")
    public Page<AtendimentoDto> listarHistorico(
            @RequestParam(required = true) Long prontuarioId,
            @RequestParam(required = true) Long unidadeAtendimentoId,
            @RequestParam(required = true) String emissao,
            @PageableDefault(sort = "emissao", direction = Sort.Direction.DESC,
                    page = 0, size = 10) Pageable paginacao) {

        LocalDateTime ldt = LocalDateTime.parse(emissao);
        return AtendimentoDto.converter(service.listar(
                prontuarioId, unidadeAtendimentoId, ldt, paginacao));
    }

    @GetMapping("/list")
    public List<AtendimentoDto> listar() {
        return AtendimentoDto.converter(service.listar());
    }

    @GetMapping("/contagem/{unidadeAtendimentoId}")
    public List<StatusAtendimentoCountDto> listarContagem(
            @PathVariable Long unidadeAtendimentoId) {
        return StatusAtendimentoCountDto.converter(service.listarContagem(unidadeAtendimentoId));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AtendimentoDto> visualizar(@PathVariable Long id) {
        return ResponseEntity.ok(new AtendimentoDto(service.buscarPor(id)));
    }

    @GetMapping("/analise/{id}")
    public ResponseEntity<AtendimentoDto> visualizarAnalise(@PathVariable Long id) {
        return ResponseEntity.ok(new AtendimentoDto(service.buscarPorAnaliseId(id)));
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> cadastrar(@RequestBody @Valid AtendimentoForm form,
            UriComponentsBuilder uriBuilder) {
        AtendimentoDto dto = service.criar(form);

        URI uri = uriBuilder.path("/atendimentos/{id}")
                .buildAndExpand(dto.getId())
                .toUri();
        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<AtendimentoDto> atualizar(
            @PathVariable Long id, @RequestBody @Valid AtendimentoForm form) {
        AtendimentoDto dto = service.atualizar(id, form);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remover(@PathVariable Long id) {
        service.remover(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/importar/{id}")
    @Transactional
    public ResponseEntity<?> importar(@PathVariable Long id) {
        Atendimento atendimento = service.buscarPor(id);

        MessageDto messageDto = service.podeImportarProntuario(atendimento);
        if (messageDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(messageDto);
        }
        AtendimentoDto dto = service.importarProntuario(atendimento);
        return ResponseEntity.ok().body(dto);
    }

    @PutMapping("/iniciar/{id}")
    @Transactional
    public ResponseEntity<AtendimentoDto> iniciar(@PathVariable Long id) {
        AtendimentoDto dto = service.ativar(id, StatusAtendimento.INICIADO);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/analisar/{id}")
    @Transactional
    public ResponseEntity<AtendimentoDto> analisar(@PathVariable Long id) {
        AtendimentoDto dto = service.ativar(id, StatusAtendimento.EM_ANALISE);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/autorizar/{id}")
    @Transactional
    public ResponseEntity<AtendimentoDto> autorizar(@PathVariable Long id) {
        AtendimentoDto dto = service.ativar(id, StatusAtendimento.AUTORIZADO);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/negar/{id}")
    @Transactional
    public ResponseEntity<AtendimentoDto> negar(@PathVariable Long id) {
        AtendimentoDto dto = service.ativar(id, StatusAtendimento.NEGADO);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }
}
