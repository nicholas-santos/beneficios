/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.prontuario;

import br.edu.ifms.beneficios.controller.dto.DependenteDto;
import br.edu.ifms.beneficios.controller.dto.GrupoSocioeducativoDto;
import br.edu.ifms.beneficios.controller.dto.HistoricoProntuarioDto;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDetalheDto;
import br.edu.ifms.beneficios.modelo.Prontuario;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class ProntuarioDetalheDto extends AbstractProntuario {
    private String descricaoSaude;
    private GrupoSocioeducativoDto grupoSocioeducativo;
    private PessoaDetalheDto titular;
    private BigDecimal valorTotalRendimentos;
    private BigDecimal valorTotalAuxilios;
    
    private List<DependenteDto> dependentes = new ArrayList();
    private List<HistoricoProntuarioDto> historicos = new ArrayList();

    public ProntuarioDetalheDto() {
    }

    public ProntuarioDetalheDto(Prontuario obj) {
        super(obj);
        this.descricaoSaude = obj.getDescricaoSaude();
        this.grupoSocioeducativo = new GrupoSocioeducativoDto(obj.getGrupoSocioeducativo());
        this.titular = new PessoaDetalheDto(obj.getTitular());
        this.valorTotalRendimentos = obj.getValorRendimentos();
        this.valorTotalAuxilios = obj.getValorAuxilios();
        this.dependentes = DependenteDto.converter(obj.getDependentes());
        this.historicos = HistoricoProntuarioDto.converter(obj.getHistoricos());
    }

    public String getDescricaoSaude() {
        return descricaoSaude;
    }

    public GrupoSocioeducativoDto getGrupoSocioeducativo() {
        return grupoSocioeducativo;
    }

    public PessoaDetalheDto getTitular() {
        return titular;
    }

    public BigDecimal getValorTotalRendimentos() {
        return valorTotalRendimentos;
    }

    public BigDecimal getValorTotalAuxilios() {
        return valorTotalAuxilios;
    }

    public List<DependenteDto> getDependentes() {
        return dependentes;
    }

    public List<HistoricoProntuarioDto> getHistoricos() {
        return historicos;
    }
    
    public static List<ProntuarioDetalheDto> converter(List<Prontuario> items) {
        return items.stream().map(ProntuarioDetalheDto::new).collect(Collectors.toList());
    }

    public static Page<ProntuarioDetalheDto> converter(Page<Prontuario> items) {
        return items.map(ProntuarioDetalheDto::new);
    }
}
