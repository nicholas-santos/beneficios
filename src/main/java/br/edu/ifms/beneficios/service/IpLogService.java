/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.modelo.IpLog;
import br.edu.ifms.beneficios.modelo.IpTracker;
import br.edu.ifms.beneficios.repository.IpLogRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author santos
 */
@Service
public class IpLogService implements IpTracker {

    private static final Logger LOG = LoggerFactory.getLogger(IpLogService.class);

    @Autowired
    private IpLogRepository ipLogRepository;

    @Override
    public void successfulLogin(String username, String ipAddress) {
        IpLog ipLog = new IpLog();

        ipLog.setIpAddress(ipAddress);
        ipLog.setLastLoginAttempt(System.currentTimeMillis());
        ipLog.setSuccessfulLogin(true);
        ipLog.setUsername(username);

        ipLogRepository.save(ipLog);
    }

    @Override
    public void unsuccessfulLogin(String username, String ipAddress) {
        IpLog ipLog = new IpLog();

        ipLog.setIpAddress(ipAddress);
        ipLog.setLastLoginAttempt(System.currentTimeMillis());
        ipLog.setSuccessfulLogin(false);
        ipLog.setUsername(username);

        ipLogRepository.save(ipLog);
    }

    @Override
    public List<IpLog> fetchIpFailureRecord(String ipAddress, Long startingTime) {
        List<IpLog> list = new ArrayList<>();

        if (ipAddress != null) {
            list = ipLogRepository.findByIpAddressAndSuccessfulLoginAndLastLoginAttemptGreaterThanEqual(ipAddress, false, startingTime);
        }

        return list;
    }
}
