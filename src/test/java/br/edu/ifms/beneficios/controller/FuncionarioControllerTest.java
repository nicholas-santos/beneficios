/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.FuncionarioForm;
import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.Logradouro;
import br.edu.ifms.beneficios.modelo.Pais;
import br.edu.ifms.beneficios.modelo.Uf;
import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.Funcao;
import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.id.CepId;
import br.edu.ifms.beneficios.modelo.id.EnderecoId;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;
import com.google.gson.Gson;
import java.net.URI;
import java.time.LocalDate;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class FuncionarioControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/funcionarios";
    private final String descricao = "Funcionario 1";
    private final String outraDescricao = "Func 2 ario";
    private Gson gson = new Gson();
    
    private static Cidade cidade;
    private static Bairro bairro;
    private static Endereco endereco;
    private static TipoUnidadeAtendimento tipoUnidadeAtendimento;
    private static Funcao funcao;
    private static UnidadeAtendimento unidadeAtendimento;

    @BeforeEach
    public void setUp() {
        BeneficiosFactory.MVC = mvc;
    }
    
    private void loadData() {
        Pais pais = BeneficiosFactory.createPais("Brasil");
        Uf uf = BeneficiosFactory.createUf("Mato Grosso do Sul", "MS", pais);
        cidade = BeneficiosFactory.createCidade("Navirai", uf);
        bairro = BeneficiosFactory.createBairro("Centro");
        
        Logradouro logradouro = BeneficiosFactory.createLogradouro("Rua 13 de fevereriro");
        
        CepId cepId = new CepId(cidade, bairro, logradouro);
        Cep cep = new Cep(cepId);
        
        EnderecoId enderecoId = new EnderecoId(cep, "1234");
        endereco = new Endereco(enderecoId);
        
        tipoUnidadeAtendimento = BeneficiosFactory.createTipoUnidadeAtendimento("GEAS");
        unidadeAtendimento = BeneficiosFactory.createUnidadeAtendimento("Gerencia", "GEAS", endereco, tipoUnidadeAtendimento, Boolean.FALSE);
        funcao = BeneficiosFactory.createFuncao("Gerente");
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosFuncionarios() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1Funcionario() throws Exception {        
        URI uri = new URI(String.format("%s/%d", sURI, 1));
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarFuncionarioComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmFuncionario() throws Exception {
        loadData();
        Funcionario obj = new Funcionario(null, descricao, LocalDate.now(), 
                funcao, unidadeAtendimento);
        obj.setSexo(Sexo.FEMININO);
        FuncionarioForm form = new FuncionarioForm(obj);

        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(form));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmFuncionario() throws Exception {
        Funcionario obj = BeneficiosFactory.createFuncionario(
                outraDescricao, LocalDate.now(), funcao, unidadeAtendimento, Sexo.FEMININO);
        FuncionarioForm form = new FuncionarioForm(obj);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        form.setNome(outraDescricao);
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(form));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString(outraDescricao)));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmFuncionario() throws Exception {
        URI uri = new URI(String.format("%s/%d", sURI, 1));
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmFuncionarioComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
}
