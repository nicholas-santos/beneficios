/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Pais;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class PaisDto {
    
    private Long id;
    private String nome;

    public PaisDto() {
    }

    public PaisDto(Pais obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
    
    public static List<PaisDto> converter(List<Pais> items) {
        return items.stream().map(PaisDto::new).collect(Collectors.toList());
    }
}
