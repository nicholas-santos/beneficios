package br.edu.ifms.beneficios.modelo.tipos;

import java.util.stream.Stream;

public enum AuditoriaTipo {
	INSERIR("INSERIR"), REMOVER("REMOVER"), ATUALIZAR("ATUALIZAR");

	private final String value;

	AuditoriaTipo(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}

	public static AuditoriaTipo of(String value) {
		return Stream.of(AuditoriaTipo.values()).filter(t -> t.getValue().equals(value)).findFirst()
				.orElseThrow(IllegalArgumentException::new);
	}

	@Override
	public String toString() {
		switch (value) {
		default:
		case "INSERIR":
			return "Inserir";
		case "REMOVER":
			return "Remover";
		case "ATUALIZAR":
			return "Atualizar";
		}
	}

}