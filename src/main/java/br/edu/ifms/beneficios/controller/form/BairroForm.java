/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.repository.BairroRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author santos
 */
public class BairroForm {
    
    @NotNull @NotEmpty @Length(min = 5)
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Bairro converter(BairroRepository repository) {
        Long maxId = repository.getMaxId() + 1;
        return new Bairro(maxId, this.nome);
    }
    
    public Bairro atualizar(Long id, BairroRepository bairroRepository) {
        Bairro bairro = bairroRepository.getOne(id);
        bairro.setNome(this.nome);
        return bairro;
    }
}
