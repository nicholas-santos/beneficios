/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.fornecedor;

import br.edu.ifms.beneficios.controller.dto.EnderecoDto;
import br.edu.ifms.beneficios.modelo.Fornecedor;
import java.util.Objects;

/**
 *
 * @author santos
 */
public abstract class AbstractFornecedorDto {

    private Long id;
    private String nome;
    private String codigo;
    private EnderecoDto enderecoDto;

    public AbstractFornecedorDto() {
    }

    public AbstractFornecedorDto(Fornecedor obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.codigo = obj.getCodigo();
        this.enderecoDto = new EnderecoDto(obj.getEndereco());
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public EnderecoDto getEnderecoDto() {
        return enderecoDto;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.id);
        hash = 61 * hash + Objects.hashCode(this.nome);
        hash = 61 * hash + Objects.hashCode(this.codigo);
        hash = 61 * hash + Objects.hashCode(this.enderecoDto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final AbstractFornecedorDto other = (AbstractFornecedorDto) obj;

        if (!Objects.equals(this.enderecoDto, other.getEnderecoDto())) {
            return false;
        }
        if (!Objects.equals(this.nome, other.getNome())) {
            return false;
        }
        if (!Objects.equals(this.codigo, other.getCodigo())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

}
