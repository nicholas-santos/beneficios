/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.OrgaoExpedidor;
import br.edu.ifms.beneficios.repository.OrgaoExpedidorRepository;

/**
 *
 * @author tanabe
 */
public class OrgaoExpedidorForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public OrgaoExpedidor converter() {
        return new OrgaoExpedidor(this.descricao);
    }

    public OrgaoExpedidor atualizar(Long id, OrgaoExpedidorRepository repository) {
    	OrgaoExpedidor orgaoExpedidor = repository.getOne(id);
    	orgaoExpedidor.setDescricao(this.descricao);

        return orgaoExpedidor;
    }
}
