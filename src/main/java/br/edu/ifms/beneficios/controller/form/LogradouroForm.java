/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.modelo.Logradouro;
import br.edu.ifms.beneficios.modelo.TipoLogradouro;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.TipoLogradouroRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author santos
 */
public class LogradouroForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String nome;
    private String tipoLogradouroNome;

    public void setNome(String nome) {
        this.nome = nome;
        /**
         * O tipo do logradouro sempre acompanhará o nome separado por um ' '
         */
        String aNome[] = this.nome.split(" ");
        this.tipoLogradouroNome = aNome[0];
    }

    public Logradouro converter(TipoLogradouroRepository tipoRepository) {
        TipoLogradouro tipo = ControllerHelper.getTipoLogradouro(tipoLogradouroNome, tipoRepository);
        return new Logradouro(this.nome, tipo);
    }

    public Logradouro atualizar(Long id,
            LogradouroRepository logradouroRepository,
            TipoLogradouroRepository tipoRepository) {
        TipoLogradouro tipo = ControllerHelper.getTipoLogradouro(tipoLogradouroNome, tipoRepository);
        Logradouro logradouro = logradouroRepository.findById(id).get();
        logradouro.setNome(this.nome);
        logradouro.setTipoLogradouro(tipo);
        return logradouro;
    }
}
