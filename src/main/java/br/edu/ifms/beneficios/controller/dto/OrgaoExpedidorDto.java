/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.OrgaoExpedidor;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class OrgaoExpedidorDto {

    private Long id;
    private String descricao;

    public OrgaoExpedidorDto() {
    }

    public OrgaoExpedidorDto(OrgaoExpedidor obj) {
        if (obj != null) {
            this.id = obj.getId();
            this.descricao = obj.getDescricao();
        }
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public static Page<OrgaoExpedidorDto> converter(Page<OrgaoExpedidor> items) {
        return items.map(OrgaoExpedidorDto::new);
    }

    public static List<OrgaoExpedidorDto> converter(List<OrgaoExpedidor> items) {
        return items.stream().map(OrgaoExpedidorDto::new).collect(Collectors.toList());
    }

}
