/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.CidadeDto;
import br.edu.ifms.beneficios.controller.form.CidadeForm;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.UfRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author santos
 */
@Service
public class CidadeService {

    @Autowired
    private CidadeRepository repository;

    @Autowired
    private UfRepository ufRepository;

    public Page<Cidade> listar(
            String nome, String nomeUf,
            Pageable paginacao
    ) {
        Boolean hasNomeCidade = nome != null && !nome.isBlank() && !nome.isEmpty(),
                hasNomeUF = nomeUf != null && !nomeUf.isBlank() && !nomeUf.isEmpty();
        Page<Cidade> items;
        if (hasNomeCidade && hasNomeUF) {
            items = repository.findByNomeContainingIgnoreCaseAndUfSigla(nome, nomeUf, paginacao);
        } else if (hasNomeCidade && !hasNomeUF) {
            items = repository.findByNomeContainingIgnoreCase(nome, paginacao);
        } else if (!hasNomeCidade && hasNomeUF) {
            items = repository.findByUfNome(nomeUf, paginacao);
        } else {
            items = repository.findAll(paginacao);
        }
        return items;
    }

    public List<Cidade> listar() {
        return repository.findAll();
    }

    public List<Cidade> listar(String nomeUf) {
        List<Cidade> items;
        if (nomeUf == null) {
            items = repository.findAll();
        } else {
            items = repository.findByUfNome(nomeUf);
        }
        return items;
    }

    public Optional<Cidade> buscarPor(Long id) {
        return repository.findById(id);
    }

    private String verificaDados(Cidade cidade) {
        Cidade op = repository.findByNomeAndUfSigla(cidade.getNome(), cidade.getUf().getSigla());

        if (op != null) {
            return String.format("O nome da Cidade %s vinculada ao UF %s "
                    + "já está cadastrada no sistema. "
                    + "Favor faça a busca primeiro antes de cadastrar "
                    + "novamente!", cidade.getNome(), cidade.getUf().getNome());
        }

        return null;
    }

    public CidadeDto criar(CidadeForm form) {
        Cidade cidade = form.converter(ufRepository);

        String msg = verificaDados(cidade);
        if (msg == null) {
            cidade = repository.save(cidade);
            return new CidadeDto(cidade);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
        }
    }

    public CidadeDto atualizar(Long id, CidadeForm form) {
        Cidade cidade = form.atualizar(id, ufRepository, repository);
        
        return new CidadeDto(cidade);
    }

    public Optional<Cidade> remover(Long id) {
        Optional<Cidade> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        }

        return optional;
    }
}
