/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.modelo.Pais;
import br.edu.ifms.beneficios.repository.PaisRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author santos
 */
public class PaisForm {
    
    @NotNull @NotEmpty
    private String nome;
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Pais converter() {
        return new Pais(this.nome);
    }
    
    public Pais atualizar(Long oid, PaisRepository paisRepository) {
        Pais pais = paisRepository.findById(oid).get();
        pais.setNome(this.nome);
        return pais;
    }
}
