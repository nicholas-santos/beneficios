/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDto;
import br.edu.ifms.beneficios.modelo.HistoricoProntuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class HistoricoProntuarioDto {

    private Long id;
    private LocalDateTime emissao;
    private Status status;
    private FuncionarioDto funcionario;
    private String observacao;

    public HistoricoProntuarioDto() {
    }

    public HistoricoProntuarioDto(HistoricoProntuario obj) {
        this.id = obj.getId();
        this.emissao = obj.getEmissao();
        this.status = obj.getStatus();
        this.funcionario = new FuncionarioDto(obj.getFuncionario());
        this.observacao = obj.getObservacao();
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public Status getStatus() {
        return status;
    }

    public FuncionarioDto getFuncionario() {
        return funcionario;
    }

    public String getObservacao() {
        return observacao;
    }

    public static List<HistoricoProntuarioDto> converter(List<HistoricoProntuario> items) {
        return items.stream().map(HistoricoProntuarioDto::new).collect(Collectors.toList());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.emissao);
        hash = 59 * hash + Objects.hashCode(this.status);
        hash = 59 * hash + Objects.hashCode(this.funcionario);
        hash = 59 * hash + Objects.hashCode(this.observacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final HistoricoProntuarioDto other = (HistoricoProntuarioDto) obj;
        if (!Objects.equals(this.observacao, other.getObservacao())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        if (!Objects.equals(this.funcionario, other.getFuncionario())) {
            return false;
        }
        return true;
    }


}
