/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDetalheDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.form.UnidadeAtendimentoForm;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.service.UnidadeAtendimentoService;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/unidades-de-atendimento")
public class UnidadeAtendimentoController {

    @Autowired
    private UnidadeAtendimentoService service;

    @GetMapping
    @Cacheable(value = "pageUnidadeAtendimento")
    public Page<UnidadeAtendimentoDetalheDto> listar(
            @PageableDefault(sort = "nome", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {
        return UnidadeAtendimentoDetalheDto.converter(service.listar(paginacao));
    }

    @GetMapping("/list")
    @Cacheable(value = "listaUnidadeAtendimento")
    public List<UnidadeAtendimentoDto> listar() {
        return UnidadeAtendimentoDto.converter(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UnidadeAtendimentoDetalheDto> visualizar(@PathVariable Long id) {
        Optional<UnidadeAtendimento> unidadeAtendimento = service.buscarPor(id);
        if (unidadeAtendimento.isPresent()) {
            return ResponseEntity.ok(new UnidadeAtendimentoDetalheDto(unidadeAtendimento.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageUnidadeAtendimento", "listaUnidadeAtendimento"}, allEntries = true)
    public ResponseEntity<?> criar(@RequestBody @Valid UnidadeAtendimentoForm form,
            UriComponentsBuilder uriBuilder) {
        MessageDto msgDto = service.isValido(null, form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            UnidadeAtendimentoDetalheDto dto = service.criar(form);

            URI uri = uriBuilder.path("/unidades-de-atendimento/{id}").buildAndExpand(dto.getId()).toUri();
            return ResponseEntity.created(uri).body(dto);
        }
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageUnidadeAtendimento", "listaUnidadeAtendimento"}, allEntries = true)
    public ResponseEntity<?> atualizar(
            @PathVariable Long id,
            @RequestBody @Valid UnidadeAtendimentoForm form) {
        MessageDto msgDto = service.isValido(id, form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            UnidadeAtendimentoDetalheDto dto = service.atualizar(id, form);
            if (dto != null) {
                return ResponseEntity.ok(dto);
            }
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageUnidadeAtendimento", "listaUnidadeAtendimento"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<UnidadeAtendimento> optional = service.remover(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

}
