/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.ContatoPessoa;
import br.edu.ifms.beneficios.modelo.AbstractPessoa;
import br.edu.ifms.beneficios.modelo.id.ContatoPessoaId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface ContatoPessoaRepository 
        extends JpaRepository<ContatoPessoa, ContatoPessoaId> {
    
    @Query("SELECT coalesce(max(cp.id.id), 0) FROM ContatoPessoa cp WHERE cp.id.pessoa = :pessoa")
    Long getMaxId(AbstractPessoa pessoa);
    
    void deleteByIdPessoa(AbstractPessoa pessoa);
    
}
