/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.ListaPerfilDto;
import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.PerfilDto;
import br.edu.ifms.beneficios.controller.form.PerfilForm;
import br.edu.ifms.beneficios.modelo.Perfil;
import br.edu.ifms.beneficios.modelo.PerfilMenu;
import br.edu.ifms.beneficios.repository.MenuRepository;
import br.edu.ifms.beneficios.repository.PerfilMenuRepository;
import br.edu.ifms.beneficios.repository.PerfilRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/perfis")
@Profile(value = {"prod", "test"})
public class PerfilController {

    @Autowired
    private PerfilRepository repository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private PerfilMenuRepository perfilMenuRepository;

    /**
     * Lista de Perfis .
     *
     * Este método é responsável por retornar uma lista de perfis cadastrados no
     * sistema.
     *
     * @param paginacao
     * @return
     */
    @GetMapping()
    @Cacheable(value = "pageDePerfis")
    public Page<PerfilDto> listar(
            @PageableDefault(sort = "nome", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

        Page<Perfil> perfis = repository.findAll(paginacao);
        return PerfilDto.converter(perfis);
    }

    @GetMapping("list")
    @Cacheable(value = "listaDePerfis")
    public List<ListaPerfilDto> listar() {
        List<Perfil> perfis = repository.findAll();
        return ListaPerfilDto.converter(perfis);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PerfilDto> visualizar(@PathVariable Long id) {
        Optional<Perfil> perfil = repository.findById(id);
        if (perfil.isPresent()) {
            return ResponseEntity.ok(new PerfilDto(perfil.get()));
        }

        return ResponseEntity.notFound().build();
    }

    private void updatePerfisMenus(Perfil perfil) {
        List<PerfilMenu> listaPerfilMenu = perfil.getMenus();
        perfilMenuRepository.deleteByIdPerfil(perfil);
        perfilMenuRepository.saveAll(listaPerfilMenu);
    }

    @PostMapping()
    @Transactional
    @CacheEvict(value = {"pageDePerfis", "listaDePerfis"}, allEntries = true)
    public ResponseEntity<PerfilDto> criar(@RequestBody @Valid PerfilForm form, UriComponentsBuilder uriBuilder) {
        Perfil perfil = form.converter(repository, menuRepository);
        repository.save(perfil);

        updatePerfisMenus(perfil);

        URI uri = uriBuilder.path("/perfis/{id}").buildAndExpand(perfil.getId()).toUri();
        return ResponseEntity.created(uri).body(new PerfilDto(perfil));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageDePerfis", "listaDePerfis"}, allEntries = true)
    public ResponseEntity<PerfilDto> atualizar(@PathVariable Long id, @RequestBody @Valid PerfilForm form) {
        Optional<Perfil> optional = repository.findById(id);
        if (optional.isPresent()) {
            Perfil perfil = form.atualizar(id, repository, menuRepository);

            updatePerfisMenus(perfil);

            return ResponseEntity.ok(new PerfilDto(perfil));
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageDePerfis", "listaDePerfis"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Perfil> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
