/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.ContatoPessoaId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author santos
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class ContatoPessoa implements Serializable {
    
    @EmbeddedId
    private ContatoPessoaId id;
    
    private String descricao;
    
    @Column(name = "status", nullable = true)
    @Convert(converter = StatusConverter.class)
    private Status status;
    
    @ManyToOne
    private Contato tipoContato;
    
    @Transient
    private Boolean deleted;

    public ContatoPessoa() {
        this.deleted = false;
    }

    public ContatoPessoa(ContatoPessoaId id, String descricao, Status status, 
            Contato tipoContato) {
        this();
        this.id = id;
        this.descricao = descricao;
        this.status = status;
        this.tipoContato = tipoContato;
    }

    public ContatoPessoaId getId() {
        return id;
    }

    public void setId(ContatoPessoaId id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Contato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(Contato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.descricao);
        hash = 97 * hash + Objects.hashCode(this.status);
        hash = 97 * hash + Objects.hashCode(this.tipoContato);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ContatoPessoa other = (ContatoPessoa) obj;
        if (!Objects.equals(this.descricao, other.getDescricao())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        if (!Objects.equals(this.tipoContato, other.getTipoContato())) {
            return false;
        }
        return true;
    }
    
    
}
