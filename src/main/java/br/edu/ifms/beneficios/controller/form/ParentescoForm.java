/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.Parentesco;
import br.edu.ifms.beneficios.repository.ParentescoRepository;

/**
 *
 * @author tanabe
 */
public class ParentescoForm {

    @NotNull
    @NotEmpty
    @Length(min = 3)
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Parentesco converter() {
        return new Parentesco(this.descricao);
    }

    public Parentesco atualizar(Long id, ParentescoRepository repository) {
    	Parentesco parentesco = repository.getOne(id);
    	parentesco.setDescricao(this.descricao);

        return parentesco;
    }
}
