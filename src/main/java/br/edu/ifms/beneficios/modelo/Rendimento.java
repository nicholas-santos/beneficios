/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.RendimentoId;
import br.edu.ifms.beneficios.modelo.tipos.LocalDateAttributeConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author santos
 */
@Entity
public class Rendimento implements Serializable {

    @EmbeddedId
    private RendimentoId id;

    @ManyToOne
    private CondicaoTrabalho condicaoTrabalho;

    @Column(name = "valor", precision = 6, scale = 2)
    private BigDecimal valor;

    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate admissao;

    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate demissao;
    
    @Transient
    private Boolean deleted;

    public Rendimento() {
        deleted = Boolean.FALSE;
    }

    public Rendimento(RendimentoId id, CondicaoTrabalho condicaoTrabalho,
            BigDecimal valor, LocalDate admissao, LocalDate demissao) {
        this();
        this.id = id;
        this.condicaoTrabalho = condicaoTrabalho;
        this.valor = valor;
        this.admissao = admissao;
        this.demissao = demissao;
    }
    
    public void update(Rendimento obj) {
        this.condicaoTrabalho = obj.getCondicaoTrabalho();
        this.valor = obj.getValor();
        this.admissao = obj.getAdmissao();
        this.demissao = obj.getDemissao();
    }

    public RendimentoId getId() {
        return id;
    }

    public void setId(RendimentoId id) {
        this.id = id;
    }

    public CondicaoTrabalho getCondicaoTrabalho() {
        return condicaoTrabalho;
    }

    public void setCondicaoTrabalho(CondicaoTrabalho condicaoTrabalho) {
        this.condicaoTrabalho = condicaoTrabalho;
    }

    public BigDecimal getValor() {
        if (demissao == null) {
            return valor;
        }
        return BigDecimal.ZERO;
    }

    public void setValor(BigDecimal rendimento) {
        this.valor = rendimento;
    }

    public LocalDate getAdmissao() {
        return admissao;
    }

    public void setAdmissao(LocalDate admissao) {
        this.admissao = admissao;
    }

    public LocalDate getDemissao() {
        return demissao;
    }

    public void setDemissao(LocalDate demissao) {
        this.demissao = demissao;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.condicaoTrabalho);
        hash = 97 * hash + Objects.hashCode(this.valor);
        hash = 97 * hash + Objects.hashCode(this.admissao);
        hash = 97 * hash + Objects.hashCode(this.demissao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Rendimento other = (Rendimento) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Rendimento{" + "id=" + id + ", condicaoTrabalho=" + condicaoTrabalho + ", valor=" + valor + ", admissao=" + admissao + ", demissao=" + demissao + '}';
    }

}
