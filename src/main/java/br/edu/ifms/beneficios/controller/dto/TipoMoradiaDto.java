/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.TipoMoradia;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class TipoMoradiaDto {
    private Long id;
    private String descricao;
    private Boolean complementar;

    public TipoMoradiaDto() {
    }

    public TipoMoradiaDto(TipoMoradia obj) {
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
        this.complementar = obj.isComplementar();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public Boolean getComplementar() {
        return complementar;
    }
    
    public static Page<TipoMoradiaDto> converter(Page<TipoMoradia> items) {
        return items.map(TipoMoradiaDto::new);
    }
    
    public static List<TipoMoradiaDto> converter(List<TipoMoradia> items) {
        return items.stream().map(TipoMoradiaDto::new).collect(Collectors.toList());
    }
}
