/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.AbstractPessoa;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class ContatoPessoaId implements Serializable {
    
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "pessoa_id", referencedColumnName = "id")
    private AbstractPessoa pessoa;

    public ContatoPessoaId() {
    }

    public ContatoPessoaId(Long id, AbstractPessoa pessoa) {
        this.id = id;
        this.pessoa = pessoa;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AbstractPessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(AbstractPessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.pessoa);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ContatoPessoaId other = (ContatoPessoaId) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.pessoa.getId(), other.getPessoa().getId())) {
            return false;
        }
        return true;
    }
    
}
