/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.helper;

import br.edu.ifms.beneficios.controller.dto.EnderecoDto;
import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.Logradouro;
import br.edu.ifms.beneficios.modelo.TipoLogradouro;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.id.CepId;
import br.edu.ifms.beneficios.modelo.id.EnderecoId;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.CepRepository;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.EnderecoRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.TipoLogradouroRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import java.util.Optional;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author santos
 */
public class ControllerHelper {

    /**
     * Recuperação de um bairro.
     * <p>
     * Método utilizado para buscar um Bairro existente. Caso contrário, cria um
     * novo.</p>
     *
     * @param nome
     * @param repository
     * @return
     */
    public static Bairro getBairro(String nome, BairroRepository repository) {
        Optional<Bairro> optional = repository.findByNomeIgnoreCase(nome);
        if (optional.isPresent()) {
            return optional.get();
        }
        Long maxId = repository.getMaxId() + 1;
        Bairro object = new Bairro(maxId, nome);

        repository.save(object);
        return object;
    }

    /**
     * Recuperação de um tipo de logradouro.
     * <p>
     * Método utilizado para buscar um TipoLogradouro existente. Caso contrário,
     * cria um novo.</p>
     *
     * @param nome
     * @param repository
     * @return
     */
    public static TipoLogradouro getTipoLogradouro(String nome, TipoLogradouroRepository repository) {
        Optional<TipoLogradouro> optional = repository.findByNomeIgnoreCase(nome);
        if (optional.isPresent()) {
            return optional.get();
        }
        Long maxId = repository.getMaxId() + 1;
        TipoLogradouro object = new TipoLogradouro(maxId, nome);

        repository.save(object);
        return object;
    }

    /**
     * Recuperação de um logradouro.
     * <p>
     * Método utilizado para buscar um Logradouro existente. Caso contrário,
     * cria um novo.</p>
     *
     * @param nome
     * @param repository
     * @param tipoRepository
     * @return
     */
    public static Logradouro getLogradouro(String nome,
            LogradouroRepository repository, TipoLogradouroRepository tipoRepository) {
        Optional<Logradouro> optional = repository.findByNomeIgnoreCase(nome);
        if (optional.isPresent()) {
            return optional.get();
        }

        String aLogradouro[] = nome.trim().split(" ");
        TipoLogradouro tipo = getTipoLogradouro(aLogradouro[0].trim(), tipoRepository);

        Long maxId = repository.getMaxId() + 1;
        Logradouro object = new Logradouro(maxId, nome, tipo);
        repository.save(object);
        return object;
    }
    
    public static Cep getCep(
            EnderecoDto enderecoDto,
            CepRepository repository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository) {
        return getCep(enderecoDto.getCidade().getId(),
                enderecoDto.getBairro().getId(),
                enderecoDto.getLogradouro().getId(),
                repository, cidadeRepository, bairroRepository,
                logradouroRepository);
    }
    
    public static Cep getCep(
            Long cidadeId, Long bairroId,
            Long logradouroId, CepRepository repository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository) {

        Cidade cidade = cidadeRepository.getOne(cidadeId);
        Bairro bairro = bairroRepository.getOne(bairroId);
        Logradouro logradouro = logradouroRepository.getOne(logradouroId);

        CepId cepId = new CepId(cidade, bairro, logradouro);
        Optional<Cep> optional = repository.findById(cepId);
        if (optional.isPresent()) {
            return optional.get();
        }

        Cep object = new Cep(cepId);
        repository.save(object);
        return object;
    }
    
    public static Endereco getEndereco(
            Cep cep, String numero, String complemento,
            String referencia,
            EnderecoRepository repository) {

        EnderecoId enderecoId = new EnderecoId(cep, numero);
        Optional<Endereco> optional = repository.findById(enderecoId);
        if (optional.isPresent()) {
            return optional.get();
        }

        Endereco object = new Endereco(enderecoId);
        object.setComplemento(complemento);
        object.setReferencia(referencia);
        repository.save(object);
        return object;
    }

    /**
     * Recuperação de um endereco.
     * <p>
     * Método utilizado para buscar um Endereco existente. Caso contrário, cria
     * um novo.</p>
     *
     * @param dto
     * @param repository
     * @param cepRepository
     * @param cidadeRepository
     * @param bairroRepository
     * @param logradouroRepository
     * @return
     */
    public static Endereco getEndereco(EnderecoDto dto,
            EnderecoRepository repository, CepRepository cepRepository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository) {
        
        Cep cep = getCep(
                dto.getCidade().getId(),
                dto.getBairro().getId(),
                dto.getLogradouro().getId(),
                cepRepository, cidadeRepository, bairroRepository,
                logradouroRepository);

        return getEndereco(cep, dto.getNumero(), dto.getComplemento(), 
                dto.getReferencia(), repository);
    }

    /**
     * Recuperação de usuário logado.Método responsável por recuperar o usuário
     * logado no sistema. Esse procedimento é utilizado para não gerar erro ao
     * executar uma transação
     *
     * @param repository
     * @return
     */
    public static Usuario getLoggedUser(UsuarioRepository repository) {
        Usuario usuario = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<Usuario> optional = repository.findDetailUsuarioById(usuario.getId());
        return optional.get();
    }
}
