/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.DependenteId;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author santos
 */
//@EntityListeners(AuditoriaListener.class)
@Entity
public class Dependente implements Serializable {
    
    @EmbeddedId
    private DependenteId id;
    
    @ManyToOne
    private Parentesco parentesco;
    
    @Transient
    private Boolean deleted;

    public Dependente() {
        deleted = Boolean.FALSE;
    }

    public Dependente(DependenteId id, Parentesco parentesco) {
        this();
        this.id = id;
        this.parentesco = parentesco;
    }

    public DependenteId getId() {
        return id;
    }

    public void setId(DependenteId id) {
        this.id = id;
    }

    public Parentesco getParentesco() {
        return parentesco;
    }

    public void setParentesco(Parentesco parentesco) {
        this.parentesco = parentesco;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.parentesco);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Dependente other = (Dependente) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.parentesco, other.getParentesco())) {
            return false;
        }
        return true;
    }
    
}
