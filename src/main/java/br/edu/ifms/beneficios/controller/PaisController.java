/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.PaisDto;
import br.edu.ifms.beneficios.controller.form.PaisForm;
import br.edu.ifms.beneficios.modelo.Pais;
import br.edu.ifms.beneficios.repository.PaisRepository;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/pais")
public class PaisController {

    @Autowired
    private PaisRepository repository;

    /**
     * Lista de países.Este método é responsável por retornar uma lista de países cadastrados no
 sistema.
     * 
     *
     * @return
     */
    @GetMapping
    @Cacheable(value = "listaDePaises")
    public List<PaisDto> listar() {
        List<Pais> items = repository.findAll();
        return PaisDto.converter(items);
    }

    /**
     * Visualizar país.
     *
     * Este método é responsável por retornar um objeto da classe
     * <code>País</code>
     *
     * @param id Número de identificação do país.
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<PaisDto> visualizar(@PathVariable Long id) {
        Optional<Pais> pais = repository.findById(id);
        if (pais.isPresent()) {
            return ResponseEntity.ok(new PaisDto(pais.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"listaDePaises"}, allEntries = true)
    public ResponseEntity<PaisDto> cadastrar(@RequestBody @Valid PaisForm form, 
            UriComponentsBuilder uriBuilder) {
        Pais pais = form.converter();
        repository.save(pais);

        URI uri = uriBuilder.path("/pais/{id}")
                .buildAndExpand(pais.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new PaisDto(pais));
    }

}
