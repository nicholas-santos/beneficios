/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Estoque;
import br.edu.ifms.beneficios.modelo.id.EstoqueId;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface EstoqueRepository extends JpaRepository<Estoque, EstoqueId> {

    Optional<Estoque> findByIdBeneficioEventualIdAndIdUnidadeAtendimentoId(Long beneficioEventualId, Long unidadeAtendimentoId);
}
