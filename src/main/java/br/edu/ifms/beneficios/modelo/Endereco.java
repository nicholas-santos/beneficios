/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.EnderecoId;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 *
 * @author santos
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class Endereco implements Serializable {
    
    @EmbeddedId
    private EnderecoId id;
    private String complemento;
    private String referencia;

    public Endereco() {
    }

    public Endereco(EnderecoId id) {
        this.id = id;
    }

    public Endereco(EnderecoId id, String complemento, String referencia) {
        this.id = id;
        this.complemento = complemento;
        this.referencia = referencia;
    }

    public EnderecoId getId() {
        return id;
    }

    public void setId(EnderecoId id) {
        this.id = id;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.complemento);
        hash = 83 * hash + Objects.hashCode(this.referencia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Endereco other = (Endereco) obj;
        if (!Objects.equals(this.complemento, other.getComplemento())) {
            return false;
        }
        if (!Objects.equals(this.referencia, other.getReferencia())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Endereco{" + "id=" + id + ", complemento=" + complemento + ", referencia=" + referencia + '}';
    }
    
}
