/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.ProgramaGoverno;
import br.edu.ifms.beneficios.repository.ProgramaGovernoRepository;

/**
 *
 * @author tanabe
 */
public class ProgramaGovernoForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ProgramaGoverno converter() {
        return new ProgramaGoverno(this.descricao);
    }

    public ProgramaGoverno atualizar(Long id, ProgramaGovernoRepository repository) {
    	ProgramaGoverno programaGoverno = repository.getOne(id);
    	programaGoverno.setDescricao(this.descricao);

        return programaGoverno;
    }
}
