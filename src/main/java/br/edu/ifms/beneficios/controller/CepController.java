/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.CepDto;
import br.edu.ifms.beneficios.controller.form.CepForm;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.repository.CepRepository;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/ceps")
@Profile(value = {"prod", "test"})
public class CepController {

    @Autowired
    private CepRepository repository;

    @Autowired
    private CidadeRepository cidadeRepository;

    @Autowired
    private BairroRepository bairroRepository;

    @Autowired
    private LogradouroRepository logradouroRepository;
    
    @GetMapping
    @Cacheable(value = "pageCeps")
    public Page<CepDto> listar(
            @PageableDefault(
                    page = 0, size = 10) 
                    Pageable paginacao) {

        Page<Cep> items = repository.findAll(paginacao);
        return CepDto.converter(items);
    }
    
    @GetMapping("/list")
    @Cacheable(value = "listaDeCeps")
    public List<CepDto> listar() {

        List<Cep> items = repository.findAll();
        return CepDto.converter(items);
    }
    
    @GetMapping("/{cidadeId}/{bairroId}/{logradouroId}")
    public ResponseEntity<CepDto> visualizar(
            @PathVariable Long cidadeId,
            @PathVariable Long bairroId,
            @PathVariable Long logradouroId) {

        Optional<Cep> cep = repository
                .findByIdCidadeIdAndIdBairroIdAndIdLogradouroId(
                cidadeId, bairroId, logradouroId);
        if (cep.isPresent()) {
            return ResponseEntity.ok(new CepDto(cep.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageCeps", "listaDeCeps"}, allEntries = true)
    public ResponseEntity<CepDto> cadastrar(@RequestBody @Valid CepForm form, 
            UriComponentsBuilder uriBuilder) {
        
        Cep cep = form.converter(cidadeRepository, bairroRepository,
                logradouroRepository);
        repository.save(cep);
        
        URI uri = uriBuilder.path("/ceps/{cidadeId}/{bairroId}/{logradouroId}")
                .buildAndExpand(cep.getId().getCidade().getId(),
                        cep.getId().getBairro().getId(),
                        cep.getId().getLogradouro().getId())
                .toUri();
        return ResponseEntity.created(uri).body(new CepDto(cep));
    }

    @DeleteMapping("/{cidadeId}/{bairroId}/{logradouroId}")
    @Transactional
    @CacheEvict(value = {"pageCeps", "listaDeCeps"}, allEntries = true)
    public ResponseEntity<?> remover(
            @PathVariable Long cidadeId,
            @PathVariable Long bairroId,
            @PathVariable Long logradouroId) {
        Optional<Cep> optional = repository
                .findByIdCidadeIdAndIdBairroIdAndIdLogradouroId(cidadeId, bairroId, logradouroId);
        if (optional.isPresent()) {
            Cep cep = optional.get();
            repository.deleteById(cep.getId());
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}
