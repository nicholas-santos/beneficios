/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimento;
import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimentoConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Type;

/**
 *
 * @author santos
 */
//@EntityListeners(AuditoriaListener.class)
@Entity
public class Atendimento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Funcionario atendente;

    @ManyToOne
    private Prontuario prontuario;

    @ManyToOne
    private Pessoa pessoa;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime emissao;

    @Lob
    @Type(type = "text")
    private String descricao;

    @Column(name = "status", nullable = false)
    @Convert(converter = StatusAtendimentoConverter.class)
    private StatusAtendimento status;    
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "atendimento_analise",
            joinColumns = {
                @JoinColumn(name = "atendimento_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "analise_id", referencedColumnName = "id")}
    )
    private Analise analise;

    @OneToMany(mappedBy = "atendimento", fetch = FetchType.LAZY)
    private List<HistoricoAtendimento> historicos = new ArrayList();

    public Atendimento() {
        this.emissao = LocalDateTime.now();
        this.status = StatusAtendimento.ABERTO;
    }

    public Atendimento(Long id, Funcionario atendente,
            Prontuario prontuario, Pessoa pessoa,
            LocalDateTime emissao, String descricao,
            StatusAtendimento status) {
        this.id = id;
        this.atendente = atendente;
        this.prontuario = prontuario;
        this.pessoa = pessoa;
        this.emissao = emissao;
        this.descricao = descricao;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Funcionario getAtendente() {
        return atendente;
    }

    public void setAtendente(Funcionario atendente) {
        this.atendente = atendente;
    }

    public Prontuario getProntuario() {
        return prontuario;
    }

    public void setProntuario(Prontuario prontuario) {
        this.prontuario = prontuario;
    }

    public Boolean hasProntuario() {
        return prontuario != null;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDateTime emissao) {
        this.emissao = emissao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public StatusAtendimento getStatus() {
        return status;
    }

    public void setStatus(StatusAtendimento status) {
        this.status = status;
    }
    
    public void autorizar() {
        this.status = StatusAtendimento.AUTORIZADO;
    }
    
    public void negar(){
        this.status = StatusAtendimento.NEGADO;
    }

    public Analise getAnalise() {
        return analise;
    }

    public void setAnalise(Analise analise) {
        this.analise = analise;
    }
    
    public Boolean hasAnalise() {
        return this.analise != null;
    }

    public List<HistoricoAtendimento> getHistoricos() {
        return historicos;
    }

    public void setHistoricos(List<HistoricoAtendimento> historicos) {
        this.historicos = historicos;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.atendente);
        hash = 97 * hash + Objects.hashCode(this.prontuario);
        hash = 97 * hash + Objects.hashCode(this.pessoa);
        hash = 97 * hash + Objects.hashCode(this.emissao);
        hash = 97 * hash + Objects.hashCode(this.descricao);
        hash = 97 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Atendimento other = (Atendimento) obj;
        if (!Objects.equals(this.descricao, other.getDescricao())) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.atendente, other.getAtendente())) {
            return false;
        }
        if (!Objects.equals(this.prontuario, other.getProntuario())) {
            return false;
        }
        if (!Objects.equals(this.pessoa, other.getPessoa())) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        if (this.analise != other.getAnalise()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Atendimento{" + "id=" + id + ", atendente=" + atendente + ", prontuario=" + 
                prontuario + ", pessoa=" + pessoa + ", emissao=" + emissao + ", descricao=" + 
                descricao + ", status=" + status + '}';
    }

}
