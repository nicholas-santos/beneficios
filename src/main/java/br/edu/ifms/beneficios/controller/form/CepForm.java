/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.BairroDto;
import br.edu.ifms.beneficios.controller.dto.CidadeDto;
import br.edu.ifms.beneficios.controller.dto.LogradouroDto;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.modelo.Logradouro;
import br.edu.ifms.beneficios.modelo.id.CepId;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;

/**
 *
 * @author santos
 */
public class CepForm {

    private CidadeDto cidade;
    private BairroDto bairro;
    private LogradouroDto logradouro;

    public CidadeDto getCidade() {
        return cidade;
    }

    public void setCidade(CidadeDto cidade) {
        this.cidade = cidade;
    }

    public BairroDto getBairro() {
        return bairro;
    }

    public void setBairro(BairroDto bairro) {
        this.bairro = bairro;
    }

    public LogradouroDto getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(LogradouroDto logradouro) {
        this.logradouro = logradouro;
    }
    
    public Cep converter(CidadeRepository cidadeRepository,
            BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository) {
        Cidade city = cidadeRepository.getOne(cidade.getId());
        Bairro neighbor = bairroRepository.getOne(bairro.getId());
        Logradouro logra = logradouroRepository.getOne(logradouro.getId());
        
        return new Cep(new CepId(city, neighbor, logra));
    }
}
