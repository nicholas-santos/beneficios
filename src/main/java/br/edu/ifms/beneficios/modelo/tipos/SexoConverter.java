/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.tipos;

import javax.persistence.AttributeConverter;

/**
 *
 * @author santos
 */
public class SexoConverter  implements AttributeConverter<Sexo, String> {

    @Override
    public String convertToDatabaseColumn(Sexo sexo) {
        if (sexo == null) {
            return null;
        }
        return sexo.toString();
    }

    @Override
    public Sexo convertToEntityAttribute(String sexo) {
        if (sexo == null)
            return null;
        return Sexo.valueOf(sexo);
    }
    
}
