package br.edu.ifms.beneficios.audit;

import br.edu.ifms.beneficios.helper.ControllerHelper;
import java.net.URI;
import java.time.LocalDateTime;

import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import br.edu.ifms.beneficios.modelo.Auditoria;
import br.edu.ifms.beneficios.modelo.tipos.AuditoriaTipo;
import br.edu.ifms.beneficios.repository.AuditoriaRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;

@Component
public class AuditoriaListener {

    static private AuditoriaRepository repository;
    static private UsuarioRepository usuarioRepository;

    /**
     * Valor Anterior do objeto, antes de ser alterado pelo usuário
     */
    private String objetoCarregado;

    @Autowired
    public void init(AuditoriaRepository repository, UsuarioRepository usuarioRepository) {
        AuditoriaListener.repository = repository;
        AuditoriaListener.usuarioRepository = usuarioRepository;
    }

    @PrePersist
    private void beforeInsert(Object object) {
        //System.out.println("AUDIT INSERT");

        Auditoria auditoria = new Auditoria();

        auditoria.setOperacao(AuditoriaTipo.INSERIR);
        auditoria.setUsuario(ControllerHelper.getLoggedUser(usuarioRepository));
        auditoria.setDadosAnterior("");
        auditoria.setDadosAtual(toJson(object));
        auditoria.setUrl(getUrl());
        LocalDateTime now = LocalDateTime.now();
        auditoria.setCreated(now);

        repository.save(auditoria);
    }

    @PreUpdate
    private void beforeUpdate(Object object) {
        //System.out.println("AUDIT UPDATE");

        Auditoria auditoria = new Auditoria();

        auditoria.setOperacao(AuditoriaTipo.ATUALIZAR);
        auditoria.setUsuario(ControllerHelper.getLoggedUser(usuarioRepository));
        auditoria.setDadosAnterior(toJson(objetoCarregado));
        auditoria.setDadosAtual(toJson(object));
        auditoria.setUrl(getUrl());
        LocalDateTime now = LocalDateTime.now();
        auditoria.setCreated(now);

        repository.save(auditoria);
    }

    @PreRemove
    private void beforeRemove(Object object) {
        //System.out.println("AUDIT REMOVE");

        Auditoria auditoria = new Auditoria();

        auditoria.setOperacao(AuditoriaTipo.REMOVER);
        auditoria.setUsuario(ControllerHelper.getLoggedUser(usuarioRepository));
        auditoria.setDadosAnterior(toJson(object));
        auditoria.setDadosAtual("");
        auditoria.setUrl(getUrl());
        LocalDateTime now = LocalDateTime.now();
        auditoria.setCreated(now);

        repository.save(auditoria);
    }

    /**
     * Pega a URL que o usuário estava acessando ao realizar a ação
     *
     * @return string url
     */
    private String getUrl() {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        URI newUri = builder.build().toUri();
        return newUri.toString();
    }

    /*
	 * Pega o valor do objetos antes da alteração do usuário
     */
    @PostLoad
    private void getObjectBeforeChange(Object object) {
        //System.out.println("AUDIT POST LOAD");

        this.objetoCarregado = object.toString();
    }

    /**
     * Convert um objeto em um json
     *
     * @param object
     * @return json
     */
    private String toJson(Object object) {

        Gson g = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("Entidade", object.getClass().getSimpleName());
        j.addProperty("Dados", g.toJson(object));
        return j.toString();

    }
}
