/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
public class HistoricoItemAnalise extends AbstractHistorico {

    @ManyToOne
    private ItemAnalise itemAnalise;
    
    private String observacao;

    public HistoricoItemAnalise() {
        super();
    }

    public HistoricoItemAnalise(Long id, ItemAnalise itemAnalise, LocalDateTime emissao, Status status) {
        super(id, emissao, status);
        this.itemAnalise = itemAnalise;
    }
    
    public HistoricoItemAnalise(Long id, ItemAnalise itemAnalise,
            Funcionario funcionario) {
        super();
        super.setId(id);
        super.setFuncionario(funcionario);
        this.itemAnalise = itemAnalise;
        this.setStatus(itemAnalise.getStatus());
    }

    public ItemAnalise getItemAnalise() {
        return itemAnalise;
    }

    public void setItemAnalise(ItemAnalise itemAnalise) {
        this.itemAnalise = itemAnalise;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.itemAnalise);
        hash = 59 * hash + Objects.hashCode(this.observacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final HistoricoItemAnalise other = (HistoricoItemAnalise) obj;
        if (!Objects.equals(this.observacao, other.getObservacao())) {
            return false;
        }
        if (!Objects.equals(this.itemAnalise, other.getItemAnalise())) {
            return false;
        }
        return true;
    }
    
}
