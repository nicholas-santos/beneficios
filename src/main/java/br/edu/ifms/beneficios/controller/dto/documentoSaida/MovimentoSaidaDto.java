/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoSaida;

import br.edu.ifms.beneficios.controller.dto.documentoEntrada.*;
import br.edu.ifms.beneficios.modelo.MovimentoEntrada;
import br.edu.ifms.beneficios.modelo.MovimentoSaida;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class MovimentoSaidaDto extends MovimentoDto {
    
    private ItemSaidaDetalheDto itemEntrada;

    public MovimentoSaidaDto() {
    }

    public MovimentoSaidaDto(MovimentoSaida obj) {
        super(obj);
        this.itemEntrada = new ItemSaidaDetalheDto(obj.getItemSaida());
    }

    public ItemSaidaDetalheDto getItemEntrada() {
        return itemEntrada;
    }

    public static Page<MovimentoSaidaDto> converter(Page<MovimentoSaida> items) {
        return items.map(MovimentoSaidaDto::new);
    }

    public static List<MovimentoSaidaDto> converter(List<MovimentoSaida> items) {
        return items.stream().map(MovimentoSaidaDto::new).collect(Collectors.toList());
    }
    
}
