/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class TipoUnidadeAtendimentoDto {
    
    private Long id;
    private String nome;

    public TipoUnidadeAtendimentoDto() {
    }

    public TipoUnidadeAtendimentoDto(TipoUnidadeAtendimento obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
    
    public static List<TipoUnidadeAtendimentoDto> converter(List<TipoUnidadeAtendimento> items) {
        return items.stream().map(TipoUnidadeAtendimentoDto::new).collect(Collectors.toList());
    }
    
    public static Page<TipoUnidadeAtendimentoDto> converter(Page<TipoUnidadeAtendimento> items) {
        return items.map(TipoUnidadeAtendimentoDto::new);
    }
    
}
