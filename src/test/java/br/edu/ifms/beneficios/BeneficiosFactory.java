/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios;

import br.edu.ifms.beneficios.controller.form.FuncionarioForm;
import br.edu.ifms.beneficios.controller.form.PessoaForm;
import br.edu.ifms.beneficios.controller.form.UnidadeAtendimentoForm;
import br.edu.ifms.beneficios.controller.form.UsuarioForm;
import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Contato;
import br.edu.ifms.beneficios.modelo.Documento;
import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.Escolaridade;
import br.edu.ifms.beneficios.modelo.Funcao;
import br.edu.ifms.beneficios.modelo.GrupoSocioeducativo;
import br.edu.ifms.beneficios.modelo.Logradouro;
import br.edu.ifms.beneficios.modelo.Menu;
import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.OrgaoExpedidor;
import br.edu.ifms.beneficios.modelo.Pais;
import br.edu.ifms.beneficios.modelo.Parentesco;
import br.edu.ifms.beneficios.modelo.Perfil;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.ProgramaGoverno;
import br.edu.ifms.beneficios.modelo.TipoLogradouro;
import br.edu.ifms.beneficios.modelo.TipoMoradia;
import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.Uf;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.MenuTipo;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.TipoContato;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

/**
 *
 * @author santos
 */
public class BeneficiosFactory {

    public static MockMvc MVC;
    private static Gson gson = new Gson();

    private static MvcResult execute(String url, String json) {
        try {
            URI uri = new URI(url);
            RequestBuilder request = MockRequestBuilderHelper
                    .post(uri, json);
            return MVC.perform(request).andReturn();
        } catch (URISyntaxException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Pais createPais(String nome) {
        Pais obj = new Pais(nome);

        try {
            MvcResult mvcResult = execute("/api/pais", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Uf createUf(String nome, String sigla, Pais pais) {
        Uf obj = new Uf(nome, sigla, pais);

        try {
            MvcResult mvcResult = execute("/api/ufs", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Cidade createCidade(String nome, Uf uf) {
        Cidade obj = new Cidade(nome, uf);

        try {
            MvcResult mvcResult = execute("/api/cidades", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static TipoLogradouro createTipoLogradouro(String nome) {
        TipoLogradouro obj = new TipoLogradouro();
        obj.setNome(nome);

        try {
            MvcResult mvcResult = execute("/api/tipos-de-logradouros", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Documento createDocumento(String nome, Boolean exigeOrgaoExpedidor) {
        Documento documento = new Documento();
        documento.setDescricao(nome);
        documento.setExigeOrgaoExpeditor(exigeOrgaoExpedidor);

        try {
            MvcResult mvcResult = execute("/api/documentos", gson.toJson(documento));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            documento.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return documento;
    }

    public static Escolaridade createEscolaridade(String nome) {
        Escolaridade obj = new Escolaridade();
        obj.setDescricao(nome);

        try {
            MvcResult mvcResult = execute("/api/escolaridades", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Funcao createFuncao(String nome) {
        Funcao obj = new Funcao();
        obj.setNome(nome);

        try {
            MvcResult mvcResult = execute("/api/funcoes", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static GrupoSocioeducativo createGrupoSocioeducativo(String nome) {
        GrupoSocioeducativo obj = new GrupoSocioeducativo();
        obj.setNome(nome);

        try {
            MvcResult mvcResult = execute("/api/grupo-socioeducativo", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static OrgaoExpedidor createOrgaoExpedidor(String nome) {
        OrgaoExpedidor obj = new OrgaoExpedidor();
        obj.setDescricao(nome);

        try {
            MvcResult mvcResult = execute("/api/orgaos-expedidores", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Parentesco createParentesco(String descricao) {
        Parentesco obj = new Parentesco(descricao);

        try {
            MvcResult mvcResult = execute("/api/parentescos", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Perfil createPerfil(String nome, Status status) {
        Perfil obj = new Perfil(nome, status);

        try {
            MvcResult mvcResult = execute("/api/perfis", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static ProgramaGoverno createProgramaGoverno(String descricao) {
        ProgramaGoverno obj = new ProgramaGoverno(descricao);

        try {
            MvcResult mvcResult = execute("/api/programas-de-governo", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Contato createContato(String descricao, TipoContato tipo) {
        Contato obj = new Contato(descricao, tipo);

        try {
            MvcResult mvcResult = execute("/api/contatos", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static TipoMoradia createTipoMoradia(String descricao) {
        TipoMoradia obj = new TipoMoradia();
        obj.setDescricao(descricao);

        try {
            MvcResult mvcResult = execute("/api/tipos-de-moradia", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static TipoUnidadeAtendimento createTipoUnidadeAtendimento(String nome) {
        TipoUnidadeAtendimento obj = new TipoUnidadeAtendimento(nome);

        try {
            MvcResult mvcResult = execute("/api/tipos-de-unidades-atendimento", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Bairro createBairro(String nome) {
        Bairro obj = new Bairro(nome);

        try {
            MvcResult mvcResult = execute("/api/bairros", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Logradouro createLogradouro(String nome) {
        Logradouro obj = new Logradouro(nome, null);

        try {
            MvcResult mvcResult = execute("/api/logradouros", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Menu createMenu(String nome, String remotePath, Boolean disponivel, MenuTipo tipo) {
        Menu obj = new Menu(nome, remotePath, disponivel);
        obj.setTipo(tipo);

        try {
            MvcResult mvcResult = execute("/api/menus", gson.toJson(obj));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static UnidadeAtendimento createUnidadeAtendimento(String nome,
            String numeroDaUnidade, Endereco endereco,
            TipoUnidadeAtendimento tipoUnidadeAtendimento,
            Boolean isMatriz) {
        UnidadeAtendimento obj = new UnidadeAtendimento(nome, numeroDaUnidade, endereco, tipoUnidadeAtendimento);
        obj.setMatriz(isMatriz);
        UnidadeAtendimentoForm form = new UnidadeAtendimentoForm(obj);

        try {
            MvcResult mvcResult = execute("/api/unidades-de-atendimento", gson.toJson(form));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Funcionario createFuncionario(String nome,
            LocalDate nascimento, Funcao funcao, UnidadeAtendimento unidadeAtendimento,
            Sexo sexo) {
        Funcionario obj = new Funcionario(null, nome, nascimento, funcao, unidadeAtendimento);
        obj.setSexo(sexo);
        
        FuncionarioForm form = new FuncionarioForm(obj);

        try {
            MvcResult mvcResult = execute("/api/funcionarios", gson.toJson(form));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public static Pessoa createPessoa(String nome, Escolaridade escolaridade, 
            LocalDate nascimento, Sexo sexo) {
        Pessoa obj = new Pessoa(null, nome, escolaridade, nascimento, sexo);
        
        PessoaForm form = new PessoaForm(obj);

        try {
            MvcResult mvcResult = execute("/api/pessoas", gson.toJson(form));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
    
    public static Usuario createUsuario(String nome, String email, 
            Status status, Funcionario funcionario, List<Perfil> perfis) {
        Usuario obj = new Usuario(nome, email, status, Boolean.TRUE);
        obj.setFuncionario(funcionario);
        obj.setPerfis(perfis);
        
        UsuarioForm form = new UsuarioForm(obj);

        try {
            MvcResult mvcResult = execute("/api/usuarios", gson.toJson(form));

            String response = mvcResult.getResponse().getContentAsString();
            Integer id = JsonPath.parse(response).read("$.id");

            obj.setId(Long.valueOf(id));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeneficiosFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

}
