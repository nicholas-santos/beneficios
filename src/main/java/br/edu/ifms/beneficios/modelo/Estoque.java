/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.EstoqueId;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 *
 * @author santos
 */
@Entity
public class Estoque implements Serializable {

    @EmbeddedId
    private EstoqueId id;

    private BigDecimal quantidade;

    public Estoque() {
        this.quantidade = BigDecimal.ZERO;
    }

    public Estoque(EstoqueId id, BigDecimal quantidade) {
        this();
        this.id = id;
        this.quantidade = quantidade;
    }

    public EstoqueId getId() {
        return id;
    }

    public void setId(EstoqueId id) {
        this.id = id;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public void add(BigDecimal quantidade) {
        this.quantidade = this.quantidade.add(quantidade);
    }

    public void subtract(BigDecimal quantidade) {
        this.quantidade = this.quantidade.subtract(quantidade);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.quantidade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Estoque other = (Estoque) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.quantidade, other.getQuantidade())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Estoque{" + "id=" + id + ", quantidade=" + quantidade + '}';
    }

}
