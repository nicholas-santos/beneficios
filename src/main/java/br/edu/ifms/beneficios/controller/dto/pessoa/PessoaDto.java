/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.pessoa;

import br.edu.ifms.beneficios.modelo.Pessoa;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class PessoaDto extends AbstractPessoaDto {

    public PessoaDto() {
    }
    
    public PessoaDto(Pessoa obj) {
        super(obj);
    }

    public PessoaDto(String nome, LocalDate nascimento) {
        super(nome, nascimento);
    }

    public PessoaDto(Long id, String nome, LocalDate nascimento) {
        super(id, nome, nascimento);
    }
    
    public static Page<PessoaDto> converter(Page<Pessoa> items) {
        return items.map(PessoaDto::new);
    }
    
    public static List<PessoaDto> converter(List<Pessoa> items) {
        return items.stream().map(PessoaDto::new).collect(Collectors.toList());
    }
}
