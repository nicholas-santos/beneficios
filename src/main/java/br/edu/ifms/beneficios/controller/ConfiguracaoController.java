/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.configuracoes.BeneficiosConfigDto;
import br.edu.ifms.beneficios.controller.dto.configuracoes.HibernateJpaConfigDto;
import br.edu.ifms.beneficios.controller.dto.configuracoes.MailConfigDto;
import br.edu.ifms.beneficios.controller.dto.configuracoes.MultpartConfigDto;
import br.edu.ifms.beneficios.controller.form.ConfiguracaoForm;
import br.edu.ifms.beneficios.property.BeneficiosProperties;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/configuracoes")
@Profile(value = {"prod", "test"})
public class ConfiguracaoController {

    @Autowired
    private BeneficiosProperties beneficioProperties;

//    @Autowired
//    private MailProperties mailProperties;

    @Autowired
    private MultipartProperties multpartProperties;

    @Autowired
    private HibernateProperties hibernateProperties;

    @Autowired
    private JpaProperties jpaProperties;

//    @GetMapping("/visualizar")
//    public ResponseEntity<ConfiguracaoForm> visualizar() {
//        BeneficiosConfigDto beneficiosConfig = new BeneficiosConfigDto(beneficioProperties);
//        MailConfigDto mailConfig = new MailConfigDto(mailProperties);
//        MultpartConfigDto multpartConfig = new MultpartConfigDto(multpartProperties);
//        HibernateJpaConfigDto hibernateJpaConfig = new HibernateJpaConfigDto(hibernateProperties,
//                jpaProperties);
//
//        return ResponseEntity.ok(new ConfiguracaoForm(beneficiosConfig,
//                mailConfig, multpartConfig, hibernateJpaConfig));
//    }
//
//    @PutMapping("/alterar")
//    public ResponseEntity<?> atualizar(
//            @RequestBody @Valid ConfiguracaoForm form) {
//        form.atualizar(beneficioProperties, mailProperties, multpartProperties,
//                hibernateProperties, jpaProperties);
//        return ResponseEntity.ok().build();
//    }
}
