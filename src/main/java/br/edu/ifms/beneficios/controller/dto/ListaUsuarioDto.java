/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;


import br.edu.ifms.beneficios.modelo.Usuario;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class ListaUsuarioDto {

    private Long id;
    private String nome;
    private String email;

    public ListaUsuarioDto(Usuario obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.email = obj.getEmail();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public static List<ListaUsuarioDto> converter(List<Usuario> items) {
        return items.stream().map(ListaUsuarioDto::new).collect(Collectors.toList());
    }
}
