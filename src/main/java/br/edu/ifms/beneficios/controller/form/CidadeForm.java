/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.UfDto;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Uf;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.UfRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author santos
 */
public class CidadeForm {

    @NotNull
    @NotEmpty
    private String nome;
    @NotNull
    private UfDto uf;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public UfDto getUf() {
        return uf;
    }

    public void setUf(UfDto uf) {
        this.uf = uf;
    }

    public Cidade converter(UfRepository ufRepository) {
        Uf uf = ufRepository.getOne(this.uf.getId());
        return new Cidade(this.nome, uf);
    }

    public Cidade atualizar(Long id, UfRepository ufRepository, CidadeRepository cidadeRepository) {
        Uf uf = ufRepository.getOne(this.uf.getId());

        Cidade cidade = cidadeRepository.findById(id).get();
        cidade.setNome(this.nome);
        cidade.setUf(uf);
        return cidade;
    }
}
