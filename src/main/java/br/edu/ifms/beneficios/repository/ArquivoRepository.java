/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Arquivo;
import br.edu.ifms.beneficios.modelo.Usuario;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface ArquivoRepository extends JpaRepository<Arquivo, Long> {
    
    Optional<Arquivo> findByUsuarioAndDocumentType(Usuario usuario, String documentType);
    
}
