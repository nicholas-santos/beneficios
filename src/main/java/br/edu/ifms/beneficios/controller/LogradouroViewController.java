/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.LogradouroViewDto;
import br.edu.ifms.beneficios.modelo.LogradouroView;
import br.edu.ifms.beneficios.repository.LogradouroViewRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/logradouro-view")
public class LogradouroViewController {

    @Autowired
    private LogradouroViewRepository repository;

    @GetMapping
    public List<LogradouroViewDto> listar(@RequestParam(required = false) String logradouro) {
        if (logradouro != null) {
            List<LogradouroView> items = repository.findFirst10ByLogradouroStartsWithIgnoreCase(logradouro);
            return LogradouroViewDto.converter(items);
        }
        return new ArrayList();
    }

}
