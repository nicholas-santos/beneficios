/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoSaida;

import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import br.edu.ifms.beneficios.modelo.DocumentoSaida;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class DocumentoSaidaDto extends AbstractDocumentoSaidaDto {

    public DocumentoSaidaDto() {
    }

    public DocumentoSaidaDto(DocumentoSaida obj) {
        super(obj);
    }
    
    public static Page<DocumentoSaidaDto> converter(Page<DocumentoSaida> items) {
        return items.map(DocumentoSaidaDto::new);
    }
    
    public static List<DocumentoSaidaDto> converter(List<DocumentoSaida> items) {
        return items.stream().map(DocumentoSaidaDto::new).collect(Collectors.toList());
    }
}
