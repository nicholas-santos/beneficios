/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class ItemEntradaId implements Serializable {
    
    private Long numero;
    
    @ManyToOne
    @JoinColumn(name = "documento_entrada_id", referencedColumnName = "id")
    private DocumentoEntrada documentoEntrada;

    public ItemEntradaId() {
    }

    public ItemEntradaId(Long numero, DocumentoEntrada documentoEntrada) {
        this.numero = numero;
        this.documentoEntrada = documentoEntrada;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public DocumentoEntrada getDocumentoEntrada() {
        return documentoEntrada;
    }

    public void setDocumentoEntrada(DocumentoEntrada documentoEntrada) {
        this.documentoEntrada = documentoEntrada;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.numero);
        hash = 71 * hash + Objects.hashCode(this.documentoEntrada);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ItemEntradaId other = (ItemEntradaId) obj;
        if (!Objects.equals(this.numero, other.getNumero())) {
            return false;
        }
        if (!Objects.equals(this.documentoEntrada, other.getDocumentoEntrada())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItemEntradaId{" + "numero=" + numero + ", documentoEntrada=" + documentoEntrada + '}';
    }
    
}
