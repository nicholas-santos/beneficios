/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.CondicaoMoradiaForm;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CondicaoMoradiaControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/condicoes-de-moradia";
    private Gson gson = new Gson();
    private CondicaoMoradiaForm condicaoMoradia1;
    private CondicaoMoradiaForm condicaoMoradia2;

    @BeforeEach
    public void setUp() {
        condicaoMoradia1 = new CondicaoMoradiaForm();
        condicaoMoradia1.setDescricao("Alugada");

        condicaoMoradia2 = new CondicaoMoradiaForm();
        condicaoMoradia2.setDescricao("Própria");
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosCondicaoMoradias() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1CondicaoMoradia() throws Exception {
        URI uri = new URI(sURI + "/1");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarCondicaoMoradiaComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmCondicaoMoradia() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(condicaoMoradia1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(3)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmCondicaoMoradia() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(condicaoMoradia1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));
        
        
        uri = new URI(sURI);
        request = MockRequestBuilderHelper.post(uri, gson.toJson(condicaoMoradia2));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(2)));

        uri = new URI(sURI + "/2");
        condicaoMoradia2.setDescricao("Emprestada");
        // dados a serem alterados
        request = MockRequestBuilderHelper.put(uri, gson.toJson(condicaoMoradia2));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("Emprestada")));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmCondicaoMoradia() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(condicaoMoradia1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(4)));
        request = MockRequestBuilderHelper.post(uri, gson.toJson(condicaoMoradia2));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(5)));

        uri = new URI(sURI + "/2");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmCondicaoMoradiaComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

}
