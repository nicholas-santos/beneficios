/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author santos
 */
public class ChangePasswordForm {
    @NotNull
    private String token;
    
    @NotNull
    @NotEmpty
    @Size(min=2, max=255)
    private String password;
    
    @NotNull
    @NotEmpty
    @Size(min=2, max=255)
    private String passwordConfirm;

    public ChangePasswordForm() {
    }

    public ChangePasswordForm(String token, String newPassword, String newPasswordConfirm) {
        this.token = token;
        this.password = newPassword;
        this.passwordConfirm = newPasswordConfirm;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
    
    
}
