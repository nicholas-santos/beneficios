/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.controller.dto.status.IStatusCount;
import br.edu.ifms.beneficios.modelo.DocumentoSaida;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface DocumentoSaidaRepository extends JpaRepository<DocumentoSaida, Long> {
    
    @Query("SELECT coalesce(max(ds.id), 0) FROM DocumentoSaida ds")
    Long getMaxId();
    
    Page<DocumentoSaida> findByStatus(Status status, Pageable paginacao);
    Page<DocumentoSaida> findByUnidadeAtendimentoId(Long unidadeAtendimentoId, Pageable paginacao);
    Page<DocumentoSaida> findByStatusAndUnidadeAtendimentoId(Status status, Long unidadeAtendimentoId, Pageable paginacao);
    
    @Query(value = "select status, totalStatus, total,\n"
            + "	round((1.0*totalStatus/total*100), 2) percentualStatus\n"
            + "  from (select status, count(status) as totalStatus,\n"
            + "		(select count(status) from documento_saida) as total\n"
            + "	  from documento_saida\n"
            + "	 group by status\n"
            + "	 order by status) as contagem", nativeQuery = true)
    List<IStatusCount> countTotalByStatus();
}
