/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.CondicaoMoradia;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class CondicaoMoradiaDto {
    
    private Long id;
    private String descricao;

    public CondicaoMoradiaDto() {
    }

    public CondicaoMoradiaDto(CondicaoMoradia obj) {
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public static Page<CondicaoMoradiaDto> converter(Page<CondicaoMoradia> items) {
        return items.map(CondicaoMoradiaDto::new);
    }
    
    public static List<CondicaoMoradiaDto> converter(List<CondicaoMoradia> items) {
        return items.stream().map(CondicaoMoradiaDto::new).collect(Collectors.toList());
    }
    
}
