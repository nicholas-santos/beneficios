/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.GrupoSocioeducativo;
import br.edu.ifms.beneficios.repository.GrupoSocioeducativoRepository;

/**
 *
 * @author tanabe
 */
public class GrupoSocioeducativoForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setDescricao(String nome) {
        this.nome = nome;
    }

    public GrupoSocioeducativo converter() {
        return new GrupoSocioeducativo(this.nome);
    }

    public GrupoSocioeducativo atualizar(Long id, GrupoSocioeducativoRepository repository) {
    	GrupoSocioeducativo tipoUnidadeAtenimento = repository.getOne(id);
    	tipoUnidadeAtenimento.setNome(this.nome);

        return tipoUnidadeAtenimento;
    }
}
