/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.FuncaoDto;
import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.form.FuncaoForm;
import br.edu.ifms.beneficios.modelo.Funcao;
import br.edu.ifms.beneficios.repository.FuncaoRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/funcoes")
public class FuncaoController {

    @Autowired
    private FuncaoRepository repository;

    @GetMapping
    @Cacheable(value = "pageFuncao")
    public Page<FuncaoDto> listar(
            @PageableDefault(sort = "nome", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

            Page<Funcao> funcao = repository.findAll(paginacao);
            return FuncaoDto.converter(funcao);
    }
    
    @GetMapping("/list")
    @Cacheable(value = "listaFuncao")
    public List<FuncaoDto> listar() {

        List<Funcao> items;
        items = repository.findAll();
        
        return FuncaoDto.converter(items);
    }
    
    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageFuncao", "listaFuncao"}, allEntries = true)
    public ResponseEntity<FuncaoDto> criar(@RequestBody @Valid FuncaoForm form, UriComponentsBuilder uriBuilder) {
    	Funcao funcao = form.converter(repository);
        repository.save(funcao);

        URI uri = uriBuilder.path("/funcoes/{id}").buildAndExpand(funcao.getId()).toUri();
        return ResponseEntity.created(uri).body(new FuncaoDto(funcao));
    }

    @GetMapping("/{id}")
    public ResponseEntity<FuncaoDto> visualizar(@PathVariable Long id) {
        Optional<Funcao> funcao = repository.findById(id);
        if (funcao.isPresent()) {
            return ResponseEntity.ok(new FuncaoDto(funcao.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageFuncao", "listaFuncao"}, allEntries = true)
    public ResponseEntity<FuncaoDto> atualizar(@PathVariable Long id, @RequestBody @Valid FuncaoForm form) {
        Optional<Funcao> optional = repository.findById(id);
        if (optional.isPresent()) {
        	Funcao tipoUnidade = form.atualizar(id, repository);
            return ResponseEntity.ok(new FuncaoDto(tipoUnidade));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageFuncao", "listaFuncao"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Funcao> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
