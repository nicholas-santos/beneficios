import React from 'react';
import { CssBaseline } from '@material-ui/core';
import './App.css';

import Routes from './routes';

function App() {
  return (
    <div>
      <CssBaseline />
      <Routes />
    </div>
  );
}

export default App;
