/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.dto.PerfilMenuDto;
import br.edu.ifms.beneficios.controller.form.PerfilForm;
import br.edu.ifms.beneficios.modelo.Menu;
import br.edu.ifms.beneficios.modelo.Perfil;
import br.edu.ifms.beneficios.modelo.PerfilMenu;
import br.edu.ifms.beneficios.modelo.id.PerfilMenuId;
import br.edu.ifms.beneficios.modelo.tipos.MenuTipo;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import com.google.gson.Gson;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PerfilControllerTest {

    @Autowired
    private MockMvc mvc;

    private final String sURI = "/api/perfis";
    private final String descricao = "ROLE_ADMIN";
    private final String outraDescricao = "ROLE_USER";
    private Gson gson = new Gson();

    private Menu menuUsuario;
    private Menu menuPerfil;

    @BeforeEach
    public void setUp() {
        BeneficiosFactory.MVC = mvc;
        menuUsuario = BeneficiosFactory.createMenu("Usuario", "/usuario", Boolean.TRUE, MenuTipo.ADMIN);
        menuPerfil = BeneficiosFactory.createMenu("Perfil", "/perfil", Boolean.TRUE, MenuTipo.ADMIN);
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosPerfils() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1Perfil() throws Exception {
        Perfil obj = BeneficiosFactory.createPerfil(descricao, Status.ATIVO);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarPerfilComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmPerfil() throws Exception {
        PerfilForm obj = new PerfilForm();
        obj.setNome(descricao);
        obj.setStatus(Status.ATIVO);

        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(obj));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(6)));
    }

    private PerfilMenu criarPerfilMenu(Menu menu) {
        PerfilMenuId perfilMenuId = new PerfilMenuId(null, menu);
        return new PerfilMenu(perfilMenuId, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE);
    }
    
    private PerfilMenuDto criarPerfilMenuDto(Menu menu) {
        return new PerfilMenuDto(criarPerfilMenu(menu));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmPerfilCom1Menu() throws Exception {
        PerfilMenuDto perfilMenuDto = criarPerfilMenuDto(menuUsuario);

        PerfilForm obj = new PerfilForm();
        obj.setNome(descricao);
        obj.setStatus(Status.ATIVO);
        obj.setMenus(Arrays.asList(perfilMenuDto));

        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(obj));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.menus.length()", CoreMatchers.is(1)));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmPerfilCom2Menus() throws Exception {
        List<PerfilMenuDto> list = new ArrayList();
        list.add(criarPerfilMenuDto(menuUsuario));
        list.add(criarPerfilMenuDto(menuPerfil));

        PerfilForm obj = new PerfilForm();
        obj.setNome(descricao);
        obj.setStatus(Status.ATIVO);
        obj.setMenus(list);

        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(obj));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.menus.length()", CoreMatchers.is(2)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmPerfil() throws Exception {
        Perfil obj = BeneficiosFactory.createPerfil(descricao, Status.ATIVO);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        obj.setNome(outraDescricao);
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(obj));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString(outraDescricao)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmPerfilAdicionar1Menu() throws Exception {
        Perfil obj = BeneficiosFactory.createPerfil(descricao, Status.ATIVO);
        
        PerfilForm form = new PerfilForm();
        form.setNome(obj.getNome());
        form.setStatus(obj.getStatus());
        form.setMenus(Arrays.asList(criarPerfilMenuDto(menuUsuario)));

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        form.setNome(outraDescricao);
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(form));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString(outraDescricao)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.menus.length()", CoreMatchers.is(1)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmPerfilAdicionar2Menus() throws Exception {
        List<PerfilMenuDto> list = new ArrayList();
        list.add(criarPerfilMenuDto(menuUsuario));
        list.add(criarPerfilMenuDto(menuPerfil));
        
        Perfil obj = BeneficiosFactory.createPerfil(descricao, Status.ATIVO);
        
        PerfilForm form = new PerfilForm();
        form.setNome(obj.getNome());
        form.setStatus(obj.getStatus());
        form.setMenus(list);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        form.setNome(outraDescricao);
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(form));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString(outraDescricao)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.menus.length()", CoreMatchers.is(2)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmPerfilAdicionar2MenusRemove1() throws Exception {
        PerfilMenu perfilUsuario = criarPerfilMenu(menuUsuario);
        PerfilMenuDto perfilUsuarioDto = new PerfilMenuDto(perfilUsuario);
        
        List<PerfilMenuDto> list = new ArrayList();
        list.add(perfilUsuarioDto);
        list.add(criarPerfilMenuDto(menuPerfil));
        
        Perfil obj = BeneficiosFactory.createPerfil(descricao, Status.ATIVO);
        
        PerfilForm form = new PerfilForm();
        form.setNome(obj.getNome());
        form.setStatus(obj.getStatus());
        form.setMenus(list);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        form.setNome(outraDescricao);
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(form));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString(outraDescricao)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.menus.length()", CoreMatchers.is(2)));
        
        list.remove(perfilUsuarioDto);
        request = MockRequestBuilderHelper.put(uri, gson.toJson(form));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.menus.length()", CoreMatchers.is(1)));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmPerfil() throws Exception {
        Perfil obj = BeneficiosFactory.createPerfil(descricao, Status.ATIVO);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmPerfilComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

}
