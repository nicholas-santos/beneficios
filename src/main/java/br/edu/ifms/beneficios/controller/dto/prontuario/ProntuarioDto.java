/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.prontuario;

import br.edu.ifms.beneficios.controller.dto.DependenteDto;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDto;
import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Prontuario;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class ProntuarioDto extends AbstractProntuario {

    private PessoaDto titular;
    private List<DependenteDto> dependentes = new ArrayList();

    public ProntuarioDto() {
    }

    public ProntuarioDto(Prontuario obj) {
        super(obj);
        this.titular= new PessoaDto(obj.getTitular());
    }

    public PessoaDto getTitular() {
        return titular;
    }

    public List<DependenteDto> getDependentes() {
        return dependentes;
    }
    
    public static List<ProntuarioDto> converter(List<Prontuario> items) {
        return items.stream().map(ProntuarioDto::new).collect(Collectors.toList());
    }

    public static Page<ProntuarioDto> converter(Page<Prontuario> items) {
        return items.map(ProntuarioDto::new);
    }

}
