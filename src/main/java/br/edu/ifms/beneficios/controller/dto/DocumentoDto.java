/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Documento;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class DocumentoDto {
    
    private Long id;
    private String descricao;
    private Boolean exigeOrgaoExpedidor;

    public DocumentoDto() {
    }

    public DocumentoDto(Documento obj) {
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
        this.exigeOrgaoExpedidor = obj.getExigeOrgaoExpeditor();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public Boolean getExigeOrgaoExpedidor() {
        return exigeOrgaoExpedidor;
    }
    
    public static Page<DocumentoDto> converter(Page<Documento> items) {
        return items.map(DocumentoDto::new);
    }
    
    public static List<DocumentoDto> converter(List<Documento> items) {
        return items.stream().map(DocumentoDto::new).collect(Collectors.toList());
    }
    
}
