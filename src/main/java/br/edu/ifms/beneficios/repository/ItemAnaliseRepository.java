/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Analise;
import br.edu.ifms.beneficios.modelo.ItemAnalise;
import br.edu.ifms.beneficios.modelo.id.ItemAnaliseId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface ItemAnaliseRepository extends JpaRepository<ItemAnalise, ItemAnaliseId> {

    @Query("SELECT coalesce(max(ia.id.id), 0) FROM ItemAnalise ia WHERE ia.id.analise = :analise")
    Long getMaxId(Analise analise);

    void deleteByIdAnalise(Analise analise);
    
    Page<ItemAnalise> findByIdAnaliseAtendimentoPessoaId(Long pessoaId, Pageable paginacao);
    Page<ItemAnalise> findByStatus(Status status, Pageable paginacao);
    Page<ItemAnalise> findByStatusAndIdAnaliseAtendimentoPessoaId(Status status, Long pessoaId, Pageable paginacao);
}
