/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.unidadeAtendimento;

import br.edu.ifms.beneficios.controller.dto.EnderecoDto;
import br.edu.ifms.beneficios.controller.dto.TipoUnidadeAtendimentoDto;
import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class UnidadeAtendimentoDetalheDto extends AbstractUnidadeAtendimento {
    
    private TipoUnidadeAtendimentoDto tipoUnidadeAtendimento;
    
    private EnderecoDto endereco;

    public UnidadeAtendimentoDetalheDto(UnidadeAtendimento obj) {
        super(obj);
        this.tipoUnidadeAtendimento = new TipoUnidadeAtendimentoDto(obj.getTipoUnidadeAtendimento());
        this.endereco = new EnderecoDto(obj.getEndereco());
    }

    public TipoUnidadeAtendimentoDto getTipoUnidadeAtendimento() {
        return tipoUnidadeAtendimento;
    }

    public EnderecoDto getEndereco() {
        return endereco;
    }
    
    public static List<UnidadeAtendimentoDetalheDto> converter(List<UnidadeAtendimento> items) {
        return items.stream().map(UnidadeAtendimentoDetalheDto::new).collect(Collectors.toList());
    }
    
    public static Page<UnidadeAtendimentoDetalheDto> converter(Page<UnidadeAtendimento> items) {
        return items.map(UnidadeAtendimentoDetalheDto::new);
    }
    
}
