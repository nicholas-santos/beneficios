/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimento;
import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimentoConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
public class HistoricoAtendimento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    private Atendimento atendimento;
    
    @ManyToOne
    private Funcionario funcionario;

    @Column(name = "status", nullable = false)
    @Convert(converter = StatusAtendimentoConverter.class)
    private StatusAtendimento status;
    
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime emissao;

    public HistoricoAtendimento() {
        this.emissao = LocalDateTime.now();
        this.status = StatusAtendimento.ABERTO;
    }

    public HistoricoAtendimento(Long id, Atendimento atendimento, 
            Funcionario funcionario, StatusAtendimento status, 
            LocalDateTime emissao) {
        this.id = id;
        this.atendimento = atendimento;
        this.funcionario = funcionario;
        this.status = status;
        this.emissao = emissao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Atendimento getAtendimento() {
        return atendimento;
    }

    public void setAtendimento(Atendimento atendimento) {
        this.atendimento = atendimento;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public StatusAtendimento getStatus() {
        return status;
    }

    public void setStatus(StatusAtendimento status) {
        this.status = status;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDateTime emissao) {
        this.emissao = emissao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.atendimento);
        hash = 97 * hash + Objects.hashCode(this.funcionario);
        hash = 97 * hash + Objects.hashCode(this.status);
        hash = 97 * hash + Objects.hashCode(this.emissao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final HistoricoAtendimento other = (HistoricoAtendimento) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.atendimento, other.getAtendimento())) {
            return false;
        }
        if (!Objects.equals(this.funcionario, other.getFuncionario())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        return true;
    }

}
