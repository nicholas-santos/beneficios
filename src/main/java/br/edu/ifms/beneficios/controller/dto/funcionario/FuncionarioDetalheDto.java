/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.funcionario;

import br.edu.ifms.beneficios.controller.dto.FuncaoDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDetalheDto;
import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class FuncionarioDetalheDto extends AbstractFuncionarioDto {
    
    private Date nascimento;
    private FuncaoDto funcao;
    private UnidadeAtendimentoDetalheDto unidadeAtendimento;

    public FuncionarioDetalheDto() {
    }

    public FuncionarioDetalheDto(Funcionario obj) {
        super(obj);
        this.nascimento = Date.from(obj.getNascimento().atStartOfDay(ZoneId.systemDefault()).toInstant());
        this.funcao = new FuncaoDto(obj.getFuncao());

        UnidadeAtendimento ua = obj.getUnidadeAtendimento();
        if (ua != null) {
            this.unidadeAtendimento = new UnidadeAtendimentoDetalheDto(obj.getUnidadeAtendimento());
        }
    }

    public Date getNascimento() {
        return nascimento;
    }

    public FuncaoDto getFuncao() {
        return funcao;
    }

    public UnidadeAtendimentoDetalheDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public static List<FuncionarioDetalheDto> converter(List<Funcionario> items) {
        return items.stream().map(FuncionarioDetalheDto::new).collect(Collectors.toList());
    }

    public static Page<FuncionarioDetalheDto> converter(Page<Funcionario> items) {
        return items.map(FuncionarioDetalheDto::new);
    }

}
