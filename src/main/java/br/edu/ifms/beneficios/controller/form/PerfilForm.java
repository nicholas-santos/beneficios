/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.PerfilMenuDto;
import br.edu.ifms.beneficios.modelo.Menu;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.edu.ifms.beneficios.modelo.Perfil;
import br.edu.ifms.beneficios.modelo.PerfilMenu;
import br.edu.ifms.beneficios.modelo.id.PerfilMenuId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.MenuRepository;
import br.edu.ifms.beneficios.repository.PerfilRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class PerfilForm {

    @NotNull
    @NotEmpty
    private String nome;

    @NotNull
    private Status status;

    private List<PerfilMenuDto> menus = new ArrayList();

    public PerfilForm() {
    }

    public PerfilForm(Perfil obj) {
        this.nome = obj.getNome();
        this.status = obj.getStatus();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<PerfilMenuDto> getMenus() {
        return menus;
    }

    public void setMenus(List<PerfilMenuDto> menus) {
        this.menus = menus;
    }

    /**
     * Converte a lista de menus para PerfilMenu. Procedimento realizado para
     * reaproveitar o stage do hibernate
     *
     * @param perfil
     * @param menuRepository
     * @return
     */
    private void updateMenuPerfil(Perfil perfil,
            MenuRepository menuRepository) {
        // marca todos para remoção
        perfil.clearPerfilMenu();

        // atualiza os itens
        menus.stream().map(dto -> {
            Menu menu = menuRepository.getOne(dto.getId());

            PerfilMenuId id = new PerfilMenuId(perfil, menu);
            PerfilMenu perfilMenu = new PerfilMenu(id, dto.getLer(),
                    dto.getEscrever(), dto.getRemover());

            perfil.add(perfilMenu);

            return dto;
        }).collect(Collectors.toList());
        
    }

    private Perfil toPerfil(Perfil perfil, MenuRepository menuRepository) {
        perfil.setNome(this.nome);
        perfil.setStatus(status);
        perfil.setUpdated(LocalDateTime.now());

        updateMenuPerfil(perfil, menuRepository);

        return perfil;
    }

    public Perfil converter(PerfilRepository repository,
            MenuRepository menuRepository) {

        return toPerfil(new Perfil(), menuRepository);
    }

    public Perfil atualizar(Long id, PerfilRepository repository,
            MenuRepository menuRepository) {
        return toPerfil(repository.getOne(id), menuRepository);
    }
}
