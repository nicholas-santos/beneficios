/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.controller.dto.status.IStatusCount;
import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface DocumentoEntradaRepository extends JpaRepository<DocumentoEntrada, Long> {
    
    @Query("SELECT coalesce(max(de.id), 0) FROM DocumentoEntrada de")
    Long getMaxId();
    
    Page<DocumentoEntrada> findByProcessoOrAtaOrPregaoOrEmpenhoContabilOrContratoOrNumeroNotaFiscalOrFornecedorId(
            String processo, String ata, String pregao, String empenhoContabil, String contrato,
            String numeroNotaFiscal, Long fornecedorId, Pageable paginacao);
    @Query(value = "select status, totalStatus, total,\n"
            + "	round((1.0*totalStatus/total*100), 2) percentualStatus\n"
            + "  from (select status, count(status) as totalStatus,\n"
            + "		(select count(status) from documento_entrada) as total\n"
            + "	  from documento_entrada\n"
            + "	 group by status\n"
            + "	 order by status) as contagem", nativeQuery = true)
    List<IStatusCount> countTotalByStatus();
}
