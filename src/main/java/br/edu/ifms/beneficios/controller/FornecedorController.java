/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.fornecedor.FornecedorDetalheDto;
import br.edu.ifms.beneficios.controller.dto.fornecedor.FornecedorDto;
import br.edu.ifms.beneficios.controller.form.FornecedorForm;
import br.edu.ifms.beneficios.modelo.Fornecedor;
import br.edu.ifms.beneficios.service.FornecedorService;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/fornecedores")
@Profile(value = {"prod", "test"})
public class FornecedorController {

    @Autowired
    private FornecedorService service;

    @GetMapping
    public Page<FornecedorDetalheDto> listar(
            @RequestParam(required = false) String nome,
            @PageableDefault(sort = "nome", direction = Sort.Direction.ASC,
                    page = 0, size = 10) Pageable paginacao) {
        return FornecedorDetalheDto.converter(service.listar(nome, paginacao));
    }

    @GetMapping("/list")
    public List<FornecedorDto> listar() {
        return FornecedorDto.converter(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<FornecedorDetalheDto> visualizar(@PathVariable Long id) {

        Optional<Fornecedor> optional = service.buscarPor(id);
        if (optional.isPresent()) {
            Fornecedor fornecedor = optional.get();
            return ResponseEntity.ok(new FornecedorDetalheDto(fornecedor));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> criar(@RequestBody @Valid FornecedorForm form, UriComponentsBuilder uriBuilder) {
        MessageDto msgDto = service.isValido(form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            FornecedorDetalheDto dto = service.criar(form);

            URI uri = uriBuilder.path("/fornecedores/{id}").buildAndExpand(dto.getId()).toUri();
            return ResponseEntity.created(uri).body(dto);
        }
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<?> atualizar(@PathVariable Long id, @RequestBody @Valid FornecedorForm form) {
        MessageDto msgDto = service.isValido(form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            FornecedorDetalheDto dto = service.atualizar(id, form);
            if (dto != null) {
                return ResponseEntity.ok(dto);
            }
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Fornecedor> optional = service.remover(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}
