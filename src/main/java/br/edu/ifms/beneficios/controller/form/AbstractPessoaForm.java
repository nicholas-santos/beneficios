/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.config.validacao.ErroDeFormularioDto;
import br.edu.ifms.beneficios.controller.dto.ContatoPessoaDto;
import br.edu.ifms.beneficios.controller.dto.DocumentoPessoaDto;
import br.edu.ifms.beneficios.modelo.AbstractPessoa;
import br.edu.ifms.beneficios.modelo.ContatoPessoa;
import br.edu.ifms.beneficios.modelo.Documento;
import br.edu.ifms.beneficios.modelo.DocumentoPessoa;
import br.edu.ifms.beneficios.modelo.OrgaoExpedidor;
import br.edu.ifms.beneficios.modelo.Contato;
import br.edu.ifms.beneficios.modelo.id.ContatoPessoaId;
import br.edu.ifms.beneficios.modelo.id.DocumentoPessoaId;
import br.edu.ifms.beneficios.repository.DocumentoRepository;
import br.edu.ifms.beneficios.repository.OrgaoExpedidorRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import br.edu.ifms.beneficios.repository.ContatoRepository;
import br.edu.ifms.beneficios.repository.DocumentoPessoaRepository;

/**
 *
 * @author santos
 */
public class AbstractPessoaForm {

    @NotNull
    @NotEmpty
    private String nome;

    private List<ContatoPessoaDto> contatos = new ArrayList();
    private List<DocumentoPessoaDto> documentos = new ArrayList();

    public AbstractPessoaForm() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoPessoaDto> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoPessoaDto> contatos) {
        this.contatos = contatos;
    }

    public List<DocumentoPessoaDto> getDocumentos() {
        return documentos;
    }

    public Boolean hasDocumentos() {
        return !this.documentos.isEmpty();
    }

    /**
     * Verifica se algum dos documentos informados já foi cadastrado antes
     *
     * @param pessoaId
     * @param repository DocumentoPessoaRepository
     * @return
     */
    public ErroDeFormularioDto existeDocumentosDuplicados(
            Long pessoaId,
            DocumentoPessoaRepository repository) {
        String message = "";
        // verifica os documentos da pessoa
        for (DocumentoPessoaDto dto : documentos) {
            List<DocumentoPessoa> list = repository
                    .findByIdNumeroAndIdDocumentoIdAndOrgaoExpedidorId(
                            dto.getNumero(), dto.getDocumentoDto().getId(),
                            dto.getOrgaoExpedidorDtoId());
            if (!list.isEmpty()) {
                DocumentoPessoa obj = list.get(0);
                if (pessoaId == null || !obj.getId().getPessoa().getId().equals(pessoaId)) {
                    message = "O documento %s, número %s já foi atribuído a outra pessoa.";

                    return new ErroDeFormularioDto.Builder()
                            .campo("Documentos")
                            .erro(String.format(
                                    message,
                                    obj.getId().getDocumento().getDescricao(),
                                    obj.getId().getNumero()))
                            .build();
                }
            }
        }
        return new ErroDeFormularioDto();
    }

    public void setDocumentos(List<DocumentoPessoaDto> documentos) {
        this.documentos = documentos;
    }

    public void updateDocumentoPessoaList(AbstractPessoa obj,
            DocumentoRepository documentoRepository,
            OrgaoExpedidorRepository orgaoExpedidorRepository) {
        documentos.stream().map(dto -> {
            Documento documento = documentoRepository
                    .getOne(dto.getDocumentoDto().getId());
            OrgaoExpedidor orgao = null;
            if (dto.getOrgaoExpedidorDtoId() != null) {
                orgao = orgaoExpedidorRepository
                        .getOne(dto.getOrgaoExpedidorDtoId());
            }
            DocumentoPessoaId docId = new DocumentoPessoaId(documento, obj, dto.getNumero());
            DocumentoPessoa documentoPessoa = new DocumentoPessoa(docId, orgao, dto.getEmissao());
            documentoPessoa.setDeleted(dto.isDeleted());

            if (documentoPessoa.isDeleted()) {
                obj.remove(documentoPessoa);
            } else {
                obj.add(documentoPessoa);
            }
            return dto;
        }).collect(Collectors.toList());
    }

    public void updateContatoPessoaList(AbstractPessoa obj,
            ContatoRepository tipoContatoRepository) {
        contatos.stream().map(dto -> {
            Contato tipoContato = tipoContatoRepository
                    .getOne(dto.getTipoContatoDto().getId());

            ContatoPessoaId id = new ContatoPessoaId(dto.getId(), obj);
            ContatoPessoa contatoPessoa = new ContatoPessoa(
                    id, dto.getDescricao(), dto.getStatus(),
                    tipoContato);
            contatoPessoa.setDeleted(dto.getDeleted());
            if (contatoPessoa.isDeleted()) {
                obj.remove(contatoPessoa);
            } else {
                obj.add(contatoPessoa);
            }

            return dto;
        }).collect(Collectors.toList());
    }
}
