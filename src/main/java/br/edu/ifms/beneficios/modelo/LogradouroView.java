/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.hibernate.annotations.Subselect;
import org.springframework.data.annotation.Immutable;

/**
 *
 * @author santos
 */
@Entity
@Immutable
@Subselect(
"select distinct concat(l.nome, ', ', b.nome, ', ', c.nome, ' - ', u.sigla) as logradouro,\n" +
"	l.id, l.nome, l.tipo_logradouro_id, b.id as bairro_id, b.nome as bairro_nome, \n" +
"	c.id as cidade_id, c.nome as cidade_nome, c.uf_id,\n" +
"        u.sigla \n" +
"  from cep as en\n" +
" inner join logradouro as l on en.logradouro_id = l.id\n" +
" inner join tipo_logradouro as tl on tl.id = l.tipo_logradouro_id\n" +
" inner join bairro as b on b.id = en.bairro_id\n" +
" inner join cidade as c on c.id = en.cidade_id\n" +
" inner join uf as u on u.id = c.uf_id"
)
public class LogradouroView implements Serializable {
    @Id
    private Long id;
    private String logradouro;
    private String nome;
    private Long tipoLogradouroId;
    private Long bairroId;
    private String bairroNome;
    private Long cidadeId;
    private String cidadeNome;
    private Long ufId;
    private String sigla;

    public LogradouroView() {
    }

    public LogradouroView(String logradouro, Long tipoLogradouroId, Long bairroId, Long cidadeId, Long ufId) {
        this.logradouro = logradouro;
        this.tipoLogradouroId = tipoLogradouroId;
        this.bairroId = bairroId;
        this.cidadeId = cidadeId;
        this.ufId = ufId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getTipoLogradouroId() {
        return tipoLogradouroId;
    }

    public void setTipoLogradouroId(Long tipoLogradouroId) {
        this.tipoLogradouroId = tipoLogradouroId;
    }

    public Long getBairroId() {
        return bairroId;
    }

    public String getBairroNome() {
        return bairroNome;
    }

    public void setBairroNome(String bairroNome) {
        this.bairroNome = bairroNome;
    }

    public String getCidadeNome() {
        return cidadeNome;
    }

    public void setCidadeNome(String cidadeNome) {
        this.cidadeNome = cidadeNome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public void setBairroId(Long bairroId) {
        this.bairroId = bairroId;
    }

    public Long getCidadeId() {
        return cidadeId;
    }

    public void setCidadeId(Long cidadeId) {
        this.cidadeId = cidadeId;
    }

    public Long getUfId() {
        return ufId;
    }

    public void setUfId(Long ufId) {
        this.ufId = ufId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.logradouro);
        hash = 97 * hash + Objects.hashCode(this.tipoLogradouroId);
        hash = 97 * hash + Objects.hashCode(this.bairroId);
        hash = 97 * hash + Objects.hashCode(this.cidadeId);
        hash = 97 * hash + Objects.hashCode(this.ufId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LogradouroView other = (LogradouroView) obj;
        if (!Objects.equals(this.logradouro, other.logradouro)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.tipoLogradouroId, other.tipoLogradouroId)) {
            return false;
        }
        if (!Objects.equals(this.bairroId, other.bairroId)) {
            return false;
        }
        if (!Objects.equals(this.cidadeId, other.cidadeId)) {
            return false;
        }
        if (!Objects.equals(this.ufId, other.ufId)) {
            return false;
        }
        return true;
    }
    
}
