/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.config.security;

import br.edu.ifms.beneficios.config.service.AutenticacaoService;
import br.edu.ifms.beneficios.exception.UserServiceAuthenticationException;
import br.edu.ifms.beneficios.property.BeneficiosProperties;
import br.edu.ifms.beneficios.util.JwtTokenUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken;
import org.springframework.stereotype.Component;

/**
 *
 * @author santos
 */
@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

    private AutenticacaoService jwtUserDetailsService;
    private BeneficiosProperties beneficioProperties;
    private String jwtSecret = null;

    public JwtAuthenticationProvider(
            AutenticacaoService jwtUserDetailsService,
            BeneficiosProperties beneficioProperties
    ) {
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.beneficioProperties = beneficioProperties;
        jwtSecret = beneficioProperties.getJwt().getSecret();
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(BearerTokenAuthenticationToken.class);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        BearerTokenAuthenticationToken bearerToken = (BearerTokenAuthenticationToken) authentication;
        Authentication auth = null;

        try {
            //validate the token
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(bearerToken.getToken());
            JwtTokenUtil jwtTokenUtil = new JwtTokenUtil(bearerToken.getToken());
            String username = jwtTokenUtil.getUsernameFromToken();
            
            UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);

            auth = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            LOG.debug("Authentication token: " + auth);
        } catch (IllegalArgumentException e) {
            throw new UserServiceAuthenticationException("Invalid token");
        } catch (ExpiredJwtException e) {
            throw new UserServiceAuthenticationException("Token expired");
        } catch (SignatureException e) {
            throw new UserServiceAuthenticationException("Invalid signature");
        }

        return auth;
    }
}
