/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Moradia;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author tanabe
 */
public class MoradiaDto {
    private Long id;
    private LocalDate dataOcupacao;
    private LocalDate dataSaida;
    private BigDecimal valor;
    private CondicaoMoradiaDto condicaoMoradiaDto;
    private TipoMoradiaDto tipoMoradiaDto;
    private EnderecoDto enderecoDto;
    private Boolean deleted;

    public MoradiaDto() {
        this.deleted = Boolean.FALSE;
        condicaoMoradiaDto = new CondicaoMoradiaDto();
        tipoMoradiaDto = new TipoMoradiaDto();
        enderecoDto = new EnderecoDto();
        valor = BigDecimal.ZERO;
        dataOcupacao = LocalDate.now();
        dataSaida = LocalDate.now();
    }

    public MoradiaDto(Moradia obj) {
        this();
        this.id = obj.getId().getId();
        this.dataOcupacao = obj.getDataOcupacao();
        this.dataSaida = obj.getDataSaida();
        this.valor = obj.getValor();
        this.condicaoMoradiaDto = new CondicaoMoradiaDto(obj.getCondicaoMoradia());
        this.tipoMoradiaDto = new TipoMoradiaDto(obj.getTipoMoradia());
        this.enderecoDto = new EnderecoDto(obj.getEndereco());
        this.deleted = obj.isDeleted();
    }

    public Long getId() {
        return id;
    }
    
    public Boolean hasId() {
        return this.id != null && this.id > 0;
    }

    public LocalDate getDataOcupacao() {
        return dataOcupacao;
    }

    public LocalDate getDataSaida() {
        return dataSaida;
    }
    
    public Boolean isOcupada() {
        return dataSaida == null;
    }
    
    public Boolean exigeComplemento() {
        return tipoMoradiaDto.getComplementar();
    }
    
    public Boolean hasComplemento() {
        return enderecoDto.hasComplemento();
    }

    public BigDecimal getValor() {
        return valor;
    }

    public CondicaoMoradiaDto getCondicaoMoradiaDto() {
        return condicaoMoradiaDto;
    }

    public TipoMoradiaDto getTipoMoradiaDto() {
        return tipoMoradiaDto;
    }

    public EnderecoDto getEnderecoDto() {
        return enderecoDto;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    
    public static List<MoradiaDto> converter(List<Moradia> items) {
        return items.stream().map(MoradiaDto::new).collect(Collectors.toList());
    }

    public static Page<MoradiaDto> converter(Page<Moradia> items) {
        return items.map(MoradiaDto::new);
    }
    
}
