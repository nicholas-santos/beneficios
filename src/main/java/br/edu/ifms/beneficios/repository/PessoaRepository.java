/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Pessoa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
    
    @Query("SELECT coalesce(max(p.id), 0) FROM Pessoa p")
    Long getMaxId();
    
    Page<Pessoa> findByNomeContainingIgnoreCase(String nome, Pageable paginacao);
    
    Page<Pessoa> findDistinctByDocumentos_IdNumeroContaining(String numero, Pageable paginacao);
    
}
