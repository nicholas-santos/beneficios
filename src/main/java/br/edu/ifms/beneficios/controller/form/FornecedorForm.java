/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.EnderecoDto;
import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.modelo.Fornecedor;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.CepRepository;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.DocumentoRepository;
import br.edu.ifms.beneficios.repository.EnderecoRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.OrgaoExpedidorRepository;
import br.edu.ifms.beneficios.repository.FornecedorRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import br.edu.ifms.beneficios.repository.ContatoRepository;

/**
 *
 * @author santos
 */
public class FornecedorForm extends AbstractPessoaForm {

    @NotNull
    @NotEmpty
    private String codigo;

    @NotNull
    private EnderecoDto enderecoDto;

    public FornecedorForm() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public EnderecoDto getEnderecoDto() {
        return enderecoDto;
    }

    public void setEnderecoDto(EnderecoDto enderecoDto) {
        this.enderecoDto = enderecoDto;
    }

    private Fornecedor toFornecedor(Fornecedor fornecedor,
            ContatoRepository tipoContatoRepository,
            EnderecoRepository enderecoRepository,
            CepRepository cepRepository,
            CidadeRepository cidadeRepository,
            BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository,
            DocumentoRepository documentoRepository,
            OrgaoExpedidorRepository orgaoExpedidorRepository) {
        fornecedor.setNome(this.getNome());
        fornecedor.setCodigo(this.codigo);
        
        updateContatoPessoaList(fornecedor,tipoContatoRepository);
        updateDocumentoPessoaList(fornecedor, documentoRepository,
                orgaoExpedidorRepository);
        fornecedor.setEndereco(ControllerHelper.getEndereco(
                this.enderecoDto,
                enderecoRepository, cepRepository,
                cidadeRepository, bairroRepository,
                logradouroRepository));

        return fornecedor;
    }

    public Fornecedor converter(FornecedorRepository repository,
            ContatoRepository tipoContatoRepository,
            EnderecoRepository enderecoRepository, CepRepository cepRepository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository,
            DocumentoRepository documentoRepository,
            OrgaoExpedidorRepository orgaoExpedidorRepository) {
        
        return toFornecedor(new Fornecedor(), tipoContatoRepository,
                enderecoRepository, cepRepository, cidadeRepository,
                bairroRepository, logradouroRepository,
                documentoRepository, orgaoExpedidorRepository);
    }

    public Fornecedor atualizar(Long id, FornecedorRepository fornecedorRepository,
            ContatoRepository tipoContatoRepository,
            EnderecoRepository enderecoRepository, CepRepository cepRepository,
            CidadeRepository cidadeRepository, BairroRepository bairroRepository,
            LogradouroRepository logradouroRepository,
            DocumentoRepository documentoRepository,
            OrgaoExpedidorRepository orgaoExpedidorRepository) {
        return toFornecedor(fornecedorRepository.getOne(id),
                tipoContatoRepository, enderecoRepository, cepRepository,
                cidadeRepository, bairroRepository, logradouroRepository,
                documentoRepository, orgaoExpedidorRepository);
    }

}
