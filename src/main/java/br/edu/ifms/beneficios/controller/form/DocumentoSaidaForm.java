/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.documentoSaida.ItemSaidaDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.helper.DateHelper;
import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import br.edu.ifms.beneficios.modelo.DocumentoSaida;
import br.edu.ifms.beneficios.modelo.ItemSaida;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.id.ItemSaidaId;
import br.edu.ifms.beneficios.repository.BeneficioEventualRepository;
import br.edu.ifms.beneficios.repository.DocumentoSaidaRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author santos
 */
public class DocumentoSaidaForm {

    @NotNull
    private UnidadeAtendimentoDto unidadeAtendimento;

    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date emissao;
    private String observacao;

    private List<ItemSaidaDto> itens = new ArrayList();

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimentoDto unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public Date getEmissao() {
        return emissao;
    }

    public void setEmissao(Date emissao) {
        this.emissao = emissao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public List<ItemSaidaDto> getItens() {
        return itens;
    }

    public void setItens(List<ItemSaidaDto> itens) {
        this.itens = itens;
    }

    public String isValido() {
        if (itens.isEmpty()) {
            return "Você deve lançar ao menos UM item no Documento de Saída.";
        }

        int countItens = this.itens.stream().mapToInt(dto
                -> !dto.hasPositiveValue() ? 1 : 0).sum();
        if (countItens > 1) {
            return "Existem itens com quantidades incorretas!";
        }

        countItens = this.itens.stream()
                .mapToInt(dto
                        -> unidadeAtendimento.getId().equals(dto.getUnidadeAtendimento().getId()) ? 1 : 0)
                .sum();
        if (countItens > 0) {
            return "Você está movimentando alguns itens para a mesma Unidade de Atendimento de origem!";
        }

        return "";
    }

    private void updateItensSaida(DocumentoSaida obj,
            BeneficioEventualRepository beneficioEventualRepository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository) {

        itens.stream().map(dto -> {
            obj.getItens().removeIf(item
                    -> dto.isDeleted()
                    && item.getUnidadeAtendimento().getId().equals(dto.getUnidadeAtendimento().getId())
                    && item.getId().getNumero().equals(dto.getNumero())
                    && item.getBeneficioEventual().getId().equals(dto.getBeneficioEventual().getId()));

            if (!dto.isDeleted() && !dto.isSetNumero()) {
                BeneficioEventual beneficio = beneficioEventualRepository
                        .getOne(dto.getBeneficioEventual().getId());
                UnidadeAtendimento ua = unidadeAtendimentoRepository.getOne(dto.getUnidadeAtendimento().getId());

                ItemSaidaId id = new ItemSaidaId(dto.getNumero(), obj);
                ItemSaida itemSaida = new ItemSaida(id, dto.getQuantidade(),
                        beneficio, ua);
                itemSaida.setDeleted(dto.isDeleted());
                obj.add(itemSaida);
            }
            return dto;
        }).collect(Collectors.toList());
    }

    private DocumentoSaida toDocumentoSaida(DocumentoSaida obj,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            BeneficioEventualRepository beneficioEventualRepository) {
        UnidadeAtendimento ua = unidadeAtendimentoRepository.getOne(unidadeAtendimento.getId());
        obj.setUnidadeAtendimento(ua);
        obj.setEmissao(DateHelper.toLocalDateTime(emissao));
        obj.setObservacao(observacao);

        updateItensSaida(obj, beneficioEventualRepository, unidadeAtendimentoRepository);

        return obj;
    }

    public DocumentoSaida converter(DocumentoSaidaRepository repository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            BeneficioEventualRepository beneficioEventualRepository) {
        Long maxId = repository.getMaxId() + 1;
        return toDocumentoSaida(new DocumentoSaida(maxId),
                unidadeAtendimentoRepository,
                beneficioEventualRepository);
    }

    public DocumentoSaida atualizar(Long id,
            DocumentoSaidaRepository repository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            BeneficioEventualRepository beneficioEventualRepository) {

        return toDocumentoSaida(repository.getOne(id),
                unidadeAtendimentoRepository,
                beneficioEventualRepository);
    }
}
