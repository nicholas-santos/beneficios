package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.service.EmailService;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.form.ChangePasswordForm;
import br.edu.ifms.beneficios.controller.form.ForgotPasswordForm;
import br.edu.ifms.beneficios.modelo.ResetPasswordToken;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.property.BeneficiosProperties;
import br.edu.ifms.beneficios.repository.ResetPasswordTokenRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping("api/auth")
@Profile("prod")
public class AutenticacaoController {

    @Autowired
    private AuthenticationManager authManager;

    @Autowired
    private ResetPasswordTokenRepository resetPasswordTokenRepository;

    @Autowired
    private UsuarioRepository repository;
    
    @Autowired
    private EmailService emailService;
    
    @Autowired
    private BeneficiosProperties beneficiosProperties;
    
    @GetMapping("/logout")
    public ResponseEntity<?> logout() {
        
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Senha alterada com sucesso!"));
    }

    @PostMapping("/reset-password")
    public ResponseEntity<?> findById(@Valid @RequestBody ChangePasswordForm form) {
        if (!form.getPassword().equals(form.getPasswordConfirm())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
                    new MessageDto(HttpStatus.BAD_REQUEST.value(),
                            "Erro de alteração de senha",
                            "A nova senha e a senha de confirmação não combinam!")
            );
        }

        Optional<ResetPasswordToken> optional
                = resetPasswordTokenRepository.findByToken(form.getToken());
        if (!optional.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
                    new MessageDto(HttpStatus.BAD_REQUEST.value(), "Erro de alteração de senha", "Link inválido")
            );
        }
        
        ResetPasswordToken resetPasswordToken = optional.get();
        
        if (resetPasswordToken.isExpired(beneficiosProperties.getResetPasswordTokenExpiration().toMillis())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
                    new MessageDto(HttpStatus.BAD_REQUEST.value(), "Erro de alteração de senha", "Link expirado")
            );
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        
        Usuario usuario = resetPasswordToken.getUsuario();
        usuario.setSenha(encoder.encode(form.getPassword()));
        repository.save(usuario);

        resetPasswordToken.setToken(null);
        resetPasswordTokenRepository.save(resetPasswordToken);

        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Senha alterada com sucesso!"));
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<?> findById(@Valid @RequestBody ForgotPasswordForm form) {
        Optional<Usuario> optional = repository.findByEmail(form.getEmail());
        if (optional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
                    new MessageDto(HttpStatus.BAD_REQUEST.value(), "Erro de Solicitação",
                            "O e-mail informado não está cadastrado no sistema.")
            );
        }

        Usuario usuario = optional.get();
        ResetPasswordToken resetToken = new ResetPasswordToken();
        resetToken.setUsuario(usuario);
        resetToken.setToken(UUID.randomUUID().toString());
        resetToken.setCreatedDate(new Date());
        resetPasswordTokenRepository.save(resetToken);

        emailService.sendResetPasswordToken(resetToken);

        StringBuilder sb = new StringBuilder();
        sb.append("Um email foi enviado para o endereço de que você forneceu");
        sb.append("Por favor, siga as instruções no e-mail para completar sua alteração de senha.");

        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), sb.toString()));
    }

}
