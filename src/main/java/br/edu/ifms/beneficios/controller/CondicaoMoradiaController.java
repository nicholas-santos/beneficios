/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.CondicaoMoradiaDto;
import br.edu.ifms.beneficios.controller.form.CondicaoMoradiaForm;
import br.edu.ifms.beneficios.modelo.CondicaoMoradia;
import br.edu.ifms.beneficios.repository.CondicaoMoradiaRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/condicoes-de-moradia")
public class CondicaoMoradiaController {

    @Autowired
    private CondicaoMoradiaRepository repository;

    @GetMapping
    @Cacheable(value = "pageCondicaoMoradia")
    public Page<CondicaoMoradiaDto> listar(
            @PageableDefault(sort = "descricao", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

            Page<CondicaoMoradia> condicaoMoradia = repository.findAll(paginacao);
            return CondicaoMoradiaDto.converter(condicaoMoradia);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaCondicaoMoradia")
    public List<CondicaoMoradiaDto> listar() {
        List<CondicaoMoradia> condicaoMoradia = repository.findAll();
        return CondicaoMoradiaDto.converter(condicaoMoradia);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CondicaoMoradiaDto> visualizar(@PathVariable Long id) {
        Optional<CondicaoMoradia> condicaoMoradia = repository.findById(id);
        if (condicaoMoradia.isPresent()) {
            return ResponseEntity.ok(new CondicaoMoradiaDto(condicaoMoradia.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageCondicaoMoradia", "listaCondicaoMoradia"}, allEntries = true)
    public ResponseEntity<CondicaoMoradiaDto> criar(@RequestBody @Valid CondicaoMoradiaForm form, UriComponentsBuilder uriBuilder) {
    	CondicaoMoradia condicaoMoradia = form.converter();
        repository.save(condicaoMoradia);

        URI uri = uriBuilder.path("/condicoes-de-moradia/{id}").buildAndExpand(condicaoMoradia.getId()).toUri();
        return ResponseEntity.created(uri).body(new CondicaoMoradiaDto(condicaoMoradia));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageCondicaoMoradia", "listaCondicaoMoradia"}, allEntries = true)
    public ResponseEntity<CondicaoMoradiaDto> atualizar(@PathVariable Long id, @RequestBody @Valid CondicaoMoradiaForm form) {
        Optional<CondicaoMoradia> optional = repository.findById(id);
        if (optional.isPresent()) {
        	CondicaoMoradia condicaoMoradia = form.atualizar(id, repository);
            return ResponseEntity.ok(new CondicaoMoradiaDto(condicaoMoradia));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageCondicaoMoradia", "listaCondicaoMoradia"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<CondicaoMoradia> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
