/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.modelo.Pais;
import br.edu.ifms.beneficios.modelo.Uf;
import br.edu.ifms.beneficios.controller.dto.PaisDto;
import br.edu.ifms.beneficios.repository.PaisRepository;
import br.edu.ifms.beneficios.repository.UfRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author santos
 */
public class UfForm {

    @NotNull
    @NotEmpty
    private String nome;

    @NotNull
    @NotEmpty
    private String sigla;
    
    @NotNull
    private PaisDto pais;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public PaisDto getPais() {
        return pais;
    }

    public void setPais(PaisDto pais) {
        this.pais = pais;
    }

    public Uf converter(PaisRepository paisRepository) {
        Pais pais = paisRepository.getOne(this.pais.getId());
        return new Uf(this.nome, this.sigla, pais);
    }

    public Uf atualizar(Long id, UfRepository ufRepository, PaisRepository paisRepository) {
        Pais pais = paisRepository.getOne(this.pais.getId());
        Uf uf = ufRepository.getOne(id);
        uf.setNome(this.nome);
        uf.setSigla(sigla);
        uf.setPais(pais);
        
        return uf;
    }
}
