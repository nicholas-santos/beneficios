/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.prontuario.ProntuarioDetalheDto;
import br.edu.ifms.beneficios.controller.dto.prontuario.ProntuarioDto;
import br.edu.ifms.beneficios.controller.dto.status.StatusCountDto;
import br.edu.ifms.beneficios.controller.form.prontuario.HistoricoProntuarioForm;
import br.edu.ifms.beneficios.controller.form.prontuario.ProntuarioForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifms.beneficios.modelo.Prontuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.service.ProntuarioService;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/prontuarios")
public class ProntuarioController {

    @Autowired
    private ProntuarioService service;

    @GetMapping
    public Page<ProntuarioDto> listar(
            @RequestParam(required = false) Long pessoaId,
            @RequestParam(required = false) Long unidadeAtendimentoId,
            @RequestParam(required = false) Status status,
            @PageableDefault(sort = "emissao", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {
        return ProntuarioDto.converter(service.listar(pessoaId, unidadeAtendimentoId, status, paginacao));
    }

    @GetMapping("/list")
    public List<ProntuarioDto> listar() {
        return ProntuarioDto.converter(service.listar());
    }

    @GetMapping("/contagem/{unidadeAtendimentoId}")
    public List<StatusCountDto> listarContagem(
            @PathVariable Long unidadeAtendimentoId) {
        return StatusCountDto.converter(service.listarContagem(unidadeAtendimentoId));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProntuarioDetalheDto> visualizar(@PathVariable Long id) {
        Optional<Prontuario> prontuario = service.buscarPor(id);
        if (prontuario.isPresent()) {
            return ResponseEntity.ok(new ProntuarioDetalheDto(prontuario.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    public ResponseEntity<ProntuarioDto> criar(
            @RequestBody @Valid ProntuarioForm form, 
            UriComponentsBuilder uriBuilder) {

        ProntuarioDto dto = service.criar(form);
        Long id = dto.getId();
        URI uri = uriBuilder.path("/prontuarios/{id}")
                .buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<ProntuarioDto> atualizar(@PathVariable Long id,
            @RequestBody @Valid ProntuarioForm form) {
        ProntuarioDto dto = service.atualizar(id, form);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Prontuario> optional = service.remover(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

    @PutMapping("/ativar/{id}")
    public ResponseEntity<ProntuarioDto> ativar(@PathVariable Long id,
            @RequestBody @Valid HistoricoProntuarioForm form) {
        ProntuarioDto dto = service.ativar(id, 
                Status.ATIVO, form.getObservacao());
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/desativar/{id}")
    public ResponseEntity<ProntuarioDto> desativar(@PathVariable Long id,
            @RequestBody @Valid HistoricoProntuarioForm form) {
        ProntuarioDto dto = service.ativar(id, 
                Status.INATIVO, form.getObservacao());
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/reativar/{id}")
    public ResponseEntity<ProntuarioDto> reativar(@PathVariable Long id,
            @RequestBody @Valid HistoricoProntuarioForm form) {
        ProntuarioDto dto = service.ativar(id, 
                Status.PENDENTE, form.getObservacao());
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

}
