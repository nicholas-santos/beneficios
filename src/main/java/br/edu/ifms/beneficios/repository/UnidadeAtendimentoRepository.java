/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 *
 * @author tanabe
 */
public interface UnidadeAtendimentoRepository extends JpaRepository<UnidadeAtendimento, Long> {
    
    @Query("SELECT coalesce(max(ua.id), 0) FROM UnidadeAtendimento ua")
    Long getMaxId();
    
    Optional<UnidadeAtendimento> findByMatriz(Boolean matriz);
}
