/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.Contato;
import br.edu.ifms.beneficios.modelo.tipos.TipoContato;
import br.edu.ifms.beneficios.repository.ContatoRepository;

/**
 *
 * @author tanabe
 */
public class ContatoForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String descricao;
    
    @NotNull
    private TipoContato tipoContato;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public Contato converter() {
        return new Contato(this.descricao, this.tipoContato);
    }

    public Contato atualizar(Long id, ContatoRepository repository) {
    	Contato contato = repository.getOne(id);
    	contato.setDescricao(this.descricao);
        contato.setTipoContato(tipoContato);

        return contato;
    }
}
