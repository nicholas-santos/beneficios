/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.OrgaoExpedidorDto;
import br.edu.ifms.beneficios.controller.form.OrgaoExpedidorForm;
import br.edu.ifms.beneficios.modelo.OrgaoExpedidor;
import br.edu.ifms.beneficios.repository.OrgaoExpedidorRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/orgaos-expedidores")
public class OrgaoExpedidorController {

    @Autowired
    private OrgaoExpedidorRepository repository;

    @GetMapping
    @Cacheable(value = "pageOrgaoExpedidor")
    public Page<OrgaoExpedidorDto> listar(
            @PageableDefault(sort = "descricao", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

            Page<OrgaoExpedidor> OrgaosExpedidores = repository.findAll(paginacao);
            return OrgaoExpedidorDto.converter(OrgaosExpedidores);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaOrgaoExpedidor")
    public List<OrgaoExpedidorDto> listar() {
        List<OrgaoExpedidor> orgaoExpedidor = repository.findAll();
        return OrgaoExpedidorDto.converter(orgaoExpedidor);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrgaoExpedidorDto> visualizar(@PathVariable Long id) {
        Optional<OrgaoExpedidor> orgaoExpedidor = repository.findById(id);
        if (orgaoExpedidor.isPresent()) {
            return ResponseEntity.ok(new OrgaoExpedidorDto(orgaoExpedidor.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageOrgaoExpedidor", "listaOrgaoExpedidor"}, allEntries = true)
    public ResponseEntity<OrgaoExpedidorDto> criar(@RequestBody @Valid OrgaoExpedidorForm form, UriComponentsBuilder uriBuilder) {
    	OrgaoExpedidor orgaoExpedidor = form.converter();
        repository.save(orgaoExpedidor);

        URI uri = uriBuilder.path("/orgaos-expedidores/{id}").buildAndExpand(orgaoExpedidor.getId()).toUri();
        return ResponseEntity.created(uri).body(new OrgaoExpedidorDto(orgaoExpedidor));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageOrgaoExpedidor", "listaOrgaoExpedidor"}, allEntries = true)
    public ResponseEntity<OrgaoExpedidorDto> atualizar(@PathVariable Long id, @RequestBody @Valid OrgaoExpedidorForm form) {
        Optional<OrgaoExpedidor> optional = repository.findById(id);
        if (optional.isPresent()) {
        	OrgaoExpedidor orgaoExpedidor = form.atualizar(id, repository);
            return ResponseEntity.ok(new OrgaoExpedidorDto(orgaoExpedidor));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageOrgaoExpedidor", "listaOrgaoExpedidor"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<OrgaoExpedidor> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
