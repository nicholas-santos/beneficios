/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.UnidadeAtendimentoForm;
import br.edu.ifms.beneficios.modelo.id.CepId;
import br.edu.ifms.beneficios.modelo.id.EnderecoId;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.Pais;
import br.edu.ifms.beneficios.modelo.Uf;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.modelo.Logradouro;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UnidadeAtendimentoControllerTest {

    @Autowired
    private MockMvc mvc;

    private final String sURI = "/api/unidades-de-atendimento";
    private final String descricao = "CRAS 1";
    private final String outraDescricao = "CRAS Paraiso";
    private Gson gson = new Gson();
    
    private static Cidade cidade;
    private static Bairro bairro;
    private static Endereco endereco;
    private static TipoUnidadeAtendimento tipoUnidadeAtendimento;

    @BeforeEach
    public void setUp() {
        BeneficiosFactory.MVC = mvc;
        loadData();
    }
    
    private void loadData() {
        Pais pais = BeneficiosFactory.createPais("Brasil");
        Uf uf = BeneficiosFactory.createUf("Mato Grosso do Sul", "MS", pais);
        cidade = BeneficiosFactory.createCidade("Navirai", uf);
        bairro = BeneficiosFactory.createBairro("Centro");
        
        Logradouro logradouro = BeneficiosFactory.createLogradouro("Rua 13 de fevereriro");
        
        CepId cepId = new CepId(cidade, bairro, logradouro);
        Cep cep = new Cep(cepId);
        
        EnderecoId enderecoId = new EnderecoId(cep, "1234");
        endereco = new Endereco(enderecoId);
        
        tipoUnidadeAtendimento = BeneficiosFactory.createTipoUnidadeAtendimento("CRAS");
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosUnidadeAtendimentos() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1UnidadeAtendimento() throws Exception {        
        URI uri = new URI(String.format("%s/%d", sURI, 1));
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarUnidadeAtendimentoComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmUnidadeAtendimento() throws Exception {
        loadData();
        UnidadeAtendimento obj = new UnidadeAtendimento(descricao, 
                "CRAS 1", endereco, tipoUnidadeAtendimento);
        obj.setMatriz(Boolean.FALSE);
        UnidadeAtendimentoForm form = new UnidadeAtendimentoForm(obj);

        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(form));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmUnidadeAtendimento() throws Exception {
        UnidadeAtendimento obj = BeneficiosFactory.createUnidadeAtendimento(
                descricao, "CRAS 1", endereco, tipoUnidadeAtendimento, Boolean.FALSE);
        UnidadeAtendimentoForm form = new UnidadeAtendimentoForm(obj);

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        form.setNome(outraDescricao);
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(form));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString(outraDescricao)));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmUnidadeAtendimento() throws Exception {
        URI uri = new URI(String.format("%s/%d", sURI, 1));
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmUnidadeAtendimentoComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
}
