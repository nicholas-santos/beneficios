/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Cidade;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class CidadeDto {
    private Long id;
    private String nome;
    private UfDto uf;

    public CidadeDto() {
    }

    public CidadeDto(Cidade obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.uf =  new UfDto(obj.getUf());
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public UfDto getUf() {
        return uf;
    }
    
    public static Page<CidadeDto> converter(Page<Cidade> items) {
        return items.map(CidadeDto::new);
    }
    
    public static List<CidadeDto> converter(List<Cidade> items) {
        return items.stream().map(CidadeDto::new).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "CidadeDto{" + "id=" + id + ", nome=" + nome + ", uf=" + uf + '}';
    }
}
