/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDto;
import br.edu.ifms.beneficios.controller.form.PessoaForm;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.service.PessoaService;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/pessoas")
@Profile(value = {"prod", "test"})
public class PessoaController {

    @Autowired
    private PessoaService service;

    @GetMapping
    public Page<PessoaDto> listar(
            @RequestParam(required = false) String nome,
            @RequestParam(required = false) String documento,
            @PageableDefault(sort = "nome", direction = Sort.Direction.ASC,
                    page = 0, size = 10) Pageable paginacao) {
        return PessoaDto.converter(service.listar(nome, documento, paginacao));
    }

    @GetMapping("/list")
    public List<PessoaDto> listar() {
        return PessoaDto.converter(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PessoaDetalheDto> visualizar(@PathVariable Long id) {

        Optional<Pessoa> optional = service.buscarPor(id);
        if (optional.isPresent()) {
            Pessoa pessoa = optional.get();
            return ResponseEntity.ok(new PessoaDetalheDto(pessoa));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> cadastrar(
            @RequestParam("file") MultipartFile file,
            @RequestPart("form") @Valid PessoaForm form,
            UriComponentsBuilder uriBuilder) {
        MessageDto msgDto = service.isValido(file, form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            PessoaDetalheDto dto = service.criar(file, form);
            URI uri = uriBuilder.path("/pessoas/{id}")
                    .buildAndExpand(dto.getId())
                    .toUri();
            return ResponseEntity.created(uri).body(dto);
        }
    }

    @PutMapping
    @Transactional
    public ResponseEntity<?> atualizar(
            @RequestParam("id") Long id,
            @RequestParam("file") MultipartFile file,
            @RequestPart("form") @Valid PessoaForm form) {
        MessageDto msgDto = service.isValido(file, id, form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            PessoaDetalheDto dto = service.atualizar(id, file, form);
            if (dto != null) {
                return ResponseEntity.ok(dto);
            }

            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Pessoa> optional = service.remover(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}
