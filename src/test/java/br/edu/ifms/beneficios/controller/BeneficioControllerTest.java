/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.BeneficioEventualForm;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author santos
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BeneficioControllerTest {

    @Autowired
    private MockMvc mvc;

    private Gson gson = new Gson();
    private BeneficioEventualForm beneficio1;
    private BeneficioEventualForm beneficio2;
    private BeneficioEventualForm beneficio3;
    private final String sURI = "/api/beneficios-eventuais";

    @BeforeEach
    public void setUp() {
        beneficio1 = new BeneficioEventualForm();
        beneficio1.setDescricao("Beneficio 1");
        beneficio2 = new BeneficioEventualForm();
        beneficio2.setDescricao("Beneficio 2");
        beneficio3 = new BeneficioEventualForm();
        beneficio3.setDescricao("Beneficio 3");
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosBeneficios() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1Beneficio() throws Exception {
        URI uri = new URI(sURI + "/2");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarBeneficioComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmBeneficio() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(beneficio1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmBeneficio() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(beneficio2));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));
        
        uri = new URI(sURI + "/2");
        beneficio2.setDescricao("TEste");
        // dados a serem alterados
        request = MockRequestBuilderHelper.put(uri, gson.toJson(beneficio2));
        
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("TEste")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(2)));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmBeneficio() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(beneficio1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));
        request = MockRequestBuilderHelper.post(uri, gson.toJson(beneficio2));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(2)));
        request = MockRequestBuilderHelper.post(uri, gson.toJson(beneficio3));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(3)));
        

        uri = new URI(sURI + "/3");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmBeneficioComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
}
