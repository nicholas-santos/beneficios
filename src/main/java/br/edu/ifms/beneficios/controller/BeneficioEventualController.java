/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.beneficioEventual.BeneficioEventualDetalheDto;
import br.edu.ifms.beneficios.controller.dto.beneficioEventual.BeneficioEventualDto;
import br.edu.ifms.beneficios.controller.form.BeneficioEventualForm;
import br.edu.ifms.beneficios.modelo.Usuario;
import java.net.URI;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import br.edu.ifms.beneficios.service.BeneficioEventualService;
import br.edu.ifms.beneficios.service.UsuarioService;
import java.util.List;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/beneficios-eventuais")
@Profile(value = {"prod", "test"})
public class BeneficioEventualController {

    @Autowired
    private BeneficioEventualService service;
    
    @Autowired
    private UsuarioService usuarioService;

    /**
     * Lista de benefícios.Este método é responsável por retornar uma lista de
     * benefícios cadastrados no sistema.
     *
     * @param nome
     * @param paginacao
     * @return
     */
    @GetMapping()
    public Page<BeneficioEventualDetalheDto> listar(
            @RequestParam(required = false) String nome,
            @PageableDefault(sort = "descricao", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {
        return BeneficioEventualDetalheDto.converter(service.listar(nome, paginacao));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BeneficioEventualDetalheDto> visualizar(@PathVariable Long id) {
        Usuario usuario = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        usuario = usuarioService.buscarPor(usuario.getId()).get();
        
        return ResponseEntity.ok(
                new BeneficioEventualDetalheDto(
                        service.buscarPor(id),
                        usuario.getFuncionario().getUnidadeAtendimento()
                ));
    }

    @GetMapping("/list")
    public List<BeneficioEventualDto> listar() {
        return BeneficioEventualDto.converter(service.listar());
    }

    @PostMapping()
    @Transactional
    public ResponseEntity<BeneficioEventualDto> criar(
            @RequestBody @Valid BeneficioEventualForm form,
            UriComponentsBuilder uriBuilder) {
        BeneficioEventualDto dto = service.criar(form);

        URI uri = uriBuilder.path("/beneficios-eventuais/{id}").buildAndExpand(dto.getId()).toUri();
        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<BeneficioEventualDto> atualizar(@PathVariable Long id, @RequestBody @Valid BeneficioEventualForm form) {
        BeneficioEventualDto dto = service.atualizar(id, form);
        return ResponseEntity.ok(dto);
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remover(@PathVariable Long id) {
        service.remover(id);
        return ResponseEntity.ok().build();
    }

}
