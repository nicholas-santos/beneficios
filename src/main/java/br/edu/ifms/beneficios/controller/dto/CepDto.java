/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Analise;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.modelo.id.CepId;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class CepDto {

    private CidadeDto cidade;
    private BairroDto bairro;
    private LogradouroDto logradouro;

    public CepDto() {
    }

    public CepDto(Cep obj) {
        CepId cepId = obj.getId();
        this.cidade = new CidadeDto(cepId.getCidade());
        this.bairro = new BairroDto(cepId.getBairro());
        this.logradouro = new LogradouroDto(cepId.getLogradouro());
    }

    public CidadeDto getCidade() {
        return cidade;
    }

    public BairroDto getBairro() {
        return bairro;
    }

    public LogradouroDto getLogradouro() {
        return logradouro;
    }

    public static List<CepDto> converter(List<Cep> items) {
        return items.stream().map(CepDto::new).collect(Collectors.toList());
    }

    public static Page<CepDto> converter(Page<Cep> items) {
        return items.map(CepDto::new);
    }
}
