/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoSaida;

import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import br.edu.ifms.beneficios.modelo.DocumentoSaida;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class DocumentoSaidaDetalheDto extends AbstractDocumentoSaidaDto {

    private List<ItemSaidaDto> itens = new ArrayList();

    public DocumentoSaidaDetalheDto() {
    }

    public DocumentoSaidaDetalheDto(DocumentoSaida obj) {
        super(obj);
        this.itens = ItemSaidaDto.converter(obj.getItens());
    }

    public List<ItemSaidaDto> getItens() {
        return itens;
    }
    
    public static Page<DocumentoSaidaDetalheDto> converter(Page<DocumentoSaida> items) {
        return items.map(DocumentoSaidaDetalheDto::new);
    }
    
    public static List<DocumentoSaidaDetalheDto> converter(List<DocumentoSaida> items) {
        return items.stream().map(DocumentoSaidaDetalheDto::new).collect(Collectors.toList());
    }
    
}
