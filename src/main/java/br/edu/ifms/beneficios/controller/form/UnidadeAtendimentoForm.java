/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.EnderecoDto;
import br.edu.ifms.beneficios.controller.dto.TipoUnidadeAtendimentoDto;
import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import javax.validation.constraints.NotEmpty;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.id.EnderecoId;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.CepRepository;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.EnderecoRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.TipoUnidadeAtendimentoRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import java.util.Optional;
import javax.validation.constraints.NotNull;

/**
 *
 * @author tanabe
 */
public class UnidadeAtendimentoForm {

    @NotEmpty
    private String nome;

    @NotEmpty
    private String numeroDaUnidade;

    @NotNull
    private TipoUnidadeAtendimentoDto tipoUnidadeAtendimento;

    @NotNull
    private EnderecoDto endereco;
    
    private Boolean matriz;

    public UnidadeAtendimentoForm() {
    }
    
    public UnidadeAtendimentoForm(UnidadeAtendimento obj) {
        this.nome = obj.getNome();
        this.numeroDaUnidade = obj.getNumeroDaUnidade();
        this.tipoUnidadeAtendimento = new TipoUnidadeAtendimentoDto(obj.getTipoUnidadeAtendimento());
        this.endereco = new EnderecoDto(obj.getEndereco());
        this.matriz = obj.isMatriz();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroDaUnidade() {
        return numeroDaUnidade;
    }

    public void setNumeroDaUnidade(String numeroDaUnidade) {
        this.numeroDaUnidade = numeroDaUnidade;
    }

    public TipoUnidadeAtendimentoDto getTipoUnidadeAtendimento() {
        return tipoUnidadeAtendimento;
    }

    public void setTipoUnidadeAtendimento(TipoUnidadeAtendimentoDto tipoUnidadeAtendimento) {
        this.tipoUnidadeAtendimento = tipoUnidadeAtendimento;
    }

    public EnderecoDto getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDto endereco) {
        this.endereco = endereco;
    }

    public Boolean getMatriz() {
        return matriz;
    }

    public void setMatriz(Boolean matriz) {
        this.matriz = matriz;
    }
    
    public Boolean isMatriz() {
        return this.matriz;
    }

    private UnidadeAtendimento toUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento,
            EnderecoRepository enderecoRepository,
            CepRepository cepRepository,
            LogradouroRepository logradouroRepository,
            BairroRepository bairroRepository,
            CidadeRepository cidadeRepository,
            TipoUnidadeAtendimentoRepository tipoRepository) {
        TipoUnidadeAtendimento tipo = tipoRepository
                .getOne(tipoUnidadeAtendimento.getId());
        
        Cep cep = ControllerHelper.getCep(
                endereco, 
                cepRepository, cidadeRepository, bairroRepository, logradouroRepository);
        EnderecoId enderecoId = new EnderecoId(cep, this.endereco.getNumero());
        Optional<Endereco> optional = enderecoRepository.findById(enderecoId);
        Endereco endereco = null;
        if (optional.isPresent()) {
            endereco = optional.get();
        } else {
            endereco = new Endereco(enderecoId);
        }
        endereco.setComplemento(this.endereco.getComplemento());
        endereco.setReferencia(this.endereco.getReferencia());

        unidadeAtendimento.setNome(this.nome);
        unidadeAtendimento.setNumeroDaUnidade(this.numeroDaUnidade);
        unidadeAtendimento.setMatriz(this.matriz);

        unidadeAtendimento.setEndereco(ControllerHelper.getEndereco(this.endereco,
                enderecoRepository, cepRepository,
                cidadeRepository, bairroRepository,
                logradouroRepository));
        unidadeAtendimento.setTipoUnidadeAtendimento(tipo);

        return unidadeAtendimento;
    }

    public UnidadeAtendimento converter(
            UnidadeAtendimentoRepository repository,
            EnderecoRepository enderecoRepository,
            CepRepository cepRepository,
            LogradouroRepository logradouroRepository,
            BairroRepository bairroRepository,
            CidadeRepository cidadeRepository,
            TipoUnidadeAtendimentoRepository tipoRepository) {
        Long maxId = repository.getMaxId() + 1;
        UnidadeAtendimento unidadeAtendimento = new UnidadeAtendimento(maxId);
        
        return toUnidadeAtendimento(unidadeAtendimento,
                enderecoRepository, cepRepository, logradouroRepository,
                bairroRepository, cidadeRepository,
                tipoRepository);
    }

    public UnidadeAtendimento atualizar(Long id,
            UnidadeAtendimentoRepository repository,
            EnderecoRepository enderecoRepository,
            CepRepository cepRepository,
            LogradouroRepository logradouroRepository,
            BairroRepository bairroRepository,
            CidadeRepository cidadeRepository,
            TipoUnidadeAtendimentoRepository tipoRepository) {

        UnidadeAtendimento unidadeAtendimento = repository.getOne(id);
        return toUnidadeAtendimento(unidadeAtendimento, enderecoRepository,
                cepRepository, logradouroRepository,
                bairroRepository, cidadeRepository, tipoRepository);
    }
}
