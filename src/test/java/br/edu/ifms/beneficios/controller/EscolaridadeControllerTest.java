/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.EscolaridadeForm;
import br.edu.ifms.beneficios.modelo.Escolaridade;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EscolaridadeControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/escolaridades";
    private Gson gson = new Gson();
    private EscolaridadeForm escolaridade;

    @BeforeEach
    public void setUp() {
        BeneficiosFactory.MVC = mvc;
        escolaridade = new EscolaridadeForm();
        escolaridade.setDescricao("Analfabeto");
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosEscolaridades() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1Escolaridade() throws Exception {
        Escolaridade obj = BeneficiosFactory.createEscolaridade("Ensino Fundamental");
        
        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarEscolaridadeComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmEscolaridade() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(escolaridade));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(4)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmEscolaridade() throws Exception {
        Escolaridade obj = BeneficiosFactory.createEscolaridade("Ensino Médio");

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        obj.setDescricao("Ensino Superior");
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(obj));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("Ensino Superior")));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmEscolaridade() throws Exception {
        Escolaridade obj = BeneficiosFactory.createEscolaridade("Ensino Técnico");

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmEscolaridadeComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

}
