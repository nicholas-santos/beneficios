/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.MenuTipo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author santos
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class Menu implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String nome;
    private String remotePath;
    
    @Enumerated(EnumType.STRING)
    private MenuTipo tipo;
    
    @Column(columnDefinition = "boolean default true")
    private Boolean disponivel;
    
    @OneToMany(mappedBy = "id.menu", fetch = FetchType.LAZY)
    @Cascade({ CascadeType.ALL })
    private List<PerfilMenu> perfis = new ArrayList();

    public Menu() {
    }

    public Menu(String nome, String remotePath, Boolean disponivel) {
        this.nome = nome;
        this.remotePath = remotePath;
        this.disponivel = disponivel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    public MenuTipo getTipo() {
        return tipo;
    }

    public void setTipo(MenuTipo tipo) {
        this.tipo = tipo;
    }

    public Boolean getDisponivel() {
        return disponivel;
    }

    public void setDisponivel(Boolean disponivel) {
        this.disponivel = disponivel;
    }

    public List<PerfilMenu> getPerfis() {
        return perfis;
    }

    public void setPerfis(List<PerfilMenu> perfis) {
        this.perfis = perfis;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.id);
        hash = 19 * hash + Objects.hashCode(this.nome);
        hash = 19 * hash + Objects.hashCode(this.remotePath);
        hash = 19 * hash + Objects.hashCode(this.tipo);
        hash = 19 * hash + Objects.hashCode(this.disponivel);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Menu other = (Menu) obj;
        if (!Objects.equals(this.nome, other.getNome())) {
            return false;
        }
        if (!Objects.equals(this.remotePath, other.getRemotePath())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (this.tipo != other.getTipo()) {
            return false;
        }
        if (!Objects.equals(this.disponivel, other.getDisponivel())) {
            return false;
        }
        return true;
    }
    
}
