#!/bin/bash

if [[ -z "$1" ]]; then
  echo "Uso: $0 argumento1"
  echo "argumento1: identificacao da versao do WAR"
  exit 1
fi

echo "Gerando xml ..."
cp target/beneficios.war beneficios.xml
echo "Adicionando no GIT"
git add beneficios.xml
echo "Commit do arquivo no repositorio"
git commit -m "Versão $1"
echo "atualizando remote ..."
git push origin master
