/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.tipos.LocalDateAttributeConverter;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class UnidadeFuncionarioId implements Serializable {
    
    @ManyToOne
    private Funcionario funcionario;
    
    @ManyToOne
    private UnidadeAtendimento unidadeAtendimento;
    
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate registro;

    public UnidadeFuncionarioId() {
    }

    public UnidadeFuncionarioId(Funcionario funcionario, UnidadeAtendimento unidadeAtendimento, LocalDate registro) {
        this.funcionario = funcionario;
        this.unidadeAtendimento = unidadeAtendimento;
        this.registro = registro;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public LocalDate getRegistro() {
        return registro;
    }

    public void setRegistro(LocalDate registro) {
        this.registro = registro;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.funcionario);
        hash = 31 * hash + Objects.hashCode(this.unidadeAtendimento);
        hash = 31 * hash + Objects.hashCode(this.registro);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeFuncionarioId other = (UnidadeFuncionarioId) obj;
        if (!Objects.equals(this.funcionario, other.funcionario)) {
            return false;
        }
        if (!Objects.equals(this.unidadeAtendimento, other.unidadeAtendimento)) {
            return false;
        }
        if (!Objects.equals(this.registro, other.registro)) {
            return false;
        }
        return true;
    }
    
    
}
