/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.ifms.beneficios.modelo.CondicaoMoradia;

/**
 *
 * @author tanabe
 */
public interface CondicaoMoradiaRepository extends JpaRepository<CondicaoMoradia, Long> {
    
}
