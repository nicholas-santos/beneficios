/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author santos
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class UnidadeAtendimento implements Serializable {
    @Id
    private Long id;
    private String nome;
    private String numeroDaUnidade;
    
    @Column(columnDefinition = "boolean default false")
    private Boolean matriz;
    
    @OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumns({
        @JoinColumn(name = "endereco_cep_bairro_id", referencedColumnName = "cep_bairro_id"),
        @JoinColumn(name = "endereco_cep_cidade_id", referencedColumnName = "cep_cidade_id"),
        @JoinColumn(name = "endereco_cep_logradouro_id", referencedColumnName = "cep_logradouro_id"),
        @JoinColumn(name = "endereco_numero", referencedColumnName = "numero")
    })
    private Endereco endereco;
    
    @ManyToOne
    private TipoUnidadeAtendimento tipoUnidadeAtendimento;
    
    @OneToMany(mappedBy = "id.funcionario", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<UnidadeFuncionario> funcionarios = new ArrayList();

    public UnidadeAtendimento() {
    }
    
    public UnidadeAtendimento(Long id) {
        this.id = id;
    }

    public UnidadeAtendimento(String nome, String numeroDaUnidade, Endereco endereco, 
            TipoUnidadeAtendimento tipoUnidadeAtendimento) {
        this.nome = nome;
        this.numeroDaUnidade = numeroDaUnidade;
        this.endereco = endereco;
        this.tipoUnidadeAtendimento = tipoUnidadeAtendimento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroDaUnidade() {
        return numeroDaUnidade;
    }

    public void setNumeroDaUnidade(String numeroDaUnidade) {
        this.numeroDaUnidade = numeroDaUnidade;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Boolean getMatriz() {
        return matriz;
    }

    public void setMatriz(Boolean matriz) {
        this.matriz = matriz;
    }
    
    public Boolean isMatriz() {
        return this.matriz;
    }

    public TipoUnidadeAtendimento getTipoUnidadeAtendimento() {
        return tipoUnidadeAtendimento;
    }

    public void setTipoUnidadeAtendimento(TipoUnidadeAtendimento tipoUnidadeAtendimento) {
        this.tipoUnidadeAtendimento = tipoUnidadeAtendimento;
    }

    public List<UnidadeFuncionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(List<UnidadeFuncionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        hash = 89 * hash + Objects.hashCode(this.nome);
        hash = 89 * hash + Objects.hashCode(this.numeroDaUnidade);
        hash = 89 * hash + Objects.hashCode(this.endereco);
        hash = 89 * hash + Objects.hashCode(this.tipoUnidadeAtendimento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final UnidadeAtendimento other = (UnidadeAtendimento) obj;
        if (!Objects.equals(this.nome, other.getNome())) {
            return false;
        }
        if (!Objects.equals(this.numeroDaUnidade, other.getNumeroDaUnidade())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.endereco, other.getEndereco())) {
            return false;
        }
        if (!Objects.equals(this.tipoUnidadeAtendimento, other.getTipoUnidadeAtendimento())) {
            return false;
        }
        return true;
    }
    
}
