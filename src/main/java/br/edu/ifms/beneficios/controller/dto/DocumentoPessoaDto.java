/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.DocumentoPessoa;
import br.edu.ifms.beneficios.modelo.id.DocumentoPessoaId;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author santos
 */
public class DocumentoPessoaDto {

    @NotNull
    @NotEmpty
    private String numero;
    @NotNull
    private DocumentoDto documentoDto;
    private LocalDate emissao;
    private OrgaoExpedidorDto orgaoExpedidorDto;
    private Boolean deleted;

    public DocumentoPessoaDto() {
        this.deleted = Boolean.FALSE;
    }

    public DocumentoPessoaDto(DocumentoPessoa obj) {
        this();
        DocumentoPessoaId id = obj.getId();
        this.numero = id.getNumero();
        this.documentoDto = new DocumentoDto(id.getDocumento());
        this.emissao = obj.getEmissao();
        this.orgaoExpedidorDto = new OrgaoExpedidorDto(obj.getOrgaoExpedidor());
        this.deleted = obj.isDeleted();
    }

    public String getNumero() {
        return numero;
    }

    public DocumentoDto getDocumentoDto() {
        return documentoDto;
    }

    public LocalDate getEmissao() {
        return emissao;
    }

    public OrgaoExpedidorDto getOrgaoExpedidorDto() {
        return orgaoExpedidorDto;
    }

    public Long getOrgaoExpedidorDtoId() {
        if (orgaoExpedidorDto != null) {
            return orgaoExpedidorDto.getId();
        }
        return null;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public static List<DocumentoPessoaDto> converter(List<DocumentoPessoa> items) {
        return items.stream().map(DocumentoPessoaDto::new).collect(Collectors.toList());
    }

}
