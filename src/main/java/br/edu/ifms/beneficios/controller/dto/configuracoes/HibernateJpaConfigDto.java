/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.configuracoes;

import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;

/**
 *
 * @author santos
 */
public class HibernateJpaConfigDto {
    private String ddlAuto;
    private boolean showSql;

    public HibernateJpaConfigDto() {
    }

    public HibernateJpaConfigDto(HibernateProperties hibernateProperties,
            JpaProperties jpaProperties) {
        this.ddlAuto = hibernateProperties.getDdlAuto();
        this.showSql = jpaProperties.isShowSql();
    }

    public String getDdlAuto() {
        return ddlAuto;
    }

    public boolean isShowSql() {
        return showSql;
    }
    
}
