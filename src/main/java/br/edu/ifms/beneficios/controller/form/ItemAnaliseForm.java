/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.modelo.Analise;
import br.edu.ifms.beneficios.modelo.ItemAnalise;
import br.edu.ifms.beneficios.modelo.id.ItemAnaliseId;
import br.edu.ifms.beneficios.repository.AnaliseRepository;
import br.edu.ifms.beneficios.repository.ItemAnaliseRepository;
import javax.validation.constraints.NotNull;

/**
 *
 * @author santos
 */
public class ItemAnaliseForm {
    
    @NotNull
    private Long itemId;
    @NotNull
    private Long analiseId;
    private String observacao;

    public ItemAnaliseForm() {
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getAnaliseId() {
        return analiseId;
    }

    public void setAnaliseId(Long analiseId) {
        this.analiseId = analiseId;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public ItemAnalise atualizar(ItemAnaliseRepository repository,
            AnaliseRepository analiseRepository) {
        Analise analise = analiseRepository.getOne(analiseId);
        
        ItemAnaliseId itemAnaliseId = new ItemAnaliseId(itemId, analise);
        ItemAnalise itemAnalise = repository.getOne(itemAnaliseId);

        return itemAnalise;
    }
    
}
