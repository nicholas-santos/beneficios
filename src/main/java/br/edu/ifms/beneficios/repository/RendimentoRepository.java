/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.Rendimento;
import br.edu.ifms.beneficios.modelo.id.RendimentoId;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface RendimentoRepository extends JpaRepository<Rendimento, RendimentoId> {

    void deleteByIdPessoa(Pessoa pessoa);
    
}
