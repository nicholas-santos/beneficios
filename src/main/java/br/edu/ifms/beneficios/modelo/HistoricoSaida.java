/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
public class HistoricoSaida extends AbstractHistorico {

    @ManyToOne
    private DocumentoSaida documentoSaida;

    public HistoricoSaida() {
        super();
    }

    public DocumentoSaida getDocumentoSaida() {
        return documentoSaida;
    }

    public void setDocumentoSaida(DocumentoSaida documentoSaida) {
        this.documentoSaida = documentoSaida;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.documentoSaida);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final HistoricoSaida other = (HistoricoSaida) obj;
        if (!Objects.equals(this.documentoSaida, other.getDocumentoSaida())) {
            return false;
        }
        return true;
    }
    
}
