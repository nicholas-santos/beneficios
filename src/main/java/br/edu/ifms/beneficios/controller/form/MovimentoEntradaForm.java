/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.documentoEntrada.ItemEntradaDetalheDto;
import br.edu.ifms.beneficios.modelo.ItemEntrada;
import br.edu.ifms.beneficios.modelo.MovimentoEntrada;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.ItemEntradaRepository;
import java.math.BigDecimal;
import java.util.Optional;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author santos
 */
public class MovimentoEntradaForm {

    @NotNull
    private ItemEntradaDetalheDto itemEntrada;

    @NotNull
    @Min(1)
    private BigDecimal quantidade;

    public ItemEntradaDetalheDto getItemEntrada() {
        return itemEntrada;
    }

    public void setItemEntrada(ItemEntradaDetalheDto itemEntrada) {
        this.itemEntrada = itemEntrada;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }
    
    public MovimentoEntrada converter(ItemEntradaRepository itemEntradaRepository) {
        Optional<ItemEntrada> opItem = itemEntradaRepository.findByIdDocumentoEntradaIdAndIdNumero(
                this.itemEntrada.getDocumentoEntradaId(),
                this.itemEntrada.getNumero()
        );
        ItemEntrada item = opItem.get();
        if (item.getSaldo().compareTo(quantidade) < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                "Quantidade solicitada é maior do que o saldo disponível");
        }
        if (item.getSaldo().subtract(quantidade).compareTo(BigDecimal.ZERO) <= 0) {
            item.setStatus(Status.RECEBIDO);
        } else {
            item.setStatus(Status.PARCIAL);
        }
        
        MovimentoEntrada movimentoEntrada = new MovimentoEntrada();
        movimentoEntrada.setItemEntrada(item);
        movimentoEntrada.setQuantidade(quantidade);
        return movimentoEntrada;
    }
    
}
