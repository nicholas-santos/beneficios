/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Escolaridade;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class EscolaridadeDto {
    
    private Long id;
    private String descricao;

    public EscolaridadeDto() {
    }

    public EscolaridadeDto(Escolaridade obj) {
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public static Page<EscolaridadeDto> converter(Page<Escolaridade> items) {
        return items.map(EscolaridadeDto::new);
    }
    
    public static List<EscolaridadeDto> converter(List<Escolaridade> items) {
        return items.stream().map(EscolaridadeDto::new).collect(Collectors.toList());
    }
    
}
