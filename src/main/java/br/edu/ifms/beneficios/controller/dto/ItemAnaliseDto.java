/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.controller.dto.beneficioEventual.BeneficioEventualDto;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.modelo.Analise;
import br.edu.ifms.beneficios.modelo.ItemAnalise;
import br.edu.ifms.beneficios.modelo.id.ItemAnaliseId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class ItemAnaliseDto {

    private Long id;
    private Long analiseId;
    private PessoaDetalheDto pessoa;
    private BeneficioEventualDto beneficioEventual;
    private BigDecimal quantidade;
    private Status status;
    private UnidadeAtendimentoDto unidadeAtendimento;

    public ItemAnaliseDto(ItemAnalise obj) {
        ItemAnaliseId itemId = obj.getId();
        Analise analise = itemId.getAnalise();
        this.id = itemId.getId();
        this.analiseId = analise.getId();
        
        this.pessoa = new PessoaDetalheDto(obj.getId().getAnalise().getAtendimento().getPessoa());
        this.beneficioEventual = new BeneficioEventualDto(obj.getBeneficioEventual());
        this.quantidade = obj.getQuantidade();
        this.status = obj.getStatus();
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getId().getAnalise().getTecnico().getUnidadeAtendimento());
    }

    public ItemAnaliseDto() {
        this.status = Status.PENDENTE;
    }

    public Long getId() {
        return id;
    }

    public Long getAnaliseId() {
        return analiseId;
    }

    public PessoaDetalheDto getPessoa() {
        return pessoa;
    }

    public BeneficioEventualDto getBeneficioEventual() {
        return beneficioEventual;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public Status getStatus() {
        return status;
    }
    
    public Boolean hasBeneficioEventual() {
        return this.beneficioEventual != null;
    }
    
    public Boolean hasQuantidade() {
        return this.quantidade != null && this.quantidade.compareTo(BigDecimal.ZERO) > 0;
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public static List<ItemAnaliseDto> converter(List<ItemAnalise> items) {
        return items.stream().map(ItemAnaliseDto::new).collect(Collectors.toList());
    }
    
    public static Page<ItemAnaliseDto> converter(Page<ItemAnalise> items) {
        return items.map(ItemAnaliseDto::new);
    }
    
}
