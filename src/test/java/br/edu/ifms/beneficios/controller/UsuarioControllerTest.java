/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import java.net.URI;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/usuarios";
    
    @Test
    public void deveriaDevolver200AoBuscarTodosUsuarios() throws Exception {
    	  System.out.println();
          System.out.println("********************************");
          System.out.println("DEVOLVER TODOS USUARIOS");
          System.out.println("********************************");
          System.out.println();
    	URI uri = new URI(sURI);
        mvc.perform(MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .is(200));
    }
    
    @Test
    public void deveriaDevolver200AoBuscarUmUsuario() throws Exception {
    	  System.out.println();
          System.out.println("********************************");
          System.out.println("DEVOLVER UM USUARIO");
          System.out.println("********************************");
          System.out.println();
    	URI uri = new URI(sURI + "/1");
        mvc.perform(MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .is(200));
    }
    
    @Test
    public void deveriaDevolver200AoBuscarUmUsuarioPeloEmail() throws Exception {
    	System.out.println();
    	System.out.println("********************************");
    	System.out.println("DEVOLVER UM USUARIO PELO EMAIL");
    	System.out.println("********************************");
    	System.out.println();
    	URI uri = new URI(sURI + "/email/admin@email.com");
    	mvc.perform(MockMvcRequestBuilders
    			.get(uri)
    			.accept(MediaType.APPLICATION_JSON))
    	.andExpect(MockMvcResultMatchers
    			.status()
    			.is(200));
    }
    
    @Test
    public void deveriaDevolver404AoBuscarUsuarioComUmIdInexistente() throws Exception {
    	System.out.println();
        System.out.println("********************************");
        System.out.println("USUARIO INEXISTENTE");
        System.out.println("********************************");
        System.out.println();
    	URI uri = new URI(sURI + "/999");
        mvc.perform(MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .is(404));
    }
    
    @Test
    public void deveriaDevolver201AoCadastrarUmUsuario() throws Exception {
    	System.out.println();
        System.out.println("********************************");
        System.out.println("CADASTRAR UM USUARIO");
        System.out.println("********************************");
        System.out.println();
    	URI uri = new URI(sURI);
        String json = "{\"nome\":\"Caroline\", \"email\":\"carol@email.com\","
                + "\"funcionario\": {"
                + "   \"id\": \"1\",  "
                + "   \"nome\": \"teste\" "
                + "}"
                + "}";
        mvc.perform(MockMvcRequestBuilders
                .post(uri)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .is(201));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmUsuario() throws Exception {
    	System.out.println();
        System.out.println("********************************");
        System.out.println("ALTERAR UM USUARIO");
        System.out.println("********************************");
        System.out.println();
    	URI uri = new URI(sURI + "/3");
        // dados a serem alterados
        String json = "{"
                + "\"nome\":\"Emily\", "
                + "\"email\":\"my@email.com\","
                + "\"funcionario\": {"
                + "   \"id\": \"1\",  "
                + "   \"nome\": \"teste\" "
                + "}"
                + "}";
        mvc.perform(MockMvcRequestBuilders
                .put(uri)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .is(200));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmUsuario() throws Exception {
    	System.out.println();
        System.out.println("********************************");
        System.out.println("REMOVER UM USUARIO");
        System.out.println("********************************");
        System.out.println();
        URI uri = new URI(sURI);
//        String json = "{\"nome\":\"Caroline\", \"email\":\"carol@email.com\","
//                + "\"funcionario\": {"
//                + "   \"id\": \"3\",  "
//                + "   \"nome\": \"teste\" "
//                + "}"
//                + "}";
//        mvc.perform(MockMvcRequestBuilders
//                .post(uri)
//                .content(json)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers
//                        .status()
//                        .is(201));
        
    	uri = new URI(sURI + "/3");
        mvc.perform(MockMvcRequestBuilders
                .delete(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .is(200));
    }
    
    @Test
    public void deveriaDevolver404AoRemoverUmUsuarioComIdInexiste() throws Exception {
    	System.out.println();
        System.out.println("********************************");
        System.out.println("REMOVER UM USUARIO INEXISTENTE");
        System.out.println("********************************");
        System.out.println();
    	URI uri = new URI(sURI + "/999");
        mvc.perform(MockMvcRequestBuilders
                .delete(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .is(404));
    }

}
