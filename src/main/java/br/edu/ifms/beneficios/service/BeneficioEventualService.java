/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.beneficioEventual.BeneficioEventualDto;
import br.edu.ifms.beneficios.controller.form.BeneficioEventualForm;
import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import br.edu.ifms.beneficios.repository.BeneficioEventualRepository;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author santos
 */
@Service
public class BeneficioEventualService {

    @Autowired
    private BeneficioEventualRepository repository;

    public Page<BeneficioEventual> listar(
            String descricao,
            Pageable paginacao
    ) {
        Boolean hasNomeBeneficioEventual = descricao != null
                && !descricao.isBlank() && !descricao.isEmpty();
        Page<BeneficioEventual> items;
        if (hasNomeBeneficioEventual) {
            items = repository.findByDescricaoContainingIgnoreCase(descricao, paginacao);
        } else {
            items = repository.findAll(paginacao);
        }
        return items;
    }

    public List<BeneficioEventual> listar() {
        return repository.findAll();
    }

    public BeneficioEventual buscarPor(Long id) {
        Optional<BeneficioEventual> optional = repository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Não existe benefício eventual com o código " + id);
    }
    
    public BeneficioEventualDto criar(BeneficioEventualForm form) {
        BeneficioEventual obj = form.converter(repository);

        obj = repository.save(obj);
        return new BeneficioEventualDto(obj);
    }

    public BeneficioEventualDto atualizar(Long id, BeneficioEventualForm form) {
        try {
            BeneficioEventual obj = form.atualizar(id, repository);
            return new BeneficioEventualDto(obj);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Não existe benefício eventual com o código " + id, ex);
        }
    }

    public void remover(Long id) {
        Optional<BeneficioEventual> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Não existe benefício eventual com o código " + id);
        }
    }
}
