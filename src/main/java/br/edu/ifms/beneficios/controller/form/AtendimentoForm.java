/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDto;
import br.edu.ifms.beneficios.modelo.Atendimento;
import br.edu.ifms.beneficios.repository.AtendimentoRepository;
import br.edu.ifms.beneficios.repository.PessoaRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author santos
 */
public class AtendimentoForm {

    @NotNull
    private PessoaDto pessoa;

    @NotNull
    @NotEmpty
    private String descricao;

    public PessoaDto getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaDto pessoa) {
        this.pessoa = pessoa;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    private Atendimento toAtendimento(Atendimento atendimento,
            PessoaRepository pessoaRepository) {
        atendimento.setPessoa(pessoaRepository.getOne(this.pessoa.getId()));
        atendimento.setDescricao(this.descricao);
        return atendimento;
    }

    public Atendimento converter(PessoaRepository pessoaRepository) {
        return toAtendimento(new Atendimento(), pessoaRepository);
    }

    public Atendimento atualizar(Long id,
            AtendimentoRepository repository,
            PessoaRepository pessoaRepository) {
        return toAtendimento(repository.getOne(id), pessoaRepository);
    }
}
