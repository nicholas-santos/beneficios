/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.ItemAnaliseDto;
import br.edu.ifms.beneficios.controller.form.ItemAnaliseForm;
import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.modelo.HistoricoItemAnalise;
import br.edu.ifms.beneficios.modelo.ItemAnalise;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.AnaliseRepository;
import br.edu.ifms.beneficios.repository.ItemAnaliseRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/item-analise")
public class ItemAnaliseController {

    @Autowired
    private ItemAnaliseRepository repository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private AnaliseRepository analiseRepository;

    @GetMapping
    public Page<ItemAnaliseDto> listar(
            @RequestParam(required = false) Long pessoaId,
            @RequestParam(required = false) Status status,
            @PageableDefault(sort = "beneficioEventual",
                    direction = Sort.Direction.ASC,
                    page = 0, size = 10) Pageable paginacao) {

        boolean hasPessoa = pessoaId != null && pessoaId > 0,
                hasStatus = status != null;
        Page<ItemAnalise> items;

        if (hasPessoa && hasStatus) {
            items = repository.findByStatusAndIdAnaliseAtendimentoPessoaId(status, pessoaId, paginacao);
        } else if (hasPessoa) {
            items = repository.findByIdAnaliseAtendimentoPessoaId(pessoaId, paginacao);
        } else if (hasStatus) {
            items = repository.findByStatus(status, paginacao);
        } else {
            items = repository.findAll(paginacao);
        }

        return ItemAnaliseDto.converter(items);
    }

    @PostMapping("/retirar")
    @Transactional
    public ResponseEntity<ItemAnaliseDto> retirar(@RequestBody @Valid ItemAnaliseForm form,
            UriComponentsBuilder uriBuilder) {

        ItemAnalise itemAnalise = form.atualizar(repository, analiseRepository);
        itemAnalise.retirar();

        Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);

        HistoricoItemAnalise his = new HistoricoItemAnalise(null, itemAnalise, usuario.getFuncionario());
        his.setObservacao(form.getObservacao());
        itemAnalise.add(his);

        return ResponseEntity.ok(new ItemAnaliseDto(itemAnalise));
    }

}
