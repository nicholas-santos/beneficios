/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.ItemSaidaId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author santos
 */
//@EntityListeners(AuditoriaListener.class)
@Entity
public class ItemSaida implements Serializable {

    @EmbeddedId
    private ItemSaidaId id;
    private BigDecimal quantidade;

    @ManyToOne
    private BeneficioEventual beneficioEventual;

    @Column(name = "status", nullable = false)
    @Convert(converter = StatusConverter.class)
    private Status status;
    
    @ManyToOne
    private UnidadeAtendimento unidadeAtendimento;

    @Transient
    private Boolean deleted;

    @OneToMany(mappedBy = "itemSaida")
    private List<MovimentoSaida> movimentos = new ArrayList();

    public ItemSaida() {
        this.status = Status.PENDENTE;
        this.deleted = Boolean.FALSE;
    }

    public ItemSaida(ItemSaidaId id, BigDecimal quantidade, 
            BeneficioEventual beneficioEventual, UnidadeAtendimento unidadeAtendimento) {
        this();
        this.id = id;
        this.quantidade = quantidade;
        this.beneficioEventual = beneficioEventual;
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public ItemSaidaId getId() {
        return id;
    }

    public void setId(ItemSaidaId id) {
        this.id = id;
    }

    public Boolean isSetNumero() {
        return id.getNumero() != null;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BeneficioEventual getBeneficioEventual() {
        return beneficioEventual;
    }

    public void setBeneficioEventual(BeneficioEventual beneficioEventual) {
        this.beneficioEventual = beneficioEventual;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
        this.movimentos.clear();
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public List<MovimentoSaida> getMovimentos() {
        return movimentos;
    }

    public void setMovimentos(List<MovimentoSaida> movimentos) {
        this.movimentos = movimentos;
    }

    public void add(MovimentoSaida movimento) {
        this.movimentos.add(movimento);
    }

    public void remove(MovimentoSaida movimento) {
        this.movimentos.remove(movimento);
        movimento.setItemSaida(null);
    }
    
    public BigDecimal getQuantidadeMovimentada() {
        if (this.getMovimentos().isEmpty()) {
            return BigDecimal.ZERO;
        }
        return this.getMovimentos().stream()
                .map(m -> m.getQuantidade())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    
    public BigDecimal getSaldo() {
        return this.quantidade.subtract(getQuantidadeMovimentada());
    }
    
    public Boolean hasSaldo() {
        return this.getSaldo().compareTo(BigDecimal.ZERO) > 0;
    }
    
    public Boolean isEnviado() {
        return Status.ENVIADO.equals(this.status);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.quantidade);
        hash = 97 * hash + Objects.hashCode(this.beneficioEventual);
        hash = 97 * hash + Objects.hashCode(this.unidadeAtendimento);
        hash = 97 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ItemSaida other = (ItemSaida) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.quantidade, other.getQuantidade())) {
            return false;
        }
        if (!Objects.equals(this.beneficioEventual, other.getBeneficioEventual())) {
            return false;
        }
        if (!Objects.equals(this.unidadeAtendimento, other.getUnidadeAtendimento())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        return true;
    }
}
