/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoSaida;

import br.edu.ifms.beneficios.modelo.ItemSaida;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class ItemSaidaDto extends AbstractItemSaidaDto {

    public ItemSaidaDto() {
    }

    public ItemSaidaDto(ItemSaida obj) {
        super(obj);
    }

    public static Page<ItemSaidaDto> converter(Page<ItemSaida> items) {
        return items.map(ItemSaidaDto::new);
    }

    public static List<ItemSaidaDto> converter(List<ItemSaida> items) {
        return items.stream().map(ItemSaidaDto::new).collect(Collectors.toList());
    }
    
}
