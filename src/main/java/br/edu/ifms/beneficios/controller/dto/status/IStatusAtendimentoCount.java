/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.controller.dto.status;

import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimento;

/**
 *
 * @author santos
 */
public interface IStatusAtendimentoCount {
    StatusAtendimento getStatusAtendimento();
    Long getTotalStatus();
    Double getPercentualStatus();
}
