/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractHistorico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime emissao;

    @Column(name = "status", nullable = true)
    @Convert(converter = StatusConverter.class)
    private Status status;
    
    @ManyToOne
    private Funcionario funcionario;

    public AbstractHistorico() {
        this.emissao = LocalDateTime.now();
        this.status = Status.ATIVO;
    }

    public AbstractHistorico(Long id, LocalDateTime emissao, Status status) {
        this.id = id;
        this.emissao = emissao;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDateTime emissao) {
        this.emissao = emissao;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.emissao);
        hash = 83 * hash + Objects.hashCode(this.status);
        hash = 83 * hash + Objects.hashCode(this.funcionario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final AbstractHistorico other = (AbstractHistorico) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        if (this.funcionario != other.getFuncionario()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbstractHistorico{" + "id=" + id + ", emissao=" + emissao + ", status=" + status + ", funcionario=" + funcionario + '}';
    }
    
}
