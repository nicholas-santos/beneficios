/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.config.validacao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author santos
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Erro no sistema de Benefícos Eventuais")
public class BeneficioErrosException extends Exception {

    public BeneficioErrosException() {
    }

    public BeneficioErrosException(String message) {
        super(message);
    }

    public BeneficioErrosException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeneficioErrosException(Throwable cause) {
        super(cause);
    }

    public BeneficioErrosException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
