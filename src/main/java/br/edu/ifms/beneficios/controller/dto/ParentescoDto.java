/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Parentesco;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class ParentescoDto {
    
    private Long id;
    private String descricao;

    public ParentescoDto() {
    }

    public ParentescoDto(Parentesco obj) {
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public static Page<ParentescoDto> converter(Page<Parentesco> items) {
        return items.map(ParentescoDto::new);
    }
    
    public static List<ParentescoDto> converter(List<Parentesco> items) {
        return items.stream().map(ParentescoDto::new).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object obj) {
        final ParentescoDto other = (ParentescoDto) obj;
        if (!Objects.equals(this.descricao, other.getDescricao())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }
    
}
