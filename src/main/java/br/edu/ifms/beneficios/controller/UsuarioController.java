/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.ListaUsuarioDto;
import br.edu.ifms.beneficios.controller.dto.MessageDto;
import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifms.beneficios.controller.dto.usuario.UsuarioDetalheDto;
import br.edu.ifms.beneficios.controller.dto.usuario.UsuarioDto;
import br.edu.ifms.beneficios.controller.form.ChangePasswordForm;
import br.edu.ifms.beneficios.controller.form.UsuarioForm;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.repository.ArquivoRepository;
import br.edu.ifms.beneficios.service.FileStorageService;
import br.edu.ifms.beneficios.service.UsuarioService;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos e tanabe
 */
@RestController
@RequestMapping("api/usuarios")
@Profile(value = {"prod", "test"})
public class UsuarioController {

    @Autowired
    private ArquivoRepository arquivoRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private UsuarioService service;

    @GetMapping()
    public Page<UsuarioDto> listar(
            @RequestParam(required = false) Long funcionarioId,
            @PageableDefault(sort = "nome", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

        Page<Usuario> usuarios = service.listar(funcionarioId, paginacao);
        return UsuarioDto.converter(usuarios);
    }

    @GetMapping("/list")
    public List<ListaUsuarioDto> listar() {
        List<Usuario> items = service.listarAtivos();
        return ListaUsuarioDto.converter(items);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UsuarioDto> visualizar(@PathVariable Long id) {
        Optional<Usuario> usuario = service.buscarPor(id);

        if (usuario.isPresent()) {
            return ResponseEntity.ok(new UsuarioDto(usuario.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<UsuarioDetalheDto> visualizar(@PathVariable String email) {
        UsuarioDetalheDto dto = service.buscarPor(email);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    public ResponseEntity<UsuarioDetalheDto> criar(@RequestBody @Valid UsuarioForm form, UriComponentsBuilder uriBuilder) {
        UsuarioDetalheDto dto = service.criar(form);

        URI uri = uriBuilder.path("/usuarios/{id}").buildAndExpand(dto.getId()).toUri();
        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<UsuarioDetalheDto> atualizar(@PathVariable Long id, @RequestBody @Valid UsuarioForm form) {
        UsuarioDetalheDto dto = service.atualizar(id, form);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Usuario> optional = service.remover(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@Valid @RequestBody ChangePasswordForm form) {
        if (!form.getPassword().equals(form.getPasswordConfirm())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
                    new MessageDto(HttpStatus.BAD_REQUEST.value(),
                            "Erro de alteração de senha",
                            "A nova senha e a senha de confirmação não combinam!")
            );
        }

        Optional<Usuario> optional = service
                .resetPassword(form.getToken(), form.getPassword());
        if (!optional.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(
                    new MessageDto(HttpStatus.BAD_REQUEST.value(), "Erro de alteração de senha", "Usuário inválido")
            );
        }

        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Senha alterada com sucesso!"));
    }

    @PutMapping(value = "/upload-foto")
    public ResponseEntity<UsuarioDetalheDto> uploadFile(
            @RequestParam("file") MultipartFile file,
            @RequestParam("userId") Long userId
    ) {
        UsuarioDetalheDto dto = service.savePhoto(file, userId);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }
    
    @GetMapping(value = "/load-photo/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] loadPhoto(
            @PathVariable Long id) {
        return service.loadPhoto(id);
    }

}
