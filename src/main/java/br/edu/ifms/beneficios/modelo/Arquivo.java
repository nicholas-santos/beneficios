/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Type;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author santos
 */
@Entity
public class Arquivo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "foto")
    private Usuario usuario;

    private String fileName;
    private String documentType;
    private String documentFormat;

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    private byte[] file;

    public Arquivo() {
        
    }
    
    public Arquivo(MultipartFile file) {
        this(null, file);
    }

    public Arquivo(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public Arquivo (Usuario usuario, MultipartFile file) {
        try {
            this.usuario = usuario;
            this.documentType = file.getOriginalFilename()
                    .substring(0, file.getOriginalFilename().lastIndexOf("."));
            this.fileName = file.getOriginalFilename();
            this.documentFormat = file.getContentType();
            this.file = file.getBytes();
        } catch (IOException ex) {
            Logger.getLogger(Arquivo.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentFormat() {
        return documentFormat;
    }

    public void setDocumentFormat(String documentFormat) {
        this.documentFormat = documentFormat;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
    
    public Boolean isFileSet() {
        return this.file != null;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.id);
        hash = 71 * hash + Objects.hashCode(this.usuario);
        hash = 71 * hash + Objects.hashCode(this.fileName);
        hash = 71 * hash + Objects.hashCode(this.documentType);
        hash = 71 * hash + Objects.hashCode(this.documentFormat);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Arquivo other = (Arquivo) obj;
        if (!Objects.equals(this.fileName, other.getFileName())) {
            return false;
        }
        if (!Objects.equals(this.documentType, other.getDocumentType())) {
            return false;
        }
        if (!Objects.equals(this.documentFormat, other.getDocumentFormat())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

}
