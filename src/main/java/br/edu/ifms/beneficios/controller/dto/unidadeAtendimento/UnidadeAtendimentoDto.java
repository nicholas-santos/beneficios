/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.unidadeAtendimento;

import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class UnidadeAtendimentoDto extends AbstractUnidadeAtendimento {

    public UnidadeAtendimentoDto() {
    }

    public UnidadeAtendimentoDto(Long id, String nome) {
        super(id, nome);
    }

    public UnidadeAtendimentoDto(UnidadeAtendimento obj) {
        super(obj);
    }
    
    public static List<UnidadeAtendimentoDto> converter(List<UnidadeAtendimento> items) {
        return items.stream().map(UnidadeAtendimentoDto::new).collect(Collectors.toList());
    }
    
}
