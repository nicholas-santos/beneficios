/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.ProgramaGoverno;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class ProgramaGovernoDto {
    
    private Long id;
    private String descricao;

    public ProgramaGovernoDto() {
    }

    public ProgramaGovernoDto(ProgramaGoverno obj) {
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public static Page<ProgramaGovernoDto> converter(Page<ProgramaGoverno> items) {
        return items.map(ProgramaGovernoDto::new);
    }
    
    public static List<ProgramaGovernoDto> converter(List<ProgramaGoverno> items) {
        return items.stream().map(ProgramaGovernoDto::new).collect(Collectors.toList());
    }
    
}
