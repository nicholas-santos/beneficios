/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalLong;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

/**
 *
 * @author santos
 */
@Entity
public class DocumentoSaida extends AbstractDocumento {

    @OneToMany(mappedBy = "id.documentoSaida",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<ItemSaida> itens = new ArrayList();

    @OneToMany(mappedBy = "documentoSaida",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<HistoricoSaida> historicos = new ArrayList();

    public DocumentoSaida() {
    }

    public DocumentoSaida(Long id) {
        super(id);
    }

    public List<ItemSaida> getItens() {
        return itens;
    }

    public void setItens(List<ItemSaida> itens) {
        this.itens = itens;
    }

    public void add(ItemSaida itemSaida) {
        if (!itemSaida.isSetNumero()) {
            OptionalLong maxId = itens.stream()
                    .mapToLong(obj -> obj.getId().getNumero())
                    .max();
            itemSaida.getId().setNumero((maxId.isPresent()
                    ? maxId.getAsLong() : 0) + 1L);
            this.itens.add(itemSaida);
        } else {
            this.itens.stream().map((obj) -> {
                if (itemSaida.getId().getNumero().equals(obj.getId().getNumero())) {
                    obj.setQuantidade(itemSaida.getQuantidade());
                    obj.setBeneficioEventual(itemSaida.getBeneficioEventual());
                    obj.setStatus(itemSaida.getStatus());
                }
                return obj;
            });
        }
    }

    public void remove(ItemSaida item) {
        this.itens.remove(item);
        item.getId().setDocumentoSaida(null);
    }

    public List<HistoricoSaida> getHistoricos() {
        return historicos;
    }

    public void setHistoricos(List<HistoricoSaida> historicos) {
        this.historicos = historicos;
    }

    public void add(HistoricoSaida historico) {
        this.historicos.add(historico);
    }

    public void remove(HistoricoSaida historico) {
        this.historicos.remove(historico);
        historico.setDocumentoSaida(null);
    }

    public void updateStatus() {
        /**
         * Atualiza o status de cada item para verificar se cada um deles já foi
         * devidamente movimentado
         */

        /**
         * Verifica se todos os itens estão definitivamente entregues
         *
         */
        Integer recebidos = this.itens.stream()
                .mapToInt(i -> i.isEnviado() ? 1 : 0)
                .sum();
        if (recebidos < this.itens.size()) {
            setStatus(Status.PARCIAL);
        } else {
            setStatus(Status.FINALIZADO);
        }
    }

}
