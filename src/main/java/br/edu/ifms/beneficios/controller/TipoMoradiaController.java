/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.TipoMoradiaDto;
import br.edu.ifms.beneficios.controller.form.TipoMoradiaForm;
import br.edu.ifms.beneficios.modelo.TipoMoradia;
import br.edu.ifms.beneficios.repository.TipoMoradiaRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/tipos-de-moradia")
public class TipoMoradiaController {

    @Autowired
    private TipoMoradiaRepository repository;

    @GetMapping
    @Cacheable(value = "pageTipoMoradia")
    public Page<TipoMoradiaDto> listar(
            @PageableDefault(sort = "descricao", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

        Page<TipoMoradia> condicaoMoradia = repository.findAll(paginacao);
        return TipoMoradiaDto.converter(condicaoMoradia);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaTipoMoradia")
    public List<TipoMoradiaDto> listar() {
        List<TipoMoradia> condicaoMoradia = repository.findAll();
        return TipoMoradiaDto.converter(condicaoMoradia);
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageTipoMoradia", "listaTipoMoradia"}, allEntries = true)
    public ResponseEntity<TipoMoradiaDto> criar(@RequestBody @Valid TipoMoradiaForm form, UriComponentsBuilder uriBuilder) {
        TipoMoradia condicaoMoradia = form.converter(repository);
        repository.save(condicaoMoradia);

        URI uri = uriBuilder.path("/tipos-de-moradia/{id}").buildAndExpand(condicaoMoradia.getId()).toUri();
        return ResponseEntity.created(uri).body(new TipoMoradiaDto(condicaoMoradia));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoMoradiaDto> visualizar(@PathVariable Long id) {
        Optional<TipoMoradia> condicaoMoradia = repository.findById(id);
        if (condicaoMoradia.isPresent()) {
            return ResponseEntity.ok(new TipoMoradiaDto(condicaoMoradia.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageTipoMoradia", "listaTipoMoradia"}, allEntries = true)
    public ResponseEntity<TipoMoradiaDto> atualizar(@PathVariable Long id, @RequestBody @Valid TipoMoradiaForm form) {
        Optional<TipoMoradia> optional = repository.findById(id);
        if (optional.isPresent()) {
            TipoMoradia condicaoMoradia = form.atualizar(id, repository);
            return ResponseEntity.ok(new TipoMoradiaDto(condicaoMoradia));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageTipoMoradia", "listaTipoMoradia"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<TipoMoradia> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
