/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.prontuario.ProntuarioDto;
import br.edu.ifms.beneficios.controller.dto.status.IStatusCount;
import br.edu.ifms.beneficios.controller.form.prontuario.ProntuarioForm;
import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.modelo.Dependente;
import br.edu.ifms.beneficios.modelo.HistoricoProntuario;
import br.edu.ifms.beneficios.modelo.Prontuario;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.GrupoSocioeducativoRepository;
import br.edu.ifms.beneficios.repository.ParentescoRepository;
import br.edu.ifms.beneficios.repository.PessoaRepository;
import br.edu.ifms.beneficios.repository.ProntuarioRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import br.edu.ifms.beneficios.repository.DependenteRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author santos
 */
@Service
public class ProntuarioService {

    @Autowired
    private ProntuarioRepository repository;

    @Autowired
    private UnidadeAtendimentoRepository unidadeAtendimentoRepository;

    @Autowired
    private GrupoSocioeducativoRepository grupoSocioeducativoRepository;

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private ParentescoRepository parentescoRepository;

    @Autowired
    private DependenteRepository composicaoFamiliarRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Page<Prontuario> listar(
            Long pessoaId, Long unidadeAtendimentoId,
            Status status, Pageable paginacao
    ) {
        Boolean hasPessoa = pessoaId != null,
                hasStatus = status != null,
                hasUaid = unidadeAtendimentoId != null && unidadeAtendimentoId > 0;
        if (hasPessoa) {
            Optional<Pessoa> op = pessoaRepository.findById(pessoaId);
            Pessoa p = op.get();
            if (hasUaid && !hasStatus) {
                return repository.findByTitularAndUnidadeAtendimentoId(p, unidadeAtendimentoId, paginacao);
            }
            if (!hasUaid && hasStatus) {
                return repository.findByTitularAndStatus(p, status, paginacao);
            }
            if (hasUaid && hasStatus) {
                return repository.findByTitularAndUnidadeAtendimentoIdAndStatus(p, unidadeAtendimentoId, status, paginacao);
            }
            return repository.findByTitular(p, paginacao);
        }

        if (hasUaid && !hasStatus) {
            return repository.findByUnidadeAtendimentoId(unidadeAtendimentoId, paginacao);
        }
        if (!hasUaid && hasStatus) {
            return repository.findByStatus(status, paginacao);
        }        
        if (hasUaid && hasStatus) {
            return repository.findByUnidadeAtendimentoIdAndStatus(unidadeAtendimentoId, status, paginacao);
        }
        
        return repository.findAll(paginacao);
    }

    public List<Prontuario> listar() {
        return repository.findAll();
    }

    public List<IStatusCount> listarContagem(Long unidadeAtendimentoId) {
        if (unidadeAtendimentoId != null && unidadeAtendimentoId > 0) {
            return repository.countTotalByStatusAndUnidadeAtendimentoId(unidadeAtendimentoId);
        }
        return repository.countTotalByStatus();
    }

    public Optional<Prontuario> buscarPor(Long id) {
        return repository.findById(id);
    }

    private String returnMessage(Optional<Prontuario> optional, int origem, String contexto, Pessoa pessoa) {
        if (optional.isPresent()) {
            Boolean isFem = pessoa.isFeminino();
            String msg
                    = (isFem ? "A %s %s já está vinculada " : "O %s %s já está vinculado ")
                    + "como %s no Prontuário N.o %05d, na Unidade de Atendimento %s.";
            Prontuario prontuario = optional.get();

            return String.format(msg, contexto, pessoa.getNome(),
                    origem == 0 ? "TITULAR" : "DEPENDENTE",
                    prontuario.getId(),
                    prontuario.getUnidadeAtendimento().getNome());
        }
        return null;
    }

    private String verificaDados(Pessoa pessoa, Prontuario prontuario, String contexto) {
        List<Prontuario> prontuarios = repository.findByTitularAndStatus(pessoa, Status.ATIVO);
        Optional<Prontuario> optional = prontuarios.stream()
                .filter(obj -> !obj.getId().equals(prontuario.getId()))
                .findAny();
        if (optional.isPresent()) {
            return returnMessage(optional, 0, contexto, pessoa);
        }

        prontuarios = repository.findByStatusAndDependentesIdPessoa(Status.ATIVO, pessoa);
        optional = prontuarios.stream()
                .filter(obj -> !obj.getId().equals(prontuario.getId()))
                .findAny();
        if (optional.isPresent()) {
            return returnMessage(optional, 1, contexto, pessoa);

        }

        return null;
    }

    private String verificaDados(Pessoa pessoa, Prontuario prontuario) {
        String msg = verificaDados(pessoa, prontuario, "TITULAR");
        if (msg != null) {
            return msg;
        }

        for (Dependente dep : prontuario.getDependentes()) {
            msg = verificaDados(dep.getId().getPessoa(), prontuario, "DEPENDENTE");
            if (msg != null) {
                return msg;
            }
        }

        return null;
    }

    public ProntuarioDto criar(ProntuarioForm form) {
        Prontuario prontuario = form.converter(repository,
                unidadeAtendimentoRepository, grupoSocioeducativoRepository,
                pessoaRepository, parentescoRepository);

        String msg = verificaDados(prontuario.getTitular(), prontuario);
        if (msg == null) {
            createHistorico(prontuario);
            prontuario = repository.save(prontuario);
            return new ProntuarioDto(prontuario);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
        }
    }

    public ProntuarioDto atualizar(Long id, ProntuarioForm form) {
        Optional<Prontuario> optional = repository.findById(id);
        if (optional.isPresent()) {
            Prontuario prontuario = form.atualizar(id, repository,
                    unidadeAtendimentoRepository, grupoSocioeducativoRepository,
                    pessoaRepository, parentescoRepository);
            String msg = verificaDados(prontuario.getTitular(), prontuario);
            if (msg == null) {
                createHistorico(prontuario);
                return new ProntuarioDto(prontuario);
            } else {
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST,
                        msg);
            }
        }

        return null;
    }

    public ProntuarioDto ativar(Long id, Status status, String observacao) {
        Optional<Prontuario> optional = repository.findById(id);
        if (optional.isPresent()) {
            Prontuario prontuario = optional.get();
            prontuario.setStatus(status);
            prontuario.setObservacao(observacao);
            createHistorico(prontuario);

            repository.save(prontuario);

            return new ProntuarioDto(prontuario);
        }

        return null;
    }

    public Optional<Prontuario> remover(Long id) {        
        Optional<Prontuario> optional = repository.findById(id);
        if (optional.isPresent()) {
            Prontuario prontuario = optional.get();
            prontuario.clearDependentes();
            prontuario.clearHistorico();
            if (!prontuario.isPendente()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("O prontuário não pode ser removido porque está %s!", prontuario.getStatus()));
            }
            repository.deleteById(id);
        }

        return optional;
    }

    private void createHistorico(Prontuario prontuario) {
        Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);

        HistoricoProntuario historico = new HistoricoProntuario(null, prontuario, usuario.getFuncionario());
        prontuario.add(historico);
    }
}
