/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifms.beneficios.modelo.PerfilMenu;
import br.edu.ifms.beneficios.modelo.id.PerfilMenuId;

/**
 *
 * @author santos
 */
@Repository()
public interface PerfilMenuRepository extends JpaRepository<PerfilMenu, PerfilMenuId> {
    
    void deleteByIdPerfil(Perfil perfil);
    
}
