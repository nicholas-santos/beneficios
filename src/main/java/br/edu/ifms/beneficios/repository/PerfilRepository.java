/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifms.beneficios.modelo.Perfil;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author tanabe
 */
@Repository()
public interface PerfilRepository extends JpaRepository<Perfil, Long> {
    @Query("SELECT coalesce(max(p.id), 0) FROM Perfil p")
    Long getMaxId();
}
