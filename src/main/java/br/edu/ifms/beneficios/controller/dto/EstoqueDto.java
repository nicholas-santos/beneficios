/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.modelo.Estoque;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class EstoqueDto {

    private UnidadeAtendimentoDto unidadeAtendimento;
    private BigDecimal quantidade;

    public EstoqueDto() {
        this.quantidade = BigDecimal.ZERO;
    }

    public EstoqueDto(Estoque obj) {
        this();
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getId().getUnidadeAtendimento());
        this.quantidade = obj.getQuantidade();
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public static Page<EstoqueDto> converter(Page<Estoque> items) {
        return items.map(EstoqueDto::new);
    }

    public static List<EstoqueDto> converter(List<Estoque> items) {
        return items.stream().map(EstoqueDto::new).collect(Collectors.toList());
    }

    /**
     * Retorna somente o estoque de todas unidades de atendimento se o parâmetro
     * <b>unidadeAtendimento</b> for matriz.
     *
     * @param items
     * @param unidadeAtendimento
     * @return
     */
    public static List<EstoqueDto> converter(
            List<Estoque> items,
            UnidadeAtendimento unidadeAtendimento
    ) {
        return items.stream()
                .filter(item
                    -> unidadeAtendimento.isMatriz()
                    || item.getId().getUnidadeAtendimento().equals(unidadeAtendimento))
                .map(EstoqueDto::new)
                .collect(Collectors.toList());
    }

}
