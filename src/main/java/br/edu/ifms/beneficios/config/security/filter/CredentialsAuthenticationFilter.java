/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.ifms.beneficios.config.security.filter;

import br.edu.ifms.beneficios.exception.InvalidCredentialsAuthenticationException;
import br.edu.ifms.beneficios.exception.UserServiceAuthenticationException;
import br.edu.ifms.beneficios.modelo.IpTracker;
import br.edu.ifms.beneficios.modelo.JwtRequest;
import br.edu.ifms.beneficios.modelo.JwtResponse;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.property.BeneficiosProperties;
import br.edu.ifms.beneficios.service.UsuarioService;
import br.edu.ifms.beneficios.util.JwtTokenUtil;
import br.edu.ifms.beneficios.util.LoginAttemptsUtil;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 *
 * @author santos
 */
public class CredentialsAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final Logger LOG = LoggerFactory.getLogger(CredentialsAuthenticationFilter.class);

    private AuthenticationManager authenticationManager;
    protected UsuarioService usuarioService;
    protected IpTracker ipTracker;
    private BeneficiosProperties beneficioProperties;

    public CredentialsAuthenticationFilter(
            AuthenticationManager man,
            BeneficiosProperties beneficioProperties
    ) {
        this.authenticationManager = man;
        this.beneficioProperties = beneficioProperties;
        JwtTokenUtil.beneficioProperties = beneficioProperties;
        
        this.setFilterProcessesUrl("/api/auth");
    }
    
    
    public void setIpTracker(IpTracker ipTracker) {
        this.ipTracker = ipTracker;
    }
    
    
    public void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
        JwtRequest jwtRequest = null;
        ObjectMapper mapper = new ObjectMapper();
        LoginAttemptsUtil loginAttemptsUtil = new LoginAttemptsUtil(usuarioService, ipTracker);
        
        try {
            //make sure the user hasn't failed login too many times from this IP address
//            loginAttemptsUtil.checkIpValidity(req);            

            //construct the JwtRequest object from the input stream
            jwtRequest = mapper.readValue(req.getInputStream(), JwtRequest.class);

            //now check to make sure this user hasn't had too many failed login attempts
            loginAttemptsUtil.checkMaxLoginAttempts(jwtRequest);
            
            //handle login
            return handleLogin(jwtRequest);
        } catch (BadCredentialsException e) {
            LOG.error("Bad credentials!", e);
            
            //gotta log to both the user service and ip tracker
            //because the user service tracks failed login attempts per user
            //while the ip tracker tracks failed login attempts per ip
            usuarioService.updateFailedLoginAttempts(jwtRequest.getEmail());
            ipTracker.unsuccessfulLogin(jwtRequest.getEmail(), req.getRemoteAddr());
            
            throw new InvalidCredentialsAuthenticationException(e.getMessage());
        } catch (JsonMappingException e) {
            LOG.error("Problem logging in user with credentials!", e);
            throw new UserServiceAuthenticationException(e.getMessage());
        } catch (IOException e) {
            LOG.error("Problem logging in user with credentials!", e);
            throw new UserServiceAuthenticationException(e.getMessage());
        }
    }
    
    /**
     * Uses the authentication manager to complete the login process
     * Will throw an exception if the credentials aren't valid.
     */
    private Authentication handleLogin(JwtRequest jwtRequest) {
        Authentication auth = null;
        
        if (jwtRequest != null) {
            auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    jwtRequest.getEmail(), jwtRequest.getSenha()));
        } else {
            throw new UserServiceAuthenticationException("Problem getting login credentials!");
        }
        
        return auth;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res,
            FilterChain chain, Authentication auth) throws IOException {

        final Usuario user = (Usuario) auth.getPrincipal();
        JwtTokenUtil.beneficioProperties = beneficioProperties;
        final JwtTokenUtil jwtTokenUtil = JwtTokenUtil.generateToken(user);
        final String token = jwtTokenUtil.getToken();
        Long expirationDate = jwtTokenUtil.getExpirationDateFromToken().getTime();

        //log a successful authentication to iplog collection
        ipTracker.successfulLogin(user.getUsername(), req.getRemoteAddr());
        
        //log a successful authentication to user collection
        usuarioService.successfulLogin(user.getUsername());
        
        JwtResponse jwtResponse = new JwtResponse(token, user, expirationDate);

        String body = new ObjectMapper().writeValueAsString(jwtResponse);
        LOG.debug("Body response is " + body);

        res.getWriter().write(body);
        res.getWriter().flush();
    }
}
