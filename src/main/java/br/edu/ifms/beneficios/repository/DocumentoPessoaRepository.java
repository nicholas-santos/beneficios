/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.DocumentoPessoa;
import br.edu.ifms.beneficios.modelo.AbstractPessoa;
import br.edu.ifms.beneficios.modelo.id.DocumentoPessoaId;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface DocumentoPessoaRepository extends JpaRepository<DocumentoPessoa, DocumentoPessoaId> {
    
    void deleteByIdPessoa(AbstractPessoa pessoa);
    List<DocumentoPessoa> findByIdNumeroAndIdDocumentoIdAndOrgaoExpedidorId(String numero, Long documentoId, Long orgaoExpedidorId);
    
}
