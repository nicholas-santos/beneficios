/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDto;
import br.edu.ifms.beneficios.modelo.Analise;
import br.edu.ifms.beneficios.modelo.tipos.Autorizacao;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class AnaliseDto {

    private Long id;
    private FuncionarioDto tecnico;
    private LocalDateTime emissao;
    private String parecer;
    private Autorizacao autorizacao;
    private ArquivoDto arquivo;
    private Boolean encaminhamento;
     
    private List<ItemAnaliseDto> itens = new ArrayList();

    public AnaliseDto() {
    }

    public AnaliseDto(Analise obj) {
        this.id = obj.getId();
        this.tecnico = new FuncionarioDto(obj.getTecnico());
        this.emissao = obj.getEmissao();
        this.parecer = obj.getParecer();
        this.autorizacao = obj.getAutorizacao();
        this.encaminhamento = obj.getEncaminhamento();
        this.itens = ItemAnaliseDto.converter(obj.getItens());
        
        if (obj.getArquivo() != null) {
            this.arquivo = new ArquivoDto(obj.getArquivo());
        }
    }

    public Long getId() {
        return id;
    }

    public FuncionarioDto getTecnico() {
        return tecnico;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public String getParecer() {
        return parecer;
    }

    public Autorizacao getAutorizacao() {
        return autorizacao;
    }

    public ArquivoDto getArquivo() {
        return arquivo;
    }

    public Boolean getEncaminhamento() {
        return encaminhamento;
    }

    public List<ItemAnaliseDto> getItens() {
        return itens;
    }

    public static List<AnaliseDto> converter(List<Analise> items) {
        return items.stream().map(AnaliseDto::new).collect(Collectors.toList());
    }

    public static Page<AnaliseDto> converter(Page<Analise> items) {
        return items.map(AnaliseDto::new);
    }

}
