/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.TipoUnidadeAtendimentoDto;
import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.form.TipoUnidadeAtendimentoForm;
import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import br.edu.ifms.beneficios.repository.TipoUnidadeAtendimentoRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/tipos-de-unidades-atendimento")
public class TipoUnidadeAtendimentoController {

    @Autowired
    private TipoUnidadeAtendimentoRepository repository;

    @GetMapping
    @Cacheable(value = "pageTipoUnidadeAtendimento")
    public Page<TipoUnidadeAtendimentoDto> listar(
            @PageableDefault(sort = "nome", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

            Page<TipoUnidadeAtendimento> tipoUnidadeAtendimento = repository.findAll(paginacao);
            return TipoUnidadeAtendimentoDto.converter(tipoUnidadeAtendimento);
    }
    
    @GetMapping("/list")
    @Cacheable(value = "listaTipoUnidadeAtendimento")
    public List<TipoUnidadeAtendimentoDto> listar() {

        List<TipoUnidadeAtendimento> items;
        items = repository.findAll();
        
        return TipoUnidadeAtendimentoDto.converter(items);
    }
    
    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageTipoUnidadeAtendimento", "listaTipoUnidadeAtendimento"}, allEntries = true)
    public ResponseEntity<TipoUnidadeAtendimentoDto> criar(@RequestBody @Valid TipoUnidadeAtendimentoForm form, UriComponentsBuilder uriBuilder) {
    	TipoUnidadeAtendimento tipoUnidadeAtendimento = form.converter(repository);
        repository.save(tipoUnidadeAtendimento);

        URI uri = uriBuilder.path("/tipos-de-unidades-atendimento/{id}").buildAndExpand(tipoUnidadeAtendimento.getId()).toUri();
        return ResponseEntity.created(uri).body(new TipoUnidadeAtendimentoDto(tipoUnidadeAtendimento));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoUnidadeAtendimentoDto> visualizar(@PathVariable Long id) {
        Optional<TipoUnidadeAtendimento> tipoUnidade = repository.findById(id);
        if (tipoUnidade.isPresent()) {
            return ResponseEntity.ok(new TipoUnidadeAtendimentoDto(tipoUnidade.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageTipoUnidadeAtendimento", "listaTipoUnidadeAtendimento"}, allEntries = true)
    public ResponseEntity<TipoUnidadeAtendimentoDto> atualizar(@PathVariable Long id, @RequestBody @Valid TipoUnidadeAtendimentoForm form) {
        Optional<TipoUnidadeAtendimento> optional = repository.findById(id);
        if (optional.isPresent()) {
        	TipoUnidadeAtendimento tipoUnidade = form.atualizar(id, repository);
            return ResponseEntity.ok(new TipoUnidadeAtendimentoDto(tipoUnidade));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageTipoUnidadeAtendimento", "listaTipoUnidadeAtendimento"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<TipoUnidadeAtendimento> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
