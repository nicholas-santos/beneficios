/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.funcionario;

import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;

/**
 *
 * @author santos
 */
public abstract class AbstractFuncionarioDto {
    private Long id;
    private String nome;
    private Sexo sexo;

    public AbstractFuncionarioDto() {
    }

    public AbstractFuncionarioDto(Funcionario obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.sexo = obj.getSexo();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public Sexo getSexo() {
        return sexo;
    }
    
}
