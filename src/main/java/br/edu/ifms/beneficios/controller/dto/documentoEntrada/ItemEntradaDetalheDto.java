/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoEntrada;

import br.edu.ifms.beneficios.modelo.ItemEntrada;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class ItemEntradaDetalheDto extends AbstractItemEntradaDto {

    private BigDecimal quantidadeConferida;
    private BigDecimal quantidadePendente;
//    private List<MovimentoEntradaDto> movimentos = new ArrayList();

    public ItemEntradaDetalheDto() {
        super();
    }

    public ItemEntradaDetalheDto(ItemEntrada obj) {
        super(obj);
        
        this.quantidadeConferida = obj.getQuantidadeMovimentada();
        this.quantidadePendente = super.getQuantidade().subtract(this.quantidadeConferida);
//        this.movimentos = MovimentoEntradaDto.converter(obj.getMovimentos());
    }

    public BigDecimal getQuantidadePendente() {
        return quantidadePendente;
    }

    public BigDecimal getQuantidadeConferida() {
        return quantidadeConferida;
    }

//    public List<MovimentoEntradaDto> getMovimentos() {
//        return movimentos;
//    }

    public static Page<ItemEntradaDetalheDto> converter(Page<ItemEntrada> items) {
        return items.map(ItemEntradaDetalheDto::new);
    }

    public static List<ItemEntradaDetalheDto> converter(List<ItemEntrada> items) {
        return items.stream().map(ItemEntradaDetalheDto::new).collect(Collectors.toList());
    }

}
