/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.Cep;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class EnderecoId implements Serializable {
    
    @ManyToOne
    private Cep cep;
    
    private String numero;

    public EnderecoId() {
    }

    public EnderecoId(Cep cep, String numero) {
        this.cep = cep;
        this.numero = numero;
    }

    public Cep getCep() {
        return cep;
    }

    public void setCep(Cep cep) {
        this.cep = cep;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.cep);
        hash = 83 * hash + Objects.hashCode(this.numero);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final EnderecoId other = (EnderecoId) obj;
        if (!Objects.equals(this.numero, other.getNumero())) {
            return false;
        }
        if (!Objects.equals(this.cep, other.getCep())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EnderecoId{" + "cep=" + cep + ", numero=" + numero + '}';
    }
    
}
