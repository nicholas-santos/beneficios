/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.TipoLogradouroDto;
import br.edu.ifms.beneficios.controller.form.TipoLogradouroForm;
import br.edu.ifms.beneficios.modelo.TipoLogradouro;
import br.edu.ifms.beneficios.repository.TipoLogradouroRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/tipos-de-logradouros")
public class TipoLogradouroController {

    @Autowired
    private TipoLogradouroRepository repository;

    @GetMapping
    @Cacheable(value = "listaTipoLogradouro")
    public List<TipoLogradouroDto> listar() {

            List<TipoLogradouro> tiposContatos = repository.findAll();
            return TipoLogradouroDto.converter(tiposContatos);
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = "listaTipoLogradouro", allEntries = true)
    public ResponseEntity<TipoLogradouroDto> criar(@RequestBody @Valid TipoLogradouroForm form, UriComponentsBuilder uriBuilder) {
    	TipoLogradouro tipoContato = form.converter();
        repository.save(tipoContato);

        URI uri = uriBuilder.path("/tipos-de-logradouros/{id}").buildAndExpand(tipoContato.getId()).toUri();
        return ResponseEntity.created(uri).body(new TipoLogradouroDto(tipoContato));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoLogradouroDto> visualizar(@PathVariable Long id) {
        Optional<TipoLogradouro> tipoContato = repository.findById(id);
        if (tipoContato.isPresent()) {
            return ResponseEntity.ok(new TipoLogradouroDto(tipoContato.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = "listaTipoLogradouro", allEntries = true)
    public ResponseEntity<TipoLogradouroDto> atualizar(@PathVariable Long id, @RequestBody @Valid TipoLogradouroForm form) {
        Optional<TipoLogradouro> optional = repository.findById(id);
        if (optional.isPresent()) {
        	TipoLogradouro tipoContato = form.atualizar(id, repository);
            return ResponseEntity.ok(new TipoLogradouroDto(tipoContato));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = "listaTipoLogradouro", allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<TipoLogradouro> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
