/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.util.Objects;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
@DiscriminatorValue(value = "E")
public class MovimentoEntrada extends Movimento {
    
    @ManyToOne
    private ItemEntrada itemEntrada;

    public MovimentoEntrada() {
        super();
    }

    public ItemEntrada getItemEntrada() {
        return itemEntrada;
    }

    public void setItemEntrada(ItemEntrada itemEntrada) {
        this.itemEntrada = itemEntrada;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.itemEntrada);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final MovimentoEntrada other = (MovimentoEntrada) obj;
        if (!Objects.equals(this.itemEntrada, other.getItemEntrada())) {
            return false;
        }
        return true;
    }
    
}
