/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Atendimento;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimento;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import br.edu.ifms.beneficios.controller.dto.status.IStatusAtendimentoCount;
import java.util.Optional;

/**
 *
 * @author santos
 */
public interface AtendimentoRepository extends JpaRepository<Atendimento, Long> {

    Page<Atendimento> findByProntuarioIdAndProntuarioUnidadeAtendimentoIdAndEmissaoLessThan(Long prontuarioId, Long unidadeAtendimentoId, LocalDateTime emissao, Pageable paginacao);

    Page<Atendimento> findByPessoa(Pessoa pessoa, Pageable paginacao);
    Page<Atendimento> findByStatus(StatusAtendimento status, Pageable paginacao);
    Page<Atendimento> findByAtendenteUnidadeAtendimentoId(Long unidadeAtendimentoId, Pageable paginacao);
    Page<Atendimento> findByPessoaAndStatus(Pessoa pessoa, StatusAtendimento status, Pageable paginacao);
    
    Page<Atendimento> findByPessoaAndAtendenteUnidadeAtendimentoId(Pessoa pessoa, Long unidadeAtendimentoId, Pageable paginacao);
    Page<Atendimento> findByStatusAndAtendenteUnidadeAtendimentoId(StatusAtendimento status, Long unidadeAtendimentoId, Pageable paginacao);
    Page<Atendimento> findByPessoaAndStatusAndAtendenteUnidadeAtendimentoId(Pessoa pessoa, StatusAtendimento status, Long unidadeAtendimentoId, Pageable paginacao);

    Optional<Atendimento> findByAnaliseId(Long analiseId);
    
    @Query(value = "select statusAtendimento, totalStatus, total,\n"
            + " CASE \n"
            + "     WHEN total > 0 THEN round((1.0*totalStatus/total*100), 2) \n"
            + "     ELSE 0 \n"
            + " END percentualStatus\n"
            + "  from (select status as statusAtendimento, count(status) as totalStatus,\n"
            + "		(select count(status) from atendimento) as total\n"
            + "	  from atendimento\n"
            + "	 group by status\n"
            + "	 order by status) as contagem", nativeQuery = true)
    List<IStatusAtendimentoCount> countTotalAtendimentoByStatusAtendimento();
    
    @Query(value = "select statusAtendimento, totalStatus, total,\n"
            + "             CASE \n"
            + "                  WHEN total > 0 THEN round((1.0*totalStatus/total*100), 2) \n"
            + "                  ELSE 0 \n"
            + "             END percentualStatus\n"
            + "  from (select at.status as statusAtendimento, count(at.status) as totalStatus,\n"
            + "		(select count(a.status)\n"
            + "            from atendimento as a\n"
            + "           inner join funcionario as f on f.id = a.atendente_id\n"
            + "           where f.unidade_atendimento_id = ?1) as total\n"
            + "	  from atendimento as at\n"
            + "  inner join funcionario as f on f.id = at.atendente_id\n"
            + " where f.unidade_atendimento_id = ?1"
            + "	 group by at.status\n"
            + "	 order by at.status) as contagem", nativeQuery = true)
    List<IStatusAtendimentoCount> countTotalAtendimentoByStatusAtendimentoAndUnidadeAtendimentoId(Long unidadeAtendimentoId);
}
