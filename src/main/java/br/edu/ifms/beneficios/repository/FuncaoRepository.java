/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Funcao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 *
 * @author tanabe
 */
public interface FuncaoRepository extends JpaRepository<Funcao, Long> {
    Funcao findByNomeIgnoreCase(String nome);
    
    @Query("SELECT coalesce(max(f.id), 0) FROM Funcao f")
    Long getMaxId();
}
