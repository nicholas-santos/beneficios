/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.DocumentoSaida;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class ItemSaidaId implements Serializable {
    
    private Long numero;
    
    @ManyToOne
    @JoinColumn(name = "documento_saida_id", referencedColumnName = "id")
    private DocumentoSaida documentoSaida;

    public ItemSaidaId() {
    }

    public ItemSaidaId(Long numero, DocumentoSaida documentoSaida) {
        this.numero = numero;
        this.documentoSaida = documentoSaida;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public DocumentoSaida getDocumentoSaida() {
        return documentoSaida;
    }

    public void setDocumentoSaida(DocumentoSaida documentoSaida) {
        this.documentoSaida = documentoSaida;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.numero);
        hash = 37 * hash + Objects.hashCode(this.documentoSaida);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ItemSaidaId other = (ItemSaidaId) obj;
        if (!Objects.equals(this.numero, other.getNumero())) {
            return false;
        }
        if (!Objects.equals(this.documentoSaida, other.getDocumentoSaida())) {
            return false;
        }
        return true;
    }
    
}
