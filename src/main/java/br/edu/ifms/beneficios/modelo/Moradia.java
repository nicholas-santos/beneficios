/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.MoradiaId;
import br.edu.ifms.beneficios.modelo.tipos.LocalDateAttributeConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;

/**
 *
 * @author santos
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class Moradia implements Serializable {
    
    @EmbeddedId
    private MoradiaId id;

    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate dataOcupacao;

    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate dataSaida;

    @Digits(integer=10, fraction=2)
    private BigDecimal valor;

    @ManyToOne
    private CondicaoMoradia condicaoMoradia;

    @ManyToOne
    private TipoMoradia tipoMoradia;

    @OneToOne(cascade = {
        CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
    })
    @JoinColumns({
        @JoinColumn(name = "endereco_cep_bairro_id", referencedColumnName = "cep_bairro_id"),
        @JoinColumn(name = "endereco_cep_cidade_id", referencedColumnName = "cep_cidade_id"),
        @JoinColumn(name = "endereco_cep_logradouro_id", referencedColumnName = "cep_logradouro_id"),
        @JoinColumn(name = "endereco_numero", referencedColumnName = "numero")
    })
    private Endereco endereco;
    
    @Transient
    private Boolean deleted;

    public Moradia() {
        deleted = Boolean.FALSE;
    }

    public Moradia(MoradiaId id, LocalDate dataOcupacao, LocalDate dataSaida, 
            BigDecimal valor, CondicaoMoradia condicaoMoradia, 
            TipoMoradia tipoMoradia, Endereco endereco) {
        this();
        this.id = id;
        this.dataOcupacao = dataOcupacao;
        this.dataSaida = dataSaida;
        this.valor = valor;
        this.condicaoMoradia = condicaoMoradia;
        this.tipoMoradia = tipoMoradia;
        this.endereco = endereco;
    }
    
    public void update(Moradia obj) {
        this.dataOcupacao = obj.getDataOcupacao();
        this.dataSaida = obj.getDataSaida();
        this.valor = obj.getValor();
        this.condicaoMoradia = obj.getCondicaoMoradia();
        this.tipoMoradia = obj.getTipoMoradia();
        this.endereco = obj.getEndereco();
    }

    public MoradiaId getId() {
        return id;
    }

    public void setId(MoradiaId id) {
        this.id = id;
    }

    public LocalDate getDataOcupacao() {
        return dataOcupacao;
    }

    public void setDataOcupacao(LocalDate dataOcupacao) {
        this.dataOcupacao = dataOcupacao;
    }

    public LocalDate getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(LocalDate dataSaida) {
        this.dataSaida = dataSaida;
    }
    
    public Boolean isDataSaidaNull() {
        return this.dataSaida == null;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public CondicaoMoradia getCondicaoMoradia() {
        return condicaoMoradia;
    }

    public void setCondicaoMoradia(CondicaoMoradia condicaoMoradia) {
        this.condicaoMoradia = condicaoMoradia;
    }

    public TipoMoradia getTipoMoradia() {
        return tipoMoradia;
    }

    public void setTipoMoradia(TipoMoradia tipoMoradia) {
        this.tipoMoradia = tipoMoradia;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        hash = 89 * hash + Objects.hashCode(this.dataOcupacao);
        hash = 89 * hash + Objects.hashCode(this.dataSaida);
        hash = 89 * hash + Objects.hashCode(this.valor);
        hash = 89 * hash + Objects.hashCode(this.condicaoMoradia);
        hash = 89 * hash + Objects.hashCode(this.tipoMoradia);
        hash = 89 * hash + Objects.hashCode(this.endereco);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Moradia other = (Moradia) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

}
