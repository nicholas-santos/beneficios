/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.configuracoes;

import org.springframework.boot.autoconfigure.mail.MailProperties;

/**
 *
 * @author santos
 */
public class MailConfigDto {
    private String host;
    private Integer port;
    private String username;
    private String password;

    public MailConfigDto() {
    }

    public MailConfigDto(MailProperties obj) {
        this.host = obj.getHost();
        this.port = obj.getPort();
        this.username = obj.getUsername();
        this.password = obj.getPassword();
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    
}
