/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.GrupoSocioeducativo;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class GrupoSocioeducativoDto {

    private Long id;
    private String nome;

    public GrupoSocioeducativoDto() {
    }

    public GrupoSocioeducativoDto(GrupoSocioeducativo obj) {
        if (obj != null) {
            this.id = obj.getId();
            this.nome = obj.getNome();
        }
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public static List<GrupoSocioeducativoDto> converter(List<GrupoSocioeducativo> items) {
        return items.stream().map(GrupoSocioeducativoDto::new).collect(Collectors.toList());
    }

    public static Page<GrupoSocioeducativoDto> converter(Page<GrupoSocioeducativo> items) {
        return items.map(GrupoSocioeducativoDto::new);
    }

}
