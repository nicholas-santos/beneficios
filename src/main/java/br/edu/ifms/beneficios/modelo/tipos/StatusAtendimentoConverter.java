/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.tipos;

import javax.persistence.AttributeConverter;

/**
 *
 * @author santos
 */
public class StatusAtendimentoConverter  implements AttributeConverter<StatusAtendimento, String> {

    @Override
    public String convertToDatabaseColumn(StatusAtendimento status) {
        if (status == null) {
            return null;
        }
        return status.toString();
    }

    @Override
    public StatusAtendimento convertToEntityAttribute(String status) {
        if (status == null)
            return null;
        return StatusAtendimento.valueOf(status);
    }
    
}