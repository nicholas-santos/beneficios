package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findByEmail(String email);
    Optional<Usuario> findByEmailAndStatusAndEnabled(String email, Status status, boolean enabled);
    Optional<Usuario> findByIdAndStatusAndEnabled(Long id, Status status, boolean enabled);

    List<Usuario> findByStatus(Status status);
    Page<Usuario> findByFuncionarioId(Long funcionarioId, Pageable paginacao);

    @Query(value = "SELECT u FROM Usuario u WHERE u.id = ?1")
    Optional<Usuario> findDetailUsuarioById(Long id);
    
    @Query(value = "UPDATE Usuario u SET u.failedAttempt = ?1 WHERE u.email = ?2")
    @Modifying
    void updateFailureAttempt(Integer failedAttempt, String email);

}
