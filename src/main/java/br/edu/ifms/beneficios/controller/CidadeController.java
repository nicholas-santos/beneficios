/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.CidadeDto;
import br.edu.ifms.beneficios.controller.form.CidadeForm;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.service.CidadeService;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/cidades")
@Profile(value = {"prod", "test"})
public class CidadeController {
    
    @Autowired CidadeService service;

    /**
     * Listar cidades.Este método é responsável por retornar uma lista de cidades cadastradas
 no sistema.
     *
     *
     * @param nome
     * @param nomeUf
     * @param paginacao
     * @return
     */
    @GetMapping
    @Cacheable(value = "pageCidades")
    public Page<CidadeDto> listar(
            @RequestParam(required = false) String nome,
            @RequestParam(required = false) String nomeUf,
            @PageableDefault(sort = "nome", direction = Sort.Direction.ASC, page = 0, size = 10) Pageable paginacao) {

        return CidadeDto.converter(service.listar(nome, nomeUf, paginacao));
    }
    
    @GetMapping("/list")
    @Cacheable(value = "listaDeCidades")
    public List<CidadeDto> listar(@RequestParam(required = false) String nomeUf) {
        return CidadeDto.converter(service.listar(nomeUf));
    }

    /**
     * Visualizar cidade.Este método é responsável por retornar um objeto da classe
    <code>Cidade</code>
     *
     *
     * @param id Número de identificação da cidade.
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<CidadeDto> visualizar(@PathVariable Long id) {
        Optional<Cidade> cidade = service.buscarPor(id);
        if (cidade.isPresent()) {
            return ResponseEntity.ok(new CidadeDto(cidade.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageCidades", "listaDeCidades"}, allEntries = true)
    public ResponseEntity<CidadeDto> cadastrar(@RequestBody @Valid CidadeForm form, 
            UriComponentsBuilder uriBuilder) {
        CidadeDto dto = service.criar(form);

        URI uri = uriBuilder.path("/cidades/{id}")
                .buildAndExpand(dto.getId())
                .toUri();
        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageCidades", "listaDeCidades"}, allEntries = true)
    public ResponseEntity<CidadeDto> atualizar(
            @PathVariable Long id, 
            @RequestBody @Valid CidadeForm form) {
        CidadeDto dto = service.atualizar(id, form);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageCidades", "listaDeCidades"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Cidade> optional = service.remover(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}
