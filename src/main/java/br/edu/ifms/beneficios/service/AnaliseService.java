/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.AnaliseDto;
import br.edu.ifms.beneficios.controller.form.AnaliseForm;
import br.edu.ifms.beneficios.modelo.Analise;
import br.edu.ifms.beneficios.modelo.Arquivo;
import br.edu.ifms.beneficios.modelo.Atendimento;
import br.edu.ifms.beneficios.modelo.Estoque;
import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.HistoricoItemAnalise;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.repository.AnaliseRepository;
import br.edu.ifms.beneficios.repository.ArquivoRepository;
import br.edu.ifms.beneficios.repository.AtendimentoRepository;
import br.edu.ifms.beneficios.repository.BeneficioEventualRepository;
import br.edu.ifms.beneficios.repository.FuncionarioRepository;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author santos
 */
@Service
public class AnaliseService {

    @Autowired
    private AnaliseRepository repository;

    @Autowired
    private ArquivoRepository arquivoRepository;

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Autowired
    private AtendimentoRepository atendimentoRepository;

    @Autowired
    private AtendimentoService atendimentoService;

    @Autowired
    private BeneficioEventualRepository beneficioEventualRepository;

    @Autowired
    private EstoqueService estoqueService;

    public Page<Analise> listar(Pageable paginacao) {
        return repository.findAll(paginacao);
    }

    public List<Analise> listar() {
        return repository.findAll();
    }

    public Optional<Analise> buscarPor(Long id) {
        return repository.findById(id);
    }

    public void verificarInconsistencias(AnaliseForm form) {
        String message = form.isValido();
        if (!"".equals(message)) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    message);
        }

        /**
         * Verificar estoque
         */
        if (!form.getItens().isEmpty()) {
            Integer countDisponibilidade = form.getItens().stream()
                    .mapToInt(itemDto -> {
                        Optional<Estoque> op = estoqueService.buscarPor(
                                itemDto.getBeneficioEventual().getId(),
                                itemDto.getUnidadeAtendimento().getId());
                        if (op.isPresent()) {
                            Estoque estoque = op.get();
                            return estoque.getQuantidade().compareTo(itemDto.getQuantidade()) >= 0 ? 0 : 1 ;
                        }
                        return 0;
                    })
                    .sum();

            if (countDisponibilidade > 0) {
                throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    String.format("Existem %d itens lançados sem estoque disponível "
                        + "para o Benefício Eventual!", countDisponibilidade));
            }
        }
    }

    private Arquivo criarArquivo(MultipartFile file) throws IOException {
        Arquivo arquivo = new Arquivo(file);
        arquivoRepository.save(arquivo);

        return arquivo;
    }

    public AnaliseDto criar(MultipartFile file, AnaliseForm form) throws IOException {
        verificarInconsistencias(form);

        Analise analise = form.converter(beneficioEventualRepository);

        Usuario usuario = (Usuario) SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();
        Funcionario tecnico = funcionarioRepository
                .getById(usuario.getFuncionario().getId());
        analise.setTecnico(tecnico);
        if (file != null) {
            analise.setArquivo(criarArquivo(file));
        }

        Atendimento atendimento = atendimentoRepository
                .getById(form.getAtendimento().getId());

        analise.setAtendimento(atendimento);
        atendimento.setAnalise(analise);

        if (analise.isAutorizado()) {
            atendimento.autorizar();
        } else {
            atendimento.negar();
        }

        atendimentoService.criarHistoricoAtendimento(atendimento);
        criarHistoricoItemAnalise(analise, atendimento.getProntuario().getUnidadeAtendimento(), tecnico);

        return new AnaliseDto(analise);
    }

    private void criarHistoricoItemAnalise(Analise analise, UnidadeAtendimento unidadeAtendimento, Funcionario funcionario) {
        analise.getItens().stream().map(itemAnalise -> {
            /**
             * Cria a baixa do estoque aqui para que ela seja confirmada depois,
             * na retirada do benefícios
             */
            estoqueService.retirar(
                    unidadeAtendimento,
                    itemAnalise);
            /**
             * Cria o histórico de movimentações do benefício.
             */
            HistoricoItemAnalise historico = new HistoricoItemAnalise(null, itemAnalise, funcionario);
            itemAnalise.add(historico);
            return historico;
        }).collect(Collectors.toList());
    }
}
