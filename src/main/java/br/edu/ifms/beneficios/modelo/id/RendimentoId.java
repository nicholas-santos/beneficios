/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.id;

import br.edu.ifms.beneficios.modelo.Pessoa;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Embeddable
public class RendimentoId implements Serializable {

    private Long sequencia;
    
    @ManyToOne
    @JoinColumn(name = "pessoa_id", referencedColumnName = "id")
    private Pessoa pessoa;

    public RendimentoId(Long sequencia, Pessoa pessoa) {
        this.sequencia = sequencia;
        this.pessoa = pessoa;
    }

    public RendimentoId() {
    }

    public Long getSequencia() {
        return sequencia;
    }

    public void setSequencia(Long sequencia) {
        this.sequencia = sequencia;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.sequencia);
        hash = 79 * hash + Objects.hashCode(this.pessoa);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final RendimentoId other = (RendimentoId) obj;
        if (!Objects.equals(this.sequencia, other.getSequencia())) {
            return false;
        }
        if (!Objects.equals(this.pessoa.getId(), other.getPessoa().getId())) {
            return false;
        }
        return true;
    }
}
