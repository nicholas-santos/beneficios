/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.GrupoSocioeducativoForm;
import br.edu.ifms.beneficios.modelo.GrupoSocioeducativo;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class GrupoSocioeducativoControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/grupo-socioeducativo";
    private Gson gson = new Gson();

    @BeforeEach
    public void setUp() {
        BeneficiosFactory.MVC = mvc;
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosGrupoSocioeducativos() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1GrupoSocioeducativo() throws Exception {
        GrupoSocioeducativo obj = BeneficiosFactory.createGrupoSocioeducativo("Alcolicos Anonimos");

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarGrupoSocioeducativoComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmGrupoSocioeducativo() throws Exception {
        GrupoSocioeducativoForm funcao = new GrupoSocioeducativoForm();
        funcao.setDescricao("Narcóticos Anonimos");
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(funcao));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmGrupoSocioeducativo() throws Exception {
        GrupoSocioeducativo obj = BeneficiosFactory.createGrupoSocioeducativo("Alcoolicos Anonimos");

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        obj.setNome("Narcoticos Anonimos");
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(obj));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("Narcoticos Anonimos")));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmGrupoSocioeducativo() throws Exception {
        GrupoSocioeducativo obj = BeneficiosFactory.createGrupoSocioeducativo("Alcoolicos Anonimos");

        URI uri = new URI(String.format("%s/%d", sURI, obj.getId()));
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmGrupoSocioeducativoComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

}
