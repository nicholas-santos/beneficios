/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.usuario;

import br.edu.ifms.beneficios.controller.dto.ListaPerfilDto;
import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Usuario;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class UsuarioDto extends AbstractUsuarioDto {

    private List<ListaPerfilDto> perfis;

    public UsuarioDto(Usuario obj) {
        super(obj);
        
        this.perfis = ListaPerfilDto.converter(obj.getPerfis());
    }

    public List<ListaPerfilDto> getPerfis() {
        return perfis;
    }
    
    public static List<UsuarioDto> converter(List<Usuario> items) {
        return items.stream().map(UsuarioDto::new).collect(Collectors.toList());
    }

    public static Page<UsuarioDto> converter(Page<Usuario> items) {
        return items.map(UsuarioDto::new);
    }
}
