/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.modelo.ResetPasswordToken;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.property.BeneficiosProperties;
import java.util.Date;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 *
 * @author santos
 */
@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailService;

    @Autowired
    private Environment environment;

    @Autowired
    private BeneficiosProperties beneficiosProperty;

    protected MimeMessage prepareResetPasswordEmail(
            ResetPasswordToken resetPasswordToken) {

        try {
            String url = String.format("%s/%s", beneficiosProperty.getResetPasswordUrl(), resetPasswordToken.getToken());
            MimeMessage msg = mailService.createMimeMessage();
            msg.setContent("Content-Type", "text/html");
            MimeMessageHelper helper = new MimeMessageHelper(msg, false);

            Usuario usuario = resetPasswordToken.getUsuario();
            helper.setTo(usuario.getEmail());
            helper.setFrom(beneficiosProperty.getFrom());
            helper.setSubject("Solicitação de redefinição de senha Software Gestão de Benefícios Eventuais");
            helper.setSentDate(new Date(System.currentTimeMillis()));

            StringBuilder sb = new StringBuilder();
            sb.append("<html><body>");
            sb.append(String.format("<h3>Olá %s!</h3>\n\n", usuario.getFuncionario().getNome()));
            sb.append("<p>O Software de Gestão de Benefícios eventuais vem por"
                    + " meio deste informar que você solicitou uma alteração de senha.</p>\n");
            sb.append("<p><b>Caso você não tenha solicitado isso, por favor, ignore este e-mail.</b></p>\n\n");
            sb.append(String.format("<p>Para gerar uma nova senha <a href=\"%s\">Clique aqui!</a></p>\n\n",
                    url));
            sb.append("<p>Caso o link não funcione, copie e cole o link abaixo:</p> \n");
            sb.append(url);

            Long tempo = beneficiosProperty.getResetPasswordTokenExpiration().toMillis() / 3600000;
            sb.append(String.format("<p><b>O link expira em %d hora%s.</b></p>\n", tempo, tempo > 1 ? "s" : ""));
            sb.append("<p>Atenciosamente</p>\n");
            sb.append("<p>Software para Gestão de Benefícios Eventuais</p>\n");
            sb.append("</body></html>");

            msg.setContent(sb.toString(), "text/html; charset=utf-8");

            return msg;
        } catch (MessagingException ex) {
            return null;
        }
    }

    public void sendResetPasswordToken(ResetPasswordToken resetPasswordToken) {
        MimeMessage msg = prepareResetPasswordEmail(resetPasswordToken);
        sendMail(msg);
    }

    public void sendMail(MimeMessage msg) {
        mailService.send(msg);
    }

}
