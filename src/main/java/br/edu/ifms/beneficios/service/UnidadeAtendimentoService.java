/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDetalheDto;
import br.edu.ifms.beneficios.controller.form.UnidadeAtendimentoForm;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.repository.BairroRepository;
import br.edu.ifms.beneficios.repository.CepRepository;
import br.edu.ifms.beneficios.repository.CidadeRepository;
import br.edu.ifms.beneficios.repository.EnderecoRepository;
import br.edu.ifms.beneficios.repository.LogradouroRepository;
import br.edu.ifms.beneficios.repository.TipoUnidadeAtendimentoRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author santos
 */
@Service
public class UnidadeAtendimentoService {

    @Autowired
    private UnidadeAtendimentoRepository repository;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private CepRepository cepRepository;

    @Autowired
    private LogradouroRepository logradouroRepository;

    @Autowired
    private TipoUnidadeAtendimentoRepository tipoUnidadeAtendimentoRepository;

    @Autowired
    private BairroRepository bairroRepository;

    @Autowired
    private CidadeRepository cidadeRepository;

    public Page<UnidadeAtendimento> listar(Pageable paginacao) {
        return repository.findAll(paginacao);
    }

    public List<UnidadeAtendimento> listar() {
        return repository.findAll();
    }

    public Optional<UnidadeAtendimento> buscarPor(Long id) {
        return repository.findById(id);
    }

    public UnidadeAtendimentoDetalheDto criar(UnidadeAtendimentoForm form) {
        UnidadeAtendimento unidadeAtendimento = form.converter(repository,
                enderecoRepository, cepRepository, logradouroRepository, 
                bairroRepository, cidadeRepository, tipoUnidadeAtendimentoRepository);
        repository.save(unidadeAtendimento);
        return new UnidadeAtendimentoDetalheDto(unidadeAtendimento);
    }

    public UnidadeAtendimentoDetalheDto atualizar(Long id, UnidadeAtendimentoForm form) {
        Optional<UnidadeAtendimento> optional = buscarPor(id);
        if (optional.isPresent()) {
            UnidadeAtendimento unidadeAtendimento = form.atualizar(id, repository,
                    enderecoRepository, cepRepository, logradouroRepository,
                    bairroRepository, cidadeRepository,
                    tipoUnidadeAtendimentoRepository);
            return new UnidadeAtendimentoDetalheDto(unidadeAtendimento);
        }

        return null;
    }

    public Optional<UnidadeAtendimento> remover(Long id) {
        Optional<UnidadeAtendimento> optional = buscarPor(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        }

        return optional;
    }

    public MessageDto isValido(Long id, UnidadeAtendimentoForm form) {
        String message = "";
        Optional<UnidadeAtendimento> optional = repository.findByMatriz(Boolean.TRUE);
        if (optional.isPresent()) {
            UnidadeAtendimento ua = optional.get();
            if (!ua.getId().equals(id) && form.isMatriz()) {
                message = "Não é possível determinar esta UNIDADE DE ATENDIMENTO como MATRIZ porque já existe uma MATRIZ definida!";
            }
        }

        if (!"".equals(message)) {
            return new MessageDto(HttpStatus.BAD_REQUEST.value(), message);
        }
        return null;
    }
}
