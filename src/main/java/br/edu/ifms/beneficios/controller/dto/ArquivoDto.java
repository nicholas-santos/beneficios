/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Arquivo;

/**
 *
 * @author santos
 */
public class ArquivoDto {
    private Long id;
    private String fileName;
    private String documentType;
    private String documentFormat;
    private byte[] file;
    public ArquivoDto() {
    }

    public ArquivoDto(Arquivo obj) {
        this.id = obj.getId();
        this.fileName = obj.getFileName();
        this.documentType = obj.getDocumentType();
        this.documentFormat = obj.getDocumentFormat();
        this.file = obj.getFile();
    }

    public Long getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public String getDocumentType() {
        return documentType;
    }

    public String getDocumentFormat() {
        return documentFormat;
    }

    public byte[] getFile() {
        return file;
    }
    
}
