/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


import br.edu.ifms.beneficios.modelo.CondicaoMoradia;
import br.edu.ifms.beneficios.repository.CondicaoMoradiaRepository;

/**
 *
 * @author tanabe
 */
public class CondicaoMoradiaForm {

    @NotNull
    @NotEmpty
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public CondicaoMoradia converter() {
        return new CondicaoMoradia(this.descricao);
    }

    public CondicaoMoradia atualizar(Long id, CondicaoMoradiaRepository repository) {
    	CondicaoMoradia condicaoMoradia = repository.getOne(id);
    	condicaoMoradia.setDescricao(this.descricao);

        return condicaoMoradia;
    }
}
