/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoEntrada;

import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class DocumentoEntradaDetalheDto extends AbstractDocumentoEntradaDto {

    private List<ItemEntradaDto> itens = new ArrayList();

    public DocumentoEntradaDetalheDto() {
    }

    public DocumentoEntradaDetalheDto(DocumentoEntrada obj) {
        super(obj);
        this.itens = ItemEntradaDto.converter(obj.getItens());
    }

    public List<ItemEntradaDto> getItens() {
        return itens;
    }
    
    public static Page<DocumentoEntradaDetalheDto> converter(Page<DocumentoEntrada> items) {
        return items.map(DocumentoEntradaDetalheDto::new);
    }
    
    public static List<DocumentoEntradaDetalheDto> converter(List<DocumentoEntrada> items) {
        return items.stream().map(DocumentoEntradaDetalheDto::new).collect(Collectors.toList());
    }
    
}
