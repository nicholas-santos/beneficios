/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Menu;
import br.edu.ifms.beneficios.modelo.tipos.MenuTipo;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class MenuDto {
    private Long id;
    private String nome;
    private String remotePath;
    private Boolean disponivel;
    private MenuTipo tipo;

    public MenuDto(Menu obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.remotePath = obj.getRemotePath();
        this.disponivel = obj.getDisponivel();
        this.tipo = obj.getTipo();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public Boolean getDisponivel() {
        return disponivel;
    }

    public MenuTipo getTipo() {
        return tipo;
    }
    
    public static List<MenuDto> converter(List<Menu> items) {
        return items.stream().map(MenuDto::new).collect(Collectors.toList());
    }
}
