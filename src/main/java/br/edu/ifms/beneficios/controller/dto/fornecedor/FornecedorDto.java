/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.fornecedor;

import br.edu.ifms.beneficios.modelo.Fornecedor;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class FornecedorDto extends AbstractFornecedorDto {

    public FornecedorDto() {
    }

    public FornecedorDto(Fornecedor obj) {
        super(obj);
    }
    
    public static Page<FornecedorDto> converter(Page<Fornecedor> items) {
        return items.map(FornecedorDto::new);
    }
    
    public static List<FornecedorDto> converter(List<Fornecedor> items) {
        return items.stream().map(FornecedorDto::new).collect(Collectors.toList());
    }
    
}
