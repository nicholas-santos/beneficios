/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Uf;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface CidadeRepository extends JpaRepository<Cidade, Long> {
    
    Page<Cidade> findByUfNome(String nomeUf, Pageable paginacao);
    
    List<Cidade> findByUfNome(String nomeUf);
    List<Cidade> findByUfId(Long idUf);
    Cidade findByIdAndUfNome(Long id, String nomeUf);
    Cidade findByNomeAndUfSigla(String nomeCidade, String sigla);
    
    @Query("SELECT coalesce(max(c.id.id), 0) FROM Cidade c WHERE c.id.uf = :uf")
    Long getMaxId(Uf uf);
    
    Page<Cidade> findByNomeContainingIgnoreCase(String nome, Pageable paginacao);
    Page<Cidade> findByNomeContainingIgnoreCaseAndUfSigla(String nome, String nomeUf, Pageable paginacao);
    Optional<Cidade> findByNomeContainingIgnoreCase(String nome);
}
