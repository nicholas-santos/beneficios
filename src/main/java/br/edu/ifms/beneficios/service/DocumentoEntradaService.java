/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.documentoEntrada.DocumentoEntradaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.documentoEntrada.MovimentoEntradaDto;
import br.edu.ifms.beneficios.controller.dto.status.IStatusCount;
import br.edu.ifms.beneficios.controller.form.DocumentoEntradaForm;
import br.edu.ifms.beneficios.controller.form.MovimentoEntradaForm;
import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.HistoricoEntrada;
import br.edu.ifms.beneficios.modelo.ItemEntrada;
import br.edu.ifms.beneficios.modelo.MovimentoEntrada;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.BeneficioEventualRepository;
import br.edu.ifms.beneficios.repository.DocumentoEntradaRepository;
import br.edu.ifms.beneficios.repository.FornecedorRepository;
import br.edu.ifms.beneficios.repository.ItemEntradaRepository;
import br.edu.ifms.beneficios.repository.MovimentoEntradaRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author santos
 */
@Service
public class DocumentoEntradaService {

    @Autowired
    private DocumentoEntradaRepository repository;

    @Autowired
    private UnidadeAtendimentoRepository unidadeAtendimentoRepository;

    @Autowired
    private FornecedorRepository fornecedorRepository;

    @Autowired
    private BeneficioEventualRepository beneficioEventualRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private ItemEntradaRepository itemEntradaRepository;
    
    @Autowired
    private MovimentoEntradaRepository movimentoEntradaRepository;
    
    @Autowired
    private EstoqueService estoqueService;

    public Page<DocumentoEntrada> listar(
            String processo,
            String ata,
            String pregao,
            String empenhoContabil,
            String contrato,
            String numeroNotaFiscal,
            Long fornecedorId, Pageable paginacao) {
        if (processo == null && ata == null && pregao == null && 
                empenhoContabil == null && contrato == null && numeroNotaFiscal == null && 
                fornecedorId == null) {
            return repository.findAll(paginacao);
        }
        
        return repository
                .findByProcessoOrAtaOrPregaoOrEmpenhoContabilOrContratoOrNumeroNotaFiscalOrFornecedorId(
                        processo, ata, pregao, empenhoContabil, contrato, 
                        numeroNotaFiscal, fornecedorId, paginacao);
    }
    
    public Page<ItemEntrada> listar(
            Status status,
            String processo,
            String ata,
            String pregao,
            String empenhoContabil,
            String contrato,
            String numeroNotaFiscal,
            Long fornecedorId, Pageable paginacao) {
        if (status == null && processo == null && ata == null && pregao == null && 
                empenhoContabil == null && contrato == null && numeroNotaFiscal == null && 
                fornecedorId == null) {
            return itemEntradaRepository.findAll(paginacao);
        }
        
        return itemEntradaRepository
                .findByStatusOrIdDocumentoEntradaProcessoOrIdDocumentoEntradaAtaOrIdDocumentoEntradaPregaoOrIdDocumentoEntradaEmpenhoContabilOrIdDocumentoEntradaContratoOrIdDocumentoEntradaNumeroNotaFiscalOrIdDocumentoEntradaFornecedorId(
                        status, processo, ata, pregao, empenhoContabil, contrato, 
                        numeroNotaFiscal, fornecedorId, paginacao);
    }
    

    public List<DocumentoEntrada> listar() {
        return repository.findAll();
    }
    
    public List<IStatusCount> listarContagem() {
        return repository.countTotalByStatus();
    }

    public List<MovimentoEntrada> listar(Long numero, Long documentoEntradaId) {
        return movimentoEntradaRepository
                .findByItemEntradaIdNumeroAndItemEntradaIdDocumentoEntradaId(
                        numero, documentoEntradaId);
    }

    public Optional<DocumentoEntrada> buscarPor(Long id) {
        return repository.findById(id);
    }

    public Optional<ItemEntrada> buscarPor(Long documentoEntradaId, Long id) {
        return itemEntradaRepository.findByIdDocumentoEntradaIdAndIdNumero(documentoEntradaId, id);
    }

    public DocumentoEntradaDetalheDto criar(DocumentoEntradaForm form) {
        Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);
        
        DocumentoEntrada obj = form.converter(repository,
                unidadeAtendimentoRepository, fornecedorRepository,
                beneficioEventualRepository);
        obj.setFuncionario(usuario.getFuncionario());
        obj = repository.save(obj);

        criarHistoricoDocumentoEntrada(obj, usuario.getFuncionario());
        return new DocumentoEntradaDetalheDto(obj);
    }

    public MovimentoEntradaDto criar(MovimentoEntradaForm form) {
        Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);
        Funcionario funcionario = usuario.getFuncionario();
        
        MovimentoEntrada obj = form.converter(itemEntradaRepository);
        obj.setFuncionario(funcionario);
        obj.setUnidadeAtendimento(funcionario.getUnidadeAtendimento());
        obj = movimentoEntradaRepository.save(obj);
        
        estoqueService.adicionar(obj);
        DocumentoEntrada documentoEntrada = obj.getItemEntrada().getId().getDocumentoEntrada();
        documentoEntrada.updateStatus();
        repository.save(documentoEntrada);

        return new MovimentoEntradaDto(obj);
    }

    public DocumentoEntradaDetalheDto atualizar(Long id, DocumentoEntradaForm form) {
        Optional<DocumentoEntrada> optional = buscarPor(id);
        if (optional.isPresent()) {
            DocumentoEntrada obj = form.atualizar(id, repository,
                    unidadeAtendimentoRepository, fornecedorRepository,
                    beneficioEventualRepository);
            Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);
            obj.setFuncionario(usuario.getFuncionario());

            return new DocumentoEntradaDetalheDto(obj);
        }

        return null;
    }

    public Optional<DocumentoEntrada> remover(Long id) {
        Optional<DocumentoEntrada> optional = buscarPor(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        }

        return optional;
    }

    private void criarHistoricoDocumentoEntrada(DocumentoEntrada obj, Funcionario funcionario) {
        HistoricoEntrada he = new HistoricoEntrada();
        he.setDocumentoEntrada(obj);
        he.setFuncionario(funcionario);
        he.setStatus(Status.PENDENTE);
        
        obj.add(he);
    }
    
    public MessageDto isValido(DocumentoEntradaForm form) {
        String message = form.isValido();
        if (!"".equals(message)) {
            return new MessageDto(HttpStatus.BAD_REQUEST.value(), message);
        }
        return null;
    }
}
