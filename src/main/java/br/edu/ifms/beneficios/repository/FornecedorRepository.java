/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Fornecedor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface FornecedorRepository extends JpaRepository<Fornecedor, Long> {

    @Query("SELECT coalesce(max(f.id), 0) FROM Fornecedor f")
    Long getMaxId();
    
    Page<Fornecedor> findByNomeContainingIgnoreCase(String nome, Pageable paginacao);
}
