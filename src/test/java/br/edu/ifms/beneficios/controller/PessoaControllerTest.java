/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import java.net.URI;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PessoaControllerTest {

    @Autowired
    private MockMvc mvc;

    private final String sURI = "/api/pessoas";
    
    private String pessoa1Json = "{\"nome\":\"Pessoa 1\",\"nascimento\":\"2000-02-26\"}";
    private String pessoa2Json = "{\"nome\":\"Pessoa 2\",\"nascimento\":\"2000-07-26\"}";
    private String pessoa1JsonAlterar = "{\"id\":\"1\",\"nome\":\"Pessoa 1\",\"nascimento\":\"2000-03-19\"}";

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosPessoaControllerTest() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscarUmPessoa() throws Exception {        
        URI uri = new URI(sURI + "/2");
        mvc.perform(MockRequestBuilderHelper.get(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarPessoaComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.get(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmPessoa() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, pessoa1Json);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmPessoa() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, pessoa2Json);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));
        
        uri = new URI(sURI + "/2");
        // dados a serem alterados
        request = MockRequestBuilderHelper.put(uri, pessoa1JsonAlterar);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmPessoa() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, pessoa1Json);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));

        uri = new URI(sURI + "/1");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmPessoaComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
}
