
const emptyPessoa = {
    id: '',
    nome: '',
    nascimento: '',
    escolaridadeDto: {
        id: "",
        descricao: "",
    },
    sexo: null,
    foto: null,
    contatos: [],
    moradias: [],
    documentos: [],
    rendimentos: [],
    auxilios: [],
};

export { emptyPessoa };