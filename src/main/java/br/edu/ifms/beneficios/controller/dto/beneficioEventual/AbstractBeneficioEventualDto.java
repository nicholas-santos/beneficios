/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.beneficioEventual;

import br.edu.ifms.beneficios.modelo.BeneficioEventual;
import java.math.BigDecimal;

/**
 *
 * @author santos
 */
public class AbstractBeneficioEventualDto {
    
    private Long id;
    private String descricao;
    private Boolean outraConcessao;
    private BigDecimal disponivel;

    public AbstractBeneficioEventualDto() {
        this.disponivel = BigDecimal.ZERO;
    }

    public AbstractBeneficioEventualDto(BeneficioEventual obj) {
        this();
        this.id = obj.getId();
        this.descricao = obj.getDescricao();
        this.disponivel = obj.getSaldo();
        this.outraConcessao = obj.getOutraConcessao();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public BigDecimal getDisponivel() {
        return disponivel;
    }

    public Boolean getOutraConcessao() {
        return outraConcessao;
    }

    @Override
    public String toString() {
        return "AbstractBeneficioEventualDto{" + "id=" + id + ", descricao=" + descricao + ", disponivel=" + disponivel + '}';
    }
    
}
