/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Auxilio;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class AuxilioDto {

    private Long id;
    private ProgramaGovernoDto programaGoverno;
    private LocalDate dataRegistro;
    private LocalDate dataFim;
    private BigDecimal valor;
    private Status status;
    private Boolean deleted;

    public AuxilioDto() {
        this.deleted = Boolean.FALSE;
    }

    public AuxilioDto(Auxilio obj) {
        this();
        this.id = obj.getId().getId();
        this.programaGoverno = new ProgramaGovernoDto(obj.getProgramaGoverno());
        this.dataRegistro = obj.getDataRegistro();
        this.dataFim = obj.getDataFim();
        this.valor = obj.getValor();
        this.status = obj.getStatus();
    }

    public Long getId() {
        return id;
    }

    public ProgramaGovernoDto getProgramaGoverno() {
        return programaGoverno;
    }

    public LocalDate getDataRegistro() {
        return dataRegistro;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public Status getStatus() {
        return status;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    
    public static List<AuxilioDto> converter(List<Auxilio> items) {
        return items.stream().map(AuxilioDto::new).collect(Collectors.toList());
    }
    
}
