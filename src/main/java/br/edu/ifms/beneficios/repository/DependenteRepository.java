/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Dependente;
import br.edu.ifms.beneficios.modelo.Prontuario;
import br.edu.ifms.beneficios.modelo.id.DependenteId;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface DependenteRepository
        extends JpaRepository<Dependente, DependenteId> {
    
    void deleteByIdProntuario(Prontuario prontuario);
    
}
