/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.controller.form.prontuario.ProntuarioForm;
import com.google.gson.Gson;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author santos
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ProntuarioControllerTest {

    @Autowired
    private MockMvc mvc;

    private final String sURI = "/api/prontuarios";
    private final String sURIPessoas = "/api/pessoas";

    private Gson gson = new Gson();

    private ProntuarioForm prontuario1;
    private String pessoa1Json = "{\"nome\":\"Pessoa 1\",\"nascimento\":\"2000-02-26\"}";
    
    public ProntuarioControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        prontuario1 = new ProntuarioForm();
        prontuario1.setUnidadeAtendimento(new UnidadeAtendimentoDto(1L, "UA1"));
        prontuario1.setDescricaoSaude("Teste de descricao");
        prontuario1.setTitular(new PessoaDto(1L, "Pessoa 1", null));
        
        try {
            URI uri = new URI(sURIPessoas);
            RequestBuilder request = MockRequestBuilderHelper
                    .post(uri, pessoa1Json);
            mvc.perform(request);
        } catch (Exception ex) {
            Logger.getLogger(ProntuarioControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosProntuarios() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscarUmProntuario() throws Exception {
        URI uri = new URI(sURI + "/1/1");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
    
    @Test
    public void deveriaDevolver404AoBuscarProntuarioComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999/1");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmProntuario() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(prontuario1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmProntuario() throws Exception {
        prontuario1.setTitular(new PessoaDto(2L, "Pessoa 2", null));
        
        URI uri = new URI(sURI + "/1/1");
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper
                .put(uri, gson.toJson(prontuario1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmProntuario() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(prontuario1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));
        
        uri = new URI(sURI + "/1/1");
        request = MockRequestBuilderHelper.delete(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmProntuarioComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999/1");
        RequestBuilder request = MockRequestBuilderHelper.delete(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
}
