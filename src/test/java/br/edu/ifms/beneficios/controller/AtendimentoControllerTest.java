/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDto;
import br.edu.ifms.beneficios.controller.form.AtendimentoForm;
import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.modelo.Cep;
import br.edu.ifms.beneficios.modelo.Cidade;
import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.Escolaridade;
import br.edu.ifms.beneficios.modelo.Funcao;
import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.Logradouro;
import br.edu.ifms.beneficios.modelo.Pais;
import br.edu.ifms.beneficios.modelo.Perfil;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.Uf;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.id.CepId;
import br.edu.ifms.beneficios.modelo.id.EnderecoId;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import com.google.gson.Gson;
import java.net.URI;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author santos
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AtendimentoControllerTest {

    @Autowired
    private MockMvc mvc;

    private final String sURI = "/api/atendimentos";
    private Gson gson = new Gson();
    
    private static Cidade cidade;
    private static Bairro bairro;
    private static Endereco endereco;
    private static TipoUnidadeAtendimento tipoUnidadeAtendimento;
    private static UnidadeAtendimento unidadeAtendimento;
    private static Funcao funcao;
    private static Funcionario funcionario;
    private static Usuario usuario;
    
    @BeforeEach
    public void setUp() {
        BeneficiosFactory.MVC = mvc;
    }
    
    private void loadPreData() {
        Pais pais = BeneficiosFactory.createPais("Brasil");
        Uf uf = BeneficiosFactory.createUf("Mato Grosso do Sul", "MS", pais);
        cidade = BeneficiosFactory.createCidade("Navirai", uf);
        bairro = BeneficiosFactory.createBairro("Centro");
        
        Logradouro logradouro = BeneficiosFactory.createLogradouro("Rua 13 de fevereriro");
        
        CepId cepId = new CepId(cidade, bairro, logradouro);
        Cep cep = new Cep(cepId);
        
        EnderecoId enderecoId = new EnderecoId(cep, "1234");
        endereco = new Endereco(enderecoId);
        
        tipoUnidadeAtendimento = BeneficiosFactory.createTipoUnidadeAtendimento("CRAS");
        unidadeAtendimento = BeneficiosFactory.createUnidadeAtendimento("CRAS Paraíso", "PARAISO", endereco, tipoUnidadeAtendimento, Boolean.TRUE);
        
        funcao = BeneficiosFactory.createFuncao("Gerente");
        funcionario = BeneficiosFactory.createFuncionario("João Paulo", LocalDate.now(), funcao, unidadeAtendimento, Sexo.MASCULINO);
        
        Perfil perfil = BeneficiosFactory.createPerfil("ROLE_ADMIN", Status.ATIVO);
        usuario = BeneficiosFactory.createUsuario("admin", "admin@email.com", Status.ATIVO, funcionario, Arrays.asList(perfil));
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosAtendimentos() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
//
//    @Test
//    public void deveriaDevolver200AoBuscar1Atendimento() throws Exception {
//        URI uri = new URI(sURI + "/2");
//        RequestBuilder request = MockRequestBuilderHelper.get(uri);
//        mvc.perform(request)
//                .andExpect(MockMvcResultMatchers.status().is(200));
//    }
//
//    @Test
//    public void deveriaDevolver404AoBuscarAtendimentoComUmIdInexistente() throws Exception {
//        URI uri = new URI(sURI + "/999");
//        RequestBuilder request = MockRequestBuilderHelper.get(uri);
//        mvc.perform(request)
//                .andExpect(MockMvcResultMatchers.status().is(404));
//    }

    @Test
    @WithUserDetails(value = "admin", userDetailsServiceBeanName = "usuarioService")
    public void deveriaDevolver201AoCadastrarUmAtendimento() throws Exception {
        Escolaridade escolaridade = BeneficiosFactory.createEscolaridade("Superior completo");
        Pessoa maria = BeneficiosFactory.createPessoa("Maria da Silva", escolaridade, LocalDate.of(1990, Month.MARCH, 5), Sexo.FEMININO);
        
        AtendimentoForm form = new AtendimentoForm();
        form.setPessoa(new PessoaDto(maria));
        form.setDescricao("Teste de criação de atendimento");
        
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(form));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201));
    }
//
//    @Test
//    public void deveriaDevolver200AoAlterarUmAtendimento() throws Exception {
//        URI uri = new URI(sURI);
//        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(cidade1));
//        mvc.perform(request)
//                .andExpect(MockMvcResultMatchers.status().is(201))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));
//
//        uri = new URI(sURI);
//        request = MockRequestBuilderHelper.post(uri, gson.toJson(cidade2));
//        mvc.perform(request)
//                .andExpect(MockMvcResultMatchers.status().is(201))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(2)));
//
//        uri = new URI(sURI + "/2");
//        cidade2.setNome("Mundo Novo");
//        // dados a serem alterados
//        request = MockRequestBuilderHelper.put(uri, gson.toJson(cidade2));
//
//        mvc.perform(request)
//                .andExpect(MockMvcResultMatchers.status().is(200))
//                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("Mundo Novo")));
//    }
//
//    @Test
//    public void deveriaDevolver200AoRemoverUmAtendimento() throws Exception {
//        URI uri = new URI(sURI);
//        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(cidade1));
//        mvc.perform(request)
//                .andExpect(MockMvcResultMatchers.status().is(201))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(4)));
//        request = MockRequestBuilderHelper.post(uri, gson.toJson(cidade2));
//        mvc.perform(request)
//                .andExpect(MockMvcResultMatchers.status().is(201))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(5)));
//
//        uri = new URI(sURI + "/2");
//        mvc.perform(MockRequestBuilderHelper.delete(uri))
//                .andExpect(MockMvcResultMatchers.status().is(200));
//    }
//
//    @Test
//    public void deveriaDevolver404AoRemoverUmAtendimentoComIdInexiste() throws Exception {
//        URI uri = new URI(sURI + "/999");
//        mvc.perform(MockRequestBuilderHelper.delete(uri))
//                .andExpect(MockMvcResultMatchers.status().is(404));
//    }

}
