/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.pessoa;

import br.edu.ifms.beneficios.controller.dto.AuxilioDto;
import br.edu.ifms.beneficios.controller.dto.MoradiaDto;
import br.edu.ifms.beneficios.controller.dto.RendimentoDto;
import br.edu.ifms.beneficios.modelo.Pessoa;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class PessoaDetalheDto extends AbstractPessoaDto {    
    private List<MoradiaDto> moradias = new ArrayList();
    private List<RendimentoDto> rendimentos = new ArrayList();
    private List<AuxilioDto> auxilios = new ArrayList();

    public PessoaDetalheDto() {
    }
    
    public PessoaDetalheDto(Pessoa obj) {
        super(obj);
        
        this.moradias = MoradiaDto.converter(obj.getMoradias());
        this.rendimentos = RendimentoDto.converter(obj.getRendimentos());
        this.auxilios = AuxilioDto.converter(obj.getAuxilios());
    }

    public PessoaDetalheDto(String nome, LocalDate nascimento) {
        super(nome, nascimento);
    }

    public PessoaDetalheDto(Long id, String nome, LocalDate nascimento) {
        super(id, nome, nascimento);
    }

    public List<MoradiaDto> getMoradias() {
        return moradias;
    }

    public List<RendimentoDto> getRendimentos() {
        return rendimentos;
    }

    public List<AuxilioDto> getAuxilios() {
        return auxilios;
    }
    
    public static Page<PessoaDetalheDto> converter(Page<Pessoa> items) {
        return items.map(PessoaDetalheDto::new);
    }
    
    public static List<PessoaDetalheDto> converter(List<Pessoa> items) {
        return items.stream().map(PessoaDetalheDto::new).collect(Collectors.toList());
    }
}
