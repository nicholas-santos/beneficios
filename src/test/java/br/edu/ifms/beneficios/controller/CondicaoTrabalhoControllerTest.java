/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.CondicaoTrabalhoForm;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CondicaoTrabalhoControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/condicoes-de-trabalho";
    private Gson gson = new Gson();
    private CondicaoTrabalhoForm condicaoTrabalho1;
    private CondicaoTrabalhoForm condicaoTrabalho2;

    @BeforeEach
    public void setUp() {
        condicaoTrabalho1 = new CondicaoTrabalhoForm();
        condicaoTrabalho1.setDescricao("Servidor público");

        condicaoTrabalho2 = new CondicaoTrabalhoForm();
        condicaoTrabalho2.setDescricao("Empregado");
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosCondicaoTrabalhos() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1CondicaoTrabalho() throws Exception {
        URI uri = new URI(sURI + "/1");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarCondicaoTrabalhoComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmCondicaoTrabalho() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(condicaoTrabalho1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(5)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmCondicaoTrabalho() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(condicaoTrabalho1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(3)));
        
        
        uri = new URI(sURI);
        request = MockRequestBuilderHelper.post(uri, gson.toJson(condicaoTrabalho2));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(4)));

        uri = new URI(sURI + "/3");
        condicaoTrabalho2.setDescricao("Terceirizado");
        // dados a serem alterados
        request = MockRequestBuilderHelper.put(uri, gson.toJson(condicaoTrabalho2));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("Terceirizado")));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmCondicaoTrabalho() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.post(uri, gson.toJson(condicaoTrabalho1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));
        request = MockRequestBuilderHelper.post(uri, gson.toJson(condicaoTrabalho2));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(2)));

        uri = new URI(sURI + "/2");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmCondicaoTrabalhoComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

}
