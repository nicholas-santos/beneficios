/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.modelo.TipoLogradouro;
import br.edu.ifms.beneficios.repository.TipoLogradouroRepository;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author santos
 */
public class TipoLogradouroForm {
    
    @NotNull @NotEmpty @Length(min = 3)
    private String nome;

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public TipoLogradouro converter() {
        return new TipoLogradouro(this.nome);
    }
    
    public TipoLogradouro atualizar(Long id, TipoLogradouroRepository repository) {
        TipoLogradouro obj = repository.findById(id).get();
        obj.setNome(this.nome);
        return obj;
    }
}
