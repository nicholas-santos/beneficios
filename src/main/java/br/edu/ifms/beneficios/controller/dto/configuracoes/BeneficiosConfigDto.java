/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.configuracoes;

import br.edu.ifms.beneficios.property.BeneficiosProperties;
import br.edu.ifms.beneficios.property.Cors;
import br.edu.ifms.beneficios.property.Jwt;

/**
 *
 * @author santos
 */
public class BeneficiosConfigDto {
    private Jwt jwt;
    private String from;
    private String urlFrontEnd;
    private String resetPasswordUrl;
    private Long resetPasswordTokenExpiration;
    private Cors cors;

    public BeneficiosConfigDto(BeneficiosProperties obj) {
        this.jwt = obj.getJwt();
        this.from = obj.getFrom();
        this.urlFrontEnd = obj.getUrlFrontEnd();
        this.resetPasswordUrl = obj.getResetPasswordUrl();
        this.resetPasswordTokenExpiration = obj.getResetPasswordTokenExpiration().toMillis();
        this.cors = obj.getCors();
    }

    public BeneficiosConfigDto() {
    }

    public Jwt getJwt() {
        return jwt;
    }

    public String getFrom() {
        return from;
    }

    public String getUrlFrontEnd() {
        return urlFrontEnd;
    }

    public String getResetPasswordUrl() {
        return resetPasswordUrl;
    }

    public Long getResetPasswordTokenExpiration() {
        return resetPasswordTokenExpiration;
    }

    public Cors getCors() {
        return cors;
    }
}
