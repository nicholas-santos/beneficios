/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.tipos;

import javax.persistence.AttributeConverter;

/**
 *
 * @author santos
 */
public class StatusConverter  implements AttributeConverter<Status, String> {

    @Override
    public String convertToDatabaseColumn(Status status) {
        if (status == null) {
            return null;
        }
        return status.toString();
    }

    @Override
    public Status convertToEntityAttribute(String status) {
        if (status == null)
            return null;
        return Status.valueOf(status);
    }
    
}
