/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.unidadeAtendimento;

import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import java.util.Objects;

/**
 *
 * @author santos
 */
public abstract class AbstractUnidadeAtendimento {
    private Long id;
    private String nome;
    private String numeroDaUnidade;
    private Boolean matriz;

    public AbstractUnidadeAtendimento() {
    }

    public AbstractUnidadeAtendimento(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public AbstractUnidadeAtendimento(UnidadeAtendimento obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.numeroDaUnidade = obj.getNumeroDaUnidade();
        this.matriz = obj.getMatriz();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getNumeroDaUnidade() {
        return numeroDaUnidade;
    }

    public Boolean getMatriz() {
        return matriz;
    }

    @Override
    public boolean equals(Object obj) {
        final AbstractUnidadeAtendimento other = (AbstractUnidadeAtendimento) obj;
        if (!Objects.equals(this.nome, other.getNome())) {
            return false;
        }
        if (!Objects.equals(this.numeroDaUnidade, other.getNumeroDaUnidade())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.matriz, other.getMatriz())) {
            return false;
        }
        return true;
    }
    
}
