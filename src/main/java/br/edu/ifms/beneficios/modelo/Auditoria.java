package br.edu.ifms.beneficios.modelo;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.edu.ifms.beneficios.modelo.tipos.AuditoriaTipo;
import br.edu.ifms.beneficios.modelo.tipos.AuditoriaTipoConverter;
import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import javax.persistence.ManyToOne;

@Entity
public class Auditoria implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Usuario usuario;
    /**
     * Atributo utilizado para registrar a operaçao do usuário.
     *
     * A operaçao pode ser:<br/>
     * <ul>
     * <li>INSERIR</li>
     * <li>REMOVER</li>
     * <li>ATUALIZAR</li>
     * </ul>
     */
    @Convert(converter = AuditoriaTipoConverter.class)
    private AuditoriaTipo operacao;
    private String url;
    private String dadosAnterior;
    private String dadosAtual;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public AuditoriaTipo getOperacao() {
        return operacao;
    }

    public void setOperacao(AuditoriaTipo operacao) {
        this.operacao = operacao;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDadosAnterior() {
        return dadosAnterior;
    }

    public void setDadosAnterior(String dadosAnterior) {
        this.dadosAnterior = dadosAnterior;
    }

    public String getDadosAtual() {
        return dadosAtual;
    }

    public void setDadosAtual(String dadosAtual) {
        this.dadosAtual = dadosAtual;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((dadosAnterior == null) ? 0 : dadosAnterior.hashCode());
        result = prime * result + ((dadosAtual == null) ? 0 : dadosAtual.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((operacao == null) ? 0 : operacao.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Auditoria other = (Auditoria) obj;
        if (created == null) {
            if (other.created != null) {
                return false;
            }
        } else if (!created.equals(other.created)) {
            return false;
        }
        if (dadosAnterior == null) {
            if (other.dadosAnterior != null) {
                return false;
            }
        } else if (!dadosAnterior.equals(other.dadosAnterior)) {
            return false;
        }
        if (dadosAtual == null) {
            if (other.dadosAtual != null) {
                return false;
            }
        } else if (!dadosAtual.equals(other.dadosAtual)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (operacao == null) {
            if (other.operacao != null) {
                return false;
            }
        } else if (!operacao.equals(other.operacao)) {
            return false;
        }
        if (url == null) {
            if (other.url != null) {
                return false;
            }
        } else if (!url.equals(other.url)) {
            return false;
        }
        if (usuario == null) {
            if (other.usuario != null) {
                return false;
            }
        } else if (!usuario.equals(other.usuario)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Auditoria [id=" + id + ", usuario=" + usuario.getId() + ", operacao=" + operacao + ", url=" + url
                + ", dadosAnterior=" + dadosAnterior + ", dadosAtual=" + dadosAtual + ", created=" + created + "]";
    }

}
