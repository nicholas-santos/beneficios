/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.Moradia;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.id.MoradiaId;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author santos
 */
public interface MoradiaRepository extends JpaRepository<Moradia, MoradiaId> {
    
    @Query("SELECT coalesce(max(m.id.id), 0) FROM Moradia m WHERE m.id.pessoa = :pessoa")
    Long getMaxId(Pessoa pessoa);
    
    void deleteByIdPessoa(Pessoa pessoa);
    
    List<Moradia> findByDataSaidaIsNullAndEndereco(Endereco endereco);
}
