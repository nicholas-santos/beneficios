/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.modelo.TipoMoradia;
import br.edu.ifms.beneficios.repository.TipoMoradiaRepository;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author santos
 */
public class TipoMoradiaForm {
    
    @NotEmpty
    private String descricao;
    private Boolean complementar;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getComplementar() {
        return complementar;
    }

    public void setComplementar(Boolean complementar) {
        this.complementar = complementar;
    }
    
    public TipoMoradia converter(TipoMoradiaRepository repository) {
        Long maxId = repository.getMaxId() + 1;
        return new TipoMoradia(maxId, this.descricao, this.complementar);
    }
    
    public TipoMoradia atualizar(Long id, TipoMoradiaRepository repository) {
        TipoMoradia tipoMoradia = repository.getById(id);
        tipoMoradia.setDescricao(this.descricao);
        tipoMoradia.setComplementar(this.complementar);
        return tipoMoradia;
    }
}
