package br.edu.ifms.beneficios.modelo.tipos;

import javax.persistence.AttributeConverter;

public class AuditoriaTipoConverter implements AttributeConverter<AuditoriaTipo, String> {

    @Override
    public String convertToDatabaseColumn(AuditoriaTipo auditoriaTipo) {
        if (auditoriaTipo == null) {
            return null;
        }
        return auditoriaTipo.getValue();
    }

    @Override
    public AuditoriaTipo convertToEntityAttribute(String auditoriaTipo) {
        if (auditoriaTipo == null)
            return null;
		return AuditoriaTipo.of(auditoriaTipo);
    }
    
}
