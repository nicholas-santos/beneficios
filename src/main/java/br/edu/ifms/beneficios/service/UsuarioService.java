/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.usuario.UsuarioDetalheDto;
import br.edu.ifms.beneficios.controller.form.UsuarioForm;
import br.edu.ifms.beneficios.modelo.Arquivo;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.ArquivoRepository;
import br.edu.ifms.beneficios.repository.FuncionarioRepository;
import br.edu.ifms.beneficios.repository.PerfilRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author santos
 */
@Service
public class UsuarioService implements UserDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private PerfilRepository perfilRepository;

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Autowired
    private ArquivoRepository arquivoRepository;

    @Autowired
    private FileStorageService fileStorageService;

    public void resetPassword(Usuario usuario, String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        usuario.setSenha(encoder.encode(password));
        repository.save(usuario);
    }

    public Page<Usuario> listar(Long funcionarioId, Pageable paginacao) {
        if (funcionarioId != null) {
            return repository.findByFuncionarioId(funcionarioId, paginacao);
        }
        return repository.findAll(paginacao);
    }

    public List<Usuario> listarAtivos() {
        return repository.findByStatus(Status.ATIVO);
    }

    public Optional<Usuario> buscarPor(Long id) {
        return repository.findDetailUsuarioById(id);
    }
    
    public Usuario loadUsuarioByEmail(String email) {
        Optional<Usuario> optional = repository.findByEmail(email);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

    public UsuarioDetalheDto buscarPor(String email) {
        Usuario usuario = loadUsuarioByEmail(email);
        if (usuario != null) {
            return new UsuarioDetalheDto(usuario);
        }
        return null;
    }

    public UsuarioDetalheDto criar(UsuarioForm form) {
        Usuario usuario = form.converter(perfilRepository, funcionarioRepository);
        repository.save(usuario);
        return new UsuarioDetalheDto(usuario);
    }

    public UsuarioDetalheDto atualizar(Long id, UsuarioForm form) {
        Optional<Usuario> optional = buscarPor(id);
        if (optional.isPresent()) {
            Usuario usuario = form.atualizar(id, repository, perfilRepository, funcionarioRepository);
            return new UsuarioDetalheDto(usuario);
        }

        return null;
    }

    public Optional<Usuario> remover(Long id) {
        Optional<Usuario> optional = buscarPor(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        }

        return optional;
    }

    public Optional<Usuario> resetPassword(String token, String password) {
        Optional<Usuario> optional = repository
                .findByEmailAndStatusAndEnabled(token,
                        Status.ATIVO, Boolean.TRUE);
        if (optional.isPresent()) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

            Usuario usuario = optional.get();
            usuario.setSenha(encoder.encode(password));
            repository.save(usuario);
        }

        return optional;
    }

    public UsuarioDetalheDto savePhoto(MultipartFile file, Long userId) {
        Optional<Usuario> optional = buscarPor(userId);
        if (optional.isPresent()) {
            Usuario usuario = optional.get();

            Arquivo arquivo
                    = usuario.getFoto() != null
                    ? arquivoRepository.getOne(usuario.getFoto().getId())
                    : new Arquivo(usuario, file);

            arquivo.setFile(fileStorageService.resizeImageToByte(file, 200));
            arquivoRepository.save(arquivo);

            usuario.setFoto(arquivo);
            repository.save(usuario);
            return new UsuarioDetalheDto(usuario);
        }
        return null;
    }

    public byte[] loadPhoto(Long id) {
        Optional<Usuario> optional = repository.findById(id);

        if (optional.isEmpty()) {
            return null;
        }

        Usuario usuario = optional.get();
        Arquivo arquivo = usuario.getFoto();
        if (arquivo == null) {
            return null;
        }

        return arquivo.getFile();
    }

    public void updateFailedLoginAttempts(String username) {
        try {
            Optional<Usuario> op = repository.findByEmail(username);
            Usuario user = op.get();

            Integer failedLoginAttempts = user.getFailedAttempt();
            if (failedLoginAttempts == null) {
                failedLoginAttempts = 1;
            } else {
                failedLoginAttempts++;
            }

            user.setFailedAttempt(failedLoginAttempts);
            user.setLastFailedLoginTime(System.currentTimeMillis());

            repository.save(user);
        } catch (UsernameNotFoundException e) {
            LOG.error("Problem attempting to update failed login attempts!", e);
        }
    }

    public void successfulLogin(String username) {
        resetFailedLoginAttempts(username);
    }

    private void resetFailedLoginAttempts(String username) {
        Optional<Usuario> op = repository.findByEmail(username);
        Usuario user = op.get();

        Integer failedLoginAttempts = user.getFailedAttempt();
        if (failedLoginAttempts != null) {
            user.setFailedAttempt(null);
            repository.save(user);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
        return loadUsuarioByEmail(string);
    }
}
