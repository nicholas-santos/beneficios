/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;


import br.edu.ifms.beneficios.modelo.Perfil;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class ListaPerfilDto {

    private Long id;
    private String nome;

    public ListaPerfilDto() {
    }

    public ListaPerfilDto(Perfil obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
    
    public static List<ListaPerfilDto> converter(List<Perfil> items) {
        return items.stream().map(ListaPerfilDto::new).collect(Collectors.toList());
    }
}
