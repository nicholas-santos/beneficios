/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.atendimento;

import br.edu.ifms.beneficios.controller.dto.AnaliseDto;
import br.edu.ifms.beneficios.controller.dto.funcionario.FuncionarioDto;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.prontuario.ProntuarioDetalheDto;
import br.edu.ifms.beneficios.modelo.Atendimento;
import br.edu.ifms.beneficios.modelo.tipos.StatusAtendimento;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class AtendimentoDto {

    private Long id;
    private FuncionarioDto atendente;
    private ProntuarioDetalheDto prontuario;
    private PessoaDetalheDto pessoa;
    private LocalDateTime emissao;
    private String descricao;
    private StatusAtendimento status;
    private AnaliseDto analise;

    public AtendimentoDto() {
    }

    public AtendimentoDto(Atendimento obj) {
        this.id = obj.getId();
        this.atendente = new FuncionarioDto(obj.getAtendente());
        this.prontuario = obj.hasProntuario() ? new ProntuarioDetalheDto(obj.getProntuario()) : null;
        this.pessoa = new PessoaDetalheDto(obj.getPessoa());
        this.emissao = obj.getEmissao();
        this.descricao = obj.getDescricao();
        this.status = obj.getStatus();
        
        if (obj.hasAnalise()) {
            this.analise = new AnaliseDto(obj.getAnalise());
        }
    }

    public Long getId() {
        return id;
    }

    public FuncionarioDto getAtendente() {
        return atendente;
    }

    public ProntuarioDetalheDto getProntuario() {
        return prontuario;
    }

    public PessoaDetalheDto getPessoa() {
        return pessoa;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public String getDescricao() {
        return descricao;
    }

    public StatusAtendimento getStatus() {
        return status;
    }

    public Boolean hasProntuario() {
        return this.prontuario != null;
    }

    public AnaliseDto getAnalise() {
        return analise;
    }

    public static List<AtendimentoDto> converter(List<Atendimento> items) {
        return items.stream().map(AtendimentoDto::new).collect(Collectors.toList());
    }

    public static Page<AtendimentoDto> converter(Page<Atendimento> items) {
        return items.map(AtendimentoDto::new);
    }

    @Override
    public String toString() {
        return "AtendimentoDto{" + "id=" + id + ", atendente=" + atendente 
                + ", prontuario=" + prontuario + ", pessoa=" + pessoa + ", emissao=" 
                + emissao + ", descricao=" + descricao + ", status=" + status 
                + ", analise=" + analise + '}';
    }
    

}
