/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.pessoa;

import br.edu.ifms.beneficios.controller.dto.ContatoPessoaDto;
import br.edu.ifms.beneficios.controller.dto.DocumentoPessoaDto;
import br.edu.ifms.beneficios.controller.dto.EscolaridadeDto;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author santos
 */
public abstract class AbstractPessoaDto {
    private Long id;
    private String nome;
    private LocalDate nascimento;
    private Integer idade;
    private Sexo sexo;
    private EscolaridadeDto escolaridadeDto;
    private byte[] foto;

    private List<DocumentoPessoaDto> documentos = new ArrayList();
    private List<ContatoPessoaDto> contatos = new ArrayList();

    public AbstractPessoaDto() {
    }

    public AbstractPessoaDto(String nome, LocalDate nascimento) {
        this(null, nome, nascimento);
    }

    public AbstractPessoaDto(Long id, String nome, LocalDate nascimento) {
        this.id = id;
        this.nome = nome;
        this.nascimento = nascimento;
    }

    public AbstractPessoaDto(Pessoa obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.nascimento = obj.getNascimento();
        this.idade = obj.getIdade();
        this.sexo = obj.getSexo();
        this.foto = obj.getFoto();
        this.escolaridadeDto = new EscolaridadeDto(obj.getEscolaridade());
        this.contatos = ContatoPessoaDto.converter(obj.getContatos());
        this.documentos = DocumentoPessoaDto.converter(obj.getDocumentos());
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public Integer getIdade() {
        return idade;
    }
    
    public Sexo getSexo() {
        return sexo;
    }

    public byte[] getFoto() {
        return foto;
    }

    public EscolaridadeDto getEscolaridadeDto() {
        return escolaridadeDto;
    }

    public List<ContatoPessoaDto> getContatos() {
        return contatos;
    }

    public List<DocumentoPessoaDto> getDocumentos() {
        return documentos;
    }

    @Override
    public boolean equals(Object obj) {
        final AbstractPessoaDto other = (AbstractPessoaDto) obj;
        if (!Objects.equals(this.nome, other.getNome())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.nascimento, other.getNascimento())) {
            return false;
        }
        if (!Objects.equals(this.sexo, other.getSexo())) {
            return false;
        }
        return true;
    }
}
