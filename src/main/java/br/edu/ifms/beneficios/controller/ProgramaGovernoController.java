/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.ProgramaGovernoDto;
import br.edu.ifms.beneficios.controller.form.ProgramaGovernoForm;
import br.edu.ifms.beneficios.modelo.ProgramaGoverno;
import br.edu.ifms.beneficios.repository.ProgramaGovernoRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/programas-de-governo")
public class ProgramaGovernoController {

    @Autowired
    private ProgramaGovernoRepository repository;

    @GetMapping
    @Cacheable(value = "pageProgramaGoverno")
    public Page<ProgramaGovernoDto> listar(
            @PageableDefault(sort = "descricao", 
                    direction = Direction.ASC, 
                    page = 0, size = 10) Pageable paginacao) {
            return ProgramaGovernoDto.converter(repository.findAll(paginacao));
    }
    
    @GetMapping("/list")
    @Cacheable(value = "listaProgramaGoverno")
    public List<ProgramaGovernoDto> listar() {
        return ProgramaGovernoDto.converter(repository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProgramaGovernoDto> visualizar(@PathVariable Long id) {
        Optional<ProgramaGoverno> programaGoverno = repository.findById(id);
        if (programaGoverno.isPresent()) {
            return ResponseEntity.ok(new ProgramaGovernoDto(programaGoverno.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageProgramaGoverno", "listaProgramaGoverno"}, allEntries = true)
    public ResponseEntity<ProgramaGovernoDto> criar(@RequestBody @Valid ProgramaGovernoForm form, UriComponentsBuilder uriBuilder) {
    	ProgramaGoverno programaGoverno = form.converter();
        repository.save(programaGoverno);

        URI uri = uriBuilder.path("/programas-de-governos/{id}").buildAndExpand(programaGoverno.getId()).toUri();
        return ResponseEntity.created(uri).body(new ProgramaGovernoDto(programaGoverno));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageProgramaGoverno", "listaProgramaGoverno"}, allEntries = true)
    public ResponseEntity<ProgramaGovernoDto> atualizar(@PathVariable Long id, @RequestBody @Valid ProgramaGovernoForm form) {
        Optional<ProgramaGoverno> optional = repository.findById(id);
        if (optional.isPresent()) {
        	ProgramaGoverno programaGoverno = form.atualizar(id, repository);
            return ResponseEntity.ok(new ProgramaGovernoDto(programaGoverno));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageProgramaGoverno", "listaProgramaGoverno"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<ProgramaGoverno> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
