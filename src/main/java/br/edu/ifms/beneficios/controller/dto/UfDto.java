/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Uf;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class UfDto {
    
    private Long id;
    private String nome;
    private String sigla;
    private PaisDto pais;

    public UfDto() {
    }

    public UfDto(Uf obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.sigla = obj.getSigla();
        this.pais = new PaisDto(obj.getPais());
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getSigla() {
        return sigla;
    }

    public PaisDto getPais() {
        return pais;
    }
    
    public static List<UfDto> converter(List<Uf> items) {
        return items.stream().map(UfDto::new).collect(Collectors.toList());
    }
}
