/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoSaida;

import br.edu.ifms.beneficios.controller.dto.beneficioEventual.BeneficioEventualDetalheDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.modelo.ItemSaida;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.math.BigDecimal;

/**
 *
 * @author santos
 */
public class AbstractItemSaidaDto {
    private Long numero;
    private Long documentoSaidaId;
    private BigDecimal quantidade;
    private BeneficioEventualDetalheDto beneficioEventual;
    private UnidadeAtendimentoDto unidadeAtendimento;
    private Status status;
    private Boolean deleted;
    private DocumentoSaidaDto documentoSaida;

    public AbstractItemSaidaDto() {
        this.deleted = Boolean.FALSE;
        this.quantidade = BigDecimal.ZERO;
        this.status = Status.PENDENTE;
    }

    public AbstractItemSaidaDto(ItemSaida obj) {
        this();
        this.documentoSaidaId = obj.getId().getDocumentoSaida().getId();
        this.numero = obj.getId().getNumero();
        this.quantidade = obj.getQuantidade();
        this.beneficioEventual = new BeneficioEventualDetalheDto(obj.getBeneficioEventual());
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getUnidadeAtendimento());
        this.status = obj.getStatus();
        this.deleted = obj.isDeleted();
        this.documentoSaida = new DocumentoSaidaDto(obj.getId().getDocumentoSaida());
    }

    public Long getNumero() {
        return numero;
    }

    public Long getDocumentoSaidaId() {
        return documentoSaidaId;
    }

    public DocumentoSaidaDto getDocumentoSaida() {
        return documentoSaida;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public BeneficioEventualDetalheDto getBeneficioEventual() {
        return beneficioEventual;
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public Status getStatus() {
        return status;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    
    public Boolean hasPositiveValue() {
        return this.quantidade.compareTo(BigDecimal.ZERO) > 0;
    }
    
    public Boolean isSetNumero() {
        return this.numero != null;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    @Override
    public String toString() {
        return "AbstractItemSaidaDto{" + "numero=" + numero + ", documentoSaidaId=" + documentoSaidaId + ", quantidade=" + quantidade + ", beneficioEventual=" + beneficioEventual + ", unidadeAtendimentoDto=" + unidadeAtendimento + ", status=" + status + ", deleted=" + deleted + ", documentoSaida=" + documentoSaida + '}';
    }
    
}
