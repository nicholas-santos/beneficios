/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateTimeAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 *
 * @author santos
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractDocumento implements Serializable {

    @Id
    private Long id;

    @ManyToOne
    private Funcionario funcionario;

    @ManyToOne
    private UnidadeAtendimento unidadeAtendimento;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime emissao;

    @Column(name = "status", nullable = false)
    @Convert(converter = StatusConverter.class)
    private Status status;
    
    private String observacao;

    public AbstractDocumento() {
        this.status = Status.PENDENTE;
    }

    public AbstractDocumento(Long id) {
        this();
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public LocalDateTime getEmissao() {
        return emissao;
    }

    public void setEmissao(LocalDateTime emissao) {
        this.emissao = emissao;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        hash = 29 * hash + Objects.hashCode(this.funcionario);
        hash = 29 * hash + Objects.hashCode(this.unidadeAtendimento);
        hash = 29 * hash + Objects.hashCode(this.emissao);
        hash = 29 * hash + Objects.hashCode(this.status);
        hash = 29 * hash + Objects.hashCode(this.observacao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final AbstractDocumento other = (AbstractDocumento) obj;
        if (!Objects.equals(this.observacao, other.getObservacao())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.funcionario, other.getFuncionario())) {
            return false;
        }
        if (!Objects.equals(this.unidadeAtendimento, other.getUnidadeAtendimento())) {
            return false;
        }
        if (!Objects.equals(this.emissao, other.getEmissao())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        return true;
    }

}
