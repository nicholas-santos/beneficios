/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Menu;
import br.edu.ifms.beneficios.modelo.PerfilMenu;
import br.edu.ifms.beneficios.modelo.id.PerfilMenuId;
import br.edu.ifms.beneficios.modelo.tipos.MenuTipo;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class PerfilMenuDto {
    private Long id;
    private String nome;
    private MenuTipo tipo;
    private String remotePath;
    private Boolean ler;
    private Boolean escrever;
    private Boolean remover;
    private Boolean deleted;

    public PerfilMenuDto() {
        this.deleted = Boolean.FALSE;
    }

    public PerfilMenuDto(PerfilMenu obj) {
        this();
        PerfilMenuId id = obj.getId();
        Menu menu = id.getMenu();
        this.id = menu.getId();
        this.nome = menu.getNome();
        this.tipo = menu.getTipo();
        this.remotePath = menu.getRemotePath();
        this.ler = obj.getLer();
        this.escrever = obj.getEscrever();
        this.remover = obj.getRemover();
        this.deleted = obj.isDeleted();
    }

    public Long getId() {
        return id;
    }
    
    public Boolean hasId() {
        return this.id != null && this.id > 0;
    }

    public String getNome() {
        return nome;
    }

    public MenuTipo getTipo() {
        return tipo;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public Boolean getLer() {
        return ler;
    }

    public Boolean getEscrever() {
        return escrever;
    }

    public Boolean getRemover() {
        return remover;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    
    public static List<PerfilMenuDto> converter(List<PerfilMenu> items) {
        return items.stream().map(PerfilMenuDto::new).collect(Collectors.toList());
    }
}
