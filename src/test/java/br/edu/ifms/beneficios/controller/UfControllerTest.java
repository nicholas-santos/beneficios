/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.dto.PaisDto;
import br.edu.ifms.beneficios.controller.form.UfForm;
import br.edu.ifms.beneficios.modelo.Pais;
import com.google.gson.Gson;
import java.net.URI;
import java.net.URISyntaxException;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author santos
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UfControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/ufs";
    private Gson gson = new Gson();
    private UfForm uf1;
    private UfForm uf2;
    
    @BeforeEach
    public void setUp() throws URISyntaxException, Exception {
        BeneficiosFactory.MVC = mvc;
        Pais pais = BeneficiosFactory.createPais("Brasil");
        
        PaisDto paisdto = new PaisDto(pais);
        
        uf1 = new UfForm();
        uf1.setNome("Mato Grosso do Sul");
        uf1.setPais(paisdto);
        uf1.setSigla("MS");
        
        uf2 = new UfForm();
        uf2.setNome("Paraná");
        uf2.setPais(paisdto);
        uf2.setSigla("PR");
    }


    @Test
    public void deveriaDevolver200AoBuscarTodosUf() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1Uf() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(uf1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)));
        
        uri = new URI(sURI + "/1");
        request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarUfComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmUf() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(uf1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(2)));
    }
}
