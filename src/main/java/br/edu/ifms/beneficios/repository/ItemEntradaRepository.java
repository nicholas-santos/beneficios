/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.ItemEntrada;
import br.edu.ifms.beneficios.modelo.id.ItemEntradaId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface ItemEntradaRepository extends JpaRepository<ItemEntrada, ItemEntradaId> {

    Page<ItemEntrada> findByStatusOrIdDocumentoEntradaProcessoOrIdDocumentoEntradaAtaOrIdDocumentoEntradaPregaoOrIdDocumentoEntradaEmpenhoContabilOrIdDocumentoEntradaContratoOrIdDocumentoEntradaNumeroNotaFiscalOrIdDocumentoEntradaFornecedorId(
            Status status,
            String processo, String ata, String pregao, String empenhoContabil, String contrato,
            String numeroNotaFiscal, Long fornecedorId, Pageable paginacao);
    
    Optional<ItemEntrada> findByIdDocumentoEntradaIdAndIdNumero(Long documentoEntradaId, Long numero);
}
