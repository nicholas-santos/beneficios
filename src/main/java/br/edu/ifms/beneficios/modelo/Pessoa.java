/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;
import br.edu.ifms.beneficios.modelo.tipos.SexoConverter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.OptionalLong;
import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.Type;

/**
 *
 * @author santos
 */
//@EntityListeners(AuditoriaListener.class)
@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Pessoa extends AbstractPessoa {

    @ManyToOne
    private Escolaridade escolaridade;

    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate nascimento;

    @Convert(converter = SexoConverter.class)
    private Sexo sexo;

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    private byte[] foto;

    @OneToMany(mappedBy = "id.pessoa",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Moradia> moradias = new ArrayList<>();

    @OneToMany(mappedBy = "id.pessoa",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Rendimento> rendimentos = new ArrayList();

    @OneToMany(mappedBy = "id.pessoa",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Auxilio> auxilios = new ArrayList();

    @OneToMany(mappedBy = "id.pessoa", fetch = FetchType.LAZY)
    private List<Dependente> dependentes = new ArrayList();

    public Pessoa() {
    }

    public Pessoa(Long id) {
        super(id);
        this.sexo = Sexo.MASCULINO;
    }

    public Pessoa(Long id, String nome, Escolaridade escolaridade, LocalDate nascimento, Sexo sexo) {
        super(id, nome);
        this.escolaridade = escolaridade;
        this.nascimento = nascimento;
        this.sexo = sexo;
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    public Integer getIdade() {
        Period period = Period.between(nascimento, LocalDate.now());
        return period.getYears();
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Boolean isFeminino() {
        return Sexo.FEMININO.equals(this.sexo);
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Escolaridade getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(Escolaridade escolaridade) {
        this.escolaridade = escolaridade;
    }

    public List<Moradia> getMoradias() {
        return moradias;
    }

    public void setMoradias(List<Moradia> moradias) {
        this.moradias.clear();
        if (!moradias.isEmpty()) {
            this.moradias.addAll(moradias);
        }
    }

    public void add(Moradia moradia) {
        if (!this.moradias.contains(moradia)) {
            OptionalLong maxId = moradias.stream()
                    .mapToLong(obj -> obj.getId().getId())
                    .max();
            moradia.getId().setId((maxId.isPresent() ? 
                    maxId.getAsLong() : 0) + 1L);
            this.moradias.add(moradia);
        } else {
            this.moradias.stream().map((obj) -> {
                obj.update(moradia);
                return obj;
            });
        }
    }

    public void remove(Moradia moradia) {
        this.moradias.remove(moradia);
        moradia.getId().setPessoa(null);
    }

    public List<Rendimento> getRendimentos() {
        return rendimentos;
    }

    public void setRendimentos(List<Rendimento> rendimentos) {
        this.rendimentos.clear();
        if (!rendimentos.isEmpty()) {
            this.rendimentos.addAll(rendimentos);
        }
    }

    public void add(Rendimento rendimento) {
        if (!this.rendimentos.contains(rendimento)) {
            OptionalLong maxId = rendimentos.stream()
                    .mapToLong(obj -> obj.getId().getSequencia())
                    .max();
            rendimento.getId().setSequencia((maxId.isPresent() ? 
                    maxId.getAsLong() : 0) + 1L);
            this.rendimentos.add(rendimento);
        } else {
            this.rendimentos.stream().map((obj) -> {
                obj.update(rendimento);
                return obj;
            });
        }
    }

    public void remove(Rendimento rendimento) {
        this.rendimentos.remove(rendimento);
        rendimento.getId().setPessoa(null);
    }

    public BigDecimal getValorRendimentos() {
        BigDecimal total = rendimentos.stream()
                .map(r -> r.getValor())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    public List<Auxilio> getAuxilios() {
        return auxilios;
    }

    public void setAuxilios(List<Auxilio> auxilios) {
        this.auxilios.clear();
        if (!auxilios.isEmpty()) {
            this.auxilios.addAll(auxilios);
        }
    }

    public void add(Auxilio auxilio) {
        if (!this.auxilios.contains(auxilio)) {
            OptionalLong maxId = auxilios.stream()
                    .mapToLong(obj -> obj.getId().getId())
                    .max();
            auxilio.getId().setId((maxId.isPresent() ? 
                    maxId.getAsLong() : 0) + 1L);
            this.auxilios.add(auxilio);
        } else {
            this.auxilios.stream().map((obj) -> {
                obj.update(auxilio);
                return obj;
            });
        }
    }

    public void remove(Auxilio auxilio) {
        this.auxilios.remove(auxilio);
        auxilio.getId().setPessoa(null);
    }

    public BigDecimal getValorAuxilios() {
        BigDecimal total = auxilios.stream()
                .map(r -> r.getValor())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    public List<Dependente> getDependentes() {
        return dependentes;
    }

    public void setDependentes(List<Dependente> dependentes) {
        this.dependentes = dependentes;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.nascimento);
        hash = 71 * hash + Objects.hashCode(this.escolaridade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}
