/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.MovimentoSaida;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface MovimentoSaidaRepository extends JpaRepository<MovimentoSaida, Long> {
    
    List<MovimentoSaida> findByItemSaidaIdNumeroAndItemSaidaIdDocumentoSaidaId(
            Long numero, Long documentoSaidaId);
    
}
