/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Endereco;
import br.edu.ifms.beneficios.modelo.id.CepId;
import br.edu.ifms.beneficios.modelo.id.EnderecoId;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class EnderecoDto {

    private LogradouroDto logradouro;
    private BairroDto bairro;
    private CidadeDto cidade;
    
    private String logradouroNome;
    private String bairroNome;
    private String cidadeNome;
    private String ufSigla;
    
    private String complemento;
    private String referencia;
    private String numero;

    public EnderecoDto() {

    }

    public EnderecoDto(Endereco obj) {
        this.complemento = obj.getComplemento();
        this.referencia = obj.getReferencia();

        EnderecoId id = obj.getId();
        this.numero = id.getNumero();

        CepId cepId = id.getCep().getId();
        this.logradouro = new LogradouroDto(cepId.getLogradouro());
        this.bairro = new BairroDto(cepId.getBairro());
        this.cidade = new CidadeDto(cepId.getCidade());
        
        this.logradouroNome = cepId.getLogradouro().getNome();
        this.bairroNome = cepId.getBairro().getNome();
        this.cidadeNome = cepId.getCidade().getNome();
        this.ufSigla = cepId.getCidade().getUf().getSigla();
    }

    public LogradouroDto getLogradouro() {
        return logradouro;
    }

    public BairroDto getBairro() {
        return bairro;
    }

    public CidadeDto getCidade() {
        return cidade;
    }

    public String getLogradouroNome() {
        return logradouroNome;
    }

    public String getComplemento() {
        return complemento;
    }
    
    public Boolean hasComplemento() {
        return complemento != null && !complemento.isBlank() && !complemento.isEmpty();
    }

    public String getReferencia() {
        return referencia;
    }

    public String getBairroNome() {
        return bairroNome;
    }

    public String getCidadeNome() {
        return cidadeNome;
    }

    public String getUfSigla() {
        return ufSigla;
    }

    public String getNumero() {
        return numero;
    }

    public static List<EnderecoDto> converter(List<Endereco> items) {
        return items.stream().map(EnderecoDto::new).collect(Collectors.toList());
    }

    public static Page<EnderecoDto> converter(Page<Endereco> items) {
        return items.map(EnderecoDto::new);
    }

    @Override
    public String toString() {
        return "EnderecoDto{" + "logradouro=" + logradouro + ", bairro=" + bairro + ", cidade=" + cidade + ", logradouroNome=" + logradouroNome + ", bairroNome=" + bairroNome + ", cidadeNome=" + cidadeNome + ", ufSigla=" + ufSigla + ", complemento=" + complemento + ", referencia=" + referencia + ", numero=" + numero + '}';
    }
}
