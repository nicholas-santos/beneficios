/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.controller.dto.status.IStatusCount;
import br.edu.ifms.beneficios.modelo.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifms.beneficios.modelo.Prontuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author tanabe
 */
@Repository()
public interface ProntuarioRepository extends JpaRepository<Prontuario, Long> {
    
    Page<Prontuario> findByTitular(Pessoa pessoa, Pageable paginacao);
    Page<Prontuario> findByStatus(Status status, Pageable paginacao);
    Page<Prontuario> findByUnidadeAtendimentoId(Long unidadeAtendimentoId, Pageable paginacao);
    
    Page<Prontuario> findByTitularAndStatus(Pessoa pessoa, Status status, Pageable paginacao);
    Page<Prontuario> findByTitularAndUnidadeAtendimentoId(Pessoa pessoa, Long unidadeAtendimentoId, Pageable paginacao);
    Page<Prontuario> findByUnidadeAtendimentoIdAndStatus(Long unidadeAtendimentoId, Status status, Pageable paginacao);
    Page<Prontuario> findByTitularAndUnidadeAtendimentoIdAndStatus(Pessoa pessoa, Long unidadeAtendimentoId, Status status, Pageable paginacao);
    
    List<Prontuario> findByStatusAndDependentesIdPessoa(Status status, Pessoa pessoa);
    List<Prontuario> findByTitularAndStatus(Pessoa pessoa, Status status);
    
    @Query(value = "select status, totalStatus, total,\n"
            + "             CASE \n"
            + "                  WHEN total > 0 THEN round((1.0*totalStatus/total*100), 2) \n"
            + "                  ELSE 0 \n"
            + "             END percentualStatus\n"
            + "  from (select status, count(status) as totalStatus,\n"
            + "		(select count(status) from prontuario) as total\n"
            + "	  from prontuario\n"
            + "	 group by status\n"
            + "	 order by status) as contagem", nativeQuery = true)
    List<IStatusCount> countTotalByStatus();
    
    @Query(value = "select status, totalStatus, total,\n"
            + "             CASE \n"
            + "                  WHEN total > 0 THEN round((1.0*totalStatus/total*100), 2) \n"
            + "                  ELSE 0 \n"
            + "             END percentualStatus\n"
            + "  from (select status, count(status) as totalStatus,\n"
            + "		(select count(status) \n"
            + "            from prontuario \n"
            + "           where unidade_atendimento_id = ?1) as total\n"
            + "	  from prontuario\n"
            + "  where unidade_atendimento_id = ?1 \n"
            + "	 group by status\n"
            + "	 order by status) as contagem", nativeQuery = true)
    List<IStatusCount> countTotalByStatusAndUnidadeAtendimentoId(Long unidadeAtendimentoId);
}
