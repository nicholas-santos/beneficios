/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.BeneficiosFactory;
import br.edu.ifms.beneficios.MockRequestBuilderHelper;
import br.edu.ifms.beneficios.controller.form.DocumentoForm;
import br.edu.ifms.beneficios.modelo.Documento;
import com.google.gson.Gson;
import java.net.URI;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tanabe
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DocumentoControllerTest {

    @Autowired
    private MockMvc mvc;
    
    private final String sURI = "/api/documentos";
    private Gson gson = new Gson();
    private DocumentoForm documento1;

    @BeforeEach
    public void setUp() {
        BeneficiosFactory.MVC = mvc;
        documento1 = new DocumentoForm();
        documento1.setDescricao("Registro Geral");
        documento1.setExigeOrgaoExpedidor(Boolean.FALSE);
    }

    @Test
    public void deveriaDevolver200AoBuscarTodosDocumentos() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver200AoBuscar1Documento() throws Exception {
        Documento doc = BeneficiosFactory.createDocumento("Registro Geral", Boolean.TRUE);
        
        URI uri = new URI(String.format("%s/%d", sURI, doc.getId()));
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoBuscarDocumentoComUmIdInexistente() throws Exception {
        URI uri = new URI(sURI + "/999");
        RequestBuilder request = MockRequestBuilderHelper.get(uri);
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void deveriaDevolver201AoCadastrarUmDocumento() throws Exception {
        URI uri = new URI(sURI);
        RequestBuilder request = MockRequestBuilderHelper
                .post(uri, gson.toJson(documento1));
        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(4)));
    }

    @Test
    public void deveriaDevolver200AoAlterarUmDocumento() throws Exception {
        Documento doc = BeneficiosFactory.createDocumento("Certidão de Pessoa Física", Boolean.FALSE);

        URI uri = new URI(String.format("%s/%d", sURI, doc.getId()));
        doc.setDescricao("Documento Militar");
        // dados a serem alterados
        RequestBuilder request = MockRequestBuilderHelper.put(uri, gson.toJson(doc));

        mvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(CoreMatchers.containsString("Documento Militar")));
    }

    @Test
    public void deveriaDevolver200AoRemoverUmDocumento() throws Exception {
        Documento doc = BeneficiosFactory.createDocumento("Certidão de Nascimento", Boolean.FALSE);

        URI uri = new URI(String.format("%s/%d", sURI, doc.getId()));
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void deveriaDevolver404AoRemoverUmDocumentoComIdInexiste() throws Exception {
        URI uri = new URI(sURI + "/999");
        mvc.perform(MockRequestBuilderHelper.delete(uri))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

}
