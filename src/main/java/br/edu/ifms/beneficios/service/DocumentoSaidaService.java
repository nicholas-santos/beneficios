/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.documentoSaida.DocumentoSaidaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.documentoSaida.MovimentoSaidaDto;
import br.edu.ifms.beneficios.controller.dto.status.IStatusCount;
import br.edu.ifms.beneficios.controller.form.DocumentoSaidaForm;
import br.edu.ifms.beneficios.controller.form.MovimentoSaidaForm;
import br.edu.ifms.beneficios.helper.ControllerHelper;
import br.edu.ifms.beneficios.modelo.DocumentoSaida;
import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.HistoricoSaida;
import br.edu.ifms.beneficios.modelo.ItemSaida;
import br.edu.ifms.beneficios.modelo.MovimentoSaida;
import br.edu.ifms.beneficios.modelo.Usuario;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.BeneficioEventualRepository;
import br.edu.ifms.beneficios.repository.DocumentoSaidaRepository;
import br.edu.ifms.beneficios.repository.ItemSaidaRepository;
import br.edu.ifms.beneficios.repository.MovimentoSaidaRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import br.edu.ifms.beneficios.repository.UsuarioRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author santos
 */
@Service
public class DocumentoSaidaService {

    @Autowired
    private DocumentoSaidaRepository repository;

    @Autowired
    private UnidadeAtendimentoRepository unidadeAtendimentoRepository;

    @Autowired
    private BeneficioEventualRepository beneficioEventualRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private ItemSaidaRepository itemSaidaRepository;
    
    @Autowired
    private MovimentoSaidaRepository movimentoSaidaRepository;
    
    @Autowired
    private EstoqueService estoqueService;

    public Page<DocumentoSaida> listar(Status status, Pageable paginacao) {
        Boolean isSetStatus = status != null;
        
        if (!isSetStatus) {
            return repository.findAll(paginacao);
        }
        
        return repository.findByStatus(status, paginacao);
    }
    
    public Page<ItemSaida> listarPor(Status status, Pageable paginacao) {
        Boolean isSetStatus = status != null;
        
        if (!isSetStatus) {
            return itemSaidaRepository.findAll(paginacao);
        }
        
        return itemSaidaRepository.findByStatus(status, paginacao);
    }
    

    public List<DocumentoSaida> listar() {
        return repository.findAll();
    }

    public List<MovimentoSaida> listar(Long numero, Long documentoSaidaId) {
        return movimentoSaidaRepository
                .findByItemSaidaIdNumeroAndItemSaidaIdDocumentoSaidaId(numero, documentoSaidaId);
    }
    
    public List<IStatusCount> listarContagem() {
        return repository.countTotalByStatus();
    }

    public Optional<DocumentoSaida> buscarPor(Long id) {
        return repository.findById(id);
    }

    public Optional<ItemSaida> buscarPor(Long documentoEntradaId, Long id) {
        return itemSaidaRepository.findByIdDocumentoSaidaIdAndIdNumero(documentoEntradaId, id);
    }

    public DocumentoSaidaDetalheDto criar(DocumentoSaidaForm form) {
        Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);
        
        DocumentoSaida obj = form.converter(repository,
                unidadeAtendimentoRepository, beneficioEventualRepository);
        obj.setFuncionario(usuario.getFuncionario());
        obj = repository.save(obj);

        criarHistoricoDocumentoSaida(obj, usuario.getFuncionario());
        return new DocumentoSaidaDetalheDto(obj);
    }

    public MovimentoSaidaDto criar(MovimentoSaidaForm form) {
        Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);
        Funcionario funcionario = usuario.getFuncionario();
        
        MovimentoSaida obj = form.converter(itemSaidaRepository);
        obj.setFuncionario(funcionario);
        obj.setUnidadeAtendimento(funcionario.getUnidadeAtendimento());
        obj = movimentoSaidaRepository.save(obj);
        
        estoqueService.retirar(obj);
        DocumentoSaida documentoSaida = obj.getItemSaida().getId().getDocumentoSaida();
        documentoSaida.updateStatus();
        repository.save(documentoSaida);
        

        return new MovimentoSaidaDto(obj);
    }

    public DocumentoSaidaDetalheDto atualizar(Long id, DocumentoSaidaForm form) {
        Optional<DocumentoSaida> optional = buscarPor(id);
        if (optional.isPresent()) {
            DocumentoSaida obj = form.atualizar(id, repository,
                    unidadeAtendimentoRepository, beneficioEventualRepository);
            Usuario usuario = ControllerHelper.getLoggedUser(usuarioRepository);
            obj.setFuncionario(usuario.getFuncionario());

            return new DocumentoSaidaDetalheDto(obj);
        }

        return null;
    }

    public Optional<DocumentoSaida> remover(Long id) {
        Optional<DocumentoSaida> optional = buscarPor(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
        }

        return optional;
    }

    private void criarHistoricoDocumentoSaida(DocumentoSaida obj, Funcionario funcionario) {
        HistoricoSaida hs = new HistoricoSaida();
        hs.setDocumentoSaida(obj);
        hs.setFuncionario(funcionario);
        hs.setStatus(Status.PENDENTE);
        
        obj.add(hs);
    }
    
    public MessageDto isValido(DocumentoSaidaForm form) {
        String message = form.isValido();
        if (!"".equals(message)) {
            return new MessageDto(HttpStatus.BAD_REQUEST.value(), message);
        }
        return null;
    }
}
