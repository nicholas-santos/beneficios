/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form.prontuario;

import br.edu.ifms.beneficios.controller.dto.DependenteDto;
import br.edu.ifms.beneficios.controller.dto.GrupoSocioeducativoDto;
import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import br.edu.ifms.beneficios.modelo.Dependente;
import br.edu.ifms.beneficios.modelo.Parentesco;
import br.edu.ifms.beneficios.modelo.Pessoa;
import br.edu.ifms.beneficios.modelo.Prontuario;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.id.DependenteId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.repository.GrupoSocioeducativoRepository;
import br.edu.ifms.beneficios.repository.ParentescoRepository;
import br.edu.ifms.beneficios.repository.PessoaRepository;
import br.edu.ifms.beneficios.repository.ProntuarioRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author santos
 */
public class ProntuarioForm {

    @NotNull
    private UnidadeAtendimentoDto unidadeAtendimento;

    @NotEmpty
    @NotNull
    @Length(min = 5)
    private String descricaoSaude;
    private Boolean acompanhamento;

    private Status status;
    private GrupoSocioeducativoDto grupoSocioeducativo;

    @NotNull
    private PessoaDto titular;

    private List<DependenteDto> dependentes = new ArrayList();

    public ProntuarioForm() {
        this.status = Status.ATIVO;
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimentoDto unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public String getDescricaoSaude() {
        return descricaoSaude;
    }

    public void setDescricaoSaude(String descricaoSaude) {
        this.descricaoSaude = descricaoSaude;
    }

    public Boolean getAcompanhamento() {
        return acompanhamento;
    }

    public void setAcompanhamento(Boolean acompanhamento) {
        this.acompanhamento = acompanhamento;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public GrupoSocioeducativoDto getGrupoSocioeducativo() {
        return grupoSocioeducativo;
    }

    public void setGrupoSocioeducativo(GrupoSocioeducativoDto grupoSocioeducativo) {
        this.grupoSocioeducativo = grupoSocioeducativo;
    }

    public PessoaDto getTitular() {
        return titular;
    }

    public void setTitular(PessoaDto titular) {
        this.titular = titular;
    }

    public List<DependenteDto> getDependentes() {
        return dependentes;
    }

    public void setDependentes(List<DependenteDto> dependentes) {
        this.dependentes = dependentes;
    }

    private void updateDependentesList(Prontuario prontuario,
            PessoaRepository pessoaRepository,
            ParentescoRepository parentescoRepository) {
        dependentes.stream().map(dto -> {
            Pessoa pessoa = pessoaRepository
                    .getOne(dto.getPessoa().getId());
            Parentesco parentesco = parentescoRepository.getOne(dto.getParentesco().getId());

            DependenteId id = new DependenteId(pessoa, prontuario);
            Dependente dependente = new Dependente(id, parentesco);
            dependente.setDeleted(dto.isDeleted());

            if (dependente.isDeleted()) {
                prontuario.remove(dependente);
            } else {
                prontuario.add(dependente);
            }
            return dto;
        }).collect(Collectors.toList());
    }

    private Prontuario toProntuario(Prontuario prontuario,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            PessoaRepository pessoaRepository,
            ParentescoRepository parentescoRepository,
            GrupoSocioeducativoRepository grupoSocioeducativoRepository) {
        
        UnidadeAtendimento ua = unidadeAtendimentoRepository
                .findById(unidadeAtendimento.getId()).get();
        Pessoa pessoa = pessoaRepository.findById(titular.getId()).get();
        
        prontuario.setUnidadeAtendimento(ua);
        prontuario.setTitular(pessoa);
        
        if (grupoSocioeducativo != null && grupoSocioeducativo.getId() != null) {
            prontuario.setGrupoSocioeducativo(
                    grupoSocioeducativoRepository
                            .getOne(grupoSocioeducativo.getId()));
        }

        prontuario.setDescricaoSaude(descricaoSaude);
        prontuario.setStatus(status);
        updateDependentesList(prontuario, pessoaRepository, parentescoRepository);

        return prontuario;
    }

    public Prontuario converter(ProntuarioRepository repository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            GrupoSocioeducativoRepository grupoSocioeducativoRepository,
            PessoaRepository pessoaRepository,
            ParentescoRepository parentescoRepository) {

        Prontuario prontuario = new Prontuario();
        return toProntuario(prontuario, unidadeAtendimentoRepository,
                pessoaRepository, parentescoRepository,
                grupoSocioeducativoRepository);
    }

    public Prontuario atualizar(Long id,
            ProntuarioRepository repository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository,
            GrupoSocioeducativoRepository grupoSocioeducativoRepository,
            PessoaRepository pessoaRepository,
            ParentescoRepository parentescoRepository) {
        Prontuario prontuario = repository.getOne(id);
        // A atualização força o status pra PENDENTE
        this.status = Status.PENDENTE;
        return toProntuario(prontuario, unidadeAtendimentoRepository,
                pessoaRepository, parentescoRepository, 
                grupoSocioeducativoRepository);
    }

}
