/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.Funcao;
import br.edu.ifms.beneficios.repository.FuncaoRepository;

/**
 *
 * @author tanabe
 */
public class FuncaoForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setDescricao(String nome) {
        this.nome = nome;
    }

    public Funcao converter(FuncaoRepository repository) {
        Long maxId = repository.getMaxId() + 1;
        return new Funcao(maxId, this.nome);
    }

    public Funcao atualizar(Long id, FuncaoRepository repository) {
    	Funcao funcao = repository.getOne(id);
    	funcao.setNome(this.nome);

        return funcao;
    }
}
