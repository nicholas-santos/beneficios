/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.service;

import br.edu.ifms.beneficios.controller.dto.EstoqueDto;
import br.edu.ifms.beneficios.modelo.Estoque;
import br.edu.ifms.beneficios.modelo.ItemAnalise;
import br.edu.ifms.beneficios.modelo.MovimentoEntrada;
import br.edu.ifms.beneficios.modelo.MovimentoSaida;
import br.edu.ifms.beneficios.modelo.UnidadeAtendimento;
import br.edu.ifms.beneficios.modelo.id.EstoqueId;
import br.edu.ifms.beneficios.repository.EstoqueRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author santos
 */
@Service
public class EstoqueService {

    @Autowired
    private EstoqueRepository repository;

    public Optional<Estoque> buscarPor(Long beneficioEventualId, Long unidadeAtendimentoId) {
        return repository.findByIdBeneficioEventualIdAndIdUnidadeAtendimentoId(beneficioEventualId, unidadeAtendimentoId);
    }

    public EstoqueDto adicionar(MovimentoEntrada movimentoEntrada) {
        Estoque estoque = null;
        EstoqueId id = new EstoqueId(
                movimentoEntrada.getUnidadeAtendimento(),
                movimentoEntrada.getItemEntrada().getBeneficioEventual()
        );
        Optional<Estoque> optional = repository.findById(id);
        if (optional.isPresent()) {
            estoque = optional.get();
            estoque.add(movimentoEntrada.getQuantidade());
        } else {
            estoque = new Estoque(id, movimentoEntrada.getQuantidade());
            estoque = repository.save(estoque);
        }
        return new EstoqueDto(estoque);
    }

    public EstoqueDto retirar(MovimentoSaida movimentoSaida) {
        /**
         * Credita no estoque destino
         */
        EstoqueId idDestino = new EstoqueId(
                movimentoSaida.getItemSaida().getUnidadeAtendimento(),
                movimentoSaida.getItemSaida().getBeneficioEventual()
        );
        Estoque estoqueDestino = null;
        Optional<Estoque> opDestino = repository.findById(idDestino);
        if (opDestino.isPresent()) {
            estoqueDestino = opDestino.get();
            estoqueDestino.add(movimentoSaida.getQuantidade());
        } else {
            estoqueDestino = new Estoque(idDestino, movimentoSaida.getQuantidade());
            repository.save(estoqueDestino);
        }

        /**
         * Debita estoque da origem
         */
        EstoqueId id = new EstoqueId(
                movimentoSaida.getUnidadeAtendimento(),
                movimentoSaida.getItemSaida().getBeneficioEventual()
        );
        Optional<Estoque> optional = repository.findById(id);
        if (optional.isPresent()) {
            Estoque estoqueOrigem = optional.get();
            estoqueOrigem.subtract(movimentoSaida.getQuantidade());
            return new EstoqueDto(estoqueOrigem);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Usuário Logado com UNIDADE de ATENDIMENTO diferente da definida para esta operação.");
        }
    }

    public EstoqueDto retirar(UnidadeAtendimento unidadeAtendimento, ItemAnalise itemAnalise) {
        Estoque estoque = null;
        EstoqueId id = new EstoqueId(
                unidadeAtendimento,
                itemAnalise.getBeneficioEventual()
        );
        Optional<Estoque> optional = repository.findById(id);
        if (optional.isPresent()) {
            estoque = optional.get();
            estoque.subtract(itemAnalise.getQuantidade());
        } else {
            estoque = new Estoque(id, itemAnalise.getQuantidade());
            estoque = repository.save(estoque);
        }
        return new EstoqueDto(estoque);
    }

}
