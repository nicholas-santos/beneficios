/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.MessageDto;
import br.edu.ifms.beneficios.controller.dto.documentoEntrada.DocumentoEntradaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.documentoEntrada.DocumentoEntradaDto;
import br.edu.ifms.beneficios.controller.dto.documentoEntrada.ItemEntradaDetalheDto;
import br.edu.ifms.beneficios.controller.dto.documentoEntrada.MovimentoEntradaDto;
import br.edu.ifms.beneficios.controller.dto.status.StatusCountDto;
import br.edu.ifms.beneficios.controller.form.DocumentoEntradaForm;
import br.edu.ifms.beneficios.controller.form.MovimentoEntradaForm;
import br.edu.ifms.beneficios.modelo.DocumentoEntrada;
import br.edu.ifms.beneficios.modelo.ItemEntrada;
import br.edu.ifms.beneficios.service.DocumentoEntradaService;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/documento-entrada")
@Profile(value = {"prod", "test"})
public class DocumentoEntradaController {

    @Autowired
    private DocumentoEntradaService service;

    @GetMapping
    public Page<DocumentoEntradaDetalheDto> listar(
            @RequestParam(required = false) String processo,
            @RequestParam(required = false) String ata,
            @RequestParam(required = false) String pregao,
            @RequestParam(required = false) String empenhoContabil,
            @RequestParam(required = false) String contrato,
            @RequestParam(required = false) String numeroNotaFiscal,
            @RequestParam(required = false) Long fornecedorId,
            @PageableDefault(sort = "emissao", direction = Sort.Direction.DESC,
                    page = 0, size = 10) Pageable paginacao) {
        return DocumentoEntradaDetalheDto.converter(
                service.listar(processo, ata, pregao, empenhoContabil, contrato,
                        numeroNotaFiscal, fornecedorId, paginacao));
    }

    @GetMapping("/itens")
    public Page<ItemEntradaDetalheDto> listar(
            @RequestParam(required = false) Status status,
            @RequestParam(required = false) String processo,
            @RequestParam(required = false) String ata,
            @RequestParam(required = false) String pregao,
            @RequestParam(required = false) String empenhoContabil,
            @RequestParam(required = false) String contrato,
            @RequestParam(required = false) String numeroNotaFiscal,
            @RequestParam(required = false) Long fornecedorId,
            @PageableDefault(sort = "id.documentoEntrada.emissao", direction = Sort.Direction.DESC,
                    page = 0, size = 10) Pageable paginacao) {
        return ItemEntradaDetalheDto.converter(
                service.listar(status, processo, ata, pregao, empenhoContabil, contrato,
                        numeroNotaFiscal, fornecedorId, paginacao));
    }

    @GetMapping("/list")
    public List<DocumentoEntradaDto> listar() {
        return DocumentoEntradaDto.converter(service.listar());
    }

    @GetMapping("/contagem")
    public List<StatusCountDto> listarContagem() {
        return StatusCountDto.converter(service.listarContagem());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DocumentoEntradaDetalheDto> visualizar(@PathVariable Long id) {
        Optional<DocumentoEntrada> optional = service.buscarPor(id);
        if (optional.isPresent()) {
            DocumentoEntrada documentoEntrada = optional.get();
            return ResponseEntity.ok(new DocumentoEntradaDetalheDto(documentoEntrada));
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/item/{documentoEntradaId}/{numero}")
    public ResponseEntity<ItemEntradaDetalheDto> visualizar(
            @PathVariable Long documentoEntradaId,
            @PathVariable Long numero
    ) {
        Optional<ItemEntrada> optional = service.buscarPor(documentoEntradaId, numero);
        if (optional.isPresent()) {
            ItemEntrada itemEntrada = optional.get();
            return ResponseEntity.ok(new ItemEntradaDetalheDto(itemEntrada));
        }

        return ResponseEntity.notFound().build();
    }
    
    @GetMapping("/movimentos-de-entrada/{numero}/{documentoEntradaId}")
    public List<MovimentoEntradaDto> listar(
            @PathVariable Long numero,
            @PathVariable Long documentoEntradaId
    ) {
        return MovimentoEntradaDto.converter(service.listar(numero, documentoEntradaId));
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> criar(@RequestBody @Valid DocumentoEntradaForm form, UriComponentsBuilder uriBuilder) {
        MessageDto msgDto = service.isValido(form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            DocumentoEntradaDetalheDto dto = service.criar(form);

            URI uri = uriBuilder.path("/documento-entrada/{id}").buildAndExpand(dto.getId()).toUri();
            return ResponseEntity.created(uri).body(dto);
        }
    }

    @PostMapping("/movimento")
    @Transactional
    public ResponseEntity<?> criar(@RequestBody @Valid MovimentoEntradaForm form, UriComponentsBuilder uriBuilder) {
        MovimentoEntradaDto dto = service.criar(form);

        URI uri = uriBuilder.path("/documento-entrada-conferencia/{documentoEntradaId/{numero}").buildAndExpand(
                dto.getItemEntrada().getDocumentoEntradaId(),
                dto.getItemEntrada().getNumero()).toUri();
        return ResponseEntity.created(uri).body(dto);

    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<?> atualizar(@PathVariable Long id, @RequestBody @Valid DocumentoEntradaForm form) {
        MessageDto msgDto = service.isValido(form);
        if (msgDto != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msgDto);
        } else {
            DocumentoEntradaDetalheDto dto = service.atualizar(id, form);
            if (dto != null) {
                return ResponseEntity.ok(dto);
            }
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<DocumentoEntrada> optional = service.remover(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}
