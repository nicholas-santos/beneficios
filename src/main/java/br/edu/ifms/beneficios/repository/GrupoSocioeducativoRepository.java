/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.GrupoSocioeducativo;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 *
 * @author tanabe
 */
public interface GrupoSocioeducativoRepository extends JpaRepository<GrupoSocioeducativo, Long> {
}
