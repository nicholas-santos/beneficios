/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.edu.ifms.beneficios.modelo.CondicaoTrabalho;
import br.edu.ifms.beneficios.repository.CondicaoTrabalhoRepository;

/**
 *
 * @author tanabe
 */
public class CondicaoTrabalhoForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public CondicaoTrabalho converter() {
        return new CondicaoTrabalho(this.descricao);
    }

    public CondicaoTrabalho atualizar(Long id, CondicaoTrabalhoRepository repository) {
    	CondicaoTrabalho condicaoTrabalho = repository.getOne(id);
    	condicaoTrabalho.setDescricao(this.descricao);

        return condicaoTrabalho;
    }
}
