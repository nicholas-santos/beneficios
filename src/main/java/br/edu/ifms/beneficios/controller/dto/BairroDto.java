/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Bairro;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class BairroDto {
    private Long id;
    private String nome;

    public BairroDto() {
    }

    public BairroDto(Bairro obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
    
    public static Page<BairroDto> converter(Page<Bairro> items) {
        return items.map(BairroDto::new);
    }
    
    public static List<BairroDto> converter(List<Bairro> items) {
        return items.stream().map(BairroDto::new).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "BairroDto{" + "id=" + id + ", nome=" + nome + '}';
    }
}
