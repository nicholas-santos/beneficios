/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.controller.dto.pessoa.PessoaDetalheDto;
import br.edu.ifms.beneficios.modelo.Dependente;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 * @author santos
 */
public class DependenteDto {
    private PessoaDetalheDto pessoa;
    private ParentescoDto parentesco;
    private Boolean deleted;

    public DependenteDto() {
        this.deleted = Boolean.FALSE;
    }

    public DependenteDto(Dependente obj) {
        this();
        this.pessoa = new PessoaDetalheDto(obj.getId().getPessoa());
        this.parentesco = new ParentescoDto(obj.getParentesco());
    }

    public PessoaDetalheDto getPessoa() {
        return pessoa;
    }

    public ParentescoDto getParentesco() {
        return parentesco;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    
    public static List<DependenteDto> converter(List<Dependente> items) {
        return items.stream().map(DependenteDto::new).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object obj) {
        final DependenteDto other = (DependenteDto) obj;
        if (!Objects.equals(this.pessoa, other.getPessoa())) {
            return false;
        }
        if (!Objects.equals(this.parentesco, other.getParentesco())) {
            return false;
        }
        return true;
    }
    
}
