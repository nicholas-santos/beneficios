/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import br.edu.ifms.beneficios.modelo.Logradouro;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class LogradouroDto {

    private Long id;
    private String nome;

    public LogradouroDto() {
    }

    public LogradouroDto(Logradouro obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public static Page<LogradouroDto> converter(Page<Logradouro> items) {
        return items.map(LogradouroDto::new);
    }

    public static List<LogradouroDto> converter(List<Logradouro> items) {
        return items.stream().map(LogradouroDto::new).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "LogradouroDto{" + "id=" + id + ", nome=" + nome + '}';
    }
}
