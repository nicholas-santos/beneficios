/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.tipos;

import javax.persistence.AttributeConverter;

/**
 *
 * @author santos
 */
public class TipoContatoConverter  implements AttributeConverter<TipoContato, String> {

    @Override
    public String convertToDatabaseColumn(TipoContato tipoContato) {
        if (tipoContato == null) {
            return null;
        }
        return tipoContato.toString();
    }

    @Override
    public TipoContato convertToEntityAttribute(String tipoContato) {
        if (tipoContato == null)
            return null;
        return TipoContato.valueOf(tipoContato);
    }
    
}
