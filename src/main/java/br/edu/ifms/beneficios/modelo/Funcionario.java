/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.tipos.LocalDateAttributeConverter;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;
import br.edu.ifms.beneficios.modelo.tipos.SexoConverter;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author santos
 */

//@EntityListeners(AuditoriaListener.class)
@Entity
public class Funcionario implements Serializable {
    @Id
    private Long id;
    private String nome;

    @Convert(converter = SexoConverter.class)
    private Sexo sexo;
    
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate nascimento;
    
    @ManyToOne
    private Funcao funcao;
    
    @OneToOne(mappedBy = "funcionario")
    private Usuario usuario;
    
    @ManyToOne
    private UnidadeAtendimento unidadeAtendimento;

    public Funcionario() {
    }

    public Funcionario(Long id, String nome, LocalDate nascimento, Funcao funcao, UnidadeAtendimento unidadeAtendimento) {
        this.id = id;
        this.nome = nome;
        this.nascimento = nascimento;
        this.funcao = funcao;
        this.unidadeAtendimento = unidadeAtendimento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    public Funcao getFuncao() {
        return funcao;
    }

    public void setFuncao(Funcao funcao) {
        this.funcao = funcao;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UnidadeAtendimento getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimento unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.id);
        hash = 73 * hash + Objects.hashCode(this.nome);
        hash = 73 * hash + Objects.hashCode(this.nascimento);
        hash = 73 * hash + Objects.hashCode(this.funcao);
        hash = 73 * hash + Objects.hashCode(this.usuario);
        hash = 73 * hash + Objects.hashCode(this.unidadeAtendimento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Funcionario other = (Funcionario) obj;
        if (!Objects.equals(this.nome, other.getNome())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.nascimento, other.getNascimento())) {
            return false;
        }
        if (!Objects.equals(this.funcao, other.getFuncao())) {
            return false;
        }
        if (!Objects.equals(this.usuario, other.getUsuario())) {
            return false;
        }
        if (!Objects.equals(this.unidadeAtendimento, other.getUnidadeAtendimento())) {
            return false;
        }
        return true;
    }
    
}
