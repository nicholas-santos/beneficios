/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.FuncaoDto;
import br.edu.ifms.beneficios.controller.dto.unidadeAtendimento.UnidadeAtendimentoDto;
import javax.validation.constraints.NotEmpty;

import br.edu.ifms.beneficios.modelo.Funcionario;
import br.edu.ifms.beneficios.modelo.tipos.Sexo;
import br.edu.ifms.beneficios.helper.DateHelper;
import br.edu.ifms.beneficios.repository.FuncaoRepository;
import br.edu.ifms.beneficios.repository.FuncionarioRepository;
import br.edu.ifms.beneficios.repository.UnidadeAtendimentoRepository;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author tanabe
 */
public class FuncionarioForm {

    @NotEmpty
    private String nome;

    @NotNull
    private Sexo sexo;

    @NotNull
    private Date nascimento;

    @NotNull
    private FuncaoDto funcao;

    @NotNull
    private UnidadeAtendimentoDto unidadeAtendimento;
    
    public FuncionarioForm() {}
    
    public FuncionarioForm(Funcionario obj) {
        this.nome = obj.getNome();
        this.sexo = obj.getSexo();
        this.nascimento = DateHelper.toDate(obj.getNascimento());
        this.funcao = new FuncaoDto(obj.getFuncao());
        this.unidadeAtendimento = new UnidadeAtendimentoDto(obj.getUnidadeAtendimento());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public FuncaoDto getFuncao() {
        return funcao;
    }

    public void setFuncao(FuncaoDto funcao) {
        this.funcao = funcao;
    }

    public UnidadeAtendimentoDto getUnidadeAtendimento() {
        return unidadeAtendimento;
    }

    public void setUnidadeAtendimento(UnidadeAtendimentoDto unidadeAtendimento) {
        this.unidadeAtendimento = unidadeAtendimento;
    }

    private Funcionario toFuncionario(Funcionario funcionario,
            FuncaoRepository funcaoRepository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(nascimento);
        
        int day = cal.get(Calendar.DATE)+1;
        int month = cal.get(Calendar.MONTH)+1;
        int year = cal.get(Calendar.YEAR);
        
        funcionario.setNome(this.nome);
        funcionario.setSexo(this.sexo);
        
        funcionario.setNascimento(LocalDate.of(year, month, day));
        funcionario.setFuncao(funcaoRepository.getOne(this.funcao.getId()));
        funcionario.setUnidadeAtendimento(unidadeAtendimentoRepository
                .getOne(this.unidadeAtendimento.getId()));

        return funcionario;
    }

    public Funcionario converter(FuncionarioRepository repository,
            FuncaoRepository funcaoRepository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository) {

        Long maxId = repository.getMaxId() + 1;
        Funcionario funcionario = new Funcionario();
        funcionario.setId(maxId);
        return toFuncionario(funcionario, funcaoRepository, unidadeAtendimentoRepository);
    }

    public Funcionario atualizar(Long id, FuncionarioRepository repository,
            FuncaoRepository funcaoRepository,
            UnidadeAtendimentoRepository unidadeAtendimentoRepository) {

        return toFuncionario(repository.getOne(id), funcaoRepository,
                unidadeAtendimentoRepository);
    }
}
