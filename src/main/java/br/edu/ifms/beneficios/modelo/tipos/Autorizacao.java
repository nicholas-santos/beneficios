/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.tipos;

import java.util.stream.Stream;

/**
 *
 * @author santos
 */
public enum Autorizacao {
    AUTORIZADO("A"), NEGADO("N");
    
    private final String value;
    
    Autorizacao(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static Autorizacao of(String value) {
        return Stream.of(Autorizacao.values()).filter(t -> t.getValue().equals(value))
                .findFirst().orElseThrow(IllegalArgumentException::new );
    }

    @Override
    public String toString() {
        switch (value) {
            default:
            case "A":
                return "Autorizado";
            case "N":
                return "Negado";
        }
    }
}
