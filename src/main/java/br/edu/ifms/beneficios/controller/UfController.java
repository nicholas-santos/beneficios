/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.UfDto;
import br.edu.ifms.beneficios.controller.form.UfForm;
import br.edu.ifms.beneficios.modelo.Uf;
import br.edu.ifms.beneficios.repository.UfRepository;
import br.edu.ifms.beneficios.repository.PaisRepository;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/ufs")
public class UfController {

    @Autowired
    private UfRepository repository;

    @Autowired
    private PaisRepository paisRepository;

    /**
     * Listar de unidades federativas.Este método é responsável por retornar uma
     * lista de unidades federativas cadastrados no sistema.
     *
     *
     * @param nome
     * @param paisId
     * @return
     */
    @GetMapping
    @Cacheable(value = "listaDeUfs")
    public List<UfDto> listar(
            @RequestParam(name = "nome", required = false) String nome,
            @RequestParam(name = "paisId", required = false) Long paisId
    ) {
        List<Uf> items;
        if (paisId != null && paisId > 0) {
            items = repository.findByPaisId(paisId);
        } else if (nome != null) {
            items = repository.findByNomeContainingIgnoreCase(nome);
        } else {
            items = repository.findAll();
        }
        
        return UfDto.converter(items);
    }

    /**
     * Visualizar unidade federativa.Este método é responsável por retornar um
     * objeto da classe <code>Uf</code>
     *
     *
     * @param id Número de identificação da unidade federativa.
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<UfDto> visualizar(@PathVariable Long id) {
        Optional<Uf> uf = repository.findById(id);
        if (uf.isPresent()) {
            return ResponseEntity.ok(new UfDto(uf.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"listaDeUfs"}, allEntries = true)
    public ResponseEntity<UfDto> cadastrar(@RequestBody @Valid UfForm form,
            UriComponentsBuilder uriBuilder) {
        Uf uf = form.converter(paisRepository);
        repository.save(uf);

        URI uri = uriBuilder.path("/ufs/{id}")
                .buildAndExpand(uf.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new UfDto(uf));
    }

}
