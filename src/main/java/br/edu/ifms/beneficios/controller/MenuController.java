/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.BairroDto;
import br.edu.ifms.beneficios.controller.dto.MenuDto;
import br.edu.ifms.beneficios.controller.form.BairroForm;
import br.edu.ifms.beneficios.controller.form.MenuForm;
import br.edu.ifms.beneficios.modelo.Bairro;
import br.edu.ifms.beneficios.modelo.Menu;
import br.edu.ifms.beneficios.modelo.tipos.MenuTipo;
import br.edu.ifms.beneficios.repository.MenuRepository;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author santos
 */
@RestController
@RequestMapping("api/menus")
public class MenuController {

    @Autowired
    private MenuRepository repository;

    /**
     * Lista de menus.
     * 
     * Este método é responsável por retornar uma lista de menus cadastrados no
     * sistema.
     *
     * @return
     */
    @GetMapping
    @Cacheable(value = "listaDeMenus")
    public List<MenuDto> listar() {
        List<Menu> items = repository.findByDisponivel(true);
        return MenuDto.converter(items);
    }
    
    /**
     * Lista de menus.
     * 
     * Este método é responsável por retornar uma lista de menus cadastrados no
     * sistema.
     *
     * @return
     */
    @GetMapping("/cadastros")
    @Cacheable(value = "listaDeMenus")
    public List<MenuDto> listarCadastros() {
        List<Menu> items = repository.findByTipo(MenuTipo.CADASTRO);
        return MenuDto.converter(items);
    }

    /**
     * Visualizar país.
     *
     * Este método é responsável por retornar um objeto da classe
     * <code>País</code>
     *
     * @param id Número de identificação do país.
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<MenuDto> visualizar(@PathVariable Long id) {
        Optional<Menu> pais = repository.findById(id);
        if (pais.isPresent()) {
            return ResponseEntity.ok(new MenuDto(pais.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"listaDeMenus"}, allEntries = true)
    public ResponseEntity<MenuDto> cadastrar(@RequestBody @Valid MenuForm form,
            UriComponentsBuilder uriBuilder) {
        Menu menu = form.converter();
        repository.save(menu);

        URI uri = uriBuilder.path("/menus/{id}")
                .buildAndExpand(menu.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new MenuDto(menu));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"listaDeMenus"}, allEntries = true)
    public ResponseEntity<MenuDto> atualizar(
            @PathVariable Long id, @RequestBody @Valid MenuForm form) {
        Optional<Menu> optional = repository.findById(id);
        if (optional.isPresent()) {
            Menu menu = form.atualizar(id, repository);
            return ResponseEntity.ok(new MenuDto(menu));
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"listaDeMenus"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<Menu> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
