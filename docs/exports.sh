#!/bin/bash

# Dados para conexão com banco de dados
export BENEFICIOS_DB_NAME=beneficiosdb
export BENEFICIOS_DB_URL=localhost:5432
export BENEFICIOS_DB_USERNAME=beneficios
export BENEFICIOS_DB_PASSWORD=beneficios

# Dados para configuração do Spring JPA
export BENEFICIOS_DB_DDLAUTO=create
export BENEFICIOS_DB_SHOWSQL=true
export BENEFICIOS_DB_FORMATSQL=true

# JWT
export BENEFICIOS_JWT_EXPIRATION=86400000

export BENEFICIOS_URL=http://localhost:3000
export BENEFICIOS_MAIL_SENDER=no-reply@email.com
export BENEFICIOS_RESET_PASSWORD_TOKEN_EXPIRATION_MILISEG=3600000

# Cors
export BENEFICIOS_COR_HEADER="Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
export BENEFICIOS_COR_ALLOWORIGIN=*

#Mail configuration
export BENEFICIOS_MAIL_HOST=smtp.gmail.com
export BENEFICIOS_MAIL_PORT=587
export BENEFICIOS_MAIL_USERNAME=sw.beneficios.eventuais@gmail.com
export BENEFICIOS_MAIL_PASSWORD=qanbilwhygqkkqrz

#File configuration
export BENEFICIOS_SIZE_THRESHOLD=2KB
export BENEFICIOS_MAX_FILE_SIZE=200MB
export BENEFICIOS_MAX_REQUEST_SIZE=215MB
export BENEFICIOS_UPLOAD_DIR=/tmp/beneficios/