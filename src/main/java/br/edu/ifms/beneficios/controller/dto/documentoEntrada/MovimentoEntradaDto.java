/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto.documentoEntrada;

import br.edu.ifms.beneficios.modelo.MovimentoEntrada;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

/**
 *
 * @author santos
 */
public class MovimentoEntradaDto extends MovimentoDto {
    
    private ItemEntradaDetalheDto itemEntrada;

    public MovimentoEntradaDto() {
    }

    public MovimentoEntradaDto(MovimentoEntrada obj) {
        super(obj);
        this.itemEntrada = new ItemEntradaDetalheDto(obj.getItemEntrada());
    }

    public ItemEntradaDetalheDto getItemEntrada() {
        return itemEntrada;
    }

    public static Page<MovimentoEntradaDto> converter(Page<MovimentoEntrada> items) {
        return items.map(MovimentoEntradaDto::new);
    }

    public static List<MovimentoEntradaDto> converter(List<MovimentoEntrada> items) {
        return items.stream().map(MovimentoEntradaDto::new).collect(Collectors.toList());
    }
    
}
