/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.dto;

import org.springframework.data.domain.Page;

import br.edu.ifms.beneficios.modelo.Perfil;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tanabe
 */
public class PerfilDto {

    private Long id;
    private String nome;
    private Status status;
    private List<PerfilMenuDto> menus;

    public PerfilDto(Perfil obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.status = obj.getStatus();
        this.menus = PerfilMenuDto.converter(obj.getMenus());
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public Status getStatus() {
        return status;
    }

    public List<PerfilMenuDto> getMenus() {
        return menus;
    }

    public static Page<PerfilDto> converter(Page<Perfil> items) {
        return items.map(PerfilDto::new);
    }
    
    public static List<PerfilDto> converter(List<Perfil> items) {
        return items.stream().map(PerfilDto::new).collect(Collectors.toList());
    }
}
