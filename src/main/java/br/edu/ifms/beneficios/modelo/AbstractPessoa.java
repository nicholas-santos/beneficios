/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.OptionalLong;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

/**
 *
 * @author santos
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class AbstractPessoa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;

    @OneToMany(mappedBy = "id.pessoa",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<DocumentoPessoa> documentos = new ArrayList();

    @OneToMany(mappedBy = "id.pessoa",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<ContatoPessoa> contatos = new ArrayList();

    public AbstractPessoa() {
    }

    public AbstractPessoa(Long id) {
        this.id = id;
    }

    public AbstractPessoa(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoPessoa> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoPessoa> contatos) {
        this.contatos.clear();
        if (!contatos.isEmpty()) {
            this.contatos.addAll(contatos);
        }
    }
    
    public void add(ContatoPessoa contato) {
        if (!this.contatos.contains(contato)) {
            OptionalLong maxId = contatos.stream()
                    .mapToLong(obj -> obj.getId().getId())
                    .max();
            
            contato.getId().setId((maxId.isPresent() ? 
                    maxId.getAsLong() : 0) + 1L);
            this.contatos.add(contato);
        } else {
            this.contatos.stream().map((obj) -> {
                obj.setTipoContato(contato.getTipoContato());
                obj.setDescricao(contato.getDescricao());
                return obj;
            });
        }
    }

    public void remove(ContatoPessoa contato) {
        this.contatos.remove(contato);
        contato.getId().setPessoa(null);
    }

    public List<DocumentoPessoa> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoPessoa> documentos) {
        this.documentos.clear();
        if (!documentos.isEmpty()) {
            this.documentos.addAll(documentos);
        }
    }

    public void add(DocumentoPessoa documentoPessoa) {
        if (!this.documentos.contains(documentoPessoa)) {
            this.documentos.add(documentoPessoa);
        } else {
            this.documentos.stream().map((obj) -> {
                obj.setOrgaoExpedidor(documentoPessoa.getOrgaoExpedidor());
                obj.setEmissao(documentoPessoa.getEmissao());
                return obj;
            });
        }
    }

    public void remove(DocumentoPessoa documentoPessoa) {
        this.documentos.remove(documentoPessoa);
        documentoPessoa.getId().setPessoa(null);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.id);
        hash = 73 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final AbstractPessoa other = (AbstractPessoa) obj;
        if (!Objects.equals(this.nome, other.getNome())) {
            return false;
        }
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbstractPessoa{" + "id=" + id + ", nome=" + nome + ", documentos=" + documentos + ", contatos=" + contatos + '}';
    }
}
