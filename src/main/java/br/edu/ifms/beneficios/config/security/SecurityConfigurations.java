package br.edu.ifms.beneficios.config.security;

import br.edu.ifms.beneficios.config.security.filter.CredentialsAuthenticationFilter;
import br.edu.ifms.beneficios.config.service.AutenticacaoService;
import br.edu.ifms.beneficios.exception.InvalidCredentialsAuthenticationException;
import br.edu.ifms.beneficios.exception.TooManyFailedIpLoginsAuthenticationException;
import br.edu.ifms.beneficios.exception.TooManyFailedLoginsAuthenticationException;
import br.edu.ifms.beneficios.modelo.ResponseStatus.StatusCode;
import br.edu.ifms.beneficios.property.BeneficiosProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.edu.ifms.beneficios.repository.UsuarioRepository;
import br.edu.ifms.beneficios.service.IpLogService;
import br.edu.ifms.beneficios.service.UsuarioService;
import br.edu.ifms.beneficios.util.ResponseUtil;
import br.edu.ifms.beneficios.util.ResponseWriterUtil;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Profile("prod")
@Configuration
@EnableWebSecurity
public class SecurityConfigurations extends WebSecurityConfigurerAdapter {

    @Autowired
    private AutenticacaoService autenticacaoService;

    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private UsuarioService usuarioService;
    
    @Autowired
    private IpLogService ipLogService;
    
    @Autowired
    private BeneficiosProperties beneficiosProperties;

    // Configurações de autenticacao
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        JwtAuthenticationProvider jwtAuthenticationProvider = new JwtAuthenticationProvider(
                autenticacaoService,
                beneficiosProperties
        );
        auth.authenticationProvider(jwtAuthenticationProvider);
        auth.userDetailsService(autenticacaoService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    //Configuracoes de autorizacao
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/").permitAll()
            .antMatchers(HttpMethod.POST, "/api/auth").permitAll()
            .antMatchers(HttpMethod.POST, "/api/auth/**").permitAll()
            .anyRequest().authenticated()
            .and()
                .cors()
            .and()
                .csrf().disable()
            
            .addFilter(bearerTokenAuthenticationFilter())
            .addFilter(credentialsAuthenticationFilter())
                
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    //Configuracoes de recursos estaticos(js, css, imagens, etc.)
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/resources/**",
                        "/static/css/*",
                        "/static/js/*",
                        "/*.js", "/*.css",
                        "/*.ico",
                        "/manifest.json",
                        "/beneficios/static/css/*",
                        "/beneficios/static/js/*",
                        "/beneficios/*.js", "/beneficios/*.css",
                        "/beneficios/*.ico",
                        "/beneficios/manifest.json"
                );
    }

    private BearerTokenAuthenticationFilter bearerTokenAuthenticationFilter() throws Exception {
        BearerTokenAuthenticationFilter filter = new BearerTokenAuthenticationFilter(authenticationManager());
        filter.setAuthenticationFailureHandler(authenticationFailureHandler());

        return filter;
    }

    private CredentialsAuthenticationFilter credentialsAuthenticationFilter() throws Exception {
        CredentialsAuthenticationFilter filter = new CredentialsAuthenticationFilter(
                authenticationManager(), beneficiosProperties);
        filter.setUsuarioService(usuarioService);
        filter.setAuthenticationFailureHandler(authenticationFailureHandler());
        filter.setIpTracker(ipLogService);
        return filter;
    }

    private AuthenticationFailureHandler authenticationFailureHandler() {
        return (request, response, ex) -> {
            if (ex instanceof InvalidCredentialsAuthenticationException) {
                ResponseUtil.invalidCredentials(response);
            } else if (ex instanceof TooManyFailedIpLoginsAuthenticationException) {
                ResponseUtil.tooManyFailedIpLogins(response);
            } else if (ex instanceof TooManyFailedLoginsAuthenticationException) {
                ResponseUtil.tooManyFailedLogins(response);
            } else {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                ResponseWriterUtil.writeResponse(response, ex.getMessage(), StatusCode.UNAUTHORIZED);                                  
            }
        };
    }

}
