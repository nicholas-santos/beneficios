/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.Uf;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author santos
 */
public interface UfRepository extends JpaRepository<Uf, Long> {

    List<Uf> findByPaisId(Long idPais);

    List<Uf> findByPaisNome(String nomePais);

    List<Uf> findByNomeContainingIgnoreCase(String nomeUf);
}
