/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.repository;

import br.edu.ifms.beneficios.modelo.TipoUnidadeAtendimento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 *
 * @author tanabe
 */
public interface TipoUnidadeAtendimentoRepository extends JpaRepository<TipoUnidadeAtendimento, Long> {
    
    @Query("SELECT coalesce(max(tua.id), 0) FROM TipoUnidadeAtendimento tua")
    Long getMaxId();
    
    TipoUnidadeAtendimento findByNome(String nome);
}
