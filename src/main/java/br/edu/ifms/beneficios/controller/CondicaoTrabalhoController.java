/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller;

import br.edu.ifms.beneficios.controller.dto.CondicaoMoradiaDto;
import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.edu.ifms.beneficios.controller.dto.CondicaoTrabalhoDto;
import br.edu.ifms.beneficios.controller.form.CondicaoTrabalhoForm;
import br.edu.ifms.beneficios.modelo.CondicaoMoradia;
import br.edu.ifms.beneficios.modelo.CondicaoTrabalho;
import br.edu.ifms.beneficios.repository.CondicaoTrabalhoRepository;
import java.util.List;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author tanabe
 */
@RestController
@RequestMapping("api/condicoes-de-trabalho")
public class CondicaoTrabalhoController {

    @Autowired
    private CondicaoTrabalhoRepository repository;

    @GetMapping
    @Cacheable(value = "pageCondicaoTrabalho")
    public Page<CondicaoTrabalhoDto> listar(
            @PageableDefault(sort = "descricao", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {

            Page<CondicaoTrabalho> condicaoTrabalho = repository.findAll(paginacao);
            return CondicaoTrabalhoDto.converter(condicaoTrabalho);
    }

    @GetMapping("/list")
    @Cacheable(value = "listaCondicaoTrabalho")
    public List<CondicaoTrabalhoDto> listar() {
        List<CondicaoTrabalho> condicaoTrabalho = repository.findAll();
        return CondicaoTrabalhoDto.converter(condicaoTrabalho);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CondicaoTrabalhoDto> visualizar(@PathVariable Long id) {
        Optional<CondicaoTrabalho> condicaoTrabalho = repository.findById(id);
        if (condicaoTrabalho.isPresent()) {
            return ResponseEntity.ok(new CondicaoTrabalhoDto(condicaoTrabalho.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = {"pageCondicaoTrabalho", "listaCondicaoTrabalho"}, allEntries = true)
    public ResponseEntity<CondicaoTrabalhoDto> criar(@RequestBody @Valid CondicaoTrabalhoForm form, UriComponentsBuilder uriBuilder) {
    	CondicaoTrabalho condicaoTrabalho = form.converter();
        repository.save(condicaoTrabalho);

        URI uri = uriBuilder.path("/condicoes-de-trabalho/{id}").buildAndExpand(condicaoTrabalho.getId()).toUri();
        return ResponseEntity.created(uri).body(new CondicaoTrabalhoDto(condicaoTrabalho));
    }

    @PutMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageCondicaoTrabalho", "listaCondicaoTrabalho"}, allEntries = true)
    public ResponseEntity<CondicaoTrabalhoDto> atualizar(@PathVariable Long id, @RequestBody @Valid CondicaoTrabalhoForm form) {
        Optional<CondicaoTrabalho> optional = repository.findById(id);
        if (optional.isPresent()) {
        	CondicaoTrabalho condicaoTrabalho = form.atualizar(id, repository);
            return ResponseEntity.ok(new CondicaoTrabalhoDto(condicaoTrabalho));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = {"pageCondicaoTrabalho", "listaCondicaoTrabalho"}, allEntries = true)
    public ResponseEntity<?> remover(@PathVariable Long id) {
        Optional<CondicaoTrabalho> optional = repository.findById(id);
        if (optional.isPresent()) {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
