/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.controller.form;

import br.edu.ifms.beneficios.controller.dto.configuracoes.BeneficiosConfigDto;
import br.edu.ifms.beneficios.controller.dto.configuracoes.HibernateJpaConfigDto;
import br.edu.ifms.beneficios.controller.dto.configuracoes.MailConfigDto;
import br.edu.ifms.beneficios.controller.dto.configuracoes.MultpartConfigDto;
import br.edu.ifms.beneficios.property.BeneficiosProperties;
import java.time.Duration;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

/**
 *
 * @author santos
 */
public class ConfiguracaoForm {
    private BeneficiosConfigDto beneficiosConfig;
    private MailConfigDto mailConfig;
    private MultpartConfigDto multpartConfig;
    private HibernateJpaConfigDto hibernateJpaConfig;

    public ConfiguracaoForm(BeneficiosConfigDto beneficiosConfig,
            MailConfigDto mailConfig,
            MultpartConfigDto multpartConfig,
            HibernateJpaConfigDto hibernateJpaConfig) {
        this.beneficiosConfig = beneficiosConfig;
        this.mailConfig = mailConfig;
        this.multpartConfig = multpartConfig;
        this.hibernateJpaConfig = hibernateJpaConfig;
    }

    public ConfiguracaoForm() {
    }

    public BeneficiosConfigDto getBeneficiosConfig() {
        return beneficiosConfig;
    }

    public void setBeneficiosConfig(BeneficiosConfigDto beneficiosConfig) {
        this.beneficiosConfig = beneficiosConfig;
    }

    public MailConfigDto getMailConfig() {
        return mailConfig;
    }

    public void setMailConfig(MailConfigDto mailConfig) {
        this.mailConfig = mailConfig;
    }

    public MultpartConfigDto getMultpartConfig() {
        return multpartConfig;
    }

    public void setMultpartConfig(MultpartConfigDto multpartConfig) {
        this.multpartConfig = multpartConfig;
    }

    public HibernateJpaConfigDto getHibernateJpaConfig() {
        return hibernateJpaConfig;
    }

    public void setHibernateJpaConfig(HibernateJpaConfigDto hibernateJpaConfig) {
        this.hibernateJpaConfig = hibernateJpaConfig;
    }
    
    public ConfiguracaoForm atualizar(BeneficiosProperties beneficioProperties,
            MailProperties mailProperties,
            MultipartProperties multpartProperties,
            HibernateProperties hibernateProperties,
            JpaProperties jpaProperties) {
        beneficioProperties.setCors(beneficiosConfig.getCors());
        beneficioProperties.setJwt(beneficiosConfig.getJwt());
        beneficioProperties.setFrom(beneficiosConfig.getFrom());
        beneficioProperties.setUrlFrontEnd(beneficiosConfig.getUrlFrontEnd());
        beneficioProperties.setResetPasswordUrl(beneficiosConfig.getResetPasswordUrl());
        
        beneficioProperties.setResetPasswordTokenExpiration(
                Duration.ofMillis(beneficiosConfig.getResetPasswordTokenExpiration()));
        
        mailProperties.setHost(mailConfig.getHost());
        mailProperties.setPort(mailConfig.getPort());
        mailProperties.setUsername(mailConfig.getUsername());
        mailProperties.setPassword(mailConfig.getPassword());
        
        multpartProperties.setLocation(multpartConfig.getLocation());
        multpartProperties.setMaxFileSize(DataSize.of(
                multpartConfig.getMaxFileSize(),
                DataUnit.MEGABYTES));
        multpartProperties.setMaxRequestSize(DataSize.of(
                multpartConfig.getMaxRequestSize(),
                DataUnit.MEGABYTES));
        multpartProperties.setFileSizeThreshold(DataSize.of(
                multpartConfig.getFileSizeThreshold(),
                DataUnit.KILOBYTES));
        
        hibernateProperties.setDdlAuto(hibernateJpaConfig.getDdlAuto());
        jpaProperties.setShowSql(hibernateJpaConfig.isShowSql());
        
        return this;
    }
}
