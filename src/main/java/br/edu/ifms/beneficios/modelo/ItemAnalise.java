/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo;

import br.edu.ifms.beneficios.modelo.id.ItemAnaliseId;
import br.edu.ifms.beneficios.modelo.tipos.Status;
import br.edu.ifms.beneficios.modelo.tipos.StatusConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author santos
 */
@Entity
public class ItemAnalise implements Serializable {

    @EmbeddedId
    private ItemAnaliseId id;

    @ManyToOne
    private BeneficioEventual beneficioEventual;

    private BigDecimal quantidade;

    @Column(name = "status", nullable = true)
    @Convert(converter = StatusConverter.class)
    private Status status;

    @OneToMany(mappedBy = "itemAnalise",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<HistoricoItemAnalise> historicos = new ArrayList();

    public ItemAnalise() {
        this.status = Status.PENDENTE;
    }

    public ItemAnalise(ItemAnaliseId id, BeneficioEventual beneficioEventual, BigDecimal quantidade, Status status) {
        this.id = id;
        this.beneficioEventual = beneficioEventual;
        this.quantidade = quantidade;
        this.status = status;
    }

    public ItemAnaliseId getId() {
        return id;
    }

    public void setId(ItemAnaliseId id) {
        this.id = id;
    }

    public BeneficioEventual getBeneficioEventual() {
        return beneficioEventual;
    }

    public void setBeneficioEventual(BeneficioEventual beneficioEventual) {
        this.beneficioEventual = beneficioEventual;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
    public void retirar() {
        this.status = Status.RETIRADO;
    }
    
    public void cancelar() {
        this.status = Status.CANCELADO;
    }

    public List<HistoricoItemAnalise> getHistoricos() {
        return historicos;
    }

    public void setHistoricos(List<HistoricoItemAnalise> historicos) {
        this.historicos = historicos;
    }

    public void add(HistoricoItemAnalise historico) {
        this.historicos.add(historico);
    }

    public void remove(HistoricoItemAnalise historico) {
        this.historicos.remove(historico);
        historico.setItemAnalise(null);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.beneficioEventual);
        hash = 59 * hash + Objects.hashCode(this.quantidade);
        hash = 59 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final ItemAnalise other = (ItemAnalise) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        if (!Objects.equals(this.beneficioEventual, other.getBeneficioEventual())) {
            return false;
        }
        if (!Objects.equals(this.quantidade, other.getQuantidade())) {
            return false;
        }
        if (this.status != other.getStatus()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItemAnalise{" + "id=" + id.toString() + ", beneficioEventual=" + 
                beneficioEventual + ", quantidade=" + quantidade + ", status=" + status + ", historicos=" + historicos + '}';
    }

}
