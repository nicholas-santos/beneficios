/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifms.beneficios.modelo.tipos;

import javax.persistence.AttributeConverter;

/**
 *
 * @author santos
 */
public class AutorizacaoConverter  implements AttributeConverter<Autorizacao, String> {

    @Override
    public String convertToDatabaseColumn(Autorizacao autorizacao) {
        if (autorizacao == null) {
            return null;
        }
        return autorizacao.getValue();
    }

    @Override
    public Autorizacao convertToEntityAttribute(String autorizacao) {
        if (autorizacao == null)
            return null;
        return Autorizacao.of(autorizacao);
    }
    
}
